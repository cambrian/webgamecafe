/* nngsmain.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996  Erik Van Riper (geek@willent.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _NNGSMAIN_H
#define _NNGSMAIN_H

/* Heartbeat functions occur approx in this time, including checking for
 * new connections and decrementing timeleft counters. */
#define HEARTBEATTIME 1

/* Number of seconds that an idle connection can stand at login or password
 * prompt. */
#define MAX_LOGIN_IDLE 120
#define MAX_IDLE 5400
#define NUM_LADDERS 2
#define LADDERSIZE 500 
/* This is the socket that the current command came in on */
extern int current_socket;

#define DEFAULT_PROMPT "#> "

#define MESS_WELCOME "welcome"
#define MESS_LOGIN "login"
#define MESS_LOGOUT "logout"
#define MESS_MOTD "motd"
#define MESS_ADMOTD "admotd"
#define MESS_UNREGISTERED "unregistered"

#define STATS_MESSAGES "messages"
#define STATS_LOGONS "logons"
#define STATS_GAMES "games"
#define STATS_RGAMES "rgames"
#define STATS_CGAMES "cgames" 

/* Arguments */
extern int port;
extern int withConsole;
extern int Ladder9;
extern int Ladder19;
extern int num_19;
extern int num_9;
extern int completed_games;
extern int num_logins, num_logouts, new_players;
extern char orig_command[1024];

#ifdef OLD_LINUX
extern time_t time();
#endif

#endif
