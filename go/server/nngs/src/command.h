/* command.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997  Erik Van Riper (geek@midway.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _COMMAND_H
#define _COMMAND_H

extern char *mess_dir;
extern char *help_dir;
extern char *comhelp_dir;
extern char *info_dir;
extern char *adhelp_dir;
extern char *stats_dir;
extern char *config_dir;
extern char *player_dir;
extern char *game_dir;
extern char *problem_dir;
extern char *board_dir;
extern char *def_prompt;
extern char *source_dir;
extern char *lists_dir;
extern char *news_dir;
extern char *server_name;
extern char *server_address;
extern char *server_email;
extern char *intergo_file;
extern char *ratings_file;
extern char *results_file;
extern char *emotes_file;
extern char *note_file;

extern int startuptime;
extern int player_high;
extern int game_high;
extern int MailGameResult;

extern struct stat RatingsBuf1, RatingsBuf2;

/* Maximum length of a login name */
#define MAX_LOGIN_NAME 10

/* Maximum number of parameters per command */
#define MAXNUMPARAMS 10

/* Maximum string length of a single command word */
#define MAX_COM_LENGTH 50

/* Maximum string length of the whole command line */
#define MAX_STRING_LENGTH 1024

#define COM_OK 0
#define COM_FAILED 1
#define COM_ISMOVE 2
#define COM_AMBIGUOUS 3
#define COM_BADPARAMETERS 4
#define COM_BADCOMMAND 5
#define COM_LOGOUT 6
#define COM_FLUSHINPUT 7
#define COM_RIGHTS 8
#define COM_OK_NOPROMPT 9
#define COM_NOSUCHUSER 10
#define COM_NOSUCHGAME 11
#define COM_OKN 12

#define ADMIN_USER	0
#define ADMIN_ADMIN	10
#define ADMIN_MASTER	20
#define ADMIN_DEMIGOD   60
#define ADMIN_GOD	100

#define TYPE_NULL 0
#define TYPE_WORD 1
#define TYPE_STRING 2
#define TYPE_INT 3
#define TYPE_FLOAT 4

typedef struct u_parameter {
  int type;
  union {
    char *word;
    char *string;
    float f;
    int integer;
  } val;
} parameter;

typedef parameter param_list[MAXNUMPARAMS];

typedef struct s_command_type {
  char *comm_name;
  char *param_string;
  int (*comm_func)();
  int adminLevel;
} command_type;

typedef struct s_alias_type {
  char *comm_name;
  char *alias;
} alias_type;

extern int commanding_player; /* The player whose command your in */

extern int process_input(int, char *);
extern int process_new_connection(int, unsigned int);
extern int process_disconnection(int);
extern int process_incomplete(int, char *);
extern int process_heartbeat(int *);

extern void commands_init(void);

extern void TerminateCleanup(void);
extern int process_command(int, char *);

extern void command_init(void);
extern int command_admin_level(int c);
extern int match_command(char *);

#endif /* _COMMAND_H */
