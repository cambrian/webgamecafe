/* channel.c
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@willent.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"

#include "common.h"
#include "channel.h"
#include "network.h"
#include "playerdb.h"

PUBLIC int *channels[MAX_CHANNELS];
PUBLIC int numOn[MAX_CHANNELS];
/* PUBLIC char *Chan_Title[MAX_CHANNELS]; */
PUBLIC chan carray[MAX_CHANNELS];

PUBLIC int *Ochannels[MAX_O_CHANNELS];
PUBLIC int OnumOn[MAX_O_CHANNELS];
PUBLIC chan Ocarray[MAX_O_CHANNELS];


PUBLIC void channel_init()
{
  int i;

  for (i = 0; i < MAX_CHANNELS; i++) {
    carray[i].locked = 0;
    carray[i].hidden = 0;
    carray[i].dNd = 0;
    carray[i].Num_Yell = 0;
    channels[i] = malloc(net_maxConn * sizeof(int));
    numOn[i] = 0;
   
    switch(i) {
    case 0:
      carray[i].Title = (char *) strdup("Admins House");
      carray[i].hidden = 1;
      break;

    case 5:
      carray[i].Title = (char *) strdup("Tournament Players Lounge");
      break;

    case 12:
      carray[i].Title = (char *) strdup("Robin's hood");
      break;

    case 22:
      carray[i].Title = (char *) strdup("Easy conversation in Japanese");
      break;

    case 33:
      carray[i].Title = (char *) strdup("NNGS Bar and Grill");
      break;

    case 42:
      carray[i].dNd = 1;
      carray[i].Title = (char *) strdup("To set the channel title, see \"help ctitle\"");
      break;

    case 46:
      carray[i].Title = (char *) strdup("Den svenska kanalen (Swedish)");
      break;

    case 49:
      carray[i].Title = (char *) strdup("Der deutsche Kanal (German)");
      break;
      
    case 81:
      /* The Japanese Channel =
	 ｢日本語チャンネル」 */
      carray[i].Title = (char *) strdup("｢日本語チャンネル」 (Japanese)");
      break;

    case 82:
      carray[i].Title = (char *) strdup("The Korean channel");
      break;

    case 88:
      /* The Chinese Channel =
	 ﾖﾐﾎﾄﾆｵｵﾀ = 0xd6 0xd0 0xce 0xc4 0xc6 0xb5 0xb5 0xc0 */
      carray[i].Title = (char *) strdup("ﾖﾐﾎﾄﾆｵｵﾀ (Chinese)");
      break;

    case 99:
      carray[i].Title = (char *) strdup("The Girls Club / Happy Channel :-)");
      break;

    default: carray[i].Title = (char *) strdup("No Title");
    }
  }
}

PUBLIC int on_channel(int ch, int p)
{
  int i;

  for (i = 0; i < numOn[ch]; i++)
    if (p == channels[ch][i])
      return 1;
  return 0;
}

PUBLIC int channel_remove(int ch, int p)
{
  int i, found;

  found = -1;
  for (i = 0; i < numOn[ch] && found < 0; i++)
    if (p == channels[ch][i])
      found = i;
  if (found < 0) return 1;
  for (i = found; i < numOn[ch] - 1; i++)
    channels[ch][i] = channels[ch][i + 1];
  numOn[ch] = numOn[ch] - 1;
  --parray[p].nochannels;
  if(numOn[ch] == 0) {
    carray[ch].locked = 0;
    carray[ch].hidden = 0;
  }
  if(parray[p].nochannels < 0) parray[p].nochannels = 0;
  return 0;
}

PUBLIC int channel_add(int ch, int p)
{
  if((ch == 0) && (parray[p].adminLevel < 20)) {
    return 3;
  }
  if (numOn[ch] >= MAX_CHANNELS) return 1;
  if (on_channel(ch, p)) return 1;
  if((carray[ch].locked) && (parray[p].adminLevel < 20)) {
    return 4;
  }
  if((carray[ch].hidden) && (parray[p].adminLevel < 20)) {
    return 5;
  }
  if ((parray[p].nochannels == MAX_INCHANNELS) && (parray[p].adminLevel < 20)) {
    return 2;
  }
  channels[ch][numOn[ch]] = p;
  numOn[ch]++;
  parray[p].nochannels++;
  
  return 0;
}

int add_to_yell_stack(int ch, char *text)
{
  int i;

  /* First, check if stack is full.  If so, remove last yell,
     shuffle rest down. */

  if (carray[ch].Num_Yell == YELL_STACK_SIZE) {
    free((char *) carray[ch].Yell_Stack[0]);
    for (i = 0; i < YELL_STACK_SIZE-1; i++) {
      carray[ch].Yell_Stack[i] = carray[ch].Yell_Stack[i + 1];
    }
    carray[ch].Num_Yell  -= 1;
  }

  carray[ch].Yell_Stack[carray[ch].Num_Yell++] = (char *) strdup(text);
  
  return(carray[ch].Num_Yell);
}

int add_to_Oyell_stack(int ch, char *text)
{
  int i;

  /* First, check if stack is full.  If so, remove last yell,
     shuffle rest down. */

  if (Ocarray[ch].Num_Yell == YELL_STACK_SIZE) {
    free((char *) Ocarray[ch].Yell_Stack[0]);
    for (i = 0; i < YELL_STACK_SIZE-1; i++) {
      Ocarray[ch].Yell_Stack[i] = Ocarray[ch].Yell_Stack[i + 1];
    }
    Ocarray[ch].Num_Yell -= 1;
  }

  Ocarray[ch].Yell_Stack[Ocarray[ch].Num_Yell++] = (char *) strdup(text);
 
  return(Ocarray[ch].Num_Yell);
}

PUBLIC void Ochannel_init()
{
  int i;

  for (i = 0; i < MAX_O_CHANNELS; i++) {
    Ocarray[i].locked = 0;
    Ocarray[i].hidden = 0;
    Ocarray[i].dNd = 0;
    Ocarray[i].Num_Yell = 0;
    Ochannels[i] = malloc(net_maxConn * sizeof(int));
    OnumOn[i] = 0;

    switch(i) {
      case CASHOUT:
      Ocarray[i].Title = (char *) strdup("Admin Shout");
      Ocarray[i].hidden = 1;
      break;

      case CSHOUT:
      Ocarray[i].Title = (char *) strdup("Normal Shouts");
      break;

      case CGSHOUT:
      Ocarray[i].Title = (char *) strdup("GoBot Shouts");
      break;

      case CLOGON:
      Ocarray[i].Title = (char *) strdup("Logons");
      break;

      case CGAME:
      Ocarray[i].Title = (char *) strdup("Games");
      break;

      case CLSHOUT:
      Ocarray[i].Title = (char *) strdup("Ladder Shouts");
      break;

      default: Ocarray[i].Title = (char *) strdup("No Title");
    }
  }
}

PUBLIC int Oon_channel(int ch, int p)
{
  int i;

  for (i = 0; i < OnumOn[ch]; i++)
    if (p == Ochannels[ch][i])
      return 1;
  return 0;
}

PUBLIC int Ochannel_remove(int ch, int p)
{
  int i, found;

  found = -1;
  for (i = 0; i < OnumOn[ch] && found < 0; i++)
    if (p == Ochannels[ch][i])
      found = i;
  if (found < 0) return 1;
  for (i = found; i < OnumOn[ch] - 1; i++)
    Ochannels[ch][i] = Ochannels[ch][i + 1];
  OnumOn[ch] = OnumOn[ch] - 1;
  if(OnumOn[ch] < 0) OnumOn[ch] = 0;
  return 0;
}

PUBLIC int Ochannel_add(int ch, int p)
{
  if (Oon_channel(ch, p)) return 1;
  Ochannels[ch][OnumOn[ch]] = p;
  OnumOn[ch]++;
  return 0;
}
