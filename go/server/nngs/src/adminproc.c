/* adminproc.c
 *
 */

#include "stdinclude.h"
#include "common.h"
#include "network.h"
#include "adminproc.h"
#include "command.h"
#include "channel.h"
#include "playerdb.h"
#include "utils.h"
#include "nngsmain.h"
#include "nngsconfig.h"
#include "ladder.h"
#ifdef SGI
#include <crypt.h>
#endif

PUBLIC int com_adrop(int p, param_list param)
{
  FILE *fp;
  const Player *LadderPlayer;
  char *lfile;
  int lnum;
  char *name = param[0].val.string;
  int type = param[1].val.integer;

  if (type == 9)
  {
    lfile = LADDER9;
    lnum = Ladder9;
  }
  else if (type == 19)
  {
    lfile = LADDER19;
    lnum = Ladder19;
  }
  else
  {
    pprintf(p, "%sNo such ladder", SendCode(p, ERROR));
    return COM_OK;
  }

  LadderPlayer = PlayerNamed(lnum, name);

  if(LadderPlayer == NULL) {
    pprintf(p, "%sNo such ladder player", SendCode(p, ERROR));
    return COM_OK;
  }
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  PlayerKillAt(lnum, LadderPlayer->idx);
  fp = fopen(lfile, "w");
  if(fp == NULL) {
    Logit("Error opening %s for write!!!", lfile);
    pprintf(p, "%sThere was an internal error.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (lnum == 9)
    num_9 = PlayerSave(fp, lnum);
  else
    num_19 = PlayerSave(fp, lnum);
  fclose(fp);
  player_resort();
  return COM_OK;
}

PUBLIC int com_createnews(int p, param_list param)
{
  FILE *fp;
  char filename[MAX_FILENAME_SIZE];
  char junk[MAX_LINE_SIZE];
  char *junkp;
  int crtime;
  char count[10];
  int len;

/* get number of next news entry */

  sprintf(count,"0");
  sprintf(filename, "%s/news.index", news_dir);
  fp = fopen(filename, "r");
  if (!fp) {
    Logit("Cant find news index.");
    return COM_OK; 
  }
  while (!feof(fp)) {
    junkp=junk; 
    fgets(junk, MAX_LINE_SIZE, fp);
    if (feof(fp))
      break;
    if ((len = strlen(junk))>1) 
      sscanf(junkp, "%d %s", &crtime, count);  
  }
  fclose(fp);
  sprintf(count, "%d", atoi(count) + 1);

/* create new entry in index file */

  fp = fopen(filename, "a");
  fprintf(fp, "%d %s %s\n", (int) time(0), count, param[0].val.string);
  fclose(fp);
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}

PUBLIC int com_createadmnews(int p, param_list param)
{
  FILE *fp;
  char filename[MAX_FILENAME_SIZE];
  char junk[MAX_LINE_SIZE];
  char *junkp;
  int crtime;
  char count[10];
  int len;

/* get number of next news entry */

  sprintf(count,"0");
  sprintf(filename, "%s/adminnews.index", news_dir);
  fp = fopen(filename, "r");
  if (!fp) {
    Logit("Cant find admin-news index.");
    return COM_OK; 
  }
  while (!feof(fp)) {
    junkp=junk; 
    fgets(junk, MAX_LINE_SIZE, fp);
    if (feof(fp))
      break;
    if ((len = strlen(junk))>1) 
      sscanf(junkp, "%d %s", &crtime, count);  
  }
  fclose(fp);
  sprintf(count, "%d", atoi(count) + 1);

/* create new entry in index file */

  fp = fopen(filename, "a");
  fprintf(fp, "%d %s %s\n", (int) time(0), count, param[0].val.string);
  fclose(fp);
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}

PUBLIC int com_anews(int p, param_list param)
{
  FILE *fp;
  char filename[MAX_FILENAME_SIZE];
  char junk[MAX_LINE_SIZE];
  char *junkp;
  int crtime;
  char count[10];
  int flag, len;

  if (((param[0].type==0) || (!strcmp(param[0].val.word,"all")))) {

/* no params - then just display index over news */
   
    pprintf(p, "%s\n    **** ADMIN BULLETIN BOARD ****\n",
    		parray[p].client ? "6 Info" : "");
    sprintf(filename, "%s/adminnews.index", news_dir);
    fp = fopen(filename, "r");
    if (!fp) {
      Logit("Cant find news index.");
      return COM_OK; 
    }
    flag=0;
    while (!feof(fp)) {
      junkp=junk; 
      fgets(junk, MAX_LINE_SIZE, fp);
      if (feof(fp))
        break;
      if ((len = strlen(junk))>1) {
        junk[len-1]='\0';
        sscanf(junkp, "%d %s", &crtime, count);
        junkp=nextword(junkp); 
        junkp=nextword(junkp);
        if (((param[0].type==TYPE_WORD) && (!strcmp(param[0].val.word,"all")))) {
          pprintf(p, "%3s (%s) %s\n", count, strltime((time_t *) &crtime), junkp);
          flag=1;
        } else {
          if ((crtime - player_lastconnect(p))>0) {
            pprintf(p, "%3s (%s) %s\n", count, strltime((time_t *) &crtime), junkp);
	    flag=1;
	  }
	}
      }
    }
    fclose(fp);
    crtime=player_lastconnect(p);
    if (!flag) {
      pprintf(p, "There is no news since your last login (%s).\n",strltime((time_t *) &crtime));
    } else {
      pprintf(p, "%s\n",
                parray[p].client ? "6 Info" : "");
    }
  } else {

/* check if the specific news file exist in index */

    sprintf(filename, "%s/adminnews.index", news_dir);
    fp = fopen(filename, "r");
    if (!fp) {
      Logit("Cant find news index.");
      return COM_OK; 
    }
    flag=0;
    while ((!feof(fp)) && (!flag)) {
      junkp=junk; 
      fgets(junk, MAX_LINE_SIZE, fp);
      if (feof(fp))
        break;
      if ((len = strlen(junk))>1) {
        junk[len-1]='\0';
        sscanf(junkp, "%d %s", &crtime, count);
        if (!strcmp(count,param[0].val.word)) {
          flag=1;
          junkp=nextword(junkp); 
          junkp=nextword(junkp);
          pprintf(p, "\nNEWS %3s (%s)\n\n         %s\n\n", count, strltime((time_t *) &crtime), junkp);
        }
      }
    }
    fclose(fp);
    if (!flag) {
      pprintf(p, "Bad index number!\n");
      return COM_OK;
    }

/* file exists - show it */

    sprintf(filename, "%s/adminnews.%s", news_dir, param[0].val.word);
    fp = fopen(filename, "r");
    if (!fp) {
      pprintf(p, "No more info.\n");
      return COM_OK; 
    }
    fclose(fp);   
    sprintf(filename, "adminnews.%s", param[0].val.word);
    if (psend_file(p, news_dir, filename) < 0) {
      pprintf(p, "Internal error - couldn't send news file!\n");
    }
  }
  pprintf(p, "%s\n", parray[p].client ? "6 Info" : "");
  return COM_OK;
}


PUBLIC int strcmpwild(char *mainstr, char *searchstr) {
  int i;

  if ((int) strlen(mainstr) < (int) strlen(searchstr))
    return 1;
  for (i = 0; i < strlen(mainstr); i++) {
    if (searchstr[i] == '*')
      return 0;
    if (mainstr[i] != searchstr[i])
      return 1;
  }
  return 0;
}

PUBLIC int com_checkIP(int p, param_list param)
{
  char *ipstr = param[0].val.word;
  int p1;

  pprintf(p, "%sMatches the following player(s):\n", SendCode(p, INFO));
  for (p1 = 0; p1 < p_num; p1++)
    if (!strcmpwild(dotQuad(parray[p1].thisHost), ipstr) && (parray[p1].status != PLAYER_EMPTY))
      pprintf(p, "%s%s", SendCode(p, INFO), parray[p1].name);
  return COM_OK;
}

PUBLIC int com_ausers(int p, param_list param)
{
  int p1;

  pprintf(p,"%s   Name        From        Last Tell  Chan Status\n",SendCode(p, INFO));
  pprintf(p,"%s---------- --------------- ---------- ---- ------\n",SendCode(p,INFO));

  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status != PLAYER_PROMPT) continue;
    pprintf(p, "%s%-10s %-15.15s %-10.10s %-4.2d %s %-3.2d\n",
                SendCode(p, INFO),
                parray[p1].name,
                dotQuad(parray[p1].thisHost),
                parray[p1].last_tell >= 0 ? 
                         parray[parray[p1].last_tell].name : "----",
                parray[p1].last_channel,
                parray[p1].game < 0 ? 
                 (parray[p1].num_observe > 0 ? "O" : "-") : "P",
                parray[p1].game < 0 ?
                 (parray[p1].num_observe>0 ? parray[p1].observe_list[0] : 0) :
                 parray[p1].game + 1);
  }
  return COM_OK;
}

PUBLIC int com_asetdebug(int p, param_list param)
{
  
  if (param[0].type == (int) TYPE_NULL) {
    pprintf(p, "%sDebug level set to: %d", SendCode(p, INFO), Debug);
    return COM_OK;
  }
  
  else {
    Debug = param[0].val.integer;
    if(Debug < 0) Debug = 0;
  }
  return COM_OK;
}

PUBLIC int com_checkSOCKET(int p, param_list param)
{
  int fd = param[0].val.integer;
  int p1, flag;

  flag = 0;
  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].socket == fd) {
      flag = 1;
      pprintf(p, "%sSocket %d is used by %s", SendCode(p, INFO),
		fd, parray[p1].name);
    }
  }
  if (!flag)
    pprintf(p, "%sSocket %d is unused!", SendCode(p, INFO), fd);
  return COM_OK;
}

PUBLIC int com_checkPLAYER(int p, param_list param)
{
  char *gplayer = param[0].val.word;
  int p1;

  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) 
     {
      p1 = (-p1) - 1;
      pprintf(p, "%s%s is not logged in.\n", SendCode(p, INFO), gplayer);
      stolower(gplayer);
      pprintf(p, "%sname = %s\n", SendCode(p, INFO), parray[p1].name);
      pprintf(p, "%slogin = %s\n", SendCode(p, INFO), parray[p1].login);
      pprintf(p, "%sfullName = %s\n", SendCode(p, INFO), (parray[p1].fullName ? parray[p1].fullName : "(none)"));
      pprintf(p, "%semailAddress = %s\n", SendCode(p, INFO), (parray[p1].emailAddress ? parray[p1].emailAddress : "(none)"));
      pprintf(p, "%sadminLevel = %d\n", SendCode(p, INFO), parray[p1].adminLevel);
      pprintf(p, "%smuzzled = %d\n", SendCode(p, INFO), parray[p1].muzzled);
      pprintf(p, "%sgmuzzled = %d\n", SendCode(p, INFO), parray[p1].gmuzzled);
      pprintf(p, "%sbmuzzled = %d\n", SendCode(p, INFO), parray[p1].bmuzzled);
      pprintf(p, "%slastHost = %s", SendCode(p, INFO), dotQuad(parray[p1].lastHost));
      player_remove(p1);
      return COM_OK;
  } else {
     p1 = p1 - 1;
    pprintf(p, "%s%s is number %d in parray of size %d\n", SendCode(p, INFO), 
                gplayer, p1, p_num + 1);
    pprintf(p, "%sname = %s\n", SendCode(p, INFO), parray[p1].name);
    pprintf(p, "%slogin = %s\n", SendCode(p, INFO), parray[p1].login);
    pprintf(p, "%sfullName = %s\n", SendCode(p, INFO), parray[p1].fullName ? parray[p1].fullName : "(none)");
    pprintf(p, "%semailAddress = %s\n", SendCode(p, INFO), parray[p1].emailAddress ? parray[p1].emailAddress : "(none)");
    pprintf(p, "%ssocket = %d\n", SendCode(p, INFO), parray[p1].socket);
    pprintf(p, "%sregistered = %d\n", SendCode(p, INFO), parray[p1].registered);
    pprintf(p, "%slast_tell = %d\n", SendCode(p, INFO), parray[p1].last_tell);
    pprintf(p, "%slast_channel = %d\n", SendCode(p, INFO), parray[p1].last_channel);
    pprintf(p, "%slogon_time = %d\n", SendCode(p, INFO), parray[p1].logon_time);
    pprintf(p, "%sadminLevel = %d\n", SendCode(p, INFO), parray[p1].adminLevel);
    pprintf(p, "%sstate = %d\n", SendCode(p, INFO), parray[p1].state);
    pprintf(p, "%smuzzled = %d\n", SendCode(p, INFO), parray[p1].muzzled);
    pprintf(p, "%sgmuzzled = %d\n", SendCode(p, INFO), parray[p1].gmuzzled);
    pprintf(p, "%sbmuzzled = %d\n", SendCode(p, INFO), parray[p1].bmuzzled);
    pprintf(p, "%sthisHost = %s\n", SendCode(p, INFO), dotQuad(parray[p1].thisHost));
    pprintf(p, "%slastHost = %s", SendCode(p, INFO), dotQuad(parray[p1].lastHost));
  }
  return COM_OK;
}

PUBLIC int com_remplayer(int p, param_list param)
{
  char *gplayer = param[0].val.word;
  char playerlower[MAX_LOGIN_NAME];
  int p1, lookup;

  strcpy(playerlower, gplayer);
  stolower(playerlower);
  p1 = player_new();
  lookup = player_read(p1, playerlower);
  if (!lookup) {
    if (parray[p].adminLevel <= parray[p1].adminLevel) {
      pprintf(p, "%sYou can't remove an admin with a level higher than or equal to yourself.", SendCode(p, ERROR));
      player_remove(p1);
      return COM_OK;
    }
  }
  player_remove(p1);
  if (lookup) {
    pprintf(p, "%sNo player by the name %s is registered.", SendCode(p, ERROR), gplayer);
    return COM_OK;
  }
  p1 = player_find_bylogin(playerlower);
  if ((p1 >= 0) && (parray[p1].registered)) {
    pprintf(p, "%sA player by that name is logged in.\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if (!player_kill(playerlower)) {
    pprintf(p, "%sPlayer %s removed.", SendCode(p, INFO), gplayer);
  } else {
    pprintf(p, "%sRemplayer failed.", SendCode(p, ERROR));
  }
  Show_Admin_Command(p, param[0].val.word, " ");
  return COM_OK;
}

PUBLIC int com_raisedead(int p, param_list param)
{
  char *gplayer = param[0].val.word;
  char *newplayer = param[0].val.word;
  char playerlower[MAX_LOGIN_NAME], newplayerlower[MAX_LOGIN_NAME];
  
  int p1,p2,lookup;

  strcpy(playerlower, gplayer);
  stolower(playerlower);
  p1 = player_new();
  lookup = player_read(p1, playerlower);
  player_remove(p1);
  if (!lookup) {
    pprintf(p, "%sA player by the name %s is already registered.\n", SendCode(p, ERROR), gplayer);
    pprintf(p, "%sObtain a new handle for the dead person.\n", SendCode(p, ERROR));
    pprintf(p, "%sThen use raisedead [oldname] [newname].", SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_find_bylogin(playerlower) >= 0) {
    pprintf(p, "%sA player by that name is logged in.\n",SendCode(p, ERROR));
    pprintf(p, "%sCan't raise until they leave.",SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[1].val.word == NULL)
     {
     if (!player_raise(playerlower)) {
       pprintf(p, "%sPlayer %s raised from dead.", SendCode(p, INFO), gplayer);
     } else {
       pprintf(p, "%sRaisedead failed.",SendCode(p, ERROR));
     }
     return COM_OK;
     }
  else
     {
     strcpy(newplayerlower, newplayer);
     stolower(newplayerlower);
     p2 = player_new();
     lookup = player_read(p2, newplayerlower);
     if (!lookup) {
       player_remove(p2);
       pprintf(p, "%sA player by the name %s is already registered.", SendCode(p, ERROR), gplayer);
       pprintf(p, "%sObtain another new handle for the dead person.", SendCode(p, ERROR));
       return COM_OK;
     }
    if (!player_rename(playerlower, newplayerlower)) {
    pprintf(p, "%sPlayer %s reincarnated to %s.", SendCode(p, INFO), gplayer, newplayer);
    do_copy(parray[p2].name, newplayer, MAX_NAME);
    player_save(p2);
    player_remove(p2);
  } else {
    pprintf(p, "%sRaisedead failed.", SendCode(p, ERROR));
    player_remove(p2);
  }
  }
  return COM_OK;
}

PRIVATE int shutdownTime = 0;
PRIVATE int lastTimeLeft;
PRIVATE int shutdownStartTime;
PRIVATE char downer[1024];

PUBLIC void ShutDown()
{
  int p1;
  int shuttime = time(0);

  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status == PLAYER_EMPTY)
      continue;
    pprintf(p1, "%s    **** Server shutdown started by %s. ****\n", SendCode(p1, DOWN), downer);
  }
  TerminateCleanup();
  fprintf(stderr, "Shutdown ordered at %s by %s.\n", strltime((time_t *) &shuttime), downer);
  Logit("Shutdown ordered at %s by %s.", strltime((time_t *) &shuttime), downer);
  net_closeAll();
  system("touch .shutdown");
  exit(0);
}

PUBLIC void ShutHeartBeat()
{
  int t = time(0);
  int p1;
  int timeLeft;
  int crossing = 0;

  if (!shutdownTime) return;
  if (!lastTimeLeft) lastTimeLeft = shutdownTime;
  timeLeft = shutdownTime - (t - shutdownStartTime);
  if ((lastTimeLeft > 3600) && (timeLeft <= 3600))
    crossing = 1;
  if ((lastTimeLeft > 2400) && (timeLeft <= 2400))
    crossing = 1;
  if ((lastTimeLeft > 1200) && (timeLeft <= 1200))
    crossing = 1;
  if ((lastTimeLeft > 600) && (timeLeft <= 600))
    crossing = 1;
  if ((lastTimeLeft > 300) && (timeLeft <= 300))
    crossing = 1;
  if ((lastTimeLeft > 120) && (timeLeft <= 120))
    crossing = 1;
  if ((lastTimeLeft > 60) && (timeLeft <= 60))
    crossing = 1;
  if ((lastTimeLeft > 10) && (timeLeft <= 10))
    crossing = 1;
  if (crossing) {
    Logit("   **** Server going down in %d minutes and %d seconds. ****",
	    timeLeft / 60,
	    timeLeft - ((timeLeft / 60) * 60));
    for (p1 = 0; p1 < p_num; p1++) {
      if (parray[p1].status == PLAYER_EMPTY)
	continue;
      pprintf_prompt(p1,
		     "%s    **** Server going down in %d minutes and %d seconds. ****\n",
		     SendCode(p1, DOWN),
		     timeLeft / 60,
		     timeLeft - ((timeLeft / 60) * 60));
    }
  }
  lastTimeLeft = timeLeft;
  if (timeLeft <= 0) {
    ShutDown();
  }
}

PUBLIC int com_shutdown(int p, param_list param)
{
  char *ptr;
  int p1, secs;

  strcpy(downer, parray[p].name);
  shutdownStartTime = time(0);   
  if (shutdownTime) {         /* Cancel any pending shutdowns */
    for (p1 = 0; p1 < p_num; p1++) {
      if (parray[p1].status == PLAYER_EMPTY)
      continue;
      pprintf(p1, "%s    **** Server shutdown canceled by %s. ****\n", SendCode(p1, INFO), downer);
    }
    shutdownTime = 0;
    if (param[0].type == (int) TYPE_NULL)
      return COM_OK;
  }
  /* Work out how soon to shut down */
  if (param[0].type == (int) TYPE_NULL)
    shutdownTime = 300;
  else {
    if (!strcmp(param[0].val.word, "now"))
      shutdownTime = 0;
    else if (!strcmp(param[0].val.word, "cancel"))
      return COM_OK;
    else {
      ptr = param[0].val.word;
      shutdownTime = secs = 0;
      p1 = 2;
      while (*ptr) {
      switch(*ptr) {
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
          secs = secs*10 + *ptr-'0';
          break;
        case ':':
          if(p1--) {
            shutdownTime = shutdownTime*60 + secs;
            secs = 0;
            break;   
          }
        default:
          shutdownTime = 0;
          pprintf(p, "%sI don't know what you mean by %s\n", SendCode(p, ERROR), param[0].val.word);
          return COM_OK;
      }
      ptr++;
      }
      shutdownTime = shutdownTime*60 + secs;
    }
  }
  if (shutdownTime <= 0)
    ShutDown();
  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status == PLAYER_EMPTY)
      continue;
    pprintf(p1, "%s    **** Server shutdown started by %s. ****\n", SendCode(p1, DOWN), downer);
    pprintf_prompt(p1,
     "%s    **** Server going down in %d minutes and %d seconds. ****\n",
		SendCode(p1, DOWN),
                shutdownTime / 60, shutdownTime % 60);
  }
  lastTimeLeft = 0;
  return COM_OK;
}


PUBLIC int server_shutdown(int secs, char *why)
{
  int p1;

  if (shutdownTime && (shutdownTime <= secs)) {
    /* Server is already shutting down, I'll let it go */
    return 0;
  }
  strcpy(downer, "Automatic");
  shutdownTime = secs;
  shutdownStartTime = time(0);
  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status == PLAYER_EMPTY)
      continue;
    pprintf(p1, "%s    **** Automatic Server shutdown. ****\n", SendCode(p1, DOWN));
    pprintf(p1, "%s%s\n", SendCode(p1, DOWN), why);
    pprintf_prompt(p1,
	"%s    **** Server going down in %d minutes and %d seconds. ****\n",
                   SendCode(p1, DOWN),
		   shutdownTime / 60,
		   shutdownTime - ((shutdownTime / 60) * 60));
  }
  Logit("    **** Automatic Server shutdown. ****");
  Logit(" %s", why);
  return 0;
}

PUBLIC int com_pose(int p, param_list param)
{
  int p1;

  if ((p1 = player_find_part_login(param[0].val.word)) < 0) {
    pprintf(p, "%s%s is not logged in.", SendCode(p, ERROR), param[0].val.word);
    return COM_OK;
  }
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
#ifdef NOPE
  Logit("POSE: %s as %s: > %s <", parray[p].name, parray[p1].name, param[1].val.string); 
  for(j = 0; j < OnumOn[CASHOUT]; j++) {
    if(Ochannels[CASHOUT][j] == p) continue;
    if ((parray[Ochannels[CASHOUT][j]].status == PLAYER_PASSWORD)
     || (parray[Ochannels[CASHOUT][j]].status == PLAYER_LOGIN))
      continue;
    pprintf_prompt(Ochannels[CASHOUT][j], "\n%s## --- ##: POSE: %s as %s: > %s <\n",
      SendCode(Ochannels[CASHOUT][j], INFO),
      parray[p].name, parray[p1].name,
      param[1].val.string);
  }
#endif
  pcommand(p1, param[1].val.string);
  return COM_OK;
}

PUBLIC int Show_Admin_Command(int p, char *comm, char *command) {
  int j;
  Logit("ADMIN: %s > %s <", parray[p].name, orig_command);
  for(j = 0; j < OnumOn[CASHOUT]; j++) {
    if ((parray[Ochannels[CASHOUT][j]].status == PLAYER_PASSWORD)
     || (parray[Ochannels[CASHOUT][j]].status == PLAYER_LOGIN))
      continue;
    pprintf_prompt(Ochannels[CASHOUT][j], "\n%s## ADMIN ##: %s : > %s <\n",
      SendCode(Ochannels[CASHOUT][j], INFO),
      parray[p].name, orig_command);
  }
  return(1);
}

PUBLIC int com_arank(int p, param_list param)
{
  int p1, connected;

  connected = 1;
  if(strlen(param[1].val.string) > 4) return COM_OK;
  p1 = player_search(p,param[0].val.word);
  if (!p1) return COM_OK;
  if (p1 < 0) {               /* player had to be connected and will be
                                 removed later */
    connected = 0;
    p1 = (-p1) - 1;
  } else {
    connected = 1;
    p1 = p1 - 1;
  }
  do_copy(parray[p1].ranked, param[1].val.string, MAX_RANKED);
  do_copy(parray[p1].srank, param[1].val.string, MAX_SRANK);
  player_save(p1);
  if(!connected) player_remove(p1);
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}

PUBLIC int com_noshout(int p, param_list param)
{
  int j;

  if(Ocarray[CSHOUT].locked) Ocarray[CSHOUT].locked = 0;
  else  Ocarray[CSHOUT].locked = 1;
  for(j = 0; j < OnumOn[CASHOUT]; j++) {
    if(Ochannels[CASHOUT][j] == p) continue;
    if ((parray[Ochannels[CASHOUT][j]].status == PLAYER_PASSWORD)
     || (parray[Ochannels[CASHOUT][j]].status == PLAYER_LOGIN))
      continue;
    pprintf_prompt(Ochannels[CASHOUT][j], "\n%s## --- ##: %s just turned %s shouts.\n", SendCode(Ochannels[CASHOUT][j], INFO),
      parray[p].name,
      Ocarray[CSHOUT].locked ? "off" : "on");
  }
  return COM_OK;
}
    

PUBLIC int com_announce(int p, param_list param)
{
  int p1;

  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status != PLAYER_PROMPT)
      continue;
    pprintf_prompt(p1, "%s!!%s!!: %s\n", 
                SendCode(p1, INFO), parray[p].name, param[0].val.string);
  }
  return COM_OK;
}

PUBLIC int com_muzzle(int p, param_list param)
{
  int p1,connected;
  
  if (param[0].type == (int) TYPE_NULL) {
    pprintf(p, "%sMuzzled players:\n",SendCode(p, INFO));
    for (p1 = 0; p1 < p_num; p1++) {
      if (parray[p1].status != PLAYER_PROMPT)
	continue;
      if (parray[p1].muzzled)
	pprintf(p, "%s%s\n", SendCode(p, INFO), parray[p1].name);
    }
    return COM_OK;
  }
  p1 = player_search(p,param[0].val.word);
    if (!p1) return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  if (parray[p1].muzzled) {
    pprintf(p, "%s%s unmuzzled.", SendCode(p, INFO), parray[p1].name);
    parray[p1].muzzled = 0;
  } else {
    pprintf(p, "%s%s muzzled.", SendCode(p, INFO), parray[p1].name);
    parray[p1].muzzled = 1;
  }
  return COM_OK;
}

PUBLIC int com_bmuzzle(int p, param_list param)
{
  int p1,connected;
  
  if (param[0].type == (int) TYPE_NULL) {
    pprintf(p, "%sBMuzzled players:\n", SendCode(p, INFO));
    for (p1 = 0; p1 < p_num; p1++) {
      if (parray[p1].status != PLAYER_PROMPT)
        continue;
      if (parray[p1].bmuzzled)
        pprintf(p, "%s%s\n", parray[p1].name,SendCode(p, INFO));
    }
    return COM_OK;
  }
  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {               /* player had to be connected and will be
                                   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  if (parray[p1].bmuzzled) {
    pprintf(p, "%s%s unbmuzzled.", SendCode(p, INFO), parray[p1].name);
    parray[p1].bmuzzled = 0;
  } else {
    pprintf(p, "%s%s bmuzzled.", SendCode(p, INFO), parray[p1].name);
    parray[p1].bmuzzled = 1;
  }
  return COM_OK;
}

PUBLIC int com_gmuzzle(int p, param_list param)
{
  int p1,connected;
  
  if (param[0].type == (int) TYPE_NULL) {
    pprintf(p, "%sGMuzzled players:\n", SendCode(p, INFO));
    for (p1 = 0; p1 < p_num; p1++) {
      if (parray[p1].status != PLAYER_PROMPT)
	continue;
      if (parray[p1].gmuzzled)
	pprintf(p, "%s%s\n", parray[p1].name,SendCode(p, INFO));
    }
    return COM_OK;
  }
  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  if (parray[p1].gmuzzled) {
    pprintf(p, "%s%s ungmuzzled.", SendCode(p, INFO), parray[p1].name);
    parray[p1].gmuzzled = 0;
  } else {
    pprintf(p, "%s%s gmuzzled.", SendCode(p, INFO), parray[p1].name);
    parray[p1].gmuzzled = 1;
  }
  return COM_OK;
}

PUBLIC int com_asetpasswd(int p, param_list param)
{
  int p1, connected;
  char salt[3];

  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  if (parray[p].adminLevel <= parray[p1].adminLevel) {
    pprintf(p, "%sYou can only set password for players below your adminlevel.",
                   SendCode(p, ERROR));
    if (!connected)
      player_remove(p1);
    return COM_OK;
  }
  if (param[1].val.word[0] == '*') {
    do_copy(parray[p1].passwd, param[1].val.word, MAX_PASSWORD);
    pprintf(p, "%sAccount %s locked!\n", SendCode(p, INFO), parray[p1].name);
  } else {
    salt[0] = 'a' + rand() % 26;
    salt[1] = 'a' + rand() % 26;
    salt[2] = '\0';
    do_copy(parray[p1].passwd, crypt(param[1].val.word, salt), MAX_PASSWORD);
    pprintf(p, "%sPassword of %s changed to \"%s\".\n", SendCode(p, INFO), parray[p1].name, param[1].val.word);
  }
  player_save(p1);
  if (connected) {
    if (param[1].val.word[0] == '*') {
      pprintf_prompt(p1, "%s%s has locked your account.\n", SendCode(p1, INFO), parray[p].name);
    } else {
      pprintf_prompt(p1, "%s%s has changed your password.\n", SendCode(p1, INFO), parray[p].name);
    }
  } else {
    player_remove(p1);
  }
  return COM_OK;
}

PUBLIC int com_asetemail(int p, param_list param)
{
  int p1, connected;

  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  if (param[1].type == (int) TYPE_NULL) {
    parray[p1].emailAddress[0] = '\0';
    pprintf(p, "%sEmail address for %s removed\n", SendCode(p, INFO), parray[p1].name);
  } else {
    do_copy(parray[p1].emailAddress, param[1].val.word, MAX_EMAIL);
    pprintf(p, "%sEmail address of %s changed to \"%s\".", SendCode(p, INFO), parray[p1].name, param[1].val.word);
  }
  player_save(p1);
  if (connected) {
    if (param[1].type == (int) TYPE_NULL) {
      pprintf_prompt(p1, "%s%s has removed your email address.\n", SendCode(p1, INFO), parray[p].name);
    } else {
      pprintf_prompt(p1, "%s%s has changed your email address.\n", SendCode(p1, INFO), parray[p].name);
    }
  } else {
    player_remove(p1);
  }
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}

PUBLIC int com_asetrealname(int p, param_list param)
{
  int p1, connected;

  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  if (param[1].type == (int) TYPE_NULL) {
    do_copy(parray[p1].fullName, "-", MAX_FULLNAME);
    pprintf(p, "%sReal name for %s removed",SendCode(p, INFO), parray[p1].name);
  } else {
    do_copy(parray[p1].fullName, param[1].val.word, MAX_FULLNAME);
    pprintf(p, "%sReal name of %s changed to \"%s\".", SendCode(p, INFO), parray[p1].name, param[1].val.word);
  }
  player_save(p1);
  if (connected) {
    if (param[1].type == (int) TYPE_NULL) {
      pprintf_prompt(p1, "%s%s has removed your real name.\n", SendCode(p1, INFO), parray[p].name);
    } else {
      pprintf_prompt(p1, "%s%s has changed your real name.\n", SendCode(p1, INFO), parray[p].name);
    }
  } else {
    player_remove(p1);
  }
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}

PUBLIC int com_asetsilent(int p, param_list param)
{
  int p1, connected;

  p1 = player_search(p,param[0].val.word);
  if (!p1)
    return COM_OK;
  if (p1 < 0) {               /* player had to be connected and will be
                                   removed later */
    connected = 0;
    p1 = (-p1) - 1;
  } else {
    connected = 1;
    p1 = p1 - 1;
  }

  parray[p1].silent_login = !parray[p1].silent_login;

  player_save(p1);

  pprintf(p, "%sChanged %s's login to %s.\n", SendCode(p, INFO), parray[p1].name, parray[p1].silent_login ? "SILENT" : "NOT SILENT");

  if (!connected) player_remove(p1);

  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}


#if 0
PUBLIC int com_asethandle(int p, param_list param)
{
  char *gplayer = param[0].val.word;
  char *newplayer = param[1].val.word;
  char playerlower[MAX_LOGIN_NAME], newplayerlower[MAX_LOGIN_NAME];
  int p1, p2;

  strcpy(playerlower, gplayer);
  stolower(playerlower);
  strcpy(newplayerlower, newplayer);
  stolower(newplayerlower);
  p1 = player_new();
  if (player_read(p1, playerlower)) {
    pprintf(p, "%sNo player by the name %s is registered.", SendCode(p, ERROR), gplayer);
    player_remove(p1);
    return COM_OK;
  } else {
    if (parray[p].adminLevel <= parray[p1].adminLevel) {
      pprintf(p, "%sYou can't set handles for an admin with a level higher than or equal to yourself.", SendCode(p, ERROR));
      player_remove(p1);
      return COM_OK;
    }
  }
  player_remove(p1);
  if (player_find_bylogin(playerlower) >= 0) {
    pprintf(p, "%sA player by that name is logged in.",SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_find_bylogin(newplayerlower) >= 0) {
    pprintf(p, "%sA player by that new name is logged in.",SendCode(p, ERROR));
    return COM_OK;
  }
  p2 = player_new();
  if ((!player_read(p2, newplayerlower)) && (strcmp(playerlower, newplayerlower))) {
    pprintf(p, "%sSorry that handle is already taken.",SendCode(p, ERROR));
    player_remove(p2);
    return COM_OK;
  }
  if ((!player_rename(playerlower, newplayerlower)) && (!player_read(p2, newplayerlower))) {
    pprintf(p, "%sPlayer %s renamed to %s.", SendCode(p, INFO), gplayer, newplayer);
    parray[p2].name = newplayer;
    player_save(p2);
    player_remove(p2);
  } else {
    pprintf(p, "%sAsethandle failed.",SendCode(p, ERROR));
    player_remove(p2);
  }
  return COM_OK;
}
#endif

PUBLIC int com_asetadmin(int p, param_list param)
{
  int p1, connected, oldlevel;
  char tmp[MAX_LOGIN_NAME+10];

  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
 
#ifdef ADMINLEVELS
  if (parray[p].adminLevel <= parray[p1].adminLevel) {
    pprintf(p, "%sYou can only set adminlevel for players below your adminlevel.",SendCode(p, ERROR));
    if (!connected)
      player_remove(p1);
    return COM_OK;
  }
  if ((parray[p1].login) == (parray[p].login)) {
    pprintf(p, "%sYou can't change your own adminlevel.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[1].val.integer >= parray[p].adminLevel) {
    pprintf(p, "%sYou can't promote someone to or above your adminlevel.",
                SendCode(p, ERROR));
    if (!connected)
      player_remove(p1);
    return COM_OK;
  }
#endif /* ADMINLEVELS */

  oldlevel=parray[p1].adminLevel;
  parray[p1].adminLevel = param[1].val.integer;
  pprintf(p, "%sAdmin level of %s set to %d.\n", 
            SendCode(p, INFO), parray[p1].name, parray[p1].adminLevel);
  if ((oldlevel == 0) && (parray[p1].adminLevel > 0)) {
    sprintf (tmp,"+admin %s\n",parray[p1].name);
    pcommand (p,tmp);
  }
  if ((oldlevel > 0) && (parray[p1].adminLevel == 0)) {
    sprintf (tmp,"-admin %s\n",parray[p1].name);
    pcommand (p,tmp);
  }
  player_save(p1);
  if (!connected) {
    player_remove(p1);
  }
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}

PUBLIC int com_asetwater(int p, param_list param)
{
  int p1, connected;

  p1 = player_search(p,param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {               /* player had to be connected and will be
                                   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }

  parray[p1].water = param[1].val.integer;
  pprintf(p, "%swater level of %s set to %d.",
            SendCode(p, INFO), parray[p1].name, parray[p1].water);
  player_save(p1);
  if (connected) {
    pprintf_prompt(p1, "%s%s has set your water level to %d.\n",
              SendCode(p1, INFO), parray[p].name, parray[p1].water);
  } else {
    player_remove(p1);
  }
  return COM_OK;
}

PUBLIC int com_nuke(int p, param_list param)
{
  int p1, fd, j;

  if ((p1 = player_find_part_login(param[0].val.word)) < 0) {
    pprintf(p, "%s%s isn't logged in.", SendCode(p, ERROR), param[0].val.word);
  } else {
    pprintf(p, "%sNuking: %s", SendCode(p, INFO), param[0].val.word);
    Logit("NUKE: %s by %s", parray[p1].name, parray[p].name);
    for(j = 0; j < OnumOn[CASHOUT]; j++) {
      if(Ochannels[CASHOUT][j] == p) continue;
      if ((parray[Ochannels[CASHOUT][j]].status == PLAYER_PASSWORD)
       || (parray[Ochannels[CASHOUT][j]].status == PLAYER_LOGIN))
        continue;
      pprintf_prompt(Ochannels[CASHOUT][j], "\n%s## --- ##: %s just nuked %s.\n", SendCode(Ochannels[CASHOUT][j], INFO),
        parray[p].name,
        parray[p1].name);
    }
    fd = parray[p1].socket;
    process_disconnection(fd);
    net_close(fd);
    player_remove(p1);
    return COM_OK;
  }
  return COM_OK;
}

PUBLIC int com_actitle(int p, param_list param)
{
  int i;

  if((param[0].val.integer > MAX_CHANNELS - 1) || (param[0].val.integer < 0)) {
    return COM_OK;
  }

  i = param[0].val.integer;

  if(strlen(param[1].val.string) > 1000) return COM_OK;

  free(carray[i].Title);
  carray[i].Title = (char *) strdup(param[1].val.string);
  Show_Admin_Command(p, param[0].val.word, param[1].val.string);
  return COM_OK;
}


PUBLIC int com_reload_ladders(int p, param_list param)
{
  FILE *fp;
  
  LadderDel(Ladder9);
  LadderDel(Ladder19);
  Ladder9 = LadderNew(LADDERSIZE);
  Ladder19 = LadderNew(LADDERSIZE);
  num_9 = 0;
  fp = fopen(LADDER9, "r");
  if(fp) {
    num_9 = PlayerLoad(fp, Ladder9);
    Logit("%d players loaded from file %s", num_9, LADDER9);
    fclose(fp);
  }
  num_19 = 0;
  fp = fopen(LADDER19, "r");
  if(fp) {
    num_19 = PlayerLoad(fp, Ladder19);
    Logit("%d players loaded from file %s", num_19, LADDER19);
    fclose(fp);
  }
  return COM_OK;
}

PUBLIC int com_hide(int p, param_list param)
{
  int i;

  if((param[0].val.integer > MAX_CHANNELS - 1) || (param[0].val.integer < 0)) {
    return COM_OK;
  }

  i = param[0].val.integer;

  carray[i].hidden = 1;
  return COM_OK;
}

PUBLIC int com_unhide(int p, param_list param)
{
  int i;

  if((param[0].val.integer > MAX_CHANNELS - 1) || (param[0].val.integer < 0)) {
    return COM_OK;
  }

  i = param[0].val.integer;

  carray[i].hidden = 0;
  return COM_OK;
}

#ifdef LADDER_SIFT
PUBLIC int com_rating_recalc(int p, param_list param)
{
  Logit("Sifting 19x19 ladder");
  PlayerSift(Ladder19, 30);  /* Do the ladder stuff, 19x19 is 14 days */
  Logit("Sifting 9x9 ladder");
  PlayerSift(Ladder9, 30); /* Do the ladder stuff, 9x9 is 7 days */
  Logit("Done sifting");
  return COM_OK;
}
#endif /*LADDER_SIFT*/

