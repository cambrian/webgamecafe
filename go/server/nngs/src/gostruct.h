typedef struct _timer_t {
  int Time;
  int OverTime;   
  int TimerType; 
} timer_t;

/* a timer is not limited to go; i think we're better off with
   a set of timers each of which is applicable to many games */

/* However, I do not want to use Go style variable names for, say, connect-4.
   Keeping everything totally seperate will be 100 times easier to debug and
   write code for.

   If i'm adding connect-4, I don't want to be bothered to write timer
   routines for that; i just want to pick one of the general ones. If you
   don't like the phrase "byo" then let's omit it.
   letting each game use it's own timerinfo is a waste of effort
*/

   Then, when our main timer routine kicks in, we add a switch(TimerType) and
call the correct timer routine for each game?

yes, except we'll switch on timer_type, where each game can choose whatever
timer it wants. there is no *inherent* correspondence between gametypes
and timertypes, just a few real-life conventions. 

*/

typedef struct _side_t {
  timer_t *Timer;
  int *PlayerList;	/* list of players (index into game->PlayerList) on 
                           this side */
} side_t;

typedef struct _game {

  char *Title;
  side_t *SideList;
  annotation_t *annotations;
  int nannotations;
  unsigned long game_id;     /* ID of the game.  Should be unique */
  void *spec;     /* pointer to game specific information */
  unsigned startTime;    /* The relative time the game started  */
  unsigned lastMoveTime; /* Last time a move was made */
  unsigned lastDecTime;  /* Last time a players clock was decremented */
  /* Should this lastDecTime be a part of the main game struct?  Maybe some
     games will not use it?
  i wans't sure how it's currently used. i'd be happy to ditch them if
  possible:)

  Ok, well, I think lastDecTime should actually be part of the player record,
  since it is player specific?  But then we run into simul games trouble.
  Perhaps this needs a little more thought.

  Yes, we will let people play multiple games at once, too.

  Let's see what we will need to know for each case:

  single game:  Should know when the last move was played, this allows us
                to update the clocks on a move-by-move basis, and occasionally
                checking the times to see if someone ran out.  
                Should also know when the game was started.

  simul:        Should know when the last move was played, and also know when
                the game was started.

  1) why do we need to know when game was started?

     In case things get de-synced between servers.  Need to store that info
     somewhere, best place is in the game record.

  2) why do we need to distinguish between single and simul games?
     the latter is simply playing multiple games simultaneously.
 
     Well, each game requires a set of timers and related info.  We cannot
     put it in the player record easily or cleanly, so it should go in the
     game record.  That is what I was driving at.

  Perhaps we should put them in the struct timer and leave them there?
  */
  int winner;

/* etc */
} game;

typedef struct _go_game_t {
  float komi;
  int num_pass; 
  int Teach;
  int gotype;
  int status;
  int result;
  int width, height;    /* board dimensions */
  move *moves;          /* move history */
  int mvsize,movenr;    /* size of moves and number of moves played */
  int *board;           /* current state of go-board */
  ulongpair *zob;       /* zobrist random numbers */
  int *uf;              /* union-find structure */
  uf_log *uflog;        /* log of changes to uf */
  int logsize,logid;    /* size of uflog & number of changes to uf */
  int handicap;         /* handicap */
  int caps[4];          /* # stones captured */
  ulong hash;
  int root,kostat;      /* root is temporary variable used in group merging */
} go_game;
