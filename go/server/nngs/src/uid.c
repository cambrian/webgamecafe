#define SERVERID  0x0001

enum {
/*     SrvrId = 0x6942, NumMask = 0x0000ffff*/
     SrvrId = SERVERID, UserMask = 0xffff
};

/* This returns the server id from the composite value in ul. */

unsigned short GetSrvrId(unsigned long ul)
{
        return (unsigned short)((ul & ~UserMask) >> 16);
}

/* This returns the user ID from the argument ul.
   Which is assumed to consist of a combined user and server id.
*/
unsigned short GetUserId(unsigned long ul)
{
        return (unsigned short)(ul & UserMask);
}

/* Given an unsigned long ul which may or may not contain a user id, this
   sets its server id to srvr.
*/
unsigned long SetSrvrId(unsigned long ul, unsigned long srvr)
{
        ul &= UserMask;
        return ul |= (srvr << 16);
}

unsigned long SetUserId(unsigned long ul, unsigned long user)
{
        ul &= ~UserMask;
        return ul |= user;
}

/* Given a server id, and a user id, this creates the composite value. */

unsigned long CreateId(unsigned srvr, unsigned user)
{
        unsigned long ul = SetSrvrId(0, srvr);
        return SetUserId(ul, user);
}

void ParseIdVal(unsigned long ul, unsigned short *pSrvr, 
                unsigned short *pUser)
{
        *pSrvr = GetSrvrId(ul);
        *pUser = GetUserId(ul);
}

int testfunc(unsigned short srvrId, unsigned short userId)
{
        unsigned long combined;

        printf("got srvr = 0x%04x user = 0x%04x\n", srvrId, userId);
        combined = CreateId(srvrId, userId);
        printf("combined id is 0x%08x\n", combined);
        printf("breaking it up: ");
        ParseIdVal(combined, &srvrId, &userId); 
        printf("got srvr = 0x%04x user = 0x%04x\n", srvrId, userId);
}

int main()
{
  testfunc(SERVERID, 50);
  return 1;
}
