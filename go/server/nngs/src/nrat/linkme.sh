#!/bin/sh

cd /work/hyoon/webgamecafe/go/server/nngs/src/nrat

for f in *.[ch]; do
  rm ../$f
  echo "ln -s nrat/$f ../"
  ln -s nrat/$f ../
done
