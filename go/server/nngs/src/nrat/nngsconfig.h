/* nngsconfig.h
 *
 */

/* Configure file locations in this include file. */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995  Erik Van Riper (geek@imageek.york.cuny.edu)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _CONFIG_H
#define _CONFIG_H

/* CONFIGURE THIS: The port on which the server binds */
#ifndef DPORT
#define DEFAULT_PORT 9696
#else
#define DEFAULT_PORT 9999
#endif

/* If you want to have a running byte count */
#define BYTE_COUNT 1

/* CONFIGURE THESE: Locations of the data, players, and games directories */
/* These must be absolute paths because some mail daemons may be called */
/* from outside the pwd */

/* ROOT must have trailing slash! */

#define ROOT "/work/hyoon/webgamecafe/go/server/nngs/src/"

#define DEFAULT_MESS ROOT"data/messages"
#define DEFAULT_HELP ROOT"data/help"
#define DEFAULT_INFO ROOT"data/info"
#define DEFAULT_ADHELP ROOT"data/admin"
#define DEFAULT_STATS ROOT"data/stats"
#define DEFAULT_PLAYERS ROOT"data/users"
#define DEFAULT_GAMES ROOT"games"
#define DEFAULT_PROBLEMS ROOT"problems"
#define DEFAULT_LISTS ROOT"data/lists"
#define DEFAULT_NEWS ROOT"data/news"
#define DEFAULT_FIND ROOT"data/find.out"
#define DEFAULT_NOTE ROOT"data/note/note"
#define LADDER9 ROOT"data/ladder/ladder9"
#define LADDER19 ROOT"data/ladder/ladder19"
#define RATINGS_FILE "/home/nngs/ratings/pett/results-rated"
#define NRATINGS_FILE ROOT"ratdb"
#define RESULTS_FILE ROOT"results"
#define NRESULTS_FILE ROOT"nresults"
#define EMOTES_FILE ROOT"data/emotestr"
#define INTERGO_FILE ROOT"intergo/intergo"
#define LOGFILE "logfile"

/* The name of the server */

#define SERVER_NAME "WGC Worldwide"
#define SERVER_ADDRESS "ruby.bluecraft.com 9696"
#define SERVER_EMAIL "bugs@ruby.bluecraft.com"


/* define this to sift the ladder around */
#define LADDER_SIFT 1

/* define this if you want pair-go compiled in */
#define PAIR 1

/* define this if you have a ratings file somewhere.  See playerdb.c */
//#define NNGSRATED 1
#undef NNGSRATED


/* define this if we want the "news" (bulleting boards). */
/* [PEM]: Disabled the news commands. They are undocumented, practically
   never used, and partially very buggy. ("anews all" breaks clients for
   instance.) Must be fixed and documented before enabled again. */
/* #define NEWS_BB 1 */

/* This is the length of the password you wish to generate */
#define PASSLEN 5

/* Where the standard ucb mail program is */
#ifdef SGI
#define MAILPROGRAM "/usr/sbin/Mail"
#elif LINUX
#define MAILPROGRAM "/bin/mail"
#elif FREEBSD
#define MAILPROGRAM "/usr/bin/mail"
#else 
#define MAILPROGRAM "/bin/mailx"
#endif

/* May need this, like on SGI, for instance. */
#ifdef SGI
extern int      strcasecmp(const char *, const char *);
extern int      strncasecmp(const char *, const char *, size_t);
extern FILE    *popen(const char *, const char *);
extern int     pclose(FILE *);
extern int gettimeofday(struct timeval *tp,...);
#ifdef HUH
extern int      fileno(FILE *);
#endif
#endif

#ifdef LINUX
extern char *crypt(const char *key, const char *salt);
#if 0
extern char *strdup(const char *s);
extern int strcasecmp(const char *s1, const char *s2);
extern int strncasecmp(const char *s1, const char *s2, size_t n);
extern long int random(void);
extern FILE *popen(const char *command, const char *type);
extern int pclose(FILE *stream);
extern int fileno( FILE *stream);
extern int truncate(const char *path, size_t length);
extern int ftruncate(int fd, size_t length);
#endif /* 0 */
extern char *index(const char *s, int c);
extern char *rindex(const char *s, int c);
#endif

#endif /* _CONFIG_H */
