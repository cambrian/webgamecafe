/* gamedb.c
 *
 */
/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@willent.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"

#include "glue.h"
#include "nngsconfig.h"
#include "common.h"
#include "nngsmain.h"
#include "gamedb.h"
#include "playerdb.h"
#include "gameproc.h"
#include "command.h"
#include "utils.h"
#ifdef NNGSRATED
#include "rdbm.h"		/* PEM */
#endif

/* [PEM]: A desperate attempt to fix the komi bug when loading
** adjourned games. :-(
**
** Note: This is not a complete string-to-float parser, it doesn't
**       handle exponents.
*/
static float
nngs_strtof(char *str, char **p)
{
  int ok = 0, sign = 1;
  float f = 0.0;
  char *s = str;

  while (isspace(*s))
    ++s;
  if (*s == '+')
    ++s;
  else if (*s == '-')
  {
    sign = -1;
    ++s;
  }
  while (isdigit(*s))
  {
    f = 10 * f + (*s - '0');
    ok = 1;
    ++s;
  }
  if (*s == '.')
  {
    float d = 10.0;

    ++s;
    while (isdigit(*s))
    {
      f += (*s - '0')/d;
      d *= 10;
      ok = 1;
      ++s;
    }
  }
  if (ok)
  {
    *p = s;
    return (sign*f);
  }
  *p = str;
  return 0.0;
}

PUBLIC game *garray = NULL;
PUBLIC int g_num = 0;

PRIVATE int get_empty_game_slot(int type)
{
  int i;

  for (i = 0; i < g_num; i++) {
    if (garray[i].status == GAME_EMPTY) {
      if(type >= TYPE_GO) garray[i].gotype = type;
      garray[i].time_type = TYPE_UNTIMED;
      if(Debug)
        Logit("get_empty_game_slot, g_num = %d, i = %d (Found one)", g_num, i);
      return i;
    }
  }
  g_num++;
  if (!garray) {	/* g_num was 0 before */
    garray = (game *) malloc(sizeof(game) * g_num);
  } else {
    garray = (game *) realloc(garray, sizeof(game) * g_num);
  }
  garray[g_num - 1].status = GAME_EMPTY;
  garray[i].gotype = type;
  if(Debug)
    Logit("get_empty_game_slot, g_num = %d, i = %d (Had to alloc)", g_num,i);
  return g_num - 1;
}

PUBLIC int game_new(int type, int size)
{
  int i; 
  
  i = get_empty_game_slot(type);
  if(Debug) Logit("In game_new, i = %d", i);
  game_zero(&garray[i], size);
  return i;
}

PUBLIC int game_zero(game *g, int size)
{
  memset(g, 0, sizeof(game));
  g->gotype = TYPE_GO;
  g->white = -1;
  g->black = -1;
  g->old_white = -1;
  g->old_black = -1;
  g->status = (size > 0) ? GAME_NEW : GAME_EMPTY;
  g->rated = 1;
  /* g->nocaps = 0; */
  g->Private = 0;
  g->komi = 0.5;
  g->result = END_NOTENDED;
  /*if (!g->type)*/
    g->time_type = TYPE_UNTIMED;
  g->num_pass = 0;
  g->wTime = 300;
  g->wInByo = 0;
  g->look = 0;
  g->Title = (char *) strdup("None");
  g->Event = (char *) strdup("None");
  g->GoGame = NULL;
  g->Teach = 0;
  g->Teach2 = 0;
  g->Byo = 300;
  g->ByoS = 25;
  g->wByoStones = -1;
  g->bTime = 300;
  g->bInByo = 0;
  g->bByoStones = -1;
  g->Ladder9 = 0;
  g->Ladder19 = 0;
  g->Tourn = 0;
  g->Ladder_Possible = 0;
#ifdef PAIR
  g->pairwith = 0;;
  g->pairstate = NOTPAIRED;
#endif
  if(size) {
    g->mvinfos = (mvinfo *) malloc(sizeof(mvinfo));
    g->mvinfos[0].kibitz = NULL;
    g->mvinfos[0].last = &(g->mvinfos[0].kibitz);
    g->nmvinfos = 1;
  } else {
    g->mvinfos = NULL;
    g->nmvinfos = 0;
  }
  return 0;
}

PRIVATE void freekib(kib *k)
{
  if (k == NULL)
    return;
  freekib(k->next);
  free(k->mess);
  free(k);
}

PRIVATE void freemvinfos(game *g)
{
  int i;

  for (i=0; i<g->nmvinfos; i++)
    freekib(g->mvinfos[i].kibitz);
  free(g->mvinfos);
  g->mvinfos = NULL;
}

PUBLIC int game_free(game *g)
{
  if(g->gotype >= TYPE_GO) {
    g->gotype = 0;
    if (g->GoGame) freeminkgame(g->GoGame);
    if (g->mvinfos) freemvinfos(g);
    free(g->Title);
    free(g->Event);
  }
  return 0;
}

PUBLIC int game_clear(game *g)
{
  game_free(g);
  game_zero(g,0);
  return 0;
}

PUBLIC int game_remove(game *g)
{
  /* Should remove game from players observation list */
  game_clear(g);
  g->status = GAME_EMPTY; 
  return 0;
}

/* This may eventually save old games for the 'oldmoves' command */
PUBLIC int game_finish(int g)
{
  player_game_ended(g);		/* Alert playerdb that game ended */
  NewOldGame(g);
  return 0;
}

PUBLIC char *ggame_str(int xtime, int size, int byo_time)
{
  static char tstr[200];

    sprintf(tstr, "%d %d %d", xtime, size, byo_time);
	    
  return tstr;
}

PUBLIC void add_kib(game *g, int movecnt, char *s)
{
  int i;
  kib **k;

  if(s == NULL) {Logit("s is null"); return; }
  /*Logit("Adding kibitz >%s< at move %d", s, movecnt); */
  while (movecnt >= g->nmvinfos) {
    i = g->nmvinfos;
    g->nmvinfos *= 2;
    g->mvinfos = (mvinfo *)realloc(g->mvinfos, g->nmvinfos * sizeof(mvinfo));
    for (; i< g->nmvinfos; i++) {
      g->mvinfos[i].kibitz = NULL;
      g->mvinfos[i].last = &(g->mvinfos[i].kibitz);
    }
  }
  k = g->mvinfos[movecnt].last;
  *k = (kib *)malloc(sizeof(kib));
  (*k)->next = NULL;
  (*k)->mess = (char *) strdup(s);
  g->mvinfos[movecnt].last = &((*k)->next);
}

PUBLIC void send_go_board_to(int g, int p)
{
  int side, count;
  int observing = 0;
  int x, wc, bc;
  twodstring statstring;
  char buf[20];
  char bbuf[20], wbuf[20];

  if (parray[p].game == g) {
    side = parray[p].side;
  } else {
    observing = 1;
    side = WHITE;
  }
  /*game_update_time(g);*/
  getcaps(garray[g].GoGame, &wc, &bc);
  if(parray[p].i_verbose) {
    if(garray[g].bByoStones > 0) sprintf(bbuf, " B %d", garray[g].bByoStones);
    if(garray[g].wByoStones > 0) sprintf(wbuf, " B %d", garray[g].wByoStones);
    printboard(garray[g].GoGame, statstring);
    count = movenum(garray[g].GoGame);
    if(count > 0) listmove(garray[g].GoGame, count, buf);
    pprintf(p, "\n%sGame %d (I): %s [%3.3s%s] vs %s [%3.3s%s]\n",
                /*SendCode(p, BEEP),*/
                SendCode(p, OBSERVE),
		g + 1,
		parray[garray[g].white].name,
		parray[garray[g].white].srank,
                parray[garray[g].white].rated ? "*" : " ",
		parray[garray[g].black].name,
		parray[garray[g].black].srank,
                parray[garray[g].black].rated ? "*" : " ");
    for(x=0; x<(garray[g].GoGame->height) + 2; x++){
      if(x == 0) 
        pprintf(p, "%s%s      H-cap %d Komi  %.1f\n", 
		/*SendCode(p, BEEP),*/
		SendCode(p, OBSERVE),
		statstring[x],
		garray[g].GoGame->handicap,
		garray[g].komi);
      else if(x==1)
	pprintf(p, "%s%s  Captured by #: %d\n", SendCode(p, OBSERVE), statstring[x], wc);
      else if(x==2)
	pprintf(p, "%s%s  Captured by O: %d\n", SendCode(p, OBSERVE), statstring[x], bc);
      else if((x==4) && (garray[g].GoGame->height > 3))
	pprintf(p, "%s%s  Wh Time %s%s\n", SendCode(p, OBSERVE), statstring[x], hms(garray[g].wTime, 1, 1, 0), (garray[g].wByoStones > 0) ? wbuf : "");
      else if((x==5) && (garray[g].GoGame->height > 4))
	pprintf(p, "%s%s  Bl Time %s%s\n", SendCode(p, OBSERVE), statstring[x], hms(garray[g].bTime, 1, 1, 0), (garray[g].bByoStones > 0) ? bbuf : "");
      else if((x==7) && (garray[g].GoGame->height > 6))
	if(count == 0)
	  pprintf(p, "%s%s   Last Move:\n", SendCode(p, OBSERVE), statstring[x]);
 	else
	  pprintf(p, "%s%s   Last Move: %s\n", SendCode(p, OBSERVE), statstring[x], buf + 1);
      else if((x==8) && (garray[g].GoGame->height > 7))
 	if(count == 0)
	  pprintf(p, "%s%s    #0 O (White)\n", SendCode(p, OBSERVE), statstring[x]);
   	else
	  pprintf(p, "%s%s   #%d %s (%s)\n", SendCode(p, OBSERVE), statstring[x],
                      count, (buf[0] == 'B') ? "#" : "O", 
		      (buf[0] == 'B') ? "Black" : "White");
      else if((x>=10) && (garray[g].GoGame->height > (x - 1))) {
	if(count > 1) {
	  count--;
          listmove(garray[g].GoGame, count, buf);
          pprintf(p, "%s%s    %c #%3d %s\n", SendCode(p, OBSERVE), statstring[x],
		      buf[0], count, buf + 1);
	}
	else pprintf(p, "%s%s\n", SendCode(p, OBSERVE), statstring[x]);
      }
      else pprintf(p, "%s%s\n", SendCode(p, OBSERVE), statstring[x]);
    }
  }
  pprintf_prompt(p, ""); 
}

PUBLIC void send_go_boards(int g, int players_only)
{
  int p, bc, wc, wp, bp;
  char buf[20], outStr[1024];

  listmove(garray[g].GoGame, movenum(garray[g].GoGame), buf); 
  getcaps(garray[g].GoGame, &wc, &bc);

  bp = garray[g].black;
  wp = garray[g].white;
  sprintf(outStr, "Game %d %s: %s (%d %d %d) vs %s (%d %d %d)\n",
        g + 1,
        "I",
        parray[wp].name,
        bc,
        garray[g].wTime,
        garray[g].wByoStones,
        parray[bp].name,
        wc,
        garray[g].bTime,
        garray[g].bByoStones);

  if((parray[wp].i_verbose) && (garray[g].Teach == 0)) 
    send_go_board_to(g, garray[g].white);

  else if((parray[wp].state != SCORING) && (garray[g].Teach == 0))  { 
    pprintf(wp, "%s%s",SendCode(wp, MOVE),outStr);
    if((movenum(garray[g].GoGame) - 1) >= 0) {
      pprintf(garray[g].white, "%s%3d(%c): %s\n",
        SendCode(garray[g].white, MOVE),
        movenum(garray[g].GoGame) - 1,
        buf[0],
        buf + 1); 
    }
    if(parray[wp].bell) 
       pprintf_prompt(wp, "%s\n", SendCode(wp, BEEP));
    else pprintf_prompt(wp, "\n");
  }

  if(parray[garray[g].black].i_verbose) send_go_board_to(g, garray[g].black);

  else if(parray[garray[g].black].state != SCORING) {
    pprintf(bp, "%s%s", SendCode(bp,MOVE), outStr);
    if((movenum(garray[g].GoGame) - 1) >= 0) {
      pprintf(garray[g].black, "%s%3d(%c): %s\n",
        SendCode(garray[g].black, MOVE),
        movenum(garray[g].GoGame) - 1,
        buf[0],
        buf + 1); 
    }
    if(parray[bp].bell)
      pprintf_prompt(bp, "%s\n", SendCode(bp, BEEP));
    else pprintf_prompt(bp, "\n");
  }

  if(players_only) return;

  for (p = 0; p < p_num; p++) {
    if (parray[p].status == PLAYER_EMPTY) continue;
    if ((player_is_observe(p, g)) && (parray[p].game != g)) {
      if(parray[p].i_verbose) send_go_board_to(g, p);
      else {
        pprintf2(p, MOVE, outStr);
        pprintf(p, "%s%3d(%c): %s\n",
           SendCode(p, MOVE),
           movenum(garray[g].GoGame) - 1,
           buf[0],
           buf + 1);
        if(parray[p].bell)
          pprintf(p, "%s\n", SendCode(p, BEEP));
        pprintf_prompt(p, "");
      }
    }
  }
} 


PUBLIC int game_get_num_ob(int g)
{
  int p, t, count = 0;

  for(p = 0; p < p_num; p++) {
    if(parray[p].num_observe == 0)
      continue;
    if(parray[p].num_observe > 1) {
      for(t = 0; t < parray[p].num_observe; t++) {
        if(parray[p].observe_list[t] == g) count++;
      }
    }
    if(parray[p].observe_list[0] == g) count++;
  }
  return count;
}

PRIVATE int oldGameArray[MAXOLDGAMES];
PRIVATE int numOldGames = 0;

PRIVATE int RemoveOldGame(int g)
{
  int i;

  for (i = 0; i < numOldGames; i++) {
    if (oldGameArray[i] == g)
      break;
  }
  if (i == numOldGames)
    return -1;			/* Not found! */
  for (; i < numOldGames - 1; i++)
    oldGameArray[i] = oldGameArray[i + 1];
  numOldGames--;
  game_remove(&garray[g]);
  return 0;
}

PRIVATE int AddOldGame(int g)
{
  if (numOldGames == MAXOLDGAMES)	/* Remove the oldest */
    RemoveOldGame(oldGameArray[0]);
  oldGameArray[numOldGames] = g;
  numOldGames++;
  return 0;
}

PUBLIC int FindOldGameFor(int p)
{
  int i;

  if (p == -1)
    return numOldGames - 1;
  for (i = numOldGames - 1; i >= 0; i--) {
    if (garray[oldGameArray[i]].old_white == p)
      return oldGameArray[i];
    if (garray[oldGameArray[i]].old_black == p)
      return oldGameArray[i];
  }
  return -1;
}

/* This just removes the game if both players have new-old games */
PUBLIC int RemoveOldGamesForPlayer(int p)
{
  int g;

  g = FindOldGameFor(p);
  if (g < 0)
    return 0;
  if (garray[g].old_white == p)
    garray[g].old_white = -1;
  if (garray[g].old_black == p)
    garray[g].old_black = -1;
  if ((garray[g].old_white == -1) && (garray[g].old_black == -1)) {
    RemoveOldGame(g);
  }
  return 0;
}

/* This recycles any old games for players who disconnect */
PUBLIC int ReallyRemoveOldGamesForPlayer(int p)
{
  int g;

  g = FindOldGameFor(p);
  if (g < 0)
    return 0;
  RemoveOldGame(g);
  return 0;
}

PUBLIC int NewOldGame(int g)
{
  RemoveOldGamesForPlayer(garray[g].white);
  RemoveOldGamesForPlayer(garray[g].black);
  garray[g].old_white = garray[g].white;
  garray[g].old_black = garray[g].black;
  garray[g].status = GAME_STORED;
  AddOldGame(g);
  return 0;
}

PUBLIC void game_disconnect(int g, int p)
{
#ifdef PAIR
  if(paired(parray[p].game)) {
    game_ended(garray[g].pairwith, NEITHER, END_LOSTCONNECTION);
  }
#endif /* PAIR */
  game_ended(g, NEITHER, END_LOSTCONNECTION);
}

PRIVATE void savekib(FILE *fp, game *g)
{
  int i;
  kib *kp;

  fprintf(fp, "kibitz: oink\n");
  for (i=0; i < g->nmvinfos; i++) {
    for (kp = g->mvinfos[i].kibitz; kp; kp = kp->next) {
       fprintf(fp,"%d %s\n", i, kp->mess);
    }
  }
  fputc('\n', fp);
}

PRIVATE void loadkib(FILE *fp, game *g)
{
  char buf[256];
  int i,k;

  while (fgets(buf, 255, fp)) {
    buf[strlen(buf) - 1] = '\0';	/* strip '\n' */
    if (buf[0] == '\0') {
      if(Debug) Logit("Got my blank line in loadkib");
      break;
    }
    if (buf[1] == '\0') {
      Logit("PANIC! Got my blank line in loadkib; buf[0]=%d",buf[0]);
      break;
    }
    sscanf(buf, "%d %n", &i,&k);
    add_kib(g, i, buf+k);
  }
}

PRIVATE int got_attr_value(game *g, char *attr, char *value, FILE * fp, char *fname)
{
  if (!strcmp(attr, "timestart:")) {
    g->timeOfStart = atoi(value);
  } else if (!strcmp(attr, "timetype:")) {
    g->time_type = atoi(value);
  } else if (!strcmp(attr, "rules:")) {
    g->rules = atoi(value);
  } else if (!strcmp(attr, "b_penalty:")) {
    g->B_penalty = atoi(value);
  } else if (!strcmp(attr, "b_over:")) {
    g->num_Bovertime = atoi(value);
  } else if (!strcmp(attr, "w_penalty:")) {
    g->W_penalty = atoi(value);
  } else if (!strcmp(attr, "w_over:")) {
    g->num_Wovertime = atoi(value);
  } else if (!strcmp(attr, "title:")) {
    g->Title = (char *) strdup(value);
  } else if (!strcmp(attr, "event:")) {
    g->Event = (char *) strdup(value);
  } else if (!strcmp(attr, "handicap:")) {
    /*sethcap(g->GoGame, atoi(value))*/;
  } else if (!strcmp(attr, "size:")) {
    g->size = atoi(value);
    g->GoGame = initminkgame(g->size, g->size, g->rules);
  } else if (!strcmp(attr, "onmove:")) {
    ; /* PEM: Ignore. g->onMove = atoi(value); */
  } else if (!strcmp(attr, "ladder9:")) {
    g->Ladder9 = atoi(value);
  } else if (!strcmp(attr, "teach2:")) {
    g->Teach2 = atoi(value);
  } else if (!strcmp(attr, "teach:")) {
    g->Teach = atoi(value);
  } else if (!strcmp(attr, "ladder_possible:")) {
    g->Ladder_Possible = atoi(value);
  } else if (!strcmp(attr, "ladder19:")) {
    g->Ladder19 = atoi(value);
  } else if (!strcmp(attr, "tourn:")) {
    g->Tourn = atoi(value);
    if(g->Tourn == 1) {
      parray[g->white].match_type = TYPE_TNETGO;
      parray[g->black].match_type = TYPE_TNETGO;
    }
  } else if (!strcmp(attr, "w_time:")) {
    g->wTime = atoi(value);
  } else if (!strcmp(attr, "b_time:")) {
    g->bTime = atoi(value);
  } else if (!strcmp(attr, "byo:")) {
    g->Byo = atoi(value);
  } else if (!strcmp(attr, "byos:")) {
    g->ByoS = atoi(value);
  } else if (!strcmp(attr, "w_byo:")) {
    g->wInByo = atoi(value);
  } else if (!strcmp(attr, "b_byo:")) {
    g->bInByo = atoi(value);
  } else if (!strcmp(attr, "w_byostones:")) {
    g->wByoStones = atoi(value);
  } else if (!strcmp(attr, "b_byostones:")) {
    g->bByoStones = atoi(value);
  } else if (!strcmp(attr, "clockstopped:")) {
    g->clockStopped = atoi(value);
  } else if (!strcmp(attr, "rated:")) {
    g->rated = atoi(value);
/*  } else if (!strcmp(attr, "nocaps:")) {
    g->nocaps = atoi(value); */
  } else if (!strcmp(attr, "private:")) {
    g->Private = atoi(value);
  } else if (!strcmp(attr, "type:")) {
    g->type = atoi(value);
  } else if (!strcmp(attr, "time_type:")) {
    g->time_type = atoi(value);
  } else if (!strcmp(attr, "gotype:")) {
    g->gotype = atoi(value);
  } else if (!strcmp(attr, "numpass:")) {
    g->num_pass = atoi(value);
  } else if (!strcmp(attr, "komi:")) {
#if 1
    /*
    ** PEM: Changed to the locally defined nngs_strtof().
    */
    char *pp;

    g->komi = nngs_strtof(value, &pp);
    if (pp == value || g->komi < -12.0 || 12.0 < g->komi)
    {
      if (pp == value)
	Logit("Bad komi value \"%s\"", value);
      else
	Logit("Bad komi value \"%s\" --> %g", value, g->komi);
      g->komi = 5.5;
    }
#else
    g->komi = atof(value);
    if (g->komi < -12.0 || g->komi > 12.0)
      g->komi = 5.5;
#endif
  } else if (!strcmp(attr, "movesrnext:")) {  /* value meaningless */
    /* PEM: Get the true onMove. */
    switch (loadgame(fp, g->GoGame)) {
    case GO_BLACK:
      g->onMove = BLACK;
      break;
    case GO_WHITE:
      g->onMove = WHITE;
      break;
    }
  } else if (!strcmp(attr, "kibitz:")) {  /* value meaningless */
    loadkib(fp, g);
  } else {
    Logit("Error bad attribute >%s< from file %s", attr, fname);
  }
  /* setnocaps(g->GoGame, g->nocaps); */
  return 0;
}


#define MAX_GLINE_SIZE 1024
PUBLIC int game_read(game *g, int wp, int bp)
{
  FILE *fp;
  int len;
  char *attr, *value;
  char fname[MAX_FILENAME_SIZE];
  char line[MAX_GLINE_SIZE];

  g->white = wp;
  g->black = bp;
  g->old_white = -1;
  g->old_black = -1;
  /* g->gameNum = g; */

  sprintf(fname, "%s/%c/%s-%s", game_dir, parray[wp].login[0],
	  parray[wp].login, parray[bp].login);
  fp = fopen(fname, "r");
  if (!fp) {
    Logit("Game not found, %s", fname);
    return -1;
  }
  /* Read the game file here */
  while (!feof(fp)) {
    fgets(line, MAX_GLINE_SIZE, fp);
    if (feof(fp))
      break;
    if ((len = strlen(line)) <= 1) 
      continue;
    line[len - 1] = '\0';
    attr = eatwhite(line);
    if (attr[0] == '#')
      continue;			/* Comment */
    if (attr[0] == ';') {
      Logit("Read move %s from game record %s!", attr, fname);
      continue;			/* Move!  Should not get here! */
    }
    value = eatword(attr);
    if (!*value) {
      Logit("Error reading file %s", fname);
      continue;
    }
    *value = '\0';
    value++;
    value = eatwhite(value);
    if (!*value) {
      Logit("NNGS: Error reading file %s", fname);
      continue;
    }
    stolower(attr);
    if (got_attr_value(g, attr, value, fp, fname))
      return -1;
  }

  fclose(fp);
  g->status = GAME_ACTIVE;
  g->startTime = tenth_secs();
  g->lastMoveTime = g->startTime;
  g->lastDecTime = g->startTime;

  /* Don't change this to 0. It doesn't work. */
#if 1
  /* PEM: This used to be done when saving, but that broke things. */
  if (g->num_pass >= 2) {
    back(g->GoGame);
    g->num_pass = 1;
    if (g->onMove==WHITE) g->onMove = BLACK;
    else g->onMove = WHITE;
  }
  parray[g->white].state = PLAYING_GO;
  parray[g->black].state = PLAYING_GO;
#else
  /* We never enter in SCORING mode, so this should not be used.
  ** (Among other things, com_load() zaps it to PLAYING_GO anyway.)
  */
  if(g->num_pass >= 2) {
    parray[g->white].state = SCORING;
    parray[g->black].state = SCORING;
  }
  else {
    parray[g->white].state = PLAYING_GO;
    parray[g->black].state = PLAYING_GO;
  }
#endif
  /* Need to do notification and pending cleanup */
  return 0;
}

PUBLIC int game_delete(int wp, int bp)
{
  char fname[MAX_FILENAME_SIZE];
  char lname[MAX_FILENAME_SIZE];

  sprintf(fname, "%s/%c/%s-%s", game_dir, parray[wp].login[0],
	  parray[wp].login, parray[bp].login);
  sprintf(lname, "%s/%c/%s-%s", game_dir, parray[bp].login[0],
	  parray[wp].login, parray[bp].login);
  unlink(fname);
  if(wp != bp) unlink(lname);
  return 0;
}

PUBLIC int game_save_complete(int g, char *fname)
{
  time_t now;
  FILE *fp;
  int wp, bp, owp = 0, obp = 0;	/* Init. to shut up warnings. */
  char resu[10];
  char command[MAX_FILENAME_SIZE];
  char *tmp;
  /* [PEM]: Not used: twodstring statstring;*/

  wp = garray[g].white;
  bp = garray[g].black;
#ifdef PAIR
  if(paired(g)) {
    owp = garray[garray[g].pairwith].white;
    obp = garray[garray[g].pairwith].black;
  }
#endif
  now = time(0);

  fp = fopen(fname, "w");
  if (!fp) {
    Logit("Problem opening file %s for write!", fname);
    return -1;
  }
  tmp = strrchr(fname, '/');
  if(garray[g].gresult == 0.0) sprintf(resu, "Resign");
  else if(garray[g].gresult == -1.0) sprintf(resu, "Time");
  else sprintf(resu, "%.1f", garray[g].gresult);
  fprintf(fp, "\n(;\n");
  fprintf(fp, "GM[1]US[Brought to you by No Name Go Server]\nCoPyright[\n");
  fprintf(fp, "  This game was played on the No Name Go Server\n\
  Permission to reproduce this game is given.]\n");
  if((garray[g].Ladder9 == 1) || (garray[g].Ladder19 == 1)) {
    fprintf(fp, "GN[%s-%s(B) NNGS (LADDER RATED)]\n",
     parray[wp].name, parray[bp].name);
#ifdef PAIR
  } else if (paired(g)) {
    fprintf(fp, "GN[%s-%s vs %s-%s(B) NNGS (RENGO)]\n",
     parray[wp].name, parray[owp].name,
     parray[bp].name, parray[obp].name);
#endif
  } else if (garray[g].Tourn == 1) {
    fprintf(fp, "GN[%s-%s(B) NNGS (Tournament)]\n",
     parray[wp].name, parray[bp].name);
  } else {
    fprintf(fp, "GN[%s-%s(B) NNGS]\n",
     parray[wp].name, parray[bp].name);
  }
  fprintf(fp, "EV[%s]\n", garray[g].Event);
  fprintf(fp, "RE[%s+%s]\n",
     (garray[g].winner == garray[g].white ? "W" : "B"), resu);
  fprintf(fp, "PW[%s]WR[%s%s]\n", 
               parray[wp].name, 
               parray[wp].registered ? parray[wp].srank : "UR",
               parray[wp].rated ? "*" : " ");
  fprintf(fp, "PB[%s]BR[%s%s]\n", 
               parray[bp].name, 
               parray[bp].registered ? parray[bp].srank : "UR",
               parray[bp].rated ? "*" : " ");
  fprintf(fp, "PC[%s: %s]\n", server_name, server_address);
  fprintf(fp, "DT[%s]\n", strDTtime(&now));
  fprintf(fp, "SZ[%d]TM[%d]KM[%.1f]\n\n", garray[g].GoGame->width,
     (garray[g].Byo), garray[g].komi);
  if(Debug) Logit("garray[g].nmvinfos = %d", garray[g].nmvinfos);
  savegame(fp, garray[g].GoGame, garray[g].mvinfos, garray[g].nmvinfos);
  fprintf(fp, ";)\n\n---\n");
  fclose(fp);
  if(!garray[g].Teach)
    if(parray[garray[g].white].automail) {
      sprintf(command, "%s -s \"%s\" %s < %s&", MAILPROGRAM,
          tmp + 1, parray[wp].emailAddress, fname);
     system(command);
    }
  if(parray[garray[g].black].automail) {
    sprintf(command, "%s -s \"%s\" %s < %s&", MAILPROGRAM,
        tmp + 1, parray[bp].emailAddress, fname);
    system(command);
  }

  return 1;
}

PUBLIC int game_save(int g)
{
  FILE *fp;
  int wp, bp;
  
  char fname[MAX_FILENAME_SIZE];
  char lname[MAX_FILENAME_SIZE];

  wp = garray[g].white;
  bp = garray[g].black;
  
  if(movenum(garray[g].GoGame) < 3) return 1;
  sprintf(fname, "%s/%c/%s-%s", game_dir, parray[wp].login[0],
	  parray[wp].login, parray[bp].login);
  sprintf(lname, "%s/%c/%s-%s", game_dir, parray[bp].login[0],
	  parray[wp].login, parray[bp].login);
  fp = fopen(fname, "w");
  if (!fp) {
    Logit("Problem opening file %s for write!", fname);
    return -1;
  }
#if 0
  /* PEM: This broke things when a peridic save was done.
  **      Moved up to game_read().
  */
  if (garray[g].num_pass >= 2) {
    back(garray[g].GoGame);
    garray[g].num_pass = 1;
    if (garray[g].onMove==WHITE) garray[g].onMove = BLACK;
    else garray[g].onMove = WHITE;
  }
  if (garray[g].num_pass == 1) {
    back(garray[g].GoGame);
    garray[g].num_pass = 0;
    if (garray[g].onMove==WHITE) garray[g].onMove = BLACK;
    else garray[g].onMove = WHITE;
  }
#endif
  fprintf(fp, "Rules: %d\n", garray[g].rules);
  fprintf(fp, "B_Penalty: %d\n", garray[g].B_penalty);
  fprintf(fp, "B_Over: %d\n", garray[g].num_Bovertime);
  fprintf(fp, "W_Penalty: %d\n", garray[g].W_penalty);
  fprintf(fp, "W_Over: %d\n", garray[g].num_Wovertime);
  fprintf(fp, "Size: %d\n", (int) garray[g].GoGame->width);
  fprintf(fp, "TimeStart: %d\n", (int) garray[g].timeOfStart);
  fprintf(fp, "W_Time: %d\n", garray[g].wTime);
  fprintf(fp, "B_Time: %d\n", garray[g].bTime);
  fprintf(fp, "Byo: %d\n", garray[g].Byo);
  fprintf(fp, "ByoS: %d\n", garray[g].ByoS);
  fprintf(fp, "W_Byo: %d\n", garray[g].wInByo);
  fprintf(fp, "B_Byo: %d\n", garray[g].bInByo);
  fprintf(fp, "W_ByoStones: %d\n", garray[g].wByoStones);
  fprintf(fp, "B_ByoStones: %d\n", garray[g].bByoStones);
  fprintf(fp, "ClockStopped: %d\n", garray[g].clockStopped);
  fprintf(fp, "Rated: %d\n", garray[g].rated);
  fprintf(fp, "Private: %d\n", garray[g].Private);
  fprintf(fp, "Type: %d\n", garray[g].type);
  fprintf(fp, "TimeType: %d\n", garray[g].time_type);
  fprintf(fp, "GoType: %d\n", garray[g].gotype);
#if 0
  /* PEM: Huh? We have changed game_read(), must not do this. */
  garray[g].num_pass = 0;
#endif
  fprintf(fp, "NumPass: %d\n", garray[g].num_pass);
  fprintf(fp, "Komi: %.1f\n", garray[g].komi);
  fprintf(fp, "Teach: %d\n", garray[g].Teach);
  fprintf(fp, "Teach2: %d\n", garray[g].Teach2);
  fprintf(fp, "Ladder9: %d\n", garray[g].Ladder9);
  fprintf(fp, "Ladder19: %d\n", garray[g].Ladder19);
  fprintf(fp, "Tourn: %d\n", garray[g].Tourn);
  fprintf(fp, "Title: %s\n", garray[g].Title);
  fprintf(fp, "Event: %s\n", garray[g].Event);
 /* if(garray[g].GoGame->handicap > 0)
    fprintf(fp, "Handicap: %d\n", garray[g].GoGame->handicap); */
  fprintf(fp, "Ladder_Possible: %d\n", garray[g].Ladder_Possible);
  fprintf(fp, "OnMove: %d\n", garray[g].onMove);
  fprintf(fp, "MovesRNext: oinkoink\n");
  savegame(fp, garray[g].GoGame,NULL,0);
  savekib(fp, &garray[g]);
  fclose(fp);
  /* Create link for easier stored game finding */
  if (parray[bp].login[0] != parray[wp].login[0])
    link(fname, lname);
  return 0;
}

PUBLIC int write_g_out(int g, char *fname, int maxlines, int isDraw, char *fdate)
{
  FILE *fp;
  int wp, bp;
  char wrnk[6], brnk[6];
  char resu[10];

  wp = garray[g].white;
  bp = garray[g].black;

  if((!parray[wp].registered) || (!parray[bp].registered)) return 0;

  sprintf(wrnk, "%3.3s%s", parray[wp].srank, parray[wp].rated ? "*" : " ");
  sprintf(brnk, "%3.3s%s", parray[bp].srank, parray[bp].rated ? "*" : " ");

  fp = fopen(fname, "a");
  if (!fp)
    return 0;
  if(garray[g].gresult == 0.0) sprintf(resu, "Resign");
  else if(garray[g].gresult == -1.0) sprintf(resu, "Time");
  else sprintf(resu, "%.1f", garray[g].gresult);
  fprintf(fp, "%-10s [%s](%s) : %-10s [%s](%s) H %d K %.1f %dx%d %s+%s %s\n",
    (garray[g].winner == garray[g].white ? parray[wp].name : parray[bp].name),
    (garray[g].winner == garray[g].white ? wrnk : brnk),
    (garray[g].winner == garray[g].white ? "W" : "B"),
    (garray[g].winner == garray[g].white ? parray[bp].name : parray[wp].name),
    (garray[g].winner == garray[g].white ? brnk : wrnk),
    (garray[g].winner == garray[g].white ? "B" : "W"),
     garray[g].GoGame->handicap,
     garray[g].komi,
     garray[g].GoGame->width, garray[g].GoGame->width,
    (garray[g].winner == garray[g].white ? "W" : "B"),
     resu, fdate);
  fclose(fp);
  truncate_file(fname, maxlines);
  return 1;
}

PUBLIC int pgames(int p, char *fname)
{
  FILE *fp;
  char line[1000];
  
  if(parray[p].client)
  pprintf(p, "%sFile\n", SendCode(p, THIST));
  fp = fopen(fname, "r");
  if (!fp) {
  if(parray[p].client)
    pprintf(p, "%sFile\n", SendCode(p, THIST));
    return COM_OK;
  }
  while (!feof(fp)) {
    fgets(line, 999, fp);
    if(!feof(fp)) pprintf(p, "%s",line);
  }
  fclose(fp);
  if(parray[p].client)
  pprintf(p, "%sFile\n", SendCode(p, THIST));
  return COM_OK;
}

PUBLIC void game_write_complete(int g, int isDraw)
{
  char fname[MAX_FILENAME_SIZE];
  char fdate[40];
  int wp, bp;
  int now = time(0);
  char wname[11], bname[11];
  FILE *fp;

  wp = garray[g].white;
  bp = garray[g].black;
  strcpy(wname, parray[wp].name);
  strcpy(bname, parray[bp].name);
  
  stolower(wname);
  stolower(bname);
  sprintf(fdate, strgmtime((time_t *) &now));
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, wname[0], wname, STATS_GAMES);
  write_g_out(g, fname, 23, isDraw, fdate);
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, bname[0], bname, STATS_GAMES);
  if(wp != bp) write_g_out(g, fname, 23, isDraw, fdate);
  if((garray[g].rated) &&
     (parray[wp].registered != 0) &&
     (parray[bp].registered != 0) &&
     (garray[g].Teach != 1)       &&
#ifdef PAIR
     (!paired(g))                  &&
#endif
     (movenum(garray[g].GoGame) >= 20) &&
     (garray[g].GoGame->width == 19)) {
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, wname[0], wname, STATS_RGAMES);
    write_g_out(g, fname, 23, isDraw, fdate);
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, bname[0], bname, STATS_RGAMES);
    write_g_out(g, fname, 23, isDraw, fdate);
  }
  sprintf(fname, "%s/%s", stats_dir, STATS_GAMES);
  write_g_out(g, fname, 250, isDraw, fdate);
  sprintf(fname, "%s/%s/%s-%s-%s", 
                  stats_dir, STATS_CGAMES, 
                  (garray[g].winner == garray[g].white ? wname : bname),
                  (garray[g].winner == garray[g].white ? bname : wname),
                  fdate);

  game_save_complete(g, fname);

  if((parray[wp].registered == 0) || 
     (parray[bp].registered == 0) ||
     (garray[g].rated == 0)       ||
     (garray[g].Teach == 1)       ||
#ifdef PAIR
     (paired(g))                  ||
#endif
     (movenum(garray[g].GoGame) <= 20) ||
     (garray[g].GoGame->width != 19))  return;

  fp = fopen(results_file, "a");
  if(!fp) {
    Logit("Cannot open %s!", results_file);
    return;
  }
  fprintf(fp, "%s -- %s -- %d %.1f %s %s\n", 
               parray[garray[g].white].name,
               parray[garray[g].black].name,
               garray[g].GoGame->handicap,
               garray[g].komi,
               (garray[g].winner == garray[g].white ? "W" : "B"),
               ResultsDate(fdate));
  fclose(fp);
#ifdef NNGSRATED
  /* [PEM]: New results file for nrating. */
  {
    rdbm_t rdb;
    rdbm_player_t rp;
    char wrank[8], brank[8];
    struct tm *tp = localtime((time_t *)&now);

    if ((rdb = rdbm_open(NRATINGS_FILE, 0)) == NULL)
    {
      strcpy(wrank, "-");
      strcpy(brank, "-");
    }
    else
    {
      if (rdbm_fetch(rdb, parray[garray[g].white].name, &rp))
	strcpy(wrank, rp.rank);
      else
	strcpy(wrank, "-");
      if (rdbm_fetch(rdb, parray[garray[g].black].name, &rp))
	strcpy(brank, rp.rank);
      else
	strcpy(brank, "-");
      rdbm_close(rdb);
    }

    fp = fopen(NRESULTS_FILE, "a");
    if (!fp)
    {
      Logit("Cannot open %s!", NRESULTS_FILE);
      return;
    }
    fprintf(fp, "\"%s\" %s \"%s\" %s %u %.1f %c %02u-%02u-%02u\n",
	    parray[garray[g].white].name,
	    wrank,
	    parray[garray[g].black].name,
	    brank,
	    garray[g].GoGame->handicap,
	    garray[g].komi,
	    (garray[g].winner == garray[g]. white ? 'W' : 'B'),
	    tp->tm_year + 1900, tp->tm_mon + 1, tp->tm_mday);
    fclose(fp);
  }
#endif /* NNGSRATED */
}

PUBLIC int game_count()
{
  int g, count = 0;

  for (g = 0; g < g_num; g++)
    if (garray[g].status == GAME_ACTIVE) {
      count++;
      if (count > game_high)
	game_high++;
    }
  return count;
}
