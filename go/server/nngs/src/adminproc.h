/* adminproc.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997  Erik Van Riper (geek@midway.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

/* Revision history:
   name		email		yy/mm/dd	Change
   Richard Nash	nash@visus.com	93/10/22	Created
*/

#ifndef _ADMINPROC_H
#define _ADMINPROC_H

extern int com_addplayer(int, param_list);
extern int com_arank(int, param_list);
extern int com_nuke(int, param_list);
extern int com_shutdown(int, param_list);
extern void ShutHeartBeat(void);
extern void ShutDown(void);
extern int com_pose(int, param_list);
extern int com_announce(int, param_list);
extern int com_muzzle(int, param_list);
extern int com_gmuzzle(int, param_list);
extern int com_bmuzzle(int, param_list);
extern int com_asetrealname(int, param_list);
extern int com_asetsilent(int, param_list);
extern int com_asetpasswd(int, param_list);
extern int com_asetemail(int, param_list);
extern int com_asethandle(int, param_list);
extern int com_asetadmin(int, param_list);
extern int com_asetwater(int, param_list);
extern int server_shutdown(int, char *);
extern int com_checkIP(int, param_list);
extern int com_checkPLAYER(int, param_list);
extern int com_checkSOCKET(int, param_list);
extern int com_remplayer(int, param_list);
extern int com_raisedead(int, param_list);
extern int strcmpwild(char *, char *);
extern int com_anews(int, param_list);
extern int com_actitle(int, param_list);
extern int com_reload_ladders(int, param_list);
extern int com_createnews(int, param_list);
extern int com_createadmnews(int, param_list);
extern int com_asetdebug(int, param_list);
extern int com_hide(int, param_list);
extern int com_high(int, param_list);
extern int com_unhide(int, param_list);
extern int com_ausers(int, param_list);
extern int com_adrop(int, param_list);
extern int com_noshout(int, param_list);
extern int Show_Admin_Command(int p, char *comm, char *command);
#endif /* _ADMINPROC_H */
