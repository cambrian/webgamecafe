/* variable.c
 * This is kludgy
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1998 Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"

#include "command.h"
#include "common.h"
#include "variable.h"
#include "playerdb.h"
#include "utils.h"
#include "nngsmain.h"
#include "nngsconfig.h"
#include "channel.h"

/* grimm */
int SetValidFormula(int p, int which, char *val);
/* added for warning */
PRIVATE int set_boolean_var(int *var, char *val)
{
  int v = -1;

  if (val == (char *) NULL)
    return (*var = !*var);

  if (sscanf(val, "%d", &v) != 1) {
    stolower(val);
    if (!strcmp(val, "off"))
      v = 0;
    if (!strcmp(val, "false"))
      v = 0;
    if (!strcmp(val, "on"))
      v = 1;
    if (!strcmp(val, "true"))
      v = 1;
  }
  if ((v == 0) || (v == 1))
    return (*var = v);
  else return (-1);
}

PRIVATE int set_open(int p, char *var, char *val)
{
  int v = set_boolean_var (&parray[p].open, val);

  if (v < 0) return VAR_BADVAL;
  if (v > 0)
    pprintf(p, "%sSet open to be True.", SendCode(p, INFO));
  else {
      player_decline_offers(p, -1, PEND_MATCH);
      player_withdraw_offers(p, -1, PEND_MATCH);
      pprintf(p, "%sSet open to be False.", SendCode(p, INFO));
      parray[p].looking = 0;
  }
  return VAR_OK;
}

PRIVATE int set_ropen(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].ropen, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet ropen to be %s.", SendCode(p, INFO), (parray[p].ropen) ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_shout(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].i_shout, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet shout to be %s.", SendCode(p, INFO), (parray[p].i_shout) ? "True" : "False");
  if(parray[p].i_shout) Ochannel_add(CSHOUT, p);
  else Ochannel_remove(CSHOUT, p);
  return VAR_OK;
}

PRIVATE int set_client(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].client, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet client to be %s.", SendCode(p, INFO), (parray[p].client)  ? "True" : "False");
  if(parray[p].client) parray[p].i_verbose = 0;
  return VAR_OK;
}

PRIVATE int set_lshout(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].i_lshout, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet lshout to be %s.", SendCode(p, INFO), (parray[p].i_lshout)  ? "True" : "False");
  if(parray[p].i_lshout) Ochannel_add(CLSHOUT, p);
  else Ochannel_remove(CLSHOUT, p);

  return VAR_OK;
}

PRIVATE int set_gshout(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].i_gshout, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet gshout to be %s.", SendCode(p, INFO), (parray[p].i_gshout)  ? "True" : "False");
  if(parray[p].i_gshout) Ochannel_add(CGSHOUT, p);
  else Ochannel_remove(CGSHOUT, p);
  return VAR_OK;
}

PRIVATE int set_looking(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].looking, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet looking to be %s.", SendCode(p, INFO), (parray[p].looking)  ? "True" : "False");
  if(parray[p].looking) parray[p].open = 1;
  return VAR_OK;
}

PRIVATE int set_kibitz(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].i_kibitz, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet kibitz to be %s.", SendCode(p, INFO), (parray[p].i_kibitz)  ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_tell(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].i_tell, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet tell to be %s.", SendCode(p, INFO), (parray[p].i_tell)  ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_robot(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].i_robot, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet robot to be %s.", SendCode(p, INFO), (parray[p].i_robot)  ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_notifiedby(int p, char *var, char *val)
{
  if (set_boolean_var(&parray[p].notifiedby, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet notified to be %s.", SendCode(p, INFO), (parray[p].notifiedby) ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_verbose(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].i_verbose, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet verbose to be %s.", SendCode(p,INFO), (parray[p].i_verbose) ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_pinform(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].i_login, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet pinform to be %s.", SendCode(p, INFO), (parray[p].i_login) ? "True" : "False");
  if(parray[p].i_login) Ochannel_add(CLOGON, p);
  else Ochannel_remove(CLOGON, p);
  return VAR_OK;
}

PRIVATE int set_quiet(int p, char *var, char *val)
{
  int  temp;

  if (set_boolean_var (&temp, val) < 0) return VAR_BADVAL;
  parray[p].i_login = !temp;
  parray[p].i_game = parray[p].i_login;

  pprintf(p, "%sSet quiet to be %s.",
          SendCode(p, INFO), (parray[p].i_login) ? "False" : "True");
  if(parray[p].i_login) {
    Ochannel_add(CLOGON, p);
    Ochannel_add(CGAME, p);
  }
  else {  
    Ochannel_remove(CLOGON, p);
    Ochannel_remove(CGAME, p);
  }

  return VAR_OK;

}

PRIVATE int set_ginform(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].i_game, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet ginform to be %s", SendCode(p, INFO), (parray[p].i_game) ? "True" : "False");
  if(parray[p].i_game) Ochannel_add(CGAME, p);
  else Ochannel_remove(CGAME, p);

  return VAR_OK;
}

PRIVATE int set_private(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].Private, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet private to be %s.", SendCode(p, INFO), (parray[p].Private) ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_automail(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].automail, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet automail to be %s.", 
              SendCode(p, INFO), parray[p].automail ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_extprompt(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].extprompt, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet extprompt to be %s.", 
              SendCode(p, INFO), parray[p].extprompt ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_bell(int p, char *var, char *val)
{
  if (set_boolean_var (&parray[p].bell, val) < 0) return VAR_BADVAL;
  pprintf(p, "%sSet bell to be %s.", 
              SendCode(p, INFO), parray[p].bell ? "True" : "False");
  return VAR_OK;
}

PRIVATE int set_rank(int p, char *var, char *val)
{
  if(val == (char *) NULL) {
    do_copy(parray[p].rank, "-", MAX_RANK);
    pprintf(p, "%sUnset your Rank Info", SendCode(p, INFO));
    return VAR_OK;
  }
  if (strlen(val) > MAX_RANK) {
    pprintf(p, "%sThat string is too long.  %d characters max.\n", SendCode(p, ERROR), MAX_RANK);
    return VAR_BADVAL;
  }
  do_copy(parray[p].rank, val, MAX_RANK);

  pprintf(p, "%sRank set to %s.\n", SendCode(p, INFO), parray[p].rank);
  return VAR_OK;
}

PRIVATE int set_realname(int p, char *var, char *val)
{
  if(val == (char *) NULL) {
    do_copy(parray[p].fullName, "Not Provided", MAX_FULLNAME);
    pprintf(p, "%sUnset your Real Name Info", SendCode(p, INFO));
    return VAR_OK;
  }
  if (strlen(val)>MAX_FULLNAME) {
    pprintf(p, "%sThat string is too long.  %d characters max.\n", SendCode(p, ERROR), MAX_FULLNAME);
    return VAR_BADVAL;
  }
  do_copy(parray[p].fullName, val, MAX_FULLNAME);

  pprintf(p, "%sFull Name set to %s.\n", SendCode(p, INFO), parray[p].fullName);
  return VAR_OK;
}

PRIVATE int set_time(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 0) || (v > 300)) return VAR_BADVAL;
  parray[p].def_time = v;
  pprintf(p, "%sDefault time set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_size(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 9) || (v > 19)) return VAR_BADVAL;
  parray[p].def_size = v;
  pprintf(p, "%sDefault size set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_channel(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 0) || (v > MAX_CHANNELS)) return VAR_BADVAL;
  parray[p].last_channel = v;
  pprintf(p, "%sDefault channel set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_byo_time(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 0) || (v > 300)) return VAR_BADVAL;
  parray[p].def_byo_time = v;
  pprintf(p, "%sDefault byo yomi time set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_byo_stones(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 0) || (v > 100)) return VAR_BADVAL;
  parray[p].def_byo_stones = v;
  pprintf(p, "%sDefault byo yomi stones set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_height(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 5) || (v > 240)) return VAR_BADVAL;
  parray[p].d_height = v;
  pprintf(p, "%sHeight set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_width(int p, char *var, char *val)
{
  int v = -1;

  if (!val) return VAR_BADVAL;
  if (sscanf(val, "%d", &v) != 1) return VAR_BADVAL;
  if ((v < 32) || (v > 240)) return VAR_BADVAL;
  parray[p].d_width = v;
  pprintf(p, "%sWidth set to %d.\n", SendCode(p, INFO), v);
  return VAR_OK;
}

PRIVATE int set_prompt(int p, char *var, char *val)
{
  if (!val) {
    do_copy(parray[p].prompt, "#> ", MAX_PROMPT);
    return VAR_OK;
  }
  do_copy(parray[p].prompt, val, MAX_PROMPT);
  return VAR_OK;
}

PRIVATE int set_busy(int p, char *var, char *val)
{
  if (!val) {
    parray[p].busy[0] = '\0';
    pprintf( p, "%sYour busy string was cleared.\n", SendCode(p, INFO));
    return VAR_OK;
  }
  if (strlen(val)>50) {
    pprintf(p, "%sThat string is too long.\n", SendCode(p, ERROR));
    return VAR_BADVAL;
  }
  sprintf(parray[p].busy,"%s",val);
    pprintf( p, "%sYour busy string is set to \"%s\"\n", 
                 SendCode(p, INFO), parray[p].busy);
  return VAR_OK;
}

#if 1
PRIVATE int
set_plan(int p, char *var, char *val)
{
  size_t count = plan_count(parray[p].plan_lines);
  int where = atoi(var);	/* Must be an integer, no test needed. */

  /* The commands uses origin 1, with 0 meaning insert-in-front. We use
     0-origin, with -1 meaning insert-in-front. */
  where -= 1;

  if (where >= MAX_PLAN)
    return VAR_BADVAL;
  if (where < 0)
  {				/* Insert or remove first. */
    if (val == NULL)
    {
      if (count > 0)
	plan_rem(0, parray[p].plan_lines); /* Remove first. */
    }
    else
    {
      plan_insert(val, 0, parray[p].plan_lines); /* Insert first. */
      if (count+1 > MAX_PLAN)
	plan_rem(MAX_PLAN, parray[p].plan_lines); /* Remove overflow. */
    }
  }
  else if (where >= count)
  {				/* Add or remove last. */
    if (val != NULL)
      plan_add(val, parray[p].plan_lines); /* Add last. */
    else if (count > 0 && where == count-1)
      plan_rem(where, parray[p].plan_lines); /* Remove last. */
  }
  else				/* 0 <= where < count */
  {				/* Replace or clear line. */
    if (val != NULL)
      plan_set(val, where, parray[p].plan_lines); /* Replace line. */
    else if (count > 0)
    {
      if (where == count-1)
	plan_rem(where, parray[p].plan_lines); /* Clear line. */
      else
	plan_set("", where, parray[p].plan_lines); /* Clear line. */
    }
  }

  return VAR_OK;
}
#else
/* this is really ugly. (Indeed, so it was replaced with new code /pem) */
PRIVATE int set_plan(int p, char *var, char *val)
{
  int which, i;

  which = atoi(var);		/* Must be an integer, no test needed */

  if (which > MAX_PLAN) return VAR_BADVAL;

  if (which > parray[p].num_plan) which = parray[p].num_plan + 1;

  if (which == 0) {		/* shove from top */
    if (parray[p].num_plan >= MAX_PLAN) {	/* free the bottom string's
						   memory */
      if (parray[p].planLines[parray[p].num_plan - 1] != NULL)
      {
	free(parray[p].planLines[parray[p].num_plan - 1]);
	parray[p].planLines[parray[p].num_plan - 1] = NULL;
      }
    }
    if (parray[p].num_plan) {
      for (i = (parray[p].num_plan >= MAX_PLAN) ? MAX_PLAN - 1 : parray[p].num_plan; i > 0; i--)
	parray[p].planLines[i] = parray[p].planLines[i - 1];
    }
    if (parray[p].num_plan < MAX_PLAN)
      parray[p].num_plan++;
    parray[p].planLines[0] = ((val == NULL) ? NULL : strdup(val));
    return VAR_OK;
  }
  if (which > parray[p].num_plan) {	/* new line at bottom */
    if (parray[p].num_plan >= MAX_PLAN) {	/* shove the old lines up */
      if (parray[p].planLines[0] != (char *) NULL)
      {
	free(parray[p].planLines[0]);
	parray[p].planLines[0] = NULL;
      }
      for (i = 0; i < parray[p].num_plan; i++)
	parray[p].planLines[i] = parray[p].planLines[i + 1];
    } else {
      parray[p].num_plan++;
    }
    parray[p].planLines[which - 1] = ((val == NULL) ? NULL : strdup(val));
    return VAR_OK;
  }
  which--;
  if (parray[p].planLines[which] != NULL) {
    free(parray[p].planLines[which]);
    parray[p].planLines[which] = NULL;
  }
  if (val != NULL) {
    parray[p].planLines[which] = strdup(val);
  } else {
    parray[p].planLines[which] = NULL;
    if (which == parray[p].num_plan - 1) {	/* clear nulls from bottom */
      while ((parray[p].num_plan > 0) &&
	     (parray[p].planLines[parray[p].num_plan - 1] == NULL)) {
	parray[p].num_plan--;
      }
    } else if (which == 0) {	/* clear nulls from top */
      while ((which < parray[p].num_plan) &&
	     (parray[p].planLines[which] == NULL)) {
	which++;
      }
      if (which != parray[p].num_plan) {
	for (i = which; i < parray[p].num_plan; i++)
	  parray[p].planLines[i - which] = parray[p].planLines[i];
      }
      parray[p].num_plan -= which;
    }
  }
  return VAR_OK;
}
#endif /* off */

PUBLIC var_list variables[] = {
  {"0", set_plan},
  {"1", set_plan},
  {"2", set_plan},
  {"3", set_plan},
  {"4", set_plan},
  {"5", set_plan},
  {"6", set_plan},
  {"7", set_plan},
  {"8", set_plan},
  {"9", set_plan},
  {"automail", set_automail},
  {"bell", set_bell},
  {"beep", set_bell},
  {"busy", set_busy},
  {"byo_time", set_byo_time},
  {"byo_stones", set_byo_stones},
  {"channel", set_channel},
  {"client", set_client},
  {"def_time", set_time},
  {"def_size", set_size},
  {"def_byo_time", set_byo_time},
  {"def_byo_stones", set_byo_stones},
  {"extprompt", set_extprompt},
  {"ginform", set_ginform},
  {"game", set_ginform},
  {"gshout", set_gshout},
  {"lshout", set_lshout},
  {"height", set_height},
  {"i_game", set_ginform},
  {"i_login", set_pinform},
  {"kibitz", set_kibitz},
  {"looking", set_looking},
  {"notifiedby", set_notifiedby},
  {"open", set_open},
  {"pinform", set_pinform},
  {"player", set_pinform},
  {"private", set_private},
  {"prompt", set_prompt},
  {"quiet", set_quiet},
  {"rank", set_rank},
  {"realname", set_realname},
  {"ropen", set_ropen},
  {"robot", set_robot},
  {"shout", set_shout},
  {"size", set_size},
  {"stones", set_byo_stones},
  {"tell", set_tell},
  {"time", set_time},
  {"verbose", set_verbose},
  {"width", set_width},
  {NULL, NULL}
};

PRIVATE int set_find(char *var)
{
  int i = 0;
  int gotIt = -1;
  int len = strlen(var);

  while (variables[i].name) {
    if (!strncmp(variables[i].name, var, len)) {
      if (gotIt >= 0) {
	return -VAR_AMBIGUOUS;
      }
      gotIt = i;
    }
    i++;
  }
  if (gotIt >= 0) {
    return gotIt;
  }
  return -VAR_NOSUCH;
}

PUBLIC int var_set(int p, char *var, char *val, int *wh)
{
  int which;

  if (!var)
    return VAR_NOSUCH;
  if ((which = set_find(var)) < 0) {
    return -which;
  }
  *wh = which;
  return variables[which].var_func(p, (isdigit(*variables[which].name) ? var : variables[which].name), val);
}

