/* utils.c
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"

#include "nngsmain.h"
#include "common.h"
#include "bm.h"
#include "utils.h"
#include "playerdb.h"
#include "network.h"
#include "nngsconfig.h"
#ifdef SGI
#include <sex.h>
#endif

SearchData SD;

PUBLIC int iswhitespace(int c)
{
#if 0
  if ((c < ' ') || (c == '\b') || (c == '\n') ||
      (c == '\t') || (c == ' ')) {	/* white */
    return 1;
  } else {
    return 0;
  }
#else
  /* PEM's whitespace. Note that c may be < 0 for 8-bit chars. */
  /* Another note:  Some code depend on c == '\0' being whitespace. /pem */
  return (((0 <= c) && (c <= ' ')) || (c == 127));
#endif
}

PUBLIC char *KillTrailWhiteSpace(char *str)
{
  int len, stop;
  stop = 0;
  len = strlen(str);

  while (!stop) {
    if(iswhitespace(str[len - 1])) {
      str[len - 1] = '\0';
      len--;
    }
    else stop = 1;
  }
  return str;
}

PUBLIC char *getword(char *str)
{
  int i;
  static char word[MAX_WORD_SIZE];

  i = 0;
  while (*str && !iswhitespace(*str)) {
    word[i] = *str;
    str++;
    i++;
    if (i == MAX_WORD_SIZE) {
      i = i - 1;
      break;
    }
  }
  word[i] = '\0';
  return word;
}

PUBLIC char *SendCode(int p, int Code)
{
  static char word[MAX_WORD_SIZE];

  if(!parray[p].client) {
    switch(Code) {
      case SHOUT:
      strcpy(word, "\n");
      break;
    
      default:
      strcpy(word, "");
      break;
    }
    return word;
  }

  switch(Code) {
    case MOVE:
    case INFO:
    sprintf(word, "%d ", Code);
    break;

    case SHOUT:
    sprintf(word, "\n%d ", Code);
    break;

    default:
    sprintf(word, "%d ", Code);
    break;
  }
  return word;
}

PUBLIC char *eatword(char *str)
{
  while (*str && !iswhitespace(*str))
    str++;
  return str;
}

PUBLIC char *eatwhite(char *str)
{
  while (*str && iswhitespace(*str))
    str++;
  return str;
}

PUBLIC char *nextword(char *str)
{
  return eatwhite(eatword(str));
}

PUBLIC int mail_string_to_address(char *addr, char *subj, char *str)
{
  char com[1000];
  FILE *fp;

  if (!safestring(addr))
    return -1;
  sprintf(com, "%s -s \"%s\" %s", MAILPROGRAM, subj, addr);
  Logit("Mail command: %s",com);
  fp = (FILE *) popen(com, "w");
  if (!fp)
    return -2;
  fprintf(fp, "From: nngs@nngs.cosmic.org (The No Name Go Server)\n");
  fprintf(fp, "%s", str);
  pclose(fp);
  return 0;
}

PUBLIC int mail_string_to_user(int p, char *str)
{

  if (parray[p].emailAddress &&
      parray[p].emailAddress[0]) {
    return mail_string_to_address(parray[p].emailAddress, "NNGS game report", str);
  } else {
    return -1;
  }
}

/* loon: _finally_ got rid of that stupid v1,v2,v3,v4,v5,v6,v7,v8,v9,etc. */

/* Process a command for a user */
PUBLIC int pcommand(int p, char *comstr,...)
{
  char tmp[MAX_LINE_SIZE];
  int retval;
  int csocket = parray[p].socket;
  va_list ap;
  va_start(ap, comstr);

  vsprintf(tmp, comstr, ap);
  retval = process_input(csocket, tmp);
  if (retval == COM_LOGOUT) {
    process_disconnection(csocket);
    net_close(csocket);
  }
  va_end(ap);
  return retval;
}

PUBLIC int Logit(char *format,...)
{
  FILE *fp;
  char fname[MAX_FILENAME_SIZE];
  char tmp[10 * MAX_LINE_SIZE];	/* Make sure you can handle 10 lines worth of
				   stuff */
  int retval;
  va_list ap;
  time_t time_in;

#ifdef DOKILL
  return 0;
#endif

  time_in = time(NULL);
  va_start(ap, format);

  retval = vsprintf(tmp, format, ap);
  if (strlen(tmp) > 10 * MAX_LINE_SIZE) {
    fprintf(stderr, "pprintf buffer overflow\n");
  }
  switch(port) {
    case 9696:
    sprintf(fname, "%s/%s", stats_dir, LOGFILE);
    break;
    default:
    sprintf(fname, "%s/%s%d", stats_dir, LOGFILE, port);
    break;
  }
  if ((fp = fopen(fname, "a")) == NULL) {
     fprintf(stderr, "Error opening logfile\n");
  }
  else  {
     fprintf(fp, "%s %s", tmp, asctime(localtime(&time_in)));
     fflush (fp);
     fclose(fp);
  }
  va_end(ap);
  return retval;
}

/* [geek] pprintf uses 20% less time than pprintf2 */

PUBLIC int pprintf2(int p, int Code, char *string)
{
  char tmp[10 * MAX_LINE_SIZE];	/* Make sure you can handle 10 lines worth of
				   stuff */
  int len;
  
  if(parray[p].client == 1)
    len = sprintf(tmp, "%d %s", Code, string);
  else {
    strcpy(tmp, string);
    len = strlen(tmp);
  }
  net_send(parray[p].socket, tmp, len);
  return(len);
}

PUBLIC int pprintf(int p, char *format,...)
{
  char tmp[10 * MAX_LINE_SIZE];	/* Make sure you can handle 10 lines worth of
				   stuff */
  int retval, len;
  va_list ap;
  va_start(ap, format);

  retval = vsprintf(tmp, format, ap);
  if ((len = strlen(tmp)) > 10 * MAX_LINE_SIZE) {
    Logit("pprintf buffer overflow");
  }
  net_send(parray[p].socket, tmp, len);
  va_end(ap);
  return retval;
}

#ifdef NEED_NOFORMAT
PUBLIC int pprintf_prompt_noformat(int p, char *format,...)
{
  char tmp[10 * MAX_LINE_SIZE];	/* Make sure you can handle 10 lines worth of
				   stuff */
  char tmp2[100];
  int retval, len;
  va_list ap;

  va_start(ap, format);

  retval = vsprintf(tmp, format, ap);
  if ((len = strlen(tmp)) > 10 * MAX_LINE_SIZE) {
    Logit("pprintf_prompt buffer overflow");
  }
  net_send(parray[p].socket, tmp, len);
  if(parray[p].client) {
    sprintf(tmp, "1 %d\n", parray[p].state);
    net_sendStr(parray[p].socket, tmp);
  }
  else if((!parray[p].client) && (parray[p].state == SCORING)) {
    sprintf(tmp, "Enter Dead Group: ");
    net_send(parray[p].socket, "Enter dead group: ", 18);
  }
  else {
    if(parray[p].extprompt) {
      sprintf(tmp2, "|%s/%d%s| %s ", 
        parray[p].last_tell >= 0 ? parray[parray[p].last_tell].name : "",
        parray[p].last_channel, 
        parray[p].busy[0] == '\0' ? "" : "(B)",
        parray[p].prompt);
    }
    else sprintf(tmp2, "%s",parray[p].prompt);
    net_sendStr(parray[p].socket, tmp2);
  }
  va_end(ap);
  return retval;
}

#endif /* NEED_NOFORMAT */

PUBLIC int pprintf_prompt(int p, char *format,...)
{
  char tmp[10 * MAX_LINE_SIZE];	/* Make sure you can handle 10 lines worth of
				   stuff */
  char tmp2[100];
  int retval, len;
  va_list ap;

  va_start(ap, format);

  retval = vsprintf(tmp, format, ap);
  if ((len = strlen(tmp)) > 10 * MAX_LINE_SIZE) {
    Logit("pprintf_prompt buffer overflow");
  }
  net_send(parray[p].socket, tmp, len);
  if(parray[p].client) {
    sprintf(tmp, "1 %d\n", parray[p].state);
    net_sendStr(parray[p].socket, tmp);
  }
  else if((!parray[p].client) && (parray[p].state == SCORING)) {
    net_send(parray[p].socket, "Enter Dead Group: ", 18);
  }
  else {
    if(parray[p].extprompt) {
      sprintf(tmp2, "|%s/%d%s| %s ", 
        parray[p].last_tell >= 0 ? parray[parray[p].last_tell].name : "",
        parray[p].last_channel, 
        parray[p].busy[0] == '\0' ? "" : "(B)",
        parray[p].prompt);
    }
    else sprintf(tmp2, "%s",parray[p].prompt);
    net_sendStr(parray[p].socket, tmp2);
  }
  va_end(ap);
  return retval;
}

#ifdef NEED_NOFORMAT
PUBLIC int pprintf_noformat(int p, char *format,...)
{
  char tmp[10 * MAX_LINE_SIZE];	/* Make sure you can handle 10 lines worth of
				   stuff */
  int retval;
  va_list ap;

  va_start(ap, format);

  retval = vsprintf(tmp, format, ap);
  if (strlen(tmp) > 10 * MAX_LINE_SIZE) {
    Logit("pprintf_noformat buffer overflow");
  }
  net_sendStr(parray[p].socket, tmp);
  va_end(ap);
  return retval;
}
#endif /* NEED_NOFORMAT */


static int
is_regfile(char *path)
{
  struct stat sbuf;

  if (stat(path, &sbuf) < 0)
    return 0;
  else
    return S_ISREG(sbuf.st_mode);
}

PUBLIC int psend_raw_file(int p, char *dir, char *file)
{
  FILE *fp;
  char tmp[MAX_LINE_SIZE * 2];
  char fname[MAX_FILENAME_SIZE];
  int num;

  if (dir)
    sprintf(fname, "%s/%s", dir, file);
  else
    strcpy(fname, file);
  if (!is_regfile(fname))
    return -1;
  fp = fopen(fname, "r");
  if (!fp)
    return -1;
  while ((num = fread(tmp, sizeof(char), (MAX_LINE_SIZE * 2) - 1, fp)) > 0) {
    net_send(parray[p].socket, tmp, num);
  }
  fclose(fp);
  return 0;
}

PUBLIC int psend_file(int p, char *dir, char *file)
{
  FILE *fp;
  char tmp[MAX_LINE_SIZE * 2];
  char fname[MAX_FILENAME_SIZE];
  int lcount=1;

  parray[p].last_file[0] = '\0';
  parray[p].last_file_line = 0;  

  if (dir) sprintf(fname, "%s/%s", dir, file);
  else     strcpy(fname, file);
  fp = fopen(fname, "r");
  if (!fp) return -1;
  if(Debug) Logit("Opened %s", fname);
  if(parray[p].client) {
    net_send(parray[p].socket, "8 File\n", 7);
  }
  while (!feof(fp) && (lcount < (parray[p].d_height-1))) {
    fgets( tmp, (MAX_LINE_SIZE * 2) - 1, fp );
    if (!feof(fp)) {
      net_sendStr(parray[p].socket, tmp);
      lcount++;
    }  
  }
  if (!feof(fp)) {
    do_copy(parray[p].last_file, fname, MAX_LASTFILE);
    parray[p].last_file_line = (parray[p].d_height-1);
    if(parray[p].client) {
      net_send(parray[p].socket, "8 File\n", 7);
    }
    pprintf(p, "%sType ` or \"next\" to see next page.\n", SendCode(p, INFO));
  }
  else {
    if(parray[p].client) {
      net_send(parray[p].socket, "8 File\n", 7);
    }
  }
  fclose(fp);
  return 0;
}

PUBLIC int pmore_file( int p )
{  
  FILE *fp;
  char tmp[MAX_LINE_SIZE * 2];
  int lcount=1;

  if (!parray[p].last_file) {
    pprintf( p, "%sThere is no more.\n", SendCode(p, ERROR));
    return -1;
  }  
  
  fp = fopen( parray[p].last_file, "r" );
  if (!fp) {
    pprintf( p, "%sFile not found!\n", SendCode(p, ERROR));
    return -1;
  }
  
  if(parray[p].client) {
    net_send(parray[p].socket, "8 File\n", 7);
  }
  while (!feof(fp) && (lcount < (parray[p].last_file_line + 
      parray[p].d_height-1))) {
    fgets( tmp, (MAX_LINE_SIZE * 2) - 1, fp );
    if (!feof(fp)) {
      if (lcount >= parray[p].last_file_line) 
        net_sendStr(parray[p].socket, tmp);
      lcount++;
    }
  }
  if (!feof(fp)) {
    parray[p].last_file_line += parray[p].d_height-1;
    if(parray[p].client) {
      net_send(parray[p].socket, "8 File\n", 7);
    }
    pprintf( p, "%sType \"next\" or `  to see next page.\n", SendCode(p, INFO));
  }
  else {
    parray[p].last_file[0] = '\0';
    parray[p].last_file_line = 0;
    if(parray[p].client) {
      net_send(parray[p].socket, "8 File\n", 7);
    }
  }
  fclose(fp);
  return 0;
}

PUBLIC int pmail_file(int p, char *dir, char *file)
{
  FILE *infile, *outfile;
  char fname[MAX_FILENAME_SIZE];
  char buffer[MAX_STRING_LENGTH];
  char com[MAX_STRING_LENGTH];
  int num, ret;

  ret = 0;

  if (dir)
    sprintf(fname, "%s/%s", eatwhite(dir), eatwhite(file));
  else
    strcpy(fname, eatwhite(file));
  if (!(infile = fopen(fname, "r")))
    return -1;

  if (parray[p].emailAddress && parray[p].emailAddress[0] &&
      safestring(parray[p].emailAddress)) {
    sprintf(com, "%s -s \"%s\" %s", 
                  MAILPROGRAM, eatwhite(file), parray[p].emailAddress);
    outfile = (FILE *) popen(com, "w");
    fwrite("\n", 1, 1, outfile);
    while (!feof(infile)) {
      num = fread(buffer, 1, 1000, infile);
      fwrite(buffer, 1, num, outfile);
    }
    fwrite("\n\n---\n", 1, 6, outfile);
    pclose(outfile);
  }
  else ret = -1;
  fclose(infile);
  return ret;
}

PUBLIC int psend_command(int p, char *command, char *input)
{
  FILE *fp;
  char tmp[MAX_LINE_SIZE];
  int num;

  if (input)
    fp = (FILE *) popen(command, "w");
  else
    fp = (FILE *) popen(command, "r");
  if (!fp)
    return -1;
  if (input) {
    fwrite(input, sizeof(char), strlen(input), fp);
  } else {
    while (!feof(fp)) {
      num = fread(tmp, sizeof(char), MAX_LINE_SIZE - 1, fp);
      net_send(parray[p].socket, tmp, num);
    }
  }
  pclose(fp);
  return 0;
}

PUBLIC char *stoupper(char *str)
{
  int i;

  if (!str)
    return NULL;
  for (i = 0; str[i]; i++) {
    if (islower(str[i])) {
      str[i] = toupper(str[i]);
    }
  }
  return str;
}

PUBLIC char *stolower(char *str)
{
  int i;

  if (!str)
    return NULL;
  for (i = 0; str[i]; i++) {
    if (isupper(str[i])) {
      str[i] = tolower(str[i]);
    }
  }
  return str;
}

PUBLIC int safechar(int c)
{
  if ((c == '>') || (c == '!') || (c == '&') || (c == '*') || (c == '?') ||
      (c == '/') || (c == '<') || (c == '|') || (c == '`') || (c == '$') ||
      (c == ';') || (c == '(') || (c == ')') || (c == '[') || (c == ']'))
    return 0;
  return 1;
}

PUBLIC int safestring(char *str)
{
  int i;

  if (!str)
    return 1;
  for (i = 0; str[i]; i++) {
    if (!safechar(str[i]))
      return 0;
  }
  return 1;
}

/* [PEM]: Don't allow dots and slashes in filenames. */
PUBLIC int safefilename(char *path)
{
  if (!safestring(path))
    return 0;
  while (*path != '\0')
    if (*path == '.' || *path == '/')
      return 0;
    else
      path +=1;
  return 1;
}

/* [PEM]: This is used to check player names when registering.
** Allowing '-' breaks file_[bw]player() below. Possibly other
** strange characters can break things too.
** The new version allows names of the regexp: [A-Za-z]+[A-Za-z0-9]*
*/
PUBLIC int alphastring(char *str)
{
  /* [PEM]: Seemed easiest to rewrite it. */
  if (!str || !isalpha(*str))
    return 0;
  while (*++str)
    if (!isalnum(*str))
      return 0;
  return 1;
}

PUBLIC int printablestring(char *str)
{
  int i;

  if (!str) return 1;

  for (i = 0; str[i]; i++) {
    if ((!isprint(str[i])) && (str[i] != '\t') && (str[i] != '\n'))
      return 0;
  }
  return 1;
}

#ifdef DEBUG_STRDUP
PUBLIC char * strdup(char *str)
{
  char *tmp;

  if (str == NULL) {
    Logit("Attempt to strdup a NULL string");
    return NULL;
  }
  tmp = (char *) calloc(strlen(str) + 1, sizeof(char));
  strcpy(tmp, str);
  return tmp;
}
#endif

PUBLIC char *newhms(int t)
{
  static char tstr[20];
  int h, m, s;

  h = t / 3600;
  t = t % 3600;
  m = t / 60;
  s = t % 60;
  if(h > 99) h = 99;
  if (h) {
      sprintf(tstr, "%dh", h);
  } else if(m) {
    sprintf(tstr, "%dm", m);
  } else {
      sprintf(tstr, "%ds", s);
  }
  return tstr;
}

PUBLIC char *strhms(int t)
{
  static char tstr[60];
  int h, m, s;

  h = t / 3600;
  t = t % 3600;
  m = t / 60;
  s = t % 60;

  sprintf(tstr, "%d hours, %d minutes, %d seconds", h ? h:0, m ? m:0, s ? s:0);
  return tstr;
}

PUBLIC char *hms(int t, int showhour, int showseconds, int spaces)
{
  static char tstr[20];
  char tmp[10];
  int h, m, s;

  h = t / 3600;
  t = t % 3600;
  m = t / 60;
  s = t % 60;
  if (h && showhour) {
    if (spaces)
      sprintf(tstr, "%d : %02d", h, m);
    else
      sprintf(tstr, "%d:%02d", h, m);
  } else {
    sprintf(tstr, "%d", m);
  }
  if (showseconds) {
    if (spaces)
      sprintf(tmp, " : %02d", s);
    else
      sprintf(tmp, ":%02d", s);
    strcat(tstr, tmp);
  }
  return tstr;
}


PRIVATE char *dayarray[] =
{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

PRIVATE char *montharray[] =
{"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug",
"Sep", "Oct", "Nov", "Dec"};

PRIVATE char *strtime(struct tm * stm)
{
  static char tstr[100];

  sprintf(tstr, "%s %3.3s %2d %02d:%02d:%02d %4d",
	  dayarray[stm->tm_wday],
	  montharray[stm->tm_mon],
	  stm->tm_mday,
	  stm->tm_hour,
	  stm->tm_min,
          stm->tm_sec,
          stm->tm_year + 1900);
  return tstr;
}

PUBLIC char *DTdate(struct tm * stm)
{
  static char tstr[12];

  sprintf(tstr, "%4d-%02d-%02d",
          stm->tm_year + 1900,
          stm->tm_mon + 1,
          stm->tm_mday);
  return tstr;
}

PUBLIC char *ResultsDate(char *fdate)
{
  static char tstr[12];

  sprintf(tstr, "%c%c/%c%c/%c%c%c%c", fdate[4], fdate[5], fdate[6], fdate[7],
                                      fdate[0], fdate[1], fdate[2], fdate[3]);

  return(tstr);
}

PUBLIC char *strgmtime(time_t * clk)
{
  static char tstr[14];
  struct tm *stm = localtime(clk);

  sprintf(tstr, "%04d%02d%02d%02d%02d",
          stm->tm_year + 1900,
          stm->tm_mon + 1,
	  stm->tm_mday,
	  stm->tm_hour,
	  stm->tm_min);
  return tstr;
}

PUBLIC char *strDTtime(time_t * clk)
{
  struct tm *stm = localtime(clk);
  return DTdate(stm);
}

PUBLIC char *strltime(time_t * clk)
{
  struct tm *stm = localtime(clk);
  return strtime(stm);
}

PUBLIC char *strgtime(time_t * clk)
{
  struct tm *stm = gmtime(clk);
  return strtime(stm);
}

/* This is used only for relative timeing since it reports seconds since
 * about 5:00pm on Feb 16, 1994
 */
PUBLIC unsigned tenth_secs()
{
  struct timeval tp;
  struct timezone tzp;

  gettimeofday(&tp, &tzp);
/* .1 seconds since 1970 almost fills a 32 bit int! So lets subtract off
 * the time right now */
  return ((tp.tv_sec - 331939277) * 10) + (tp.tv_usec / 100000);
}

/* This is to translate tenths-secs time back into 1/1/70 time in full
 * seconds, because vek didn't read utils.c when he programmed new ratings.
   1 sec since 1970 fits into a 32 bit int OK. 
*/
PUBLIC int untenths(unsigned int tenths)
{
  return( tenths/10 + 331939277 );
}

PUBLIC char *tenth_str(unsigned int t, int spaces)
{
  return hms((t + 5) / 10, 0, 1, spaces);	/* Round it */
}

#define MAX_TRUNC_SIZE 100


/* Warning, if lines in the file are greater than 1024 bytes in length, this
   won't work! */
PUBLIC int truncate_file(char *file, int lines)
{
  FILE *fp;
  int bptr = 0, ftrunc = 0, i;
  char tBuf[MAX_TRUNC_SIZE][MAX_LINE_SIZE];

  if (lines > MAX_TRUNC_SIZE)
    lines = MAX_TRUNC_SIZE;
  fp = fopen(file, "r+");
  if (!fp)
    return 1;
  if(Debug) Logit("Opened %s", file);
  while (!feof(fp)) {
    fgets(tBuf[bptr], MAX_LINE_SIZE, fp);
    if (feof(fp))
      break;
    if (tBuf[bptr][strlen(tBuf[bptr]) - 1] != '\n') {	/* Line too long */
      fclose(fp);
      return -1;
    }
    bptr++;
    if (bptr == lines) {
      ftrunc = 1;
      bptr = 0;
    }
  }
  if (ftrunc) {
    fseek(fp, 0, SEEK_SET);
    ftruncate(fileno(fp), 0);
    for (i = 0; i < lines; i++) {
      fputs(tBuf[bptr], fp);
      bptr++;
      if (bptr == lines) {
	bptr = 0;
      }
    }
  }
  fclose(fp);
  return 0;
}


#ifdef NEWTRUNC
PUBLIC int truncate_file(char *file, int lines)
{
  FILE *fp;
  int bptr = 0, trunc = 0, i;
  char tBuf[MAX_TRUNC_SIZE+10][MAX_LINE_SIZE];

  if (lines > MAX_TRUNC_SIZE)
    lines = MAX_TRUNC_SIZE;
  fp = fopen(file, "r");
  if (!fp)
    return 1;
  if(Debug) Logit("Opened %s", file);
  while (fgets(tBuf[bptr], MAX_LINE_SIZE, fp)) {
    if (tBuf[bptr][strlen(tBuf[bptr]) - 1] != '\n') {
      fclose(fp);
      return -1;
    }
    bptr++;
    if (bptr == lines) {
      trunc = 1;
    }
  }
  if (trunc) {
    fclose(fp);
    fp = fopen(file, "w");    
    for (i = bptr-lines; i < bptr; i++) {
      fputs(tBuf[i], fp);
    }
  }
  fclose(fp);
  return 0;
} 
#endif

/* Warning, if lines in the file are greater than 1024 bytes in length, this
   won't work! */
PUBLIC int lines_file(char *file)
{
  FILE *fp;
  int lcount = 0;
  char tmp[MAX_LINE_SIZE];

  fp = fopen(file, "r");
  if (!fp)
    return 0;
  while (!feof(fp)) {
    if (fgets(tmp, MAX_LINE_SIZE, fp))
      lcount++;
  }
  fclose(fp);
  return lcount;
}

PUBLIC int file_has_pname(char *fname, char *plogin)
{
  if (!strcmp(file_wplayer(fname), plogin))
    return 1;
  if (!strcmp(file_bplayer(fname), plogin))
    return 1;
  return 0;
}

PUBLIC char *file_wplayer(char *fname)
{
  static char tmp[MAX_FILENAME_SIZE];
  char *ptr;
  strcpy(tmp, fname);
  ptr = rindex(tmp, '-');
  if (!ptr)
    return "";
  *ptr = '\0';
  return tmp;
}

PUBLIC char *file_bplayer(char *fname)
{
  char *ptr;

  ptr = rindex(fname, '-');
  if (!ptr)
    return "";
  return ptr + 1;
}

PUBLIC char *dotQuad(unsigned int a)
{
  static char tmp[20];

#ifndef LITTLE_ENDIAN
  sprintf(tmp, "%d.%d.%d.%d", (a & 0xff),
	  (a & 0xff00) >> 8,
	  (a & 0xff0000) >> 16,
	  (a & 0xff000000) >> 24);
#else
  sprintf(tmp, "%d.%d.%d.%d", (a & 0xff000000) >> 24,
	  (a & 0xff0000) >> 16,
	  (a & 0xff00) >> 8,
	  (a & 0xff));
#endif
  return tmp;
}

PUBLIC int available_space()
{
#if defined(SYSTEM_NEXT)
  struct statfs buf;

  statfs(player_dir, &buf);
  return ((buf.f_bsize * buf.f_bavail));
#elif defined(SYSTEM_ULTRIX)
  struct fs_data buf;

  statfs(player_dir, &buf);
  return ((1024 * buf.bfreen));
#else
   return 100000000;		/* Infinite space */
#endif
}

PUBLIC int file_exists(char *fname)
{
  FILE *fp;

  fp = fopen(fname, "r");
  if (!fp)
    return 0;
  fclose(fp);
  return 1;
}

int search_directory(char *dir, char *filter, char *buffer,
		      int buffersize)
/* read a directory into memory. Strings are stored successively in buffer */
/* returns -1 on error, or a count of strings stored */
/* A filter can be passed too */
{
  FILE *fp;
  char command[MAX_FILENAME_SIZE];
  char temp[MAX_LINE_SIZE];
  char *s = buffer;
  int count = 0;
  int bytecount = 0;
  int stop_now = 0;

  sprintf(command, "ls -1 %s", dir);
  fp = (FILE *) popen(command, "r");
  if (!fp)
    return -1;
  fgets(temp, MAX_LINE_SIZE - 1, fp);
  while (!feof(fp) && !stop_now) {
    if (bytecount + strlen(temp) < buffersize) {
      if (filter && (strncmp(filter, temp, strlen(filter)) < 0)) {
	/* already past filenames that could match */
	pclose(fp);
	return count;
      }
      if ((!filter) || (!strncmp(filter, temp, strlen(filter)))) {
	strcpy(s, temp);
	count++;
	bytecount += strlen(temp);
	s += strlen(temp);
	*(s - 1) = '\0';
      }
    } else {
      pclose(fp);
      return count;
    }
    fgets(temp, MAX_LINE_SIZE - 1, fp);
  }
  pclose(fp);
  return count;
}

PUBLIC int display_directory(int p, char *buffer, int count)
/* buffer contains 'count' 0-terminated strings in succession. */
{
#define MAX_DISP 800		/* max. no. filenames to display */

  char *s = buffer;
  multicol *m = multicol_start(MAX_DISP);
  int i;

  for (i = 0; (i < count && i < MAX_DISP); i++) {
    multicol_store(m, s);
    s += strlen(s) + 1;
  }
  multicol_pprint(m, p, 78, 1);
  multicol_end(m);
  return i;
}

PUBLIC void bldSD(char *psz)
{
  if (psz[0] == '-') {
    psz++;
    SD.where = HOST;
  } else {
    SD.where = USER;
  }
  bmInit(strlwr(psz), &SD.bmData);
}

PUBLIC char *strlwr(char *psz)
{
  char *ret = psz;
  while (*psz) {
    *psz = tolower(*psz);
    psz++;
  }
  return ret;
}

PUBLIC const SearchResult *search(char *psz)
{
  static SearchResult sr;
  char *pcStr = 0;
  char *pcTmp = strchr(psz, ':');

  if (!pcTmp)			/* PEM */
    return 0;
  *pcTmp = 0;
  strcpy(sr.szPlayer,pcTmp + 1);
  strcpy(sr.szMailAddr, psz);

  pcTmp = strchr(psz, '@');
  if (!pcTmp)			/* PEM */
    return 0;
  *pcTmp = 0;

  if (SD.where == HOST) {
    psz = pcTmp + 1;
  }

  pcStr = bmSrch(strlwr(psz), &SD.bmData);

  if (pcStr) {
    return &sr;
  }

  return 0;
}

PUBLIC int blank(char *psz)
{
  while (*psz) {
    if (!isspace(*psz)) {
      return 0;
    } else {
      psz++;
    }
  }
  return 1;
}

PUBLIC int do_copy(char *dest, char *s, int max)
{
  /* [PEM]: If the logging is not really necessary, it's probably
     more efficient to skip this. */
#if 1
  int i;

  i = strlen(s);
  if(i > max) {
    Logit("Attempt to copy large string %s (len = %d, max = %d)", s, i, max);
  }

  i = (i < max ? i : max);
  strncpy(dest, s, i);
  dest[i] = '\0';		/* [PEM]: Make sure it's terminated. */
#else
  strncpy(dest, s, max);
  dest[max] = '\0';		/* [PEM]: Make sure it's terminated. */
#endif
  return i;
}

