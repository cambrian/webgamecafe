/* comproc.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997  Erik Van Riper (geek@midway.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _COMPROC_H
#define _COMPROC_H

#ifdef CHESS
extern const none;      /* = 0;  */
extern const blitz_rat; /* = 1;  */
extern const std_rat;   /* = 2;  */
#endif
extern int com_rating_recalc(int, param_list);
extern int com_more(int, param_list);

extern int com_quit(int, param_list);
extern int com_register(int, param_list);
extern int com_ayt(int, param_list);
extern int com_help(int, param_list);
extern int com_info(int, param_list);
extern int com_rank(int, param_list);
extern int com_ranked(int, param_list);
extern int com_infor(int, param_list);
extern int com_adhelp(int, param_list);
extern int com_shout(int, param_list);
extern int com_ashout(int, param_list);
extern int com_cshout(int, param_list);
extern int com_gshout(int, param_list);
extern int com_query(int, param_list);
extern int com_it(int, param_list);
extern int com_git(int, param_list);
extern int com_tell(int, param_list);
extern int com_say(int, param_list);
extern int com_choice(int, param_list);
extern int com_whisper(int, param_list);
extern int com_kibitz(int, param_list);
extern int com_set(int, param_list);
extern int com_stats(int, param_list);
extern int com_cstats(int, param_list);
extern int com_password(int, param_list);
extern int com_uptime(int, param_list);
extern int com_date(int, param_list);
extern int com_llogons(int, param_list);
extern int com_logons(int, param_list);
extern int com_who(int, param_list);
extern int com_censor(int, param_list);
extern int com_uncensor(int, param_list);
extern int com_notify(int, param_list);
extern int com_unnotify(int, param_list);
extern int com_channel(int, param_list);
extern int com_inchannel(int, param_list);
extern int com_gmatch(int, param_list);
extern int com_tmatch(int, param_list);
extern int com_goematch(int, param_list);
extern int com_cmatch(int, param_list);
extern int com_decline(int, param_list);
extern int com_withdraw(int, param_list);
extern int com_pending(int, param_list);
extern int com_accept(int, param_list);
extern int com_refresh(int, param_list);
extern int com_open(int, param_list);
extern int com_bell(int, param_list);
extern int com_style(int, param_list);
extern int com_promote(int, param_list);
extern int com_alias(int, param_list);
extern int com_unalias(int, param_list);
extern int create_new_gomatch(int, int, int, int, int, int, int, int, int);
extern int com_servers(int, param_list);
extern int com_sendmessage(int, param_list);
extern int com_messages(int, param_list);
extern int com_clearmess(int, param_list);
extern int com_variables(int, param_list);
extern int com_mailsource(int, param_list);
extern int com_mailhelp(int, param_list);
extern int com_handles(int, param_list);
extern int com_znotify(int, param_list);
extern int com_addlist(int, param_list);
extern int com_sublist(int, param_list);
extern int com_showlist(int, param_list);
extern int in_list(char *, char *);
extern int com_news(int, param_list);
extern int com_beep(int, param_list);
extern int com_qtell(int, param_list);
extern int com_getpi(int, param_list);
extern int com_getps(int, param_list);
extern int com_pass(int, param_list);
extern int com_undo(int, param_list);
extern int com_komi(int, param_list);
extern int com_handicap(int, param_list);
extern int com_ladder(int, param_list);
extern int com_join(int, param_list);
extern int com_drop(int, param_list);
extern int com_score(int, param_list);
extern int com_awho(int, param_list);
extern int com_review(int, param_list);
extern int com_watching(int, param_list);
extern int com_clntvrfy(int, param_list);
extern int com_mailme(int, param_list);
extern int com_me(int, param_list);
extern int com_pme(int, param_list);
extern int com_teach(int, param_list);
extern int com_admins(int, param_list);
extern int com_spair(int, param_list);
extern int com_nocaps(int, param_list);
extern int com_reset(int, param_list);
extern int com_translate(int, param_list);
extern int com_find(int, param_list);
extern int com_best(int, param_list);
extern int com_ctitle(int, param_list);
extern int com_lock(int, param_list);
extern int com_unlock(int, param_list);
extern int com_invite(int, param_list);
extern int com_emote(int, param_list);
extern int com_mailmess(int, param_list);
extern int com_suggest(int, param_list);
extern int com_nsuggest(int, param_list);
extern int com_note(int, param_list);
extern int com_shownote(int, param_list);
extern int com_ping(int, param_list);

extern int com_nrating(int, param_list);
#ifdef NNGSRATED
extern int com_rating(int, param_list);
#endif
extern int plogins(int, char *);
extern int a_who(int, int, int *);
extern int com_dnd(int, param_list);
extern int com_which_client(int, param_list);
extern void AutoMatch(int, int, int *, float *);
extern int in_list(char *, char *);
extern int com_lchan(int, param_list);
extern int com_lashout(int, param_list);

#define UNKNOWN_CLIENT	0
#define TELNET		1
#define XIGC		2
#define WINIGC		3
#define WIGC		4
#define CGOBAN		5
#define JAVA		6
#define TGIGC		7
#define TGWIN		8
#define FIGC		9
#define PCIGC		10
#define GOSERVANT	11
#define MACGO		12
#define AMIGAIGC	13
#define HAICLIENT	14
#define IGC		15
#define KGO		16
#define NEXTGO		17
#define OS2IGC		18
#define STIGC		19
#define XGOSPEL		20
#define TKGC		21
#define JAGOCLIENT	22
#define GTKGO		23

#endif /* _COMPROC_H */
