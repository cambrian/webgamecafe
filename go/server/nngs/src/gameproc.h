/* gameproc.h
 *
*/

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997  Erik Van Riper (geek@midway.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _GAMEPROC_H
#define _GAMEPROC_H

#ifndef SAVEFREQ
#define SAVEFREQ	5
#endif

extern void game_ended(int, int, int);

#ifdef PAIR
extern void process_move(int, char *, int);
#endif
#ifndef PAIR
extern void process_move(int, char *);
#endif /* PAIR */

extern int com_resign(int, param_list);
extern int com_draw(int, param_list);
extern int com_pause(int, param_list);
extern int com_unpause(int, param_list);
extern int com_abort(int, param_list);
extern int com_games(int, param_list);
extern int com_observe(int, param_list);
extern int com_allob(int, param_list);
extern int com_moves(int, param_list);
extern int com_gomoves(int, param_list);
extern int com_mailmoves(int, param_list);
extern int com_oldmoves(int, param_list);
extern int com_mailoldm(int, param_list);
extern int com_load(int, param_list);
extern int com_look(int, param_list);
extern int com_touch(int, param_list);
extern int com_stored(int, param_list);
extern int com_sgf(int, param_list);
extern int com_adjourn(int, param_list);
extern int com_done(int, param_list);
extern int com_history(int, param_list);
extern int com_rhistory(int, param_list);
extern int com_time(int, param_list);
extern int com_title(int, param_list);
extern int com_event(int, param_list);
extern int com_status(int, param_list);
extern int com_save(int, param_list);
extern int com_pair(int, param_list);
extern int com_ginfo(int, param_list);
extern int com_sresign(int, param_list);
extern int com_problem(int, param_list);
extern int com_free(int, param_list);
extern int com_unfree(int, param_list);
extern int com_pteach(int, param_list);
extern int paired(int);

extern int com_moretime(int, param_list);

#endif /* _GAMEPROC_H */
