/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@willent.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"
#include <errno.h>
#include <utime.h>
#ifdef LINUX
/* [PEM]: #include <math.h> gives stupid warnings in linux. */
extern double fabs(double);
#else
#include <math.h>
#endif

#include "common.h"
#include "command.h"
#include "nngsmain.h"
#include "nngsconfig.h"
#include "playerdb.h"
#include "gamedb.h"
#include "gameproc.h"
#include "utils.h"
#include "comproc.h"
#include "ladder.h"
#include "mink.h"

PUBLIC void game_ended(int g, int winner, int why)
{
  const Player *Ladder_B, *Ladder_W;
  char outstr[180];
  char tmp[50];
  char statZ1[200];
  char statZ2[200];
  int p, p2,
  wterr,  /* W's territory */
  wocc,   /* W stones on the board */
  bterr,  /* B's territory */
  bocc,   /* B stones on the board */
  wcaps,  /* Number of captured W stones (add to B's score in Japanese score) */  bcaps,  /* Number of captured B stones (add to W's score in Japanese score) */  rate_change = 0,
  isDraw = 0,
  until, x,
  black_player, white_player;

  twodstring statstring;
  float wscore, bscore;
  FILE *fp;
  time_t now;

  now = time(0);

  sprintf(outstr, "{Game %d: %s vs %s :",
          g + 1,
          parray[garray[g].white].name,
          parray[garray[g].black].name);

  garray[g].result = why;
  garray[g].winner = winner;

  switch (why) {
  case END_DONE:
    completed_games++;
    getcaps(garray[g].GoGame, &wcaps, &bcaps);
    countscore(garray[g].GoGame, statstring, &wterr, &bterr, &wocc, &bocc);
    if(Debug)
      Logit("k=%.1f wtr=%d, btr=%d, wocc=%d, bocc=%d, wcaps=%d, bcaps=%d",
                 garray[g].komi, wterr, bterr,  wocc, bocc, wcaps, bcaps);
#ifdef CHINESESCORE
    if (garray[g].komi > 0) wscore = wterr + wocc + garray[g].komi;
    else wscore = wterr + wocc;
    if (garray[g].komi < 0) bscore = bterr + bocc + fabs(garray[g].komi);
    else bscore = bterr + bocc;
#endif /* CHINESESCORE */
    if (garray[g].komi > 0) wscore = wterr + bcaps + garray[g].komi;
    else wscore = wterr + bcaps;
    if (garray[g].komi < 0) bscore = bterr + wcaps + fabs(garray[g].komi);
    else bscore = bterr + wcaps;
    if (wscore > bscore) {
      garray[g].gresult = (float) wscore - bscore;
      winner = garray[g].white;
    }
    else {
      garray[g].gresult = (float) bscore - wscore;
      winner = garray[g].black;
    }
    black_player = garray[g].black;
    white_player = garray[g].white;
    sprintf(statZ1, "%s %3.3s%s %d %d %d T %.1f %d\n",
                parray[white_player].name,
                parray[white_player].srank,
                parray[white_player].rated ? "*" : " ",
                bcaps,
                garray[g].wTime,
                garray[g].wByoStones,
                garray[g].komi,
                garray[g].GoGame->handicap);
    sprintf(statZ2, "%s %3.3s%s %d %d %d T %.1f %d\n",
                parray[black_player].name,
                parray[black_player].srank,
                parray[black_player].rated ? "*" : " ",
                wcaps,
                garray[g].bTime,
                garray[g].bByoStones,
                garray[g].komi,
                garray[g].GoGame->handicap);

    if (parray[black_player].client) {
      pprintf(black_player, "\n%s%s%s%s",
                SendCode(black_player, STATUS),
                statZ1,
                SendCode(black_player, STATUS),
                statZ2);

      until = garray[g].GoGame->height;
      for(x = 0; x < until; x++) {
        pprintf(black_player, "%s%2d: %s\n",
                  SendCode(black_player, STATUS),
                  x,
                  statstring[x]);
      }
    }
    if (parray[white_player].client) {
      pprintf(white_player, "\n%s%s%s%s",
                SendCode(white_player, STATUS),
                statZ1,
                SendCode(white_player, STATUS),
                statZ2);

      until = garray[g].GoGame->height;
      for(x = 0; x < until; x++) {
        pprintf(white_player, "%s%2d: %s\n",
                  SendCode(white_player, STATUS),
                  x,
                  statstring[x]);
      }
    }
    pprintf(white_player, "%s%s (W:O): %.1f to %s (B:#): %.1f\n",
                          SendCode(white_player, SCORE_M),
                          parray[garray[g].white].name,
                          wscore,
                          parray[garray[g].black].name,
                          bscore);

    pprintf(black_player, "%s%s (W:O): %.1f to %s (B:#): %.1f\n",
                          SendCode(black_player, SCORE_M),
                          parray[garray[g].white].name,
                          wscore,
                          parray[garray[g].black].name,
                          bscore);

    pprintf(garray[g].white, "%s%s has resigned the game.\n",
           SendCode(garray[g].white, INFO),
           (winner == garray[g].white) ? \
           parray[garray[g].black].name : parray[garray[g].white].name);

    pprintf(garray[g].black, "%s%s has resigned the game.\n",
           SendCode(garray[g].black, INFO),
           (winner == garray[g].white) ? \
           parray[garray[g].black].name : parray[garray[g].white].name);

    sprintf(tmp, " %s resigns. W %.1f B %.1f}\n",
            (winner == garray[g].white) ? "Black" : "White",
            wscore, bscore);

    rate_change = 1;
    for (p = 0; p < p_num; p++) {
      if (parray[p].status != PLAYER_PROMPT) continue;
      if (!parray[p].i_game && !player_is_observe(p, g)) continue;
/*      pprintf_prompt(p, "%s%s%s", SendCode(p, SHOUT), outstr, tmp); */
      if (player_is_observe(p, g)) {
        /*pprintf_prompt(p, "%s%s%s", SendCode(p, INFO), outstr, tmp);*/
        if (parray[p].client) {
          pprintf(p, "\n%s%s%s%s",
                SendCode(p, STATUS), statZ1, SendCode(p, STATUS), statZ2);
          until = garray[g].GoGame->height;
          for(x = 0; x < until; x++) {
            pprintf(p, "%s%2d: %s\n",
                  SendCode(p, STATUS),
                  x,
                  statstring[x]);
          }
        }
        pprintf_prompt(p, "%s%s%s", SendCode(p, INFO), outstr, tmp);
        player_remove_observe(p, g); 
      }
    }
    garray[g].winner = winner;
    game_delete(garray[g].white, garray[g].black);
    break;

  case END_RESIGN:
    completed_games++;
    sprintf(tmp, " %s resigns.}\n",
            (winner == garray[g].white) ? "Black" : "White");
    rate_change = 1;
    garray[g].winner = winner;
    garray[g].gresult = 0.0;
    game_delete(garray[g].white, garray[g].black);

    pprintf(garray[g].white, "%s%s has resigned the game.\n",
           SendCode(garray[g].white, INFO),
           (winner == garray[g].white) ? \
           parray[garray[g].black].name : parray[garray[g].white].name);

    pprintf(garray[g].black, "%s%s has resigned the game.\n",
           SendCode(garray[g].black, INFO),
           (winner == garray[g].white) ? \
           parray[garray[g].black].name : parray[garray[g].white].name);

    break;

  case END_FLAG:
    completed_games++;
    sprintf(tmp, " %s forfeits on time.}\n",
            (winner == garray[g].white) ? "Black" : "White");
    rate_change = 1;
    garray[g].winner = winner;
    garray[g].gresult = -1.0;
    game_delete(garray[g].white, garray[g].black);
    break;

  case END_ADJOURN:
  case END_LOSTCONNECTION:
    sprintf(tmp, " has adjourned.}\n");
    rate_change = 0;
    game_save(g);
    pprintf(garray[g].black, "%sGame has been adjourned.\n",
            SendCode(garray[g].black, INFO));
    pprintf(garray[g].black, "%sGame %d: %s vs %s : has adjourned.\n",
          SendCode(garray[g].black, INFO),
          g + 1,
          parray[garray[g].white].name,
          parray[garray[g].black].name);

    if (!garray[g].Teach) {
      pprintf(garray[g].white, "%sGame has been adjourned.\n",
            SendCode(garray[g].white, INFO));
      pprintf(garray[g].white, "%sGame %d: %s vs %s : has adjourned.\n",
          SendCode(garray[g].white, INFO),
          g + 1,
          parray[garray[g].white].name,
          parray[garray[g].black].name);
    }
    break;

  default:
    sprintf(tmp, " Hmm, the game ended and I don't know why.} *\n");
    break;
  }
  strcat(outstr, tmp);
  
  for (p = 0; p < p_num; p++) {
    if (parray[p].status != PLAYER_PROMPT) continue;
    if (parray[p].i_game || player_is_observe(p, g))
    pprintf_prompt(p, "%s%s", SendCode(p, SHOUT), outstr);
    if ((player_is_observe(p, g)) || 
        (p == garray[g].black)   || 
        (p == garray[g].white)) {
      pprintf_prompt(p, "%s%s", SendCode(p, INFO), outstr);
    }
      player_remove_observe(p, g); 
  }

  if ((winner == garray[g].black) && (rate_change == 1)) {
    player_resort();
    if ((garray[g].Ladder9 == 1) || (garray[g].Ladder19 == 1)) {
      if (garray[g].Ladder9 == 1) {
        Ladder_W = PlayerNamed(Ladder9, parray[garray[g].white].name);
        Ladder_B = PlayerNamed(Ladder9, parray[garray[g].black].name);
        PlayerRotate(Ladder9, Ladder_W->idx, Ladder_B->idx);
        PlayerUpdTime(Ladder9, Ladder_W->idx, now);
        PlayerUpdTime(Ladder9, Ladder_B->idx, now);
        PlayerAddWin(Ladder9, Ladder_B->idx);
        PlayerAddLoss(Ladder9, Ladder_W->idx);
        fp = fopen(LADDER9, "w");
        if (fp == NULL) {
          Logit("Error opening %s for write!!!", LADDER9);
          pprintf(p,"%sThere was an internal error.  Please notify an admin!\n", SendCode(p, ERROR));
        } else {
          num_9 = PlayerSave(fp, Ladder9);
          fclose(fp);
          Ladder_W = PlayerNamed(Ladder9, parray[garray[g].white].name);
          Ladder_B = PlayerNamed(Ladder9, parray[garray[g].black].name);
          pprintf(garray[g].black, "%sYou are now at position %d in the 9x9 ladder.  Congrats!\n", SendCode(garray[g].black, INFO), (Ladder_B->idx) + 1);
          pprintf(garray[g].white, "%sYou are now at position %d in the 9x9 ladder.\n", SendCode(garray[g].white, INFO), (Ladder_W->idx) + 1);
          for (p2 = 0; p2 < p_num; p2++) {
            if (parray[p2].status != PLAYER_PROMPT) continue;
            if (!parray[p2].i_lshout) continue;
            pprintf_prompt(p2,"%s!! Ladder9 Result: %s takes position %d from %s !!\n",
                  SendCode(p2,SHOUT),
                  parray[garray[g].black].name,
                  (Ladder_B->idx) + 1,
                  parray[garray[g].white].name,
                  (Ladder_W->idx) + 1);
          }
        }
      }
      if (garray[g].Ladder19 == 1) {
        Ladder_W = PlayerNamed(Ladder19, parray[garray[g].white].name);
        Ladder_B = PlayerNamed(Ladder19, parray[garray[g].black].name);
        PlayerRotate(Ladder19, Ladder_W->idx, Ladder_B->idx);
        PlayerUpdTime(Ladder19, Ladder_W->idx, now);
        PlayerUpdTime(Ladder19, Ladder_B->idx, now);
        PlayerAddWin(Ladder19, Ladder_B->idx);
        PlayerAddLoss(Ladder19, Ladder_W->idx);
        fp = fopen(LADDER19, "w");
        if (fp == NULL) {
          Logit("Error opening %s for write!!!", LADDER19);
          pprintf(p,"%sThere was an internal error.  Please notify an admin!\n", SendCode(p, ERROR));
        } else {
          num_19 = PlayerSave(fp, Ladder19);
          fclose(fp);
          Ladder_W = PlayerNamed(Ladder19, parray[garray[g].white].name);
          Ladder_B = PlayerNamed(Ladder19, parray[garray[g].black].name);
          pprintf(garray[g].black, "%sYou are now at position %d in the 19x19 ladder.  Congrats!\n", SendCode(garray[g].black, INFO), (Ladder_B->idx) + 1);
          pprintf(garray[g].white, "%sYou are now at position %d in the 19x19 ladder.\n", SendCode(garray[g].white, INFO), (Ladder_W->idx) + 1);
          for (p2 = 0; p2 < p_num; p2++) {
            if (parray[p2].status != PLAYER_PROMPT) continue;
            if (!parray[p2].i_lshout) continue;
            pprintf_prompt(p2,"%s!! Ladder19 Result: %s takes position %d from %s !!\n",
                  SendCode(p2,SHOUT),
                  parray[garray[g].black].name,
                  (Ladder_B->idx) + 1,
                  parray[garray[g].white].name,
                  (Ladder_W->idx) + 1);
          }
        }
      }
    }
  }
  else if ((winner == garray[g].white) && (rate_change == 1)) {
    if ((garray[g].Ladder9 == 1) || (garray[g].Ladder19 == 1)) {
      if (garray[g].Ladder9 == 1) {
        Ladder_W = PlayerNamed(Ladder9, parray[garray[g].white].name);
        Ladder_B = PlayerNamed(Ladder9, parray[garray[g].black].name);
        PlayerUpdTime(Ladder9, Ladder_W->idx, now);
        PlayerUpdTime(Ladder9, Ladder_B->idx, now);
        PlayerAddWin(Ladder9, Ladder_W->idx);
        PlayerAddLoss(Ladder9, Ladder_B->idx);
        fp = fopen(LADDER9, "w");
        if (fp == NULL) {
          Logit("Error opening %s for write!!!", LADDER9);
          pprintf(p,"%sThere was an internal error.  Please notify an admin!\n", SendCode(p, ERROR));
        } else {
          num_9 = PlayerSave(fp, Ladder9);
          fclose(fp);
        }
      }
      if (garray[g].Ladder19 == 1) {
        Ladder_W = PlayerNamed(Ladder19, parray[garray[g].white].name);
        Ladder_B = PlayerNamed(Ladder19, parray[garray[g].black].name);
        PlayerUpdTime(Ladder19, Ladder_W->idx, now);
        PlayerUpdTime(Ladder19, Ladder_B->idx, now);
        PlayerAddWin(Ladder19, Ladder_W->idx);
        PlayerAddLoss(Ladder19, Ladder_B->idx);
        fp = fopen(LADDER19, "w");
        if (fp == NULL) {
          Logit("Error opening %s for write!!!", LADDER19);
          pprintf(p,"%sThere was an internal error.  Please notify an admin!\n", SendCode(p, ERROR));
        } else {
          num_19 = PlayerSave(fp, Ladder19);
          fclose(fp);
        }
      }
    }
  }
  if (rate_change) game_write_complete(g, isDraw);
  parray[garray[g].white].state = WAITING;
  parray[garray[g].black].state = WAITING;
  pprintf_prompt(garray[g].white, "\n");
  if (!garray[g].Teach) pprintf_prompt(garray[g].black, "\n");
  if ((why == END_RESIGN) || (why == END_FLAG) || (why == END_DONE)) {
    if (!garray[g].Teach) {
      parray[garray[g].black].lastColor = BLACK;
      parray[garray[g].white].lastColor = WHITE;
      parray[garray[g].white].gonum_white++;
      parray[garray[g].black].gonum_black++;
      if (garray[g].winner == garray[g].white) {
        parray[garray[g].white].gowins++;
        parray[garray[g].white].water++;
        parray[garray[g].black].golose++;
      }
      else if (garray[g].winner == garray[g].black) {
        parray[garray[g].white].golose++;
        parray[garray[g].black].gowins++;
        parray[garray[g].black].water++;
      }
    }
  }
  parray[garray[g].white].game = -1;
  parray[garray[g].black].game = -1;
  parray[garray[g].white].opponent = -1;
  parray[garray[g].black].opponent = -1;
  parray[garray[g].white].last_opponent = garray[g].black;
  parray[garray[g].black].last_opponent = garray[g].white;
  parray[garray[g].white].match_type = 0;
  parray[garray[g].black].match_type = 0;
  game_finish(g);
}

#ifdef PAIR
int paired(int g)
{
  int i;

  return garray[g].pairstate != NOTPAIRED &&
         garray[i = garray[g].pairwith].pairstate != NOTPAIRED &&
         garray[i].pairwith == g && garray[i].status == GAME_ACTIVE;
}
#endif

#ifdef PAIR
PUBLIC void process_move(int p, char *command, int original)
#else
PUBLIC void process_move(int p, char *command)
#endif
{
  int g;
  int good;
  unsigned now;
  int gmove;
  int players_only = 0;


  if (parray[p].game < 0) {
    pprintf_prompt(p, "%sYou are not playing a game.\n", SendCode(p, ERROR));
    return;
  }

  player_decline_offers(p, -1, -1);

  g = parray[p].game;

  if (((garray[g].Teach == 0) && (garray[g].Teach2 == 0)) && 
      ((parray[p].side != garray[g].onMove) && (parray[p].state != SCORING))) {
    pprintf_prompt(p, "%sIt is not your move.\n", SendCode(p, ERROR));
    return;
  }

  if (garray[g].clockStopped) {
    pprintf_prompt(p, "Game clock is paused, use \"unpause\" to resume.\n");
    return;
  }

#ifdef PAIR
/* pair1 = (W1,B1), pair2 = (W2,B2)
   movenum % 4      0   1   2   3   ( == movenum & 3)
   player to move   B1  W1  B2  W2
*/
  if (paired(g) && original
      && parray[p].state != SCORING &&
      ((movenum(garray[g].GoGame) & 3) / 2) != (garray[g].pairstate == PAIR2)) {
    pprintf_prompt(p, "%sIt is your partner's move.\n", SendCode(p, ERROR));
    return;
  }
#endif


  /* test if we are removing dead stones */
  if (parray[p].state == SCORING) {
    if (strcmp(command, "pass") == 0) {    /* User passes */
      pprintf(p, "%Pass is not valid during scoring.\n", SendCode(p, ERROR));
      ASSERT(parray[garray[g].white].state == SCORING);
      ASSERT(parray[garray[g].black].state == SCORING);
      return;
    }

    /* Remove all "done"'s, still removing stones..... */
    player_remove_request(parray[p].opponent, p, PEND_DONE);
    player_remove_request(p, parray[p].opponent, PEND_DONE);
    player_decline_offers(p, -1, PEND_DONE);
    player_decline_offers(parray[p].opponent, -1, PEND_DONE);
    player_withdraw_offers(p, -1, PEND_DONE);
    player_withdraw_offers(parray[p].opponent, -1, PEND_DONE);

/*    Logit("(Removing) g = %d, move = %d command = %s", g, go_move(garray[g].GoGame, command), command); */
    good = removedead(garray[g].GoGame, 
                go_move(garray[g].GoGame, command),
			  p == garray[g].white ? GO_WHITE : GO_BLACK);
  /* We are scoring, and removed a stone */
    if (good) {
      players_only = 1;
      pprintf_prompt(garray[g].white, "%sRemoving @ %s\n",
		SendCode(garray[g].white, INFO),
		stoupper(command));
      pprintf_prompt(garray[g].black, "%sRemoving @ %s\n",
		SendCode(garray[g].black, INFO),
		stoupper(command));
    }
  }

  /* Play the move, test if valid */
  else {
    if (strcmp(command, "pass") == 0) {    /* User passes */
      /* pass is valid */
      garray[g].num_pass = pass(garray[g].GoGame); 
      /* Check if we need to start scoring.... */
      if ((garray[g].Teach == 0) && (garray[g].num_pass >= 2)) {
        parray[garray[g].white].state = SCORING;
        parray[garray[g].black].state = SCORING;
        pprintf_prompt(garray[g].white, "%sYou can check your score with the score command, type 'done' when finished.\n", SendCode(garray[g].white, INFO));
        pprintf_prompt(garray[g].black, "%sYou can check your score with the score command, type 'done' when finished.\n", SendCode(garray[g].black, INFO));
      }
    }
    else if (!play(garray[g].GoGame, go_move(garray[g].GoGame, command),1)) { 
      pprintf(p, "%sYour move is not valid.\n", SendCode(p, ERROR));
      pprintf_prompt(p, "%sIllegal Move!\n", SendCode(p, ERROR));
      return;
    }
    game_update_time(g);
    if(garray[g].status != GAME_ACTIVE) return;
    now = tenth_secs();
    garray[g].lastMoveTime = now;
    if (garray[g].onMove==WHITE) { 
      garray[g].onMove = BLACK;
      if (garray[g].wByoStones > 0) {
        if (garray[g].wByoStones == 1) {
          garray[g].wByoStones = garray[g].ByoS;
          garray[g].wTime = garray[g].Byo;
        }
        else garray[g].wByoStones--;
      }
    } else {
      garray[g].onMove = WHITE;
      if (garray[g].bByoStones > 0) {
        if (garray[g].bByoStones == 1) {
          garray[g].bByoStones = garray[g].ByoS;
          garray[g].bTime = garray[g].Byo;
        }
        else garray[g].bByoStones--;
      }
    }
  } 

  /* Check to see if we should be saving the game */
  gmove = movenum(garray[g].GoGame);
  if (gmove) if ((gmove % SAVEFREQ) == 0) game_save(g);

  /* send out the boards to everyone.... */
  send_go_boards(g, players_only);

#ifdef PAIR
  if (paired(g) && original) {
    process_move(garray[g].onMove == WHITE ? garray[garray[g].pairwith].black :
                                             garray[garray[g].pairwith].white,
                 stolower(command), 0);
  }
#endif
}

PUBLIC int com_title(int p, param_list param)
{
  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.",SendCode(p, ERROR));
    return COM_OK;
  }
  free(garray[parray[p].game].Title);
  garray[parray[p].game].Title = (char *) strdup(param[0].val.string); 
  return COM_OK;
}

PUBLIC int com_event(int p, param_list param)
{
  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.",SendCode(p, ERROR));
    return COM_OK;
  }
  free(garray[parray[p].game].Event);
  garray[parray[p].game].Event = (char *) strdup(param[0].val.string); 
  return COM_OK;
}

PUBLIC int com_resign(int p, param_list param)
{
  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.",SendCode(p, ERROR));
    return COM_OK;
  }
  player_decline_offers(p, -1, -1);
  game_ended(parray[p].game, (garray[parray[p].game].white == p) ? garray[parray[p].game].black : garray[parray[p].game].white, END_RESIGN);
  return COM_OK;
}

PUBLIC int com_pause(int p, param_list param)
{
  int g;
  int now;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;
  if (garray[g].wTime == 0) {
    pprintf(p, "%sYou can't pause untimed games.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (garray[g].clockStopped) {
    pprintf(p, "%sGame is already paused, use \"unpause\" to resume.", 
               SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_find_pendfrom(p, parray[p].opponent, PEND_PAUSE) >= 0) {
    player_remove_request(parray[p].opponent, p, PEND_PAUSE);
    garray[g].clockStopped = 1;
    /* Roll back the time */
    if (garray[g].lastDecTime < garray[g].lastMoveTime)
    {
      if (garray[g].onMove == WHITE) {
	garray[g].wTime += (garray[g].lastDecTime - garray[g].lastMoveTime);
      } else {
	garray[g].bTime += (garray[g].lastDecTime - garray[g].lastMoveTime);
      }
    }
    now = tenth_secs();
    garray[g].lastMoveTime = now;
    garray[g].lastDecTime = now;
    pprintf_prompt(parray[p].opponent, "%s%s accepted pause. Game clock paused.\n",
		   SendCode(parray[p].opponent, INFO),
		   parray[p].name);
    pprintf(p, "%sGame clock paused.\n", SendCode(p, INFO));
  } else {
    pprintf_prompt(parray[p].opponent, "%s%s requests to pause the game.\n",
		   SendCode(parray[p].opponent, INFO),
		   parray[p].name);
    pprintf(p, "%sPause request sent.\n",SendCode(p, INFO));
    player_add_request(p, parray[p].opponent, PEND_PAUSE, 0);
  }
  return COM_OKN;
}

PUBLIC int com_unpause(int p, param_list param)
{
  int now;
  int g;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.",SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;
  if (!garray[g].clockStopped) {
    pprintf(p, "%sGame is not paused.", SendCode(p, ERROR));
    return COM_OK;
  }
  garray[g].clockStopped = 0;
  now = tenth_secs();
  garray[g].lastMoveTime = now;
  garray[g].lastDecTime = now;
  pprintf(p, "%sGame clock resumed.", SendCode(p, INFO));
  pprintf_prompt(parray[p].opponent, "%sGame clock resumed.\n", 
                SendCode(parray[p].opponent, INFO));
  return COM_OK;
}

PUBLIC int com_done(int p, param_list param)
{
  if ((parray[p].game < 0) || (garray[parray[p].game].gotype < TYPE_GO)) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (garray[parray[p].game].num_pass < 2) {
    pprintf(p, "%sCannot type done until both sides have passed.", 
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_find_pendfrom(p, parray[p].opponent, PEND_DONE) >= 0) {
    player_remove_request(parray[p].opponent, p, PEND_DONE);
    player_decline_offers(p, -1, -1);
    game_ended(parray[p].game, NEITHER, END_DONE);
  } else {
    player_add_request(p, parray[p].opponent, PEND_DONE, 0);
    pprintf_prompt(parray[p].opponent, "");
  }
  return COM_OK;
}

PUBLIC int com_adjourn(int p, param_list param)
{
  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (garray[parray[p].game].Teach == 1)
    game_ended(parray[p].game, NEITHER, END_ADJOURN);
  else if (player_find_pendfrom(p, parray[p].opponent, PEND_ADJOURN) >= 0) {
    player_remove_request(parray[p].opponent, p, PEND_ADJOURN);
    player_decline_offers(p, -1, -1);
#ifdef PAIR
    if (paired(parray[p].game)) {
      game_ended(garray[parray[p].game].pairwith, NEITHER, END_ADJOURN);
    }
#endif /* PAIR */
    game_ended(parray[p].game, NEITHER, END_ADJOURN);
  } else {
    pprintf_prompt(parray[p].opponent, "%s%s would like to adjourn the game.\n%sUse adjourn to adjourn the game.\n",
                   SendCode(parray[p].opponent, INFO),
		   parray[p].name, SendCode(parray[p].opponent, INFO));
    pprintf(p, "%sRequest for adjournment sent.\n", SendCode(p,INFO));
    player_add_request(p, parray[p].opponent, PEND_ADJOURN, 0);
  }
  return COM_OKN;
}

PUBLIC int com_pteach(int p, param_list param)
{
  int g, p1;

  g = parray[p].game;
  if ((g < 0) || (g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }

  if(garray[g].Teach2 == 1) {
    pprintf(p, "%sSorry, this is already a teaching game.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  p1 = parray[p].opponent;
 
  if (player_find_pendfrom(p, parray[p].opponent, PEND_TEACH) >= 0) {
    player_remove_request(parray[p].opponent, p, PEND_TEACH);
    player_decline_offers(p, -1, -1);

    pprintf(p, "%sThis is now a FREE, UNRATED teaching game.\n", 
                   SendCode(p, INFO));
    pprintf(p1, "%sThis is now a FREE, UNRATED teaching game.\n", 
                   SendCode(p1, INFO));
    garray[g].Teach2 = 1;
    garray[g].rated = 0;
  } else {
    player_add_request(p, parray[p].opponent, PEND_TEACH, 0);
  }
  return COM_OK;
}

PUBLIC int com_ladder(int p, param_list param)
{
  int g, p1, p2;
  int size;
  const Player *LP1, *LP2;

  g = parray[p].game;
  if ((g < 0) || (g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }

  if (garray[g].Ladder_Possible == 0) {
    pprintf(p, "%sSorry, this cannot be a ladder rated game.", 
                SendCode(p, ERROR));
    return COM_OK;
  }
  p1 = parray[p].opponent;
  
  if (player_find_pendfrom(p, parray[p].opponent, PEND_LADDER) >= 0) {
    player_remove_request(parray[p].opponent, p, PEND_LADDER);
    player_decline_offers(p, -1, -1);

    size = garray[g].GoGame->width;

    if (size == 9) {
      LP1 = PlayerNamed(Ladder9, parray[garray[g].white].name);
      LP2 = PlayerNamed(Ladder9, parray[garray[g].black].name);
    } else {
      LP1 = PlayerNamed(Ladder19, parray[garray[g].white].name);
      LP2 = PlayerNamed(Ladder19, parray[garray[g].black].name);
    }

    if ((LP1 == NULL) || (LP2 == NULL)) {
      pprintf(p, "%sYou must first join the ladder with \"join\".", 
                  SendCode(p, ERROR));
      return COM_OK;
    }

    if (size == 9) { garray[g].Ladder9 = 1;  garray[g].komi = (float) 5.5; }
    if (size == 19) { garray[g].Ladder19 = 1; garray[g].komi = (float) 5.5; }
    pprintf(p, "%sThis is now a ladder rated game.\n", SendCode(p, INFO));
    pprintf(p1, "%sThis is now a ladder rated game.\n", SendCode(p1, INFO));
    pprintf_prompt(p1, "%sKomi is now set to 5.5\n", SendCode(p1, INFO));
    pprintf(p, "%sKomi is now set to 5.5\n", SendCode(p, INFO));
    for (p2 = 0; p2 < p_num; p2++) {
      if (parray[p2].status != PLAYER_PROMPT) continue;
      if (!parray[p2].i_lshout) continue;
      pprintf_prompt(p2,"%s!! Ladder%d Game: Match %d  %s (%d) vs. %s (%d) !!\n", 
         SendCode(p2,SHOUT),
         size,
         g + 1,
         parray[garray[g].white].name,
         (LP1->idx) + 1,
         parray[garray[g].black].name,
         (LP2->idx) + 1);
    }
  } else {
    player_add_request(p, parray[p].opponent, PEND_LADDER, 0);
  }
  return COM_OK;
}

PUBLIC int com_komi(int p, param_list param)
{
  int g, p1;
  float newkomi = 0.5;		/* Init to shut up warnings. */
  int pendfrom, pendto, ppend, p1pend;

  if (param[0].type == (int) TYPE_NULL) {
      return COM_OK;
  } else if (param[0].type == TYPE_INT) {              /* Must be an integer */
    newkomi = (float) param[0].val.integer;
  } else if (param[0].type == TYPE_FLOAT) {       
    newkomi = param[0].val.f;
  }

  g = parray[p].game;
  if ((g < 0) || (g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }

  if(garray[g].Teach == 1) {
    garray[g].komi = newkomi;
    pprintf(p, "%sSet the komi to %.1f", SendCode(p, INFO), newkomi);
    return COM_OK;
  }

  p1 = parray[p].opponent;
  
  pendto = player_find_pendto(p, p1, PEND_KOMI);
  pendfrom = player_find_pendfrom(p, p1, PEND_KOMI);
  
  if (pendto >= 0) {
    pprintf(p, "Updating offer already made to \"%s\".\n", parray[p1].name);
  }
  if (pendfrom >= 0) {
    if (pendto >= 0) {
      pprintf(p, "Internal error Report circumstances and bug \"PENDFROMTO\"");
      Logit("This shouldn't happen. You can't have a komi pending from and to the same person.");
      return COM_OK;
    }
    if (newkomi == parray[p].p_from_list[pendfrom].float1) {
      /* komi is agreed upon.  Alter game record */
      garray[g].komi = newkomi;
      pprintf(p, "%sSet the komi to %.1f", SendCode(p, INFO), newkomi);
      pprintf_prompt(p1, "%sKomi is now set to %.1f\n", 
                      SendCode(p1, INFO), newkomi);
      for (p1 = 0; p1 < p_num; p1++) {
        if (parray[p1].status != PLAYER_PROMPT) continue;
        if (player_is_observe(p1, g)) {
          pprintf(p1, "%sKomi set to %.1f in match %d\n",
		  SendCode(p1, INFO), newkomi, g+1);
        }
      }
      
#ifdef PAIR
      if (paired(g)) {
        garray[garray[g].pairwith].komi = newkomi;
      }
#endif

      return COM_OK;
    }
    else {
      player_remove_pendfrom(p, p1, PEND_KOMI);
      player_remove_pendto(p1, p, PEND_KOMI);
    }
  }
  if (pendto < 0) {
    ppend = player_new_pendto(p);
    p1pend = player_new_pendfrom(p1);
  } else {
    ppend = pendto;
    p1pend = player_find_pendfrom(p1, p, PEND_KOMI);
  }
  parray[p].p_to_list[ppend].float1 = newkomi;
  parray[p].p_to_list[ppend].type = PEND_KOMI;
  parray[p].p_to_list[ppend].whoto = p1;
  parray[p].p_to_list[ppend].whofrom = p;

  parray[p1].p_from_list[p1pend].float1 = newkomi;
  parray[p1].p_from_list[p1pend].type = PEND_KOMI;
  parray[p1].p_from_list[p1pend].whoto = p1;
  parray[p1].p_from_list[p1pend].whofrom = p;

  if (pendfrom >= 0) {
    pprintf(p, "%sDeclining komi offer from %s and offering new komi.\n",
                SendCode(p, INFO), parray[p1].name);
    pprintf_prompt(p1,"%s%s declines your komi offer, and offers a new komi:\n",
		SendCode(p1, INFO), parray[p].name);
  }
  if (pendto >= 0) {
    pprintf(p, "%sUpdating komi offer to: ", SendCode(p, INFO));
    pprintf_prompt(p1, "%s%s updates the komi offer.\n", 
                    SendCode(p1, INFO), parray[p].name);
  } else {
    pprintf(p, "%sOffering a komi of %.1f to %s", SendCode(p, INFO), newkomi, parray[p1].name);
  }
  pprintf(p1, "%s%s offers a new komi of %.1f.\n", 
		SendCode(p1, INFO), parray[p].name, newkomi);
  pprintf_prompt(p1, "%sUse \"komi %.1f\" to accept, or \"decline %s\" to respond.\n", 
		SendCode(p1, INFO), newkomi, parray[p].name);
  return COM_OK;
}

PUBLIC int com_status(int p, param_list param)
{
  int g, p1, x, white_player, black_player, wc, bc, until;
  twodstring statstring;

  until = 0;
  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) {
      g = parray[p].game;
    } else if (parray[p].num_observe > 0) {
      g = parray[p].observe_list[0];
    } else {
      pprintf(p, "You are neither playing nor observing a game.");
      return COM_OK;
    }
  } else if (param[0].type == TYPE_WORD) {
    p1 = player_find_part_login(param[0].val.word);
    if (p1 < 0) {
      pprintf(p, "No user named \"%s\" is logged in.", param[0].val.word);
      return COM_OK;
    }
    g = parray[p1].game;
  } else {                      /* Must be an integer */
    g = param[0].val.integer - 1;
  }
  if (param[1].type == TYPE_INT) {
     until = param[1].val.integer;
  }
  if ((g < 0) || (g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
    return COM_NOSUCHGAME;
  }
  if ((garray[g].white != p) && (garray[g].black != p) && garray[g].Private) {
    pprintf(p, "Sorry, that is a private game.");
    return COM_OK;
  }

  black_player = garray[g].black;
  white_player = garray[g].white;

  getcaps(garray[g].GoGame, &wc, &bc);
  boardstatus(garray[g].GoGame, statstring);

  pprintf(p, "%s%s %3.3s%s %d %d %d T %.1f %d\n",
		SendCode(p, STATUS),
		parray[white_player].name,
		parray[white_player].srank,
		parray[white_player].rated ? "*" : " ",
		wc,
		garray[g].wTime,
   		garray[g].wByoStones,
		garray[g].komi,
                garray[g].GoGame->handicap);
  pprintf(p, "%s%s %3.3s%s %d %d %d T %.1f %d\n",
		SendCode(p, STATUS),
		parray[black_player].name,
		parray[black_player].srank,
		parray[black_player].rated ? "*" : " ",
		bc,
		garray[g].bTime,
   		garray[g].bByoStones,
		garray[g].komi,
                garray[g].GoGame->handicap);
  if (!until) until = garray[g].GoGame->height;
  if((until - 1) > garray[g].GoGame->height) return COM_OKN;
  for(x = 0; x < until; x++) {
    pprintf(p, "%s%2d: %s\n",
		SendCode(p, STATUS),
		x,
		statstring[x]);
  }
  return COM_OKN;
}

PUBLIC int com_undo(int p, param_list param)
{
  int gmove, wplayer, bplayer;
  int g, p1, pairg;
  unsigned now;
  char buf[20];
  int num, x;

  if (param[0].type == (int) TYPE_NULL) {
    num = 1;
  }
  else num = param[0].val.integer;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  player_decline_offers(p, -1, -1);
  g = parray[p].game;
  wplayer = garray[g].white;
  bplayer = garray[g].black;

  if ((parray[p].side != garray[g].onMove) && 
      (parray[p].state != SCORING)         &&
      ((garray[g].Teach2 == 0) && (garray[g].Teach == 0))) {
    pprintf(p, "%sIt is not your move.", SendCode(p, ERROR));
    return COM_OK;
  }
  gmove = movenum(garray[g].GoGame);
#ifdef PAIR
  pairg = garray[g].pairwith;

/* pair1 = (W1,B1), pair2 = (W2,B2)
   movenum % 4      0   1   2   3   ( == movenum & 3)
   player to move   B1  W1  B2  W2
*/

  if ((paired(g)) && (gmove == movenum(garray[pairg].GoGame))
      && ((gmove&3)/2) != (garray[g].pairstate==PAIR2)) {
    pprintf(p, "%s It is your partner's move.\n", SendCode(p, ERROR));
    return COM_OK;
  }
#endif

  if (parray[p].state == SCORING) {
    parray[wplayer].state = PLAYING_GO;
    parray[bplayer].state = PLAYING_GO;
    garray[g].num_pass = 1;
    replay(garray[g].GoGame);
  }
  player_remove_request(parray[p].opponent, p, PEND_DONE);
  
  for(x = 0; x < num; x++) {
    if (gmove == 0) {  /* At beginning of game. */
      garray[g].onMove = BLACK; 
      return COM_OK; 
    }
    if(((garray[g].Teach == 0) && (garray[g].Teach2 == 0)) && x == 0) x = num;
    listmove(garray[g].GoGame, gmove, buf);
    pprintf(garray[g].black, "%s%s undid the last move (%s).\n",
              SendCode(garray[g].black, UNDO),
              parray[p].name,
              buf + 1);
    if (garray[g].Teach == 0) 
      pprintf(garray[g].white, "%s%s undid the last move (%s).\n",
              SendCode(garray[g].white, UNDO),
              parray[p].name,
              buf + 1);
    for (p1 = 0; p1 < p_num; p1++) {
      if (parray[p1].status == PLAYER_EMPTY) continue;
      if (player_is_observe(p1, g)) {
        pprintf_prompt(p1, "%sUndo in game %d: %s vs %s:  %s\n",
            SendCode(p1, UNDO),
            g + 1,
            parray[garray[g].white].name,
            parray[garray[g].black].name,
            buf + 1);
      }
    }
    back(garray[g].GoGame);
    now = tenth_secs();
    garray[g].lastMoveTime = now;
    garray[g].onMove = BLACK+WHITE - garray[g].onMove;
    send_go_boards(g, 0);
    gmove = movenum(garray[g].GoGame);
    if(gmove == 0) {
      garray[g].GoGame->handicap = 0;
    }
  }
#ifdef PAIR
  if (paired(g) && gmove == movenum(garray[pairg].GoGame)) {
    com_undo(garray[g].onMove == WHITE ? garray[garray[g].pairwith].black :
                                             garray[garray[g].pairwith].white,
                 param);
      Logit("DUPLICATING undo");
  }
#endif
  return COM_OKN;
}

PUBLIC int com_games(int p, param_list param)
{
  int i;
  int wp, bp;

  int selected = 0;  int count = 0;  int totalcount = 0;
  char *s = NULL;  int slen = 0;
  int mnum = 0;

  if (param[0].type == TYPE_WORD) {
    s = param[0].val.word;
    slen = strlen(s);
    selected = atoi(s);
    if (selected < 0) selected = 0;
  }
  pprintf(p, "%s[##]  white name [ rk ]      black name [ rk ] (Move size H Komi BY FR) (###)", SendCode(p, GAMES));
  for (i = 0; i < g_num; i++) {
    if (garray[i].status != GAME_ACTIVE)
      continue;
    totalcount++;
    if ((selected) && (selected != i+1))
      continue;  /* not selected game number */
    wp = garray[i].white;
    bp = garray[i].black;
    if ((!selected) && s && strncmp(s, parray[wp].login, slen) && 
          strncmp(s, parray[bp].login, slen))
      continue;  /* player names did not match */
    count++;
    mnum = movenum(garray[i].GoGame);
    pprintf(p, "\n%s[%2d] %11.11s [%3.3s%s] vs. %11.11s [%3.3s%s] (%3d   %2d  %d  %2.1f %2d %c%c) (%3d)",
            SendCode(p, GAMES),
            i + 1,
	    parray[wp].name,
            parray[wp].srank,
            parray[wp].rated ? "*" : " ",
	    parray[bp].name,
            parray[bp].srank,
            parray[bp].rated ? "*" : " ",
	    mnum,
            garray[i].GoGame->width,
            garray[i].GoGame->handicap,
            garray[i].komi,
            (garray[i].Byo) / 60,
	    (garray[i].rated) ? ' ' : ((garray[i].Teach) || 
                                      (garray[i].Teach2))? 'T' : 'F',
/*	    (garray[i].gotype) ? 'I' : '*', */
	    (garray[i].rules == RULES_NET) ? (parray[wp].match_type == TYPE_TNETGO ? '*' : 'I') : 'G',
            game_get_num_ob(i));
  }
  return COM_OK;
}

PUBLIC int com_gomoves(int p, param_list param)
{
  int i, p1;
  int wc, bc;

  int g = 0;  int count = 0;

  char buf[20], outStr[1024];

  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) {
      g = parray[p].game;
    } else {
      pprintf(p, "%sYou are neither playing nor observing a game.", 
                   SendCode(p, ERROR));
      return COM_OK;
    }
  } else if (param[0].type == TYPE_WORD) {
    p1 = player_find_part_login(param[0].val.word);
    if (p1 < 0) {
      pprintf(p, "%sNo user named \"%s\" is logged in.", 
                   SendCode(p, ERROR), param[0].val.word);
      return COM_OK;
    }
    g = parray[p1].game;
  } else {                      /* Must be an integer */
    g = param[0].val.integer - 1;
  }
  if ((g < 0) || (g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
    return COM_NOSUCHGAME;
  }
  if ((garray[g].white != p) && (garray[g].black != p) && garray[g].Private) {
    pprintf(p, "%sSorry, that is a private game.", SendCode(p, ERROR));
    return COM_OK;
  }

  getcaps(garray[g].GoGame, &wc, &bc);
  sprintf(outStr, "Game %d %s: %s (%d %d %d) vs %s (%d %d %d)",
        g + 1,
        "I",
        parray[garray[g].white].name,
        bc,
        garray[g].wTime,
        garray[g].wByoStones,
        parray[garray[g].black].name,
        wc,
        garray[g].bTime,
        garray[g].bByoStones);
  count = movenum(garray[g].GoGame);
  if (count == 0) {
    pprintf_prompt(p, "%s%s\n", SendCode(p, MOVE), outStr);  
    return COM_OKN;
  }
  pprintf(p, "%s%s\n", SendCode(p, MOVE), outStr);  
  for(i = 0; i < count; i++) {
    listmove(garray[g].GoGame, i + 1, buf);
    pprintf(p, "%s%3d(%c): %s\n", SendCode(p, MOVE), i, buf[0], buf + 1);
  }
  return COM_OKN;
}

PRIVATE int do_observe(int p, int obgame)
{
  int wc, bc, g;
  int gmove = 0;
  char buf[200];

  if ((garray[obgame].Private) && (parray[p].adminLevel < ADMIN_ADMIN)) {
    pprintf(p, "%sSorry, game %d is a private game.", 
                SendCode(p, INFO),
                obgame + 1);
    return COM_OK;
  }
  if ((garray[obgame].white == p) || (garray[obgame].black == p)) {
    pprintf(p, "%sYou cannot observe a game that you are playing.",
                SendCode(p, INFO));
    return COM_OK;
  }
  if (player_is_observe(p, obgame)) {
    pprintf(p, "%sRemoving game %d from observation list.\n", 
                SendCode(p, INFO),
                obgame + 1);
    player_remove_observe(p, obgame);
  } else {
    if (!player_add_observe(p, obgame)) {
      pprintf(p, "%sAdding game to observation list.\n", 
                  SendCode(p, INFO),
                  obgame + 1);
      g = obgame;
      sprintf(buf, " %s started observation.", parray[p].name);
      add_kib(&garray[g], movenum(garray[g].GoGame), buf);
      if (parray[p].client) {
        getcaps(garray[g].GoGame, &wc, &bc);
        gmove = movenum(garray[obgame].GoGame);
        pprintf(p, "%sGame %d %s: %s (%d %d %d) vs %s (%d %d %d)\n",
                    SendCode(p, MOVE),
                    g + 1,
                    "I",
                    parray[garray[g].white].name,
                    bc,
                    garray[g].wTime,
                    garray[g].wByoStones,
                    parray[garray[g].black].name,
                    wc,
                    garray[g].bTime,
                    garray[g].bByoStones);

        if (gmove) {
          listmove(garray[g].GoGame, gmove, buf);
          pprintf(p, "%s%3d(%c): %s", 
                      SendCode(p, MOVE), gmove - 1, buf[0], buf + 1);
        }
      }
      else if (parray[p].i_verbose) {
        pcommand (p,"refresh %d", g + 1);
      }
      parray[p].state = OBSERVING;
    } else {
      pprintf(p, "%sYou are already observing the maximum number of games.\n",
                  SendCode(p, INFO));
    }
  }
  return COM_OKN;
}

PUBLIC int com_observe(int p, param_list param)
{
  int i;
  int p1 = -1, obgame;

  if (param[0].type == (int) TYPE_NULL) {
    for (i = 0; i < parray[p].num_observe; i++) {
      pprintf(p, "%sRemoving game %d from observation list.", 
                SendCode(p, INFO), parray[p].observe_list[i] + 1);
    }
    parray[p].num_observe = 0;
    parray[p].state = WAITING;
    return COM_OK;
  } else if (param[0].type == TYPE_WORD) {
    p1 = player_find_part_login(param[0].val.word);
    if (p1 < 0) {
      pprintf(p, "%sNo user named \"%s\" is logged in.", 
              SendCode(p, ERROR), param[0].val.word);
      return COM_OK;
    }
    obgame = parray[p1].game;
  } else {			/* Must be an integer */
    obgame = param[0].val.integer - 1;
  }
  if ((obgame < 0) || 
     (obgame >= g_num) || 
     (garray[obgame].status != GAME_ACTIVE)) {
    return COM_NOSUCHGAME;
  }
  if ((garray[obgame].white == p) || (garray[obgame].black == p)) {
    pprintf(p, "%sYou cannot observe a game that you are playing.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  do_observe(p, obgame);
  return COM_OK;
}

#ifdef PAIR 
PUBLIC int com_pair(int p, param_list param)
{
  int gmove, p2;
  int ourgame, theirgame;
  int btim, ttim;

  theirgame = param[0].val.integer - 1;
  ourgame = parray[p].game;

  if ((ourgame < 0) || 
      (ourgame >= g_num) || 
      (garray[ourgame].status != GAME_ACTIVE)) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (garray[ourgame].pairstate != NOTPAIRED) {
    pprintf(p, "%sYour game is paired already.", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((theirgame < 0) || 
      (theirgame >= g_num) || 
      (garray[theirgame].status != GAME_ACTIVE)) {
    pprintf(p, "%sNo such game.", SendCode(p, ERROR));
    return COM_OK;
  }

  p2 = garray[theirgame].white;

  if (p != garray[ourgame].white) {
    pprintf(p, "%sOnly the White player may request a Pair match",
                SendCode(p, ERROR));
    return COM_OK;
  }
  
  if((p == garray[ourgame].black)   ||
     (p == garray[theirgame].white) ||
     (p == garray[theirgame].black)) {
    pprintf(p, "%sYou are one of the other players.  Cannot pair.",
                SendCode(p, ERROR));
    return COM_OK;
  }

  gmove = movenum(garray[ourgame].GoGame) - movenum(garray[theirgame].GoGame);
  if (gmove != 0) {
    pprintf(p, "%sYou must pair before your first move, or at equal moves in a match", SendCode(p, ERROR));
    return COM_OK;
  }

  if (garray[theirgame].GoGame->width != garray[ourgame].GoGame->width) {
    pprintf(p, "%sThe boards are not the same size.", SendCode(p, ERROR));
    return COM_OK;
  }
   
  /* Test if other side already requested pair with us */

  if (player_find_pendfrom(p, p2, PEND_PAIR) >= 0) {
    player_remove_request(p2, p, PEND_PAIR);
    player_decline_offers(p, -1, -1);
    
    /* Ok, in here we have a match for the pairs */

    /* First, get all the times the same */

    ttim = (garray[theirgame].wTime > garray[ourgame].wTime) ? 
            garray[theirgame].wTime : garray[ourgame].wTime;
    btim = (garray[theirgame].Byo > garray[ourgame].Byo)     ? 
            garray[theirgame].Byo : garray[ourgame].Byo;

    garray[theirgame].wTime = ttim;
    garray[ourgame].wTime   = ttim;
    garray[theirgame].bTime = ttim;
    garray[ourgame].bTime   = ttim;
    garray[theirgame].Byo   = btim;
    garray[ourgame].Byo     = btim;

    /* Match the games up in a pair fashion */

    garray[theirgame].pairstate = PAIR1;
    garray[ourgame].pairstate = PAIR2;
    garray[theirgame].pairwith = ourgame;
    garray[ourgame].pairwith = theirgame;

    /* Remove ladder problems (for now) */

    garray[theirgame].Ladder_Possible = 0; 
    garray[theirgame].Ladder9 = 0;
    garray[theirgame].Ladder19 = 0;
    garray[ourgame].Ladder_Possible = 0; 
    garray[ourgame].Ladder9 = 0;
    garray[ourgame].Ladder19 = 0;

    /* Make the game unrated by default */
    garray[theirgame].rated = 0;
    garray[ourgame].rated = 0;

    /* Tell the players */

    pprintf_prompt(garray[theirgame].white, "%sGames %d and %d are now paired.\n",
      SendCode(garray[theirgame].white, INFO), ourgame+1,theirgame+1);
    pprintf_prompt(garray[theirgame].black, "%sGames %d and %d are now paired.\n",
      SendCode(garray[theirgame].black, INFO), ourgame+1,theirgame+1);
    pprintf(garray[ourgame].white, "%sGames %d and %d are now paired.\n",
      SendCode(garray[ourgame].white, INFO), ourgame+1,theirgame+1);
    pprintf_prompt(garray[ourgame].black, "%sGames %d and %d are now paired.\n",
      SendCode(garray[ourgame].black, INFO), ourgame+1,theirgame+1);

    return COM_OK;
  } else {

    /* Send pair request to other game White player */

    player_add_request(p, p2, PEND_PAIR, ourgame);
    pprintf(p2, "%s%s and %s would like to pair their game with yours.\n",
            SendCode(p2, INFO),
            parray[p].name, parray[parray[p].opponent].name);
    pprintf_prompt(p2, "%sTo do so, please type \"pair %d\"\n",
            SendCode(p2, INFO),
            ourgame + 1);
  }
  return COM_OK;
}
#endif /* PAIR */

PUBLIC int com_ginfo(int p, param_list param)
{
  int obgame;
  int g;

  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) obgame = parray[p].game;
    else if (parray[p].num_observe > 0) obgame = parray[p].observe_list[0];
    else return COM_BADPARAMETERS;
  } else {			/* Must be an integer */
    obgame = param[0].val.integer - 1;
  }
  if (obgame >= g_num)
    return COM_OK;
  if (obgame <= 0)
    obgame = 0;
  g = obgame;
  pprintf(p, "%sGame info: %s\n", 
            SendCode(p, INFO), 
            (garray[g].Title) == NULL ? "No info available" : garray[g].Title);
  pprintf(p, "%sEvent Info: %s", SendCode(p, INFO), garray[g].Event);
  return COM_OK;
}

PUBLIC int com_allob(int p, param_list param)
{
  int obgame;
  int p1;
  int g;
  int count;

  count = 0;
  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) obgame = parray[p].game;
    else if (parray[p].num_observe > 0) obgame = parray[p].observe_list[0];
    else {
      pprintf(p, "%sYou are not playing or observing a game", 
                  SendCode(p, ERROR));
      return COM_OK;
    }
  } else if (param[0].type == TYPE_WORD) {
    p1 = player_find_part_login(param[0].val.word);
    if (p1 < 0) {
      pprintf(p, "%sNo user named \"%s\" is logged in.", 
                  SendCode(p, ERROR), param[0].val.word);
      return COM_OK;
    }
    obgame = parray[p1].game;
  } else {			/* Must be an integer */
    obgame = param[0].val.integer - 1;
  }
  if (obgame >= g_num) return COM_OK;
  if(garray[obgame].status != GAME_ACTIVE) return COM_OK;
  if (obgame <= 0) obgame = 0;
  g = obgame;
  
  pprintf(p, "%sObserving game %2d (%s vs. %s) :\n%s",
                SendCode(p, INFO),
                g + 1,
	        parray[garray[g].white].name,
	        parray[garray[g].black].name,
                SendCode(p, INFO));
  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status == PLAYER_EMPTY) continue;
    if (player_is_observe(p1, g)) {
      if (((count % 3) == 0) && (count > 0)) { 
        pprintf(p, "\n%s", SendCode(p, INFO)); 
      }
      pprintf(p, "%15s %3.3s%s ", 
                  parray[p1].name, 
                  parray[p1].srank,
                  parray[p1].rated ? "*" : " ");
      count++;
    }
  }
  pprintf(p, "\n%sFound %d observers.", SendCode(p, INFO), count);
  return COM_OK;
}

PUBLIC int com_moves(int p, param_list param)
{
  int g;
  int p1;

  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) {
      g = parray[p].game;
    } else if (parray[p].num_observe > 0) {
      g = parray[p].observe_list[0];
    } else {
      pprintf(p, "%sYou are neither playing nor observing a game.", 
                     SendCode(p, ERROR));
      return COM_OK;
    }
  } else if (param[0].type == TYPE_WORD) {
    p1 = player_find_part_login(param[0].val.word);
    if (p1 < 0) {
      pprintf(p, "%sNo user named \"%s\" is logged in.", 
               SendCode(p, ERROR), param[0].val.word);
      return COM_OK;
    }
    g = parray[p1].game;
  } else {			/* Must be an integer */
    g = param[0].val.integer - 1;
  }
  if ((g < 0) || (g >= g_num) || 
      (garray[g].status != GAME_ACTIVE) || (garray[g].gotype < TYPE_GO)) {
    return COM_NOSUCHGAME;
  }
  if ((garray[g].white != p) && (garray[g].black != p) && garray[g].Private) {
    pprintf(p, "%sSorry, that is a private game.", SendCode(p, ERROR));
    return COM_OK;
  }
  return COM_OK;
}

PUBLIC int com_touch(int p, param_list param)
{
  int wp, bp;
  char fname[MAX_FILENAME_SIZE];
  char tmpb[80], tmpw[80];
  
  strcpy(tmpb, file_bplayer(param[0].val.string));
  strcpy(tmpw, file_wplayer(param[0].val.string));
  wp = player_find_part_login(tmpw);
  bp = player_find_part_login(tmpb);
  
  if ((wp != p) && (bp != p)) {
    pprintf(p, "%sYou cannot touch someone elses stored game", 
                SendCode(p, ERROR));
    return COM_OK;
  }

  sprintf(fname, "%s/%c/%s-%s", game_dir, tmpb[0], tmpb, tmpw);
  utime(fname, NULL);
  sprintf(fname, "%s/%c/%s-%s", game_dir, tmpw[0], tmpw, tmpb);
  utime(fname, NULL);
  pprintf(p, "%sThe game %s-%s has been touched.", SendCode(p, INFO), tmpw, tmpb);
  return COM_OK;
}

#define LOAD_TIME_WARNING 30

PUBLIC int com_load(int p, param_list param)
{
  int wp, bp, p3;
  int g;
  struct stat statbuf;
  char fname[MAX_FILENAME_SIZE];
  const Player *LadderPlayer;
  int bpos = -1, wpos = -1;

  wp = player_find_part_login(file_wplayer(param[0].val.string));
  bp = player_find_part_login(file_bplayer(param[0].val.string));

  if ((bp != p) && (wp != p)) {	/* Works, even if bp or wp < 0. */
    pprintf(p, "%sYou cannot load someone else's game.  Try \"look\" to see the game.", SendCode(p, ERROR));
    return COM_OK;
  }

  if (wp < 0) {
    pprintf(p, "%s%s is not logged in.", 
               SendCode(p, ERROR), file_wplayer(param[0].val.string));
    return COM_OK;
  }
  if (parray[wp].game >= 0) {
    pprintf(p, "%s%s is currently playing a game.", 
                SendCode(p, ERROR), parray[wp].name);
    return COM_OK;
  }
  if (bp < 0) {
    pprintf(p, "%sNo player named %s is logged in.", 
            SendCode(p, ERROR), file_bplayer(param[0].val.string));
    return COM_OK;
  }
  if (parray[bp].game >= 0) {
    pprintf(p, "%s%s is currently playing a game.", 
                    SendCode(p, ERROR), parray[bp].name);
    return COM_OK;
  }

  if (p == bp) {
    if (!parray[wp].open) {
      pprintf(p, "%sYour opponent is not open for matches.\n%sGame failed to load.", SendCode(p, ERROR),SendCode(p, ERROR));
      return COM_OK;
    }
  } else {
    if (!parray[bp].open) {
      pprintf(p, "%sYour opponent is not open for matches.\n%sGame failed to load.", SendCode(p, ERROR),SendCode(p, ERROR));
      return COM_OK;
    }
  }
  sprintf(fname, "%s/%c/%s-%s", game_dir, parray[wp].login[0], parray[wp].login, parray[bp].login);
    Logit("Trying to load %s", fname);
  if ((stat(fname, &statbuf)) != 0) {
    pprintf(p, "%sThere is no stored game %s vs. %s", SendCode(p, ERROR), parray[wp].name,parray[bp].name);
    return COM_OK;
  }
  if(statbuf.st_size == 0) {
    pprintf(p, "%sThere is a serious problem with your game record.  This is sometimes", SendCode(p, ERROR));
    pprintf(p, "%scaused by an NNGS crash during your game.  ", SendCode(p, ERROR));
    pprintf(p, "%sWe apologize, but the game is lost.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = game_new(TYPE_GO,19);
  if (game_read(&garray[g], wp, bp) < 0) {
    pprintf(p, "%sThere is no stored game %s vs. %s", SendCode(p, ERROR), parray[wp].name,parray[bp].name);
    return COM_OK;
  }
  garray[g].white = wp;
  garray[g].black = bp;
  for (p3 = 0; p3 < p_num; p3++) {
    if ((p3 == wp) || (p3 == bp))
      continue;
    if (parray[p3].status != PLAYER_PROMPT)
      continue;
    if (player_find_pendfrom(p3, wp, PEND_MATCH) >= 0) {
      pprintf_prompt(p3, "%s%s is continuing a game with %s\n",
		     SendCode(p3, SHOUT),
		     parray[wp].name, parray[bp].name);
      player_remove_pendfrom(p3, wp, PEND_MATCH);
      player_remove_pendto(wp, p3, PEND_MATCH);
    }
    if (player_find_pendfrom(p3, bp, PEND_MATCH) >= 0) {
      pprintf_prompt(p3, "%s%s is continuing a game with %s\n",
		     SendCode(p3, SHOUT),
		     parray[bp].name, parray[wp].name);
      player_remove_pendfrom(p3, bp, PEND_MATCH);
      player_remove_pendto(bp, p3, PEND_MATCH);
    }
    if (player_find_pendto(p3, wp, PEND_MATCH) >= 0) {
      pprintf_prompt(p3, "%s%s is continuing a game with %s\n",
		     SendCode(p3, SHOUT),
		     parray[wp].name, parray[bp].name);
      player_remove_pendto(p3, wp, PEND_MATCH);
      player_remove_pendfrom(wp, p3, PEND_MATCH);
    }
    if (player_find_pendto(p3, bp, PEND_MATCH) >= 0) {
      pprintf_prompt(p3, "%s%s is continuing a game with %s\n",
		     SendCode(p3, SHOUT), 
		     parray[bp].name, parray[wp].name);
      player_remove_pendto(p3, bp, PEND_MATCH);
      player_remove_pendfrom(bp, p3, PEND_MATCH);
    }
  }
  player_remove_request(wp, bp, PEND_MATCH);
  player_remove_request(bp, wp, PEND_MATCH);
  garray[g].white = wp;
  garray[g].black = bp;
  garray[g].status = GAME_ACTIVE;
  garray[g].startTime = tenth_secs();
  garray[g].lastMoveTime = garray[g].startTime;
  garray[g].lastDecTime = garray[g].startTime;

  if (wp == p) {
    pprintf(bp, "%s%s has restored your old game.\n", 
            SendCode(bp, INFO), parray[wp].name);
    pprintf(bp, "%s%s has restarted your game.\n", 
            SendCode(bp, INFO), parray[wp].name);
    pprintf(wp, "%s%s has restarted your game.\n", 
            SendCode(wp, INFO), parray[wp].name);
  }
  else {
    pprintf(wp, "%s%s has restored your old game.\n", 
            SendCode(wp, INFO), parray[bp].name);
    pprintf(wp, "%s%s has restarted your game.\n", 
            SendCode(wp, INFO), parray[bp].name);
    pprintf(bp, "%s%s has restarted your game.\n", 
            SendCode(bp, INFO), parray[bp].name);
  }

  for (p3 = 0; p3 < p_num; p3++) {
    if ((p3 == wp) || (p3 == bp)) continue;
    if (parray[p3].status != PLAYER_PROMPT) continue;
    if (!parray[p3].i_game) continue;
    pprintf_prompt(p3, "%s{Game %d: %s vs %s @ Move %d}\n", SendCode(p3, SHOUT), 
                   g+1, parray[wp].name, parray[bp].name, 
                   movenum(garray[g].GoGame));
  } 
  parray[wp].game = g;
  parray[wp].opponent = bp;
  parray[wp].side = WHITE;
  parray[bp].game = g;
  parray[bp].opponent = wp;
  parray[bp].side = BLACK;
  parray[bp].state = PLAYING_GO;
  parray[wp].state = PLAYING_GO;
  if ((garray[g].Ladder9) || (garray[g].Ladder19)) {
    if (garray[g].size == 19) {
      if ((LadderPlayer=PlayerNamed(Ladder19,parray[bp].name)) != NULL) {
        bpos = LadderPlayer->idx;
      }
      else bpos = -1;
      if ((LadderPlayer=PlayerNamed(Ladder19,parray[wp].name)) != NULL) {
        wpos = LadderPlayer->idx;
      }
      else wpos = -1;
    }
    else if (garray[g].size == 9) {
      if ((LadderPlayer=PlayerNamed(Ladder9,parray[bp].name)) != NULL) {
        bpos = LadderPlayer->idx;
      }
      else bpos = -1;
      if ((LadderPlayer=PlayerNamed(Ladder9,parray[wp].name)) != NULL) {
        wpos = LadderPlayer->idx;
      }
      else wpos = -1;
    }
    if ((wpos < 0) || (bpos < 0) || (wpos > bpos)) {
      garray[g].Ladder9 = 0;
      garray[g].Ladder19 = 0;
    }
  }
  send_go_boards(g, 0);

  /* [PEM]: Added some time checks and automatic pausing. */
  if (garray[g].wTime < LOAD_TIME_WARNING &&
      (garray[g].Byo == 0 || garray[g].wInByo))
  {
    if (garray[g].bTime < LOAD_TIME_WARNING &&
	(garray[g].Byo == 0 || garray[g].bInByo))
    {				/* both */
      pprintf(wp, "%sBoth players have less than %d seconds left.\n",
	      SendCode(wp, INFO), LOAD_TIME_WARNING);
      pprintf(bp, "%sBoth players have less than %d seconds left.\n",
	      SendCode(bp, INFO), LOAD_TIME_WARNING);
    }
    else
    {				/* white */
      pprintf(wp, "%sYou have only %d seconds left.\n",
	      SendCode(wp, INFO), garray[g].wTime);
      pprintf(bp, "%sWhite has only %d seconds left.\n",
	      SendCode(bp, INFO), garray[g].wTime);
    }
    garray[g].clockStopped = 1;
  }
  else if (garray[g].bTime < LOAD_TIME_WARNING &&
	   (garray[g].Byo == 0 || garray[g].bInByo))
  {				/* black */
    pprintf(wp, "%sBlack has only %d seconds left.\n",
	    SendCode(wp, INFO), garray[g].bTime);
    pprintf(bp, "%sYou have only %d seconds left.\n",
	    SendCode(bp, INFO), garray[g].bTime);
    garray[g].clockStopped = 1;
  }
  if (garray[g].clockStopped)
  {
    pprintf(wp, "%sGame clock paused. Use \"unpause\" to resume.\n",
	    SendCode(wp, INFO));
    pprintf(bp, "%sGame clock paused. Use \"unpause\" to resume.\n",
	    SendCode(bp, INFO));
  }

  return COM_OK;
}

PUBLIC int com_stored(int p, param_list param)
{
  DIR *dirp;
  /*struct direct *dp;*/ /* EVR 11/11/1998 */
  struct dirent *dp;
  int p1, connected;
  char dname[MAX_FILENAME_SIZE];
  int count;

  if (param[0].type == TYPE_WORD) {
    if ((p1 = player_find_part_login(param[0].val.word)) < 0) {	/* not logged in */
      connected = 0;
      p1 = player_new();
      if (player_read(p1, param[0].val.word)) {
	player_remove(p1);
	pprintf(p, "%sThere is no player by that name.\n", SendCode(p, ERROR));
        pprintf(p, "%sFound 0 stored games.", SendCode(p, INFO));
	return COM_OK;
      }
    } else {
      connected = 1;
    }
  } else {
    p1 = p;
    connected = 1;
  }

  /*
  ** [PEM]: Rewrote this stuff to get the SendCodes right.
  */
  pprintf(p, "%sStored games for %s:", SendCode(p, INFO), parray[p1].name);

  sprintf(dname, "%s/%c", game_dir, parray[p1].login[0]);
  dirp = opendir(dname);
  if (!dirp) {
    pprintf(p, "\n%sFound 0 stored games.", SendCode(p, INFO));

    if (!connected) player_remove(p1);
    return COM_OK;
  }
  count = 0;
  for (dp = readdir(dirp); 
       dp != NULL; 
       dp = readdir(dirp)) {
    if (file_has_pname(dp->d_name, parray[p1].login)) {
      if (count % 3 == 0)
	pprintf(p, "\n%s", SendCode(p, STORED));
      pprintf(p, " %25s", dp->d_name);
      count++;
    }
  }

  closedir(dirp);
  if (count)
    pprintf(p, "\n%sFound %d stored games.", SendCode(p, INFO), count);
  else
    pprintf(p, "\n%sFound 0 stored games.", SendCode(p, INFO));

  if (!connected) player_remove(p1);
  return COM_OK;
}

PUBLIC int com_sgf(int p, param_list param)
{
  DIR *dirp;
/*  struct direct *dp; */ /* EVR 11/11/1998 */
  struct dirent *dp;
  char dname[MAX_FILENAME_SIZE];
  char pname[11];
  int count;

  if (param[0].type == TYPE_WORD) {
    strncpy(pname, param[0].val.word, 11);
  } else {
    /* Lowercase name */
    for (count = 0 ; count < 11 ; count++)
      pname[count] = tolower(parray[p].name[count]);
  }
  pname[10] = '\0';

  count = 0;
  sprintf(dname, "%s/%s", stats_dir, STATS_CGAMES);
  dirp = opendir(dname);
  if (!dirp) {
    pprintf(p, "%sPlayer %s has no SGF games.", 
           SendCode(p, ERROR), pname);
    return COM_OK;
  }
  pprintf(p,"%sCompleted games for %s:\n%s", SendCode(p, INFO), pname,
               SendCode(p, INFO));
  for (dp = readdir(dirp); dp != NULL; dp = readdir(dirp)) {
    if ((strstr(dp->d_name, pname)) != NULL) {
      pprintf(p, "%30s", dp->d_name);
      count++;
      if (count % 2 == 0)
        pprintf(p, "\n%s", SendCode(p, INFO));
    }
  }

  closedir(dirp);
  if (count) 
    pprintf(p,"\n%sFound %d completed games.", SendCode(p, INFO), count);

  return COM_OK;
}

PUBLIC int com_history(int p, param_list param)
{
  int p1 = p, connected = 1;
  char fname[MAX_FILENAME_SIZE];

  if (param[0].type == TYPE_WORD) {
    p1 = player_search(p, param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
      if(parray[p].registered == 0) return COM_OK;
    }
  } else {
    p1 = -1;
  }
  if (p1 >= 0) {
    if(parray[p].registered == 0) return COM_OK;
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, 
                    parray[p1].login[0], parray[p1].login, STATS_GAMES);
  } else {
    sprintf(fname, "%s/%s", stats_dir, STATS_GAMES);
  }
  pgames(p, fname);
  if (!connected) player_remove(p1);
  return COM_OKN;
}

PUBLIC int com_rhistory(int p, param_list param)
{
  int p1 = p, connected = 1;
  char fname[MAX_FILENAME_SIZE];

  if (param[0].type == TYPE_WORD) {
    p1 = player_search(p, param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {               /* player had to be connected and will be
                                   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
      if(parray[p].registered == 0) return COM_OK;
    }
  } else {
    p1 = -1;
  }
  if (p1 >= 0) {
    if(parray[p].registered == 0) return COM_OK;
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir,
                    parray[p1].login[0], parray[p1].login, STATS_RGAMES);
  } else {
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_RGAMES);
  }
  pgames(p, fname);
  if (!connected) player_remove(p1);
  return COM_OKN;
}


PUBLIC int com_time(int p, param_list param)
{
  int p1, g;

  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game < 0) {
      pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
      return COM_OK;
    }
    g = parray[p].game;
  } else if (param[0].type == TYPE_WORD) {
    p1 = player_find_part_login(param[0].val.word);
    if (p1 < 0) {
      pprintf(p, "%sNo user named \"%s\" is logged in.", 
                     SendCode(p, ERROR), param[0].val.word);
      return COM_OK;
    }
    g = parray[p1].game;
  } else {			/* Must be an integer */
    g = param[0].val.integer - 1;
  }
  if ((g < 0) || (g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
    return COM_NOSUCHGAME;
  }
  game_update_time(g);

  pprintf(p, "%sGame : %d\n", SendCode(p, TIM), g + 1);
  pprintf(p, "%sWhite(%s) : %d:%02d\n",
	  SendCode(p, TIM),
	  parray[garray[g].white].name,
	  garray[g].wTime / 60,
	  (garray[g].wTime % 60));
  pprintf(p, "%sBlack(%s) : %d:%02d",
	  SendCode(p, TIM),
	  parray[garray[g].black].name,
	  garray[g].bTime / 60,
	  (garray[g].bTime % 60 ));
  return COM_OK;
}

/*
** [PEM]: Changed this to a non-toggling command. See com_unfree() below.
*/

PUBLIC int com_free(int p, param_list param)
{
  int gmove;
  int g;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;

  gmove = movenum(garray[g].GoGame);
  if (gmove > 1) {
    pprintf(p, "%sOnly valid as your first move.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (garray[g].rated == 0)
    pprintf(p, "%sGame is already free. (Use \"unfree\" to change this.)\n",
          SendCode(p, INFO));
  else
  {
    garray[g].rated = 0;
    pprintf(p, "%sGame will NOT count toward ratings.\n",
          SendCode(p, INFO));
    pprintf(parray[p].opponent, "%sGame will NOT count toward ratings.\n",
          SendCode(parray[p].opponent, INFO));
  }
  return COM_OK;
}
 
/*
** [PEM]: Added this after several incidents where confusion about a game's
**        status arised.
*/
PUBLIC int com_unfree(int p, param_list param)
{
  int gmove;
  int g;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;

  gmove = movenum(garray[g].GoGame);
  if (gmove > 1) {
    pprintf(p, "%sOnly valid as your first move.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (garray[g].rated == 1)
    pprintf(p, "%sGame already counts toward ratings.\n",
          SendCode(p, INFO));
  else
  {
    garray[g].rated = 1;
    pprintf(p, "%sGame will count toward ratings.\n",
          SendCode(p, INFO));
    pprintf(parray[p].opponent, "%sGame will count toward ratings.\n",
          SendCode(parray[p].opponent, INFO));
  }
  return COM_OK;
}

#ifdef NOUSED
PUBLIC int com_nocaps(int p, param_list param)
{
  int gmove, nc;
  int g, nocap;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;

  gmove = movenum(garray[g].GoGame);
  if (gmove > 1) {
    pprintf(p, "%sOnly valid as your first move", SendCode(p, ERROR));
    return COM_OK;
  }
  nc = garray[g].nocaps = !garray[g].nocaps;
  setnocaps(garray[g].GoGame, garray[g].nocaps);
  pprintf(p, "%sCaptures %sabled", SendCode(p, INFO), nc ? "dis" : "en");
  return COM_OK;
}
#endif

PUBLIC int com_handicap(int p, param_list param)
{
  int gmove;
  int test, g, handicap;

  handicap = param[0].val.integer;

  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;

  gmove = movenum(garray[g].GoGame);
  if (gmove > 0) return COM_OK;
  
  if (handicap > 9) {
    pprintf(p, "%sUse 'handicap 9' then ask White to pass, please.", 
                SendCode(p, ERROR));
    return COM_OK;
  }

  if (handicap < 1) {
    pprintf(p, "%sUse 'handicap 1 - 9' please.", 
                SendCode(p, ERROR));
    return COM_OK;
  }
  
  test = sethcap(garray[g].GoGame, handicap);
  gmove = movenum(garray[g].GoGame);

  garray[g].onMove = WHITE;
  send_go_boards(g, 0);
#ifdef PAIR
  if (paired(g) && !movenum(garray[garray[g].pairwith].GoGame)) {
    com_handicap(garray[garray[g].pairwith].black, param);
    Logit("DUPLICATING handicap");
  }
#endif
  /* [PEM]: Point out that we change komi when setting handicap. */
  if (garray[g].komi != 0.5)
  {
    int p1 = parray[p].opponent;

    garray[g].komi = 0.5;
    pprintf(p, "%sKomi is now set to 0.5.", SendCode(p, INFO));
    pprintf(p1, "%sKomi is now set to 0.5.\n", SendCode(p1, INFO));
    for (p1 = 0; p1 < p_num; p1++)
    {
      if (parray[p1].status != PLAYER_PROMPT)
	continue;
      if (player_is_observe(p1, g))
	pprintf(p1, "%sKomi set to 0.5 in match %d.\n",
		SendCode(p1, INFO), g+1);
    }
  }
  return COM_OK;
}

PUBLIC int com_save(int p, param_list param)
{
  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  
  game_save(parray[p].game);
  pprintf(p, "%sGame saved.", SendCode(p, INFO));

  return COM_OK;
}


PUBLIC int com_moretime(int p, param_list param)
{
  int g, increment;

  increment = param[0].val.integer;
  if (increment <= 0) {
    pprintf(p, "%saddtime requires an integer value greater than zero.", 
                 SendCode(p, ERROR));
    return COM_OK;
  }
  if (parray[p].game < 0) {
    pprintf(p, "%sYou are not playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  g = parray[p].game;
  if (increment > 60000) {
    pprintf(p, "%saddtime has a maximum limit of 60000 minutes.", 
                SendCode(p, ERROR));
    increment = 60000;
  }
  if (garray[g].white == p) {
    garray[g].bTime += increment * 60;
#ifdef PAIR
    if(paired(g)) {
      garray[garray[g].pairwith].bTime += increment * 60;
    }
#endif
  }
  if (garray[g].black == p) {
    garray[g].wTime += increment * 60;
#ifdef PAIR
    if(paired(g)) {
      garray[garray[g].pairwith].wTime += increment * 60;
    }
#endif
  }
  pprintf(p, "%s%d minutes were added to your opponents clock",
	    SendCode(p, INFO),
	    increment);
#ifdef PAIR
  if(paired(g)) {
    if(p == garray[g].black) {
      pprintf_prompt(garray[garray[g].pairwith].black, 
                     "%s%d minutes were added to your opponents clock\n",
                     SendCode(garray[garray[g].pairwith].black, INFO), 
                     increment);
    } else {
      pprintf_prompt(garray[garray[g].pairwith].white, 
                     "%s%d minutes were added to your opponents clock\n",
                     SendCode(garray[garray[g].pairwith].white, INFO), 
                     increment);
    }
  }
#endif /* PAIR */

  pprintf_prompt(parray[p].opponent,
	   "%sYour opponent has added %d minutes to your clock.\n",
	   SendCode(parray[p].opponent, INFO),
	   increment);

#ifdef PAIR
  if(paired(g)) {
    if(p == garray[g].black) {
      pprintf_prompt(garray[garray[g].pairwith].white, 
                     "%sYour opponent has added %d minutes to your clock.\n",
                     SendCode(garray[garray[g].pairwith].white, INFO), 
                     increment);
    } else {
      pprintf_prompt(garray[garray[g].pairwith].black, 
                     "%sYour opponent has added %d minutes to your clock.\n",
                     SendCode(garray[garray[g].pairwith].black, INFO), 
                     increment);
    }
  }
#endif /* PAIR */

  return COM_OK;
}

PUBLIC void game_update_time(int g)
{
  unsigned now, timesince;
  int wp, bp, gmove;

  /* If players have "paused" the game */
  if (garray[g].clockStopped) return;

  /* If players have time controls of 0 0 (untimed) */
  if (garray[g].time_type == TYPE_UNTIMED) {
    garray[g].bTime = 480;
    garray[g].wTime = 480;
    garray[g].bInByo = 0;
    garray[g].wInByo = 0;
    return;
  }

  /* If a teaching game */
  if ((garray[g].Teach == 1) || (garray[g].Teach2 == 1)) {
    garray[g].bTime = 600;
    garray[g].wTime = 600;
    garray[g].bInByo = 0;
    garray[g].wInByo = 0;
    return;
  }

  /* Courtesy to allow hcap setup, etc.... */
  gmove = movenum(garray[g].GoGame);
  if(gmove < 2) return;

  bp = garray[g].black;
  wp = garray[g].white;

  /* If players are scoring */
  if((parray[wp].state == SCORING) || (parray[bp].state == SCORING)) {
    return;
  }
 
  now = tenth_secs();
  timesince = now - garray[g].lastDecTime;

  /* Game over, ran out of time! */
  if (((garray[g].wTime < 1) && (garray[g].wByoStones > 1))) {
#ifdef PAIR
    if (paired(g)) {
      game_ended(garray[g].pairwith, garray[garray[g].pairwith].black, END_FLAG);
    }
#endif /* PAIR */
    game_ended(g, garray[g].black, END_FLAG);
  }
  else if (((garray[g].bTime < 1) && (garray[g].bByoStones > 1))) {
#ifdef PAIR
    if (paired(g)) {
      game_ended(garray[g].pairwith, garray[garray[g].pairwith].white, END_FLAG);
    }
#endif /* PAIR */
    game_ended(g, garray[g].white, END_FLAG);
  }
  if (garray[g].onMove == WHITE) {
    garray[g].wTime -= (timesince / 10);
#ifdef PAIR
    if(paired(g)) {
      garray[garray[g].pairwith].wTime = garray[g].wTime;
    }
#endif /* PAIR */
    if ((garray[g].wTime <= 0) && (garray[g].wInByo == 0)) {
      garray[g].wInByo = 1;
      garray[g].wByoStones = garray[g].ByoS - 1;
      garray[g].wTime = garray[g].Byo;
      pprintf_prompt(garray[g].white, "%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
                SendCode(garray[g].white, INFO), parray[garray[g].white].name,
                SendCode(garray[g].white, INFO), garray[g].wByoStones,
                garray[g].wTime/60);
      pprintf_prompt(garray[g].black, "%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
                SendCode(garray[g].black, INFO), parray[garray[g].white].name,
                SendCode(garray[g].black, INFO), garray[g].wByoStones,
                garray[g].wTime/60);
    }
    else if (garray[g].wByoStones > 0) {
      if (garray[g].wByoStones == 0) {
        garray[g].wByoStones = garray[g].ByoS - 1;
        garray[g].wTime = garray[g].Byo;
      }
    }
#ifdef PAIR
    if(paired(g)) {
    if ((garray[garray[g].pairwith].wTime <= 0) && (garray[garray[g].pairwith].wInByo == 0)) {
      garray[garray[g].pairwith].wInByo = 1;
      garray[garray[g].pairwith].wByoStones = garray[garray[g].pairwith].ByoS - 1;
      garray[garray[g].pairwith].wTime = garray[garray[g].pairwith].Byo;
      pprintf_prompt(garray[garray[g].pairwith].white, 
"%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
         SendCode(garray[garray[g].pairwith].white, INFO), 
                  parray[garray[garray[g].pairwith].white].name,
         SendCode(garray[garray[g].pairwith].white, INFO), 
                  garray[garray[g].pairwith].wByoStones,
                  garray[garray[g].pairwith].wTime/60);
      pprintf_prompt(garray[garray[g].pairwith].black, 
"%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
         SendCode(garray[garray[g].pairwith].black, INFO), 
                  parray[garray[garray[g].pairwith].white].name,
         SendCode(garray[garray[g].pairwith].black, INFO), 
                  garray[garray[g].pairwith].wByoStones,
                  garray[garray[g].pairwith].wTime/60);
    }
    else if (garray[garray[g].pairwith].wByoStones > 0) {
      if (garray[garray[g].pairwith].wByoStones == 0) {
        garray[garray[g].pairwith].wByoStones = garray[garray[g].pairwith].ByoS - 1;
        garray[garray[g].pairwith].wTime = garray[garray[g].pairwith].Byo;
      }
    }
    }
#endif /* PAIR */

  } else {  /* onMove == BLACK */
    garray[g].bTime -= (timesince / 10);
#ifdef PAIR
    if(paired(g)) {
      garray[garray[g].pairwith].bTime = garray[g].bTime;
    }
#endif /* PAIR */
    if ((garray[g].bTime <= 0) && (garray[g].bInByo == 0)) {
      garray[g].bInByo = 1;
      garray[g].bByoStones = garray[g].ByoS - 1;
      garray[g].bTime = garray[g].Byo;
      pprintf_prompt(garray[g].white, "%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
                SendCode(garray[g].white, INFO), parray[garray[g].black].name,
                SendCode(garray[g].white, INFO), garray[g].bByoStones,
                garray[g].bTime/60);
      pprintf_prompt(garray[g].black, "%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
                SendCode(garray[g].black, INFO), parray[garray[g].black].name,
                SendCode(garray[g].black, INFO), garray[g].bByoStones,
                garray[g].bTime/60);
    }
    else if (garray[g].bByoStones > 0) {
      if (garray[g].bByoStones == 0) {
        garray[g].bByoStones = garray[g].ByoS - 1;
        garray[g].bTime = garray[g].Byo;
      }
    }
#ifdef PAIR
    if(paired(g)) {
    if ((garray[garray[g].pairwith].bTime <= 0) && (garray[garray[g].pairwith].bInByo == 0)) {
      garray[garray[g].pairwith].bInByo = 1;
      garray[garray[g].pairwith].bByoStones = garray[garray[g].pairwith].ByoS - 1;
      garray[garray[g].pairwith].bTime = garray[garray[g].pairwith].Byo;
      pprintf_prompt(garray[garray[g].pairwith].white, 
"%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
         SendCode(garray[garray[g].pairwith].white, INFO), 
         parray[garray[garray[g].pairwith].black].name,
         SendCode(garray[garray[g].pairwith].white, INFO), 
         garray[garray[g].pairwith].bByoStones,
         garray[garray[g].pairwith].bTime/60);
      pprintf_prompt(garray[garray[g].pairwith].black, 
"%sThe player %s is now in byo-yomi.\n%sYou have %d stones and %d minutes\n",
         SendCode(garray[garray[g].pairwith].black, INFO), 
         parray[garray[garray[g].pairwith].black].name,
         SendCode(garray[garray[g].pairwith].black, INFO), 
         garray[garray[g].pairwith].bByoStones,
         garray[garray[g].pairwith].bTime/60);
    }
    else if (garray[garray[g].pairwith].bByoStones > 0) {
      if (garray[garray[g].pairwith].bByoStones == 0) {
        garray[garray[g].pairwith].bByoStones = garray[garray[g].pairwith].ByoS - 1;
        garray[garray[g].pairwith].bTime = garray[garray[g].pairwith].Byo;
      }
    }
    }
#endif /* PAIR */
  }
  garray[g].lastDecTime = now;
#ifdef PAIR
  if(paired(g)) 
    garray[garray[g].pairwith].lastDecTime = now;
#endif /* PAIR */    
}

PUBLIC void game_update_times()
{
  int g;

  for (g = 0; g < g_num; g++) {
    if (garray[g].status != GAME_ACTIVE)
      continue;
    if (garray[g].clockStopped)
      continue;
    game_update_time(g);
  }
}

PUBLIC int com_sresign(int p, param_list param)
{
  int wp, bp, g, bcon, wcon, oldwstate, oldbstate;
  int old_w_game, old_b_game;
  struct stat statbuf;

  char fname[MAX_FILENAME_SIZE];
  char *wname, *bname;

  bname = file_bplayer(param[0].val.string);
  wname = file_wplayer(param[0].val.string);

  if ((int) strlen(bname) < 2) return COM_BADPARAMETERS;
  if ((int) strlen(wname) < 2) return COM_BADPARAMETERS;

  if ((wp = player_find_part_login(file_wplayer(param[0].val.string))) < 0) {
    wcon = 0;
    wp = player_new();
    if (player_read(wp, file_wplayer(param[0].val.string))) {
      player_remove(wp);
      pprintf(p, "%sThere is no player by that name.", SendCode(p, ERROR));
      return COM_OK;
    }
    parray[wp].status = PLAYER_LOOKING;
  } else {
      wcon = 1;
  }
  if ((bp = player_find_part_login(file_bplayer(param[0].val.string))) < 0) {
    bcon = 0;
    bp = player_new();
    if (player_read(bp, file_bplayer(param[0].val.string))) {
      player_remove(bp);
      pprintf(p, "%sThere is no player by that name.", SendCode(p, ERROR));
      return COM_OK;
    }
    parray[bp].status = PLAYER_LOOKING;
  } else {
      bcon = 1;
  }
  
  if((p != wp) && (p != bp)) {
    pprintf(p, "%sYou must be one of the two players to sresign.",
                SendCode(p, ERROR));
    if (!wcon) player_remove(wp);
    if (!bcon) player_remove(bp);
    return COM_OK;
  }

  oldwstate = parray[wp].state;
  oldbstate = parray[bp].state;
  old_w_game = parray[wp].game;
  old_b_game = parray[bp].game;

  sprintf(fname, "%s/%c/%s-%s", game_dir, wname[0], wname, bname);
  if ((stat(fname, &statbuf)) != 0) {
    pprintf(p, "%sThere is no stored game %s vs. %s", 
               SendCode(p, ERROR), parray[wp].name,parray[bp].name);
    if (!wcon) player_remove(wp);
    if (!bcon) player_remove(bp);
    return COM_OK;
  }
  g = game_new(TYPE_GO,19);
  if (game_read(&garray[g], wp, bp) < 0) {
    pprintf(p, "%sThere is no stored game %s vs. %s (Hmmmmm)", 
           SendCode(p, ERROR), parray[wp].name,parray[bp].name);
    game_remove(&garray[g]);
    parray[wp].state = oldwstate;
    parray[bp].state = oldbstate;
    if (!wcon) player_remove(wp);
    if (!bcon) player_remove(bp);
    return COM_OK;
  }
  game_ended(g, (p == wp) ? bp : wp, END_RESIGN);
  if (!wcon) player_remove(wp);
  if (!bcon) player_remove(bp);
  if(wcon) {
    parray[wp].state = oldwstate;
    parray[wp].game = old_w_game;
  }
  if(bcon) {
    parray[bp].state = oldbstate;
    parray[bp].game = old_b_game;
  }

  return COM_OKN;
}

PUBLIC int com_look(int p, param_list param)
{
  int wp, bp, g, bcon, wcon, x, until, wc, bc, oldwstate, oldbstate;
  struct stat statbuf;
  twodstring statstring;

  char fname[MAX_FILENAME_SIZE];
  char *wname, *bname;

  until = 0;

  bname = file_bplayer(param[0].val.string);
  wname = file_wplayer(param[0].val.string);

  if ((int) strlen(bname) < 2) return COM_BADPARAMETERS;
  if ((int) strlen(wname) < 2) return COM_BADPARAMETERS;

  if ((wp = player_find_part_login(file_wplayer(param[0].val.string))) < 0) {
    wcon = 0;
    wp = player_new();
    if (player_read(wp, file_wplayer(param[0].val.string))) {
      player_remove(wp);
      pprintf(p, "%sThere is no player by that name.", SendCode(p, ERROR));
      return COM_OK;
    }
    parray[wp].status = PLAYER_LOOKING;
  } else {
      wcon = 1;
  }
  if ((bp = player_find_part_login(file_bplayer(param[0].val.string))) < 0) {
    bcon = 0;
    bp = player_new();
    if (player_read(bp, file_bplayer(param[0].val.string))) {
      player_remove(bp);
      pprintf(p, "%sThere is no player by that name.", SendCode(p, ERROR));
      return COM_OK;
    }
    parray[bp].status = PLAYER_LOOKING;
  } else {
      bcon = 1;
  }
  
  oldwstate = parray[wp].state;
  oldbstate = parray[bp].state;

  sprintf(fname, "%s/%c/%s-%s", game_dir, wname[0], wname, bname);
  if ((stat(fname, &statbuf)) != 0) {
    pprintf(p, "%sThere is no stored game %s vs. %s", 
               SendCode(p, ERROR), parray[wp].name,parray[bp].name);
    if (!wcon) player_remove(wp);
    if (!bcon) player_remove(bp);
    return COM_OK;
  }
  g = game_new(TYPE_GO,19);
  if (game_read(&garray[g], wp, bp) < 0) {
    pprintf(p, "%sThere is no stored game %s vs. %s (Hmmmmm)", 
           SendCode(p, ERROR), parray[wp].name,parray[bp].name);
    game_remove(&garray[g]);
    parray[wp].state = oldwstate;
    parray[bp].state = oldbstate;
    if (!wcon) player_remove(wp);
    if (!bcon) player_remove(bp);
    return COM_OK;
  }
  if (parray[p].i_verbose) send_go_board_to(g, p);
  else {
    getcaps(garray[g].GoGame, &wc, &bc);
    boardstatus(garray[g].GoGame, statstring);
    pprintf(p, "%s%s %3.3s%s %d %d %d T %.1f %d\n",
                SendCode(p, STATUS),
                parray[wp].name,
                parray[wp].srank,
                parray[wp].rated ? "*" : " ",
                wc,
                garray[g].wTime,
                garray[g].wByoStones,
                garray[g].komi,
                garray[g].GoGame->handicap);
    pprintf(p, "%s%s %3.3s%s %d %d %d T %.1f %d\n",
                SendCode(p, STATUS),
                parray[bp].name,
                parray[bp].srank,
                parray[bp].rated ? "*" : " ",
                bc,
                garray[g].bTime,
                garray[g].bByoStones,
                garray[g].komi,
                garray[g].GoGame->handicap);
    if (!until) until = garray[g].GoGame->height;
    for(x = 0; x < until; x++) {
      pprintf(p, "%s%2d: %s\n",
                  SendCode(p, STATUS),
                  x,
                  statstring[x]);
    }
  }
  game_remove(&garray[g]);
  if (!wcon) player_remove(wp);
  if (!bcon) player_remove(bp);
  return COM_OKN;
}

PUBLIC int com_problem(int p, param_list param)
{
  int g, x, until, wc, bc;
  struct stat statbuf;
  twodstring statstring;
  int problem = 0;
  FILE *fp;

  char fname[MAX_FILENAME_SIZE];

  until = 0;

  if(param[0].type == (int) TYPE_NULL) {
    problem = parray[p].last_problem + 1;
    parray[p].last_problem++;
  }
  else
    problem = param[0].val.integer;

  sprintf(fname, "%s/xxqj%d.sgf", problem_dir, problem);
  if ((stat(fname, &statbuf)) != 0) {
    pprintf(p, "%sThere is no problem number %d",SendCode(p, ERROR), problem);
    return COM_OK;
  }
  g = game_new(TYPE_GO,19);

  fp = fopen(fname, "r");
  if (!fp) {
    Logit("File not found, %s", fname);
    pprintf(p, "%sThere is no problem %d (Hmmmmm)",SendCode(p, ERROR),problem);
    game_remove(&garray[g]);
    return COM_OK;
  }
  garray[g].size = 19;
  garray[g].GoGame = initminkgame(19,19, RULES_NET);
  garray[g].wTime = 600;
  garray[g].bTime = 600;
  garray[g].Title = (char *) strdup("Black to play");
  garray[g].white = p;
  garray[g].black = p;
  loadpos(fp, garray[g].GoGame);
  fclose(fp);

  if (parray[p].i_verbose) send_go_board_to(g, p);
  else {
    getcaps(garray[g].GoGame, &wc, &bc);
    boardstatus(garray[g].GoGame, statstring);
    pprintf(p, "%s%s %3.3s%s %d %d %d T %.1f %d\n",
                SendCode(p, STATUS),
                parray[p].name,
                parray[p].srank,
                parray[p].rated ? "*" : " ",
                wc,
                garray[g].wTime,
                garray[g].wByoStones,
                garray[g].komi,
                garray[g].GoGame->handicap);
    pprintf(p, "%s%s %3.3s%s %d %d %d T %.1f %d\n",
                SendCode(p, STATUS),
                parray[p].name,
                parray[p].srank,
                parray[p].rated ? "*" : " ",
                bc,
                garray[g].bTime,
                garray[g].bByoStones,
                garray[g].komi,
                garray[g].GoGame->handicap);
    if (!until) until = garray[g].GoGame->height;
    for(x = 0; x < until; x++) {
      pprintf(p, "%s%2d: %s\n",
                  SendCode(p, STATUS),
                  x,
                  statstring[x]);
    }
  }
  game_remove(&garray[g]);
  return COM_OKN;
}
