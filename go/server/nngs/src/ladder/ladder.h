/* -*-C-*-

   ladder.h -- go players list stuff

   Copyright 1995 J. Alan Eldridge.	

   %G% %W%
*/
#ifndef players_h
#define players_h

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif /* C++ */

typedef struct {
  int idx;
  char *szName;
  time_t tLast;
  void *pvUser;
  int nWins, nLosses;
} Player;

void LadderInit(int nLadders);

int LadderNew(int maxPlayers);
void LadderDel(int id);

int LadderCnt(int id);

const Player *PlayerAt(int id, int at);

void PlayerUpdTime(int id, int at, time_t t);
void PlayerSetData(int id, int at, void *pv);
void PlayerAddWin(int id, int at);
void PlayerAddLoss(int id, int at);

void PlayerKillAt(int id, int at);
void PlayerRotate(int id, int from, int to);

const Player *PlayerNamed(int id, const char *psz);

const Player * const *PlayersSortedByPosn(int id); /* just the array, in order,
						 with a lot of const shit so
						 you can't f*** it up */

const Player * const *PlayersSortedByName(int id);

int PlayerNew(int id, const char *szName); /* add at end - return 0 if full */

void PlayerDump(FILE *pf, const Player *p);

int PlayerSave(FILE *pf, int id);
int PlayerLoad(FILE *pf, int id);

#ifdef __cplusplus
}
#endif /* C++ */

#endif /* ifndef players_h */
