#include <stdio.h>
#include "ladder.h"
#include "../stdinclude.h"
#include "../common.h"
#include "../ficsmain.h"
#include "../config.h"
#include "../utils.h"


int Ladder9, Ladder19, num_19, num_9, i;

const Player *LadderPlayer;

FILE *fp;

main()
{
  LadderInit(NUM_LADDERS);
  Ladder9 = LadderNew(LADDERSIZE);
  Ladder19 = LadderNew(LADDERSIZE);
  fp = fopen(LADDER9, "r");
  if(fp) {
    num_9 = PlayerLoad(fp, Ladder9);
    fclose(fp);
  }
  num_19 = 0;
  fp = fopen(LADDER19, "r");
  if(fp) {
    num_19 = PlayerLoad(fp, Ladder19);
    fclose(fp);
  }

  printf("<!DOCTYPE htmlplus PUBLIC \"-//Internet/RFC xxxx//EN\">\
<HTMLPLUS> <HEAD> <TITLE>NNGS Ladder Stats</TITLE> </HEAD> <BODY> <HR>\
<B>19 x 19 ladder:</B><PRE>\n\n");

  printf("Position      Name       W    L       Date last played\n");
  printf("--------  -----------   ---  ---  -------------------------\n");

  for(i = 0; i < num_19; i++) {
    LadderPlayer = PlayerAt(Ladder19, i);
    printf(" %3d      %-10s    %3d  %3d  %s\n",
            i+1,
            LadderPlayer->szName,
            LadderPlayer->nWins, LadderPlayer->nLosses,
            strgtime(&LadderPlayer->tLast));
  }

  printf("\n<PRE><P><B>9 x 9 ladder:</B><PRE>\n\n");

  printf("Position      Name       W    L       Date last played\n");
  printf("--------  -----------   ---  ---  -------------------------\n");

  for(i = 0; i < num_9; i++) {
    LadderPlayer = PlayerAt(Ladder9, i);
    printf(" %3d      %-10s    %3d  %3d  %s\n",
            i+1,
            LadderPlayer->szName,
            LadderPlayer->nWins, LadderPlayer->nLosses,
            strgtime(&LadderPlayer->tLast));
  }

  printf("\n</PRE><P><B>WebMaster: </B>\
<ADDRESS>geek@imageek.york.cuny.edu</ADDRESS>\
</BODY>\n");
}

char *dayarray[] =
{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};


char *montharray[] =
{"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug",
"Sep", "Oct", "Nov", "Dec"};


char *strtime(struct tm * stm)
{
  static char tstr[100];

  sprintf(tstr, "%s %3.3s %2d %02d:%02d:%02d 19%2d",
          dayarray[stm->tm_wday],
          montharray[stm->tm_mon],
          stm->tm_mday,
          stm->tm_hour,
          stm->tm_min,
          stm->tm_sec,
          stm->tm_year);
  return tstr;
}

char *strgtime(time_t * clock)
{
  struct tm *stm = gmtime(clock);
  return strtime(stm);
}
