#include <stdio.h>
#include "ladder.h"

int main(void)
{
  int id, n, i;
  time_t now;
  const Player *LP, *LP2;

  LadderInit(2);

  id = LadderNew(500);

  for (;;) {
    char sz[100];
    char szCmd[10], szName[100];

    gets(sz);
    sscanf(sz, "%s %s", szCmd, szName);
    if (!strcmp(szCmd, "p")) {
      PlayerSave(stdout, id);
    } else if (!strcmp(szCmd, "r")) {
      int l, h;
      sscanf(szName, "%d.%d", &l, &h);
      PlayerRotate(id, l, h);
      PlayerSave(stdout, id);
    } else if (!strcmp(szCmd, "k")) {
      int i = atoi(szName);
      PlayerKillAt(id, i);
      PlayerSave(stdout, id);
    } else if (!strcmp(szCmd, "a")) {
      PlayerNew(id, szName);
      PlayerSave(stdout, id);
    } else if (!strcmp(szCmd, "n")) {
      int i, lim = PlayerCnt(id);
      const Player * const * ppp = PlayersSortedByName(id);

      for (i = 0; i < lim; i++) {
	PlayerDump(stdout, ppp[i]);
      }
    } else if (!strcmp(szCmd, "s")) {
      FILE *pf = fopen(szName, "w");

      if (pf) {
	int n = PlayerSave(pf, id);
	printf("%d players saved to file '%s'\n", n, szName);
	fclose(pf);
      } else {
	printf("Can't save to file '%s'\n", szName);
      }
    } else if (!strcmp(szCmd, "l")) {
      FILE *pf = fopen(szName, "r");

      if (pf) {
	n = PlayerLoad(pf, id);
	printf("%d players loaded from file '%s'\n", n, szName);
	fclose(pf);
      } else {
	printf("Can't load from file '%s'\n", szName);
      }
    } else if (!strcmp(szCmd, "z")) {
      PlayerSift(id, 7);
    }
  }
}
