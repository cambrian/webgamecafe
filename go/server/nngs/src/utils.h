/* utils.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995  Erik Van Riper (geek@imageek.york.cuny.edu)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <time.h>
#include "multicol.h"
#include "bm.h"

#define MAX_WORD_SIZE 1024

/* Maximum length of an output line */
#define MAX_LINE_SIZE 1024

/* Maximum size of a filename */
#ifdef FILENAME_MAX
#  define MAX_FILENAME_SIZE FILENAME_MAX
#else
#  define MAX_FILENAME_SIZE 1024
#endif

extern int iswhitespace(int);
extern char *getword(char *);
/* Returns a pointer to the first whitespace in the argument */
extern char *eatword(char *);
/* Returns a pointer to the first non-whitespace char in the argument */
extern char *eatwhite(char *);
/* Returns the next word in a given string >eatwhite(eatword(foo))< */
extern char *nextword(char *);

extern int mail_string_to_address(char *, char *, char *);
extern int mail_string_to_user(int, char *);
extern int pcommand(int, char *, ...);
extern int pprintf(int, char *, ...);
extern int pprintf2(int, int, char *);
extern int Logit(char *, ...);
extern int pprintf_highlight(int, char *, ...);
extern int psprintf_highlight(int, char *, char *, ...);
extern int pprintf_prompt(int, char *, ...);
extern int pprintf_prompt_noformat(int, char *, ...);
extern int pprintf_noformat(int, char *, ...);
extern int psend_raw_file(int, char *, char *);
extern int psend_file(int, char *, char *);
extern int pmore_file(int);
extern int pmail_file(int, char *, char *);
extern int psend_command(int, char *, char *);

extern char *stolower(char *);
extern char *stoupper(char *);

extern int safechar(int);
extern int safestring(char *);
extern int safefilename(char *path);
extern int alphastring(char *);
extern int printablestring(char *);
extern char *strdup2(char *);

extern char *hms(int, int, int, int);
extern char *newhms(int);
extern char *strhms(int);
extern char *DTdate(struct tm *);
extern char *strDTtime(time_t *);
extern char *ResultsDate(char *);
extern char *strltime(time_t *);
extern char *strgtime(time_t *);
extern char *strgmtime(time_t *);
extern unsigned tenth_secs(void);
extern char *tenth_str(unsigned int, int);
extern int untenths(unsigned int);
extern int do_copy(char *, char *, int);

extern int truncate_file(char *, int);
extern int lines_file(char *);

extern int file_has_pname(char *, char*);
extern char *file_wplayer(char *);
extern char *file_bplayer(char *);

extern char *dotQuad(unsigned int);

extern int available_space();
extern int file_exists(char *);

extern int search_directory(char *, char *, char *, int);
extern int display_directory(int, char *, int);
extern char *SendCode(int, int);
extern char *KillTrailWhiteSpace(char *str);
extern char *strlwr(char *psz);
extern void bldSD(char *psz);


#define USER 1
#define HOST 2

typedef struct {
  int where;
  BoyerMoore bmData;
} SearchData;

typedef struct {
  char szPlayer[100];
  char szMailAddr[200];
} SearchResult;

extern const SearchResult *search(char *);
extern int blank(char *);

#endif /* _UTILS_H */

