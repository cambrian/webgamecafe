/* #[info:		*/
/************************************************************************
 *									*
 *	    	   intergo --- An online Go Dictionary			*
 *									*
 *			    Jan van der Steen				*
 *		       Amsterdam, the Netherlands			*
 *									*
 *----------------------------------------------------------------------*
 * File    : interface.c 						*
 * Purpose : Interface to the godict database library			*
 * Version : 1.6 						*
 * Modified: 12/20/93 18:03:52						*
 * Author  : Jan van der Steen (jansteen@cwi.nl) 			*
 ************************************************************************/
/* #]info:		*/ 
/* #[include:		*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "godict.h"
#include "io.h"

/* #]include:		*/ 
/* #[define:		*/

#define MAXPAT	1024	/* Maximum pattern request */

/* #]define:		*/ 
/* #[static:		*/

static int	interactive = 0;
static int	verbose	    = 0;
static int types  = ~0;
 static int	langs	    = ~0;	/* No languages	*/

/*
 * For our own getopt()
 */
static int	optind = 1;		/* index into argv vector	*/
static char *	optarg;			/* option associated argument	*/


/* #]static:		*/ 
/* #[prototype:		*/

#ifdef __STDC__
#	define	PROTO(s) s
#else
#	define	PROTO(s) ()
#endif

static void	usage		PROTO((int exitcode));
static int	mygetopt	PROTO((int argc, char **argv, char *s));
static void	getoptions	PROTO((int argc, char **argv));
static void	addtype		PROTO((char *s));
static void	addlang		PROTO((char *s));

/* #]prototype:		*/ 

/* #[main:		*/

void
main(argc, argv)
int    argc;
char **argv;
{
    GODICT *d;			/* The Go Dictionary	*/
    FOUND   found;		/* What did we find?	*/

    /*
     * Get the command line options
     */
    getoptions(argc, argv);

    /*
     * Pass the settings to the library
     */
    dict_types   (types);
    dict_langs   (langs);
    dict_verbose (verbose);
    dict_interact(interactive);

    /*
     * Load the Go Dictionary
     */
    if ((d = dict_load()) == (GODICT *) 0) {
	msg(MSG_SYS_ERR, "Couldn't open the Go dictionary\n");
	exit(1);
    }

    if (interactive) {
	/*
	 * Interactive search
	 */
	char pattern[MAXPAT];	/* Pattern to look for	*/

	while (msg(MSG_USR_OUT, DPROMPT), fgets(pattern, MAXPAT, stdin)) {
	    char *s;

	    if ((s = strchr(pattern, '\n')) != NULL) *s = 0;
	    msg(MSG_USR_OUT, "\n");
	    dict_retrieve(d, lstr(pattern), &found);
	    msg(MSG_USR_OUT, "\n");
	}
    }
    else {
	/*
	 * No keys, then usage
	 */
	if (optind == argc) usage(0);

	while (optind < argc) {
	    dict_retrieve(d, lstr(argv[optind++]), &found);
	    msg(MSG_USR_OUT, "\n");
	}
    }

    exit(!found.fnd_records);
}

/* #]main:		*/ 

/* #[addtype:		*/

static void
addtype(s)
char *s;
{
    while (*s) switch(*s++) {
	case 'm': types |= CD_MISC; break;	/* Miscellaneous	*/
	case 't': types |= CD_TECH; break;	/* Technical		*/
	case 'p': types |= CD_POLI; break;	/* Conversation		*/
	case 'd': types |= CD_DIGI; break;	/* Numbers, counting	*/
	case 'c': types |= CD_CHAM; break;	/* Championship names	*/
	case 'n': types |= CD_NAME; break;	/* Player names		*/
	case 'a': types  = ~0;	     break;	/* All types...		*/
	default :		     break;	/* Ignore it...		*/
    }
}

/* #]addtype:		*/ 
/* #[addlang:		*/

static void
addlang(s)
char *s;
{
    if (!strncmp(s, "jp", 2)) { langs |= LANG_JP; return; } /* Japanese	*/
    if (!strncmp(s, "ch", 2)) { langs |= LANG_CH; return; } /* Chinese	*/
    if (!strncmp(s, "rk", 2)) { langs |= LANG_RK; return; } /* Korean	*/
    if (!strncmp(s, "gb", 2)) { langs |= LANG_GB; return; } /* English	*/
    if (!strncmp(s, "nl", 2)) { langs |= LANG_NL; return; } /* Dutch	*/
    if (!strncmp(s, "ge", 2)) { langs |= LANG_GE; return; } /* German	*/
    if (!strncmp(s, "fr", 2)) { langs |= LANG_FR; return; } /* France	*/
    if (!strncmp(s, "sv", 2)) { langs |= LANG_SV; return; } /* Swedish	*/
    if (!strncmp(s, "it", 2)) { langs |= LANG_IT; return; } /* Italian	*/
    if (!strncmp(s, "dg", 2)) { langs |= LANG_DG; return; } /* Diagrams	*/
    if (!strncmp(s, "cp", 2)) { langs |= LANG_CP; return; } /* Captions	*/
    if (!strncmp(s, "a" , 1)) { langs  = ~0;	 return; } /* All types	*/

    return;						   /* Ignore...	*/
}

/* #]addlang:		*/ 
/* #[mygetopt:		*/

#define	ARGCH		(int)':'
#define BADCH		(int)'?'
#define EMSG		""
#define tell(c,s)	fputs(*nargv, stderr);	\
			fputs(     s, stderr);	\
			fputc(     c, stderr);	\
			fputc(  '\n', stderr);	\
			return(BADCH);

static int
mygetopt(nargc,nargv,ostr)
int	   nargc;
char	** nargv;
char	*  ostr;
{
    static char	*place = EMSG;	/* option letter processing */
    char	*oli;		/* option letter list index */
    int		 optopt;

    if (*place == 0) {		/* update scanning pointer */
	if (optind >= nargc			||
	    *(place = nargv[optind]) != '-'	||
	    ! *++place) return(EOF);
	if (*place == '-') {	/* found "--" */
		++optind;
		return(EOF);
	}
    }				/* option letter okay? */
    if ((optopt = (int)*place++) == ARGCH ||
	(oli = strchr(ostr, optopt)) == 0)   {
	if (*place == 0) ++optind;
	tell(optopt, ": illegal option -- ");
    }
    if (*++oli != ARGCH) {		/* don't need argument */
	optarg = NULL;
	if (*place == 0) ++optind;
    }
    else {				/* need an argument */
	if (*place) optarg = place;	/* no white space */
	else if (nargc <= ++optind) {	/* no arg */
		place = EMSG;
		tell(optopt, ": option requires an argument -- ");
	}
 	else optarg = nargv[optind];	/* white space */
	place = EMSG;
	++optind;
    }
    return(optopt);			/* dump back option letter */
}

/* #]mygetopt:		*/ 
/* #[getoptions:	*/

#define OPTIONS "divhl:t:"

static void
getoptions(argc, argv)
int    argc;
char **argv;
{
    int	c;

    while ((c = mygetopt(argc, argv, OPTIONS)) != EOF)
    switch (c) {
	case 'd': langs &= ~(LANG_DG|LANG_CP);	break;
	case 'i': interactive = 1;		break;
	case 'v': verbose     = 1;		break;
	case 'h': usage(0);			break;
	case 'l': addlang(optarg);		break;
	case 't': addtype(optarg);		break;
	case '?':
	default : usage(1);
    }
}

/* #]getoptions:	*/ 
/* #[usage:		*/

#define FMT	"%-16s%-30s%s\n"	/* string + string + string */
#define DOTS	"-----------------------------------------------------"
#define EXAMPLE	"\nExamples:\n\
intergo -i -ta -la\n\
    Run the program interactively (-i), check all entry types (-ta) while\n\
    searching in the database and print all language definitions (-la)\n\
intergo -tt -lch -ljp geta\n\
    Run the program in batch mode, look for the technical term \"geta\"\n\
    and print the Chinese and Japanese translation (-lch -ljp)"

static void
usage(code)
int code;
{
    msg(MSG_USR_ERR, "Usage: intergo [options] [key1 [key2 key3 ...]]\n");
    msg(MSG_USR_ERR, "%s\n", DOTS);
    msg(MSG_USR_ERR, FMT, "Option"   , "Description"		, "Default");
    msg(MSG_USR_ERR, "%s\n", DOTS);
    msg(MSG_USR_ERR, FMT, "[-i]"	    , "interactive mode"	, "off");
    msg(MSG_USR_ERR, FMT, "[-d]"	    , "no diagrams/captions"	, "off");
    msg(MSG_USR_ERR, FMT, "[-v]"	    , "verbose mode (also help)", "off");
    msg(MSG_USR_ERR, FMT, "[-h]"	    , "print usage"		,    "");
    msg(MSG_USR_ERR, FMT, "[-t type]", "checked types"		,    "");
    if (verbose) {
    msg(MSG_USR_ERR, FMT, ""	    , "    t  = technical"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    n  = player names"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    c  = tournaments"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    d  = numbers"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    p  = conversation"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    a  = all"		, "off");
    }
    msg(MSG_USR_ERR, FMT, "[-l lang]", "emitted languages"	,    "");
    if (verbose) {
    msg(MSG_USR_ERR, FMT, ""	    , "    jp = japanese"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    ch = chinese"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    rk = korean"		, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    gb = english"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    nl = dutch"		, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    ge = german"		, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    fr = france"		, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    sv = swedish"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    it = italian"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    dg = diagrams"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "    cp = captions"	, "off");
    msg(MSG_USR_ERR, FMT, ""	    , "     a = all"		, "off");
    msg(MSG_USR_ERR, "%s\n", EXAMPLE);
    }

    exit(code);
}

/* #]usage:		*/ 

