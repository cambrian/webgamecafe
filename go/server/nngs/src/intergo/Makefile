#
# Are we building IGS code
#
IGS = 0

#
# Are we building a Go dictionary server
# or a IGS client interface?
#
SERVER = 1

#
# Where is the Go dictionary
#
DEFDICT = '"../dict/intergo.dct"'

#
# The maximum of bytes emitted during a query. If the query produced
# more output than this amount then just a message will be displayed:
#
#   [464 records (120644 bytes) matched in [JP][CH][GB][RK][NL][DG][CP]]
#
# Set MAXOUTPUT to 0 (zero) to remove this restriction all together.
#
MAXOUTPUT = 2048

#
# The prompt used in interactive mode
#
DPROMPT = '"keyword> "'

CC = gcc -ansi
#CFLAGS=-g

#################################################################
#	     Don't change anything after this			#
#################################################################

OPTIONS = -DSERVER=${SERVER} -DMAXOUTPUT=${MAXOUTPUT} -DINTERGO=${INTERGO} -DDEFDICT=${DEFDICT} -DDPROMPT=${DPROMPT} -DIGS=${IGS} ${CFLAGS}

DICTS	=			\
		champion.dct	\
		names.dct	\
		numbers.dct	\
		polite.dct	\
		technical.dct

all: intergo intergo.dct

intergo: godict.o io.o interface.o
	$(CC) -o intergo godict.o io.o interface.o

godict.o: Makefile godict.c godict.h io.h
	$(CC) -c ${OPTIONS} godict.c

interface.o: Makefile interface.c godict.h io.h
	$(CC) -c ${OPTIONS} interface.c

io.o: Makefile io.c io.h
	$(CC) -c ${OPTIONS} io.c

intergo.dct: ${DICTS}
	cat ${DICTS} > intergo.dct

interface.c godict.c godict.h io.c io.h:
	sccs get $@

${DICTS}:
	sccs get $@

tar:	intergo.tar.Z
zoo:	intergo.zoo
ftp:	ftp.date
mail:	mail.date

SRC	= Makefile interface.c godict.c godict.h io.c io.h runstats
DOC	= README CONTRIBUTORS UPDATES INTERNET STATISTICS
FTPSITE	= mcsun.eu.net
FTPDIR	= /usr/spool/pub/games/go/intergo
MAILUSR	= BARRYP@otago.ac.nz juwhan@eve.kaist.ac.kr job@shusaku.escape.de

intergo.tar.Z: ${DOC} ${SRC} ${DICTS} intergo.dct
	-rm -f intergo.tar intergo.tar.Z
	tar cvf intergo.tar ${DOC} ${SRC} ${DICTS} intergo.dct
	compress intergo.tar

intergo.zoo: ${SRC} ${DICTS}
	-rm -f intergo.zoo
	zoo a intergo.zoo ${SRC} ${DICTS}
	touch intergo.zoo

ftp.date: intergo.tar.Z 
	rcp -p README intergo.tar.Z ${FTPSITE}:${FTPDIR}
	rsh ${FTPSITE} ls -l ${FTPDIR}
	touch ftp.date

mail.date: intergo.tar.Z
	uuencode intergo.tar.Z intergo.tar.Z | split -900 - x
	for user in ${MAILUSR}						;\
	do								 \
	    echo "[Mailing $$user]"					;\
	    for part in xa*						;\
	    do								 \
		echo "[Mailing part $$part]"				;\
		mail -s "Intergo update = part $$part" $$user < $$part	;\
	    done							;\
	done
	touch mail.date

