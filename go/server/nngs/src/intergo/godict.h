/* #[info:		*/
/************************************************************************
 *									*
 *	    	   intergo --- An online Go Dictionary			*
 *									*
 *			    Jan van der Steen				*
 *		       Amsterdam, the Netherlands			*
 *									*
 *----------------------------------------------------------------------*
 * File    : godict.h 						*
 * Purpose : Define datatypes to implement a Go dictionary		*
 * Version : 1.7 						*
 * Modified: 9/10/93 17:19:23						*
 * Author  : Jan van der Steen (jansteen@cwi.nl) 			*
 ************************************************************************/
/* #]info:		*/ 
/* #[define:		*/

/*
 * Default we are a server
 */
#ifndef SERVER
#define SERVER 1
#endif

/*
 * Default dictionary (full pathname to file)
 */
#ifndef DEFDICT
#define DEFDICT	"/2/nngs/src/intergo/intergo.dct"
#endif

/*
 * The maximum number of bytes printed.
 * If MAXOUTPUT=0, no restriction is imposed on the amount of output.
 */
#ifndef MAXOUTPUT
#define MAXOUTPUT	0
#endif

/*
 * Prompt used in interactive mode
 */
#ifndef PROMPT
#define PROMPT	"keyword> "
#endif

/*
 * Entry seperator in case of multiple hits
 */
#define SEP	"\n"

/*
 * Special input characters
 */
#define	COMMENT	'#'		/* Comment indicator */

/*
 * Dictionary codes
 */
#define	CD_MISC	0x01
#define	CD_NAME	0x02
#define	CD_CHAM	0x04
#define	CD_TECH	0x08
#define	CD_POLI	0x10
#define	CD_DIGI	0x20

/*
 * Dictionary languages
 */
#define	LANG_DG	0x0001
#define	LANG_CP	0x0002
#define	LANG_JP	0x0004
#define	LANG_CH	0x0008
#define	LANG_RK	0x0010
#define	LANG_GB	0x0020
#define	LANG_NL	0x0040
#define	LANG_GE	0x0080
#define	LANG_FR	0x0100
#define	LANG_SV	0x0200
#define	LANG_IT	0x0400

/*
 * Type messages
 */
#define MSG_MISC	"Unclassified"
#define MSG_NAME   	"Player name"
#define MSG_CHAM   	"Championship title"
#define MSG_TECH   	"Technical term"
#define MSG_POLI   	"Conversation"
#define MSG_DIGI   	"Number"

/*
 * Language specifiers (while writing)
 */
#define LB_CD		"Type    : "
#define LB_JP		"Japanese: "
#define LB_CH		"Chinese : "
#define LB_RK		"Korean  : "
#define LB_GB		"English : "
#define LB_NL		"Dutch   : "
#define LB_GE		"German  : "
#define LB_FR		"French  : "
#define LB_SV		"Swedish : "
#define LB_IT		"Italian : "
#define LB_DG		"Diagram : "
#define LB_CP		"Caption : "
#define LB_EOT		"EOT"	/* end of search */

/*
 * Language specifiers (while reading)
 */
#if SERVER
#   define RD_CD	"CD="
#   define RD_JP	"JP="
#   define RD_CH	"CH="
#   define RD_RK	"RK="
#   define RD_GB	"GB="
#   define RD_NL	"NL="
#   define RD_GE	"GE="
#   define RD_FR	"FR="
#   define RD_SV	"SV="
#   define RD_IT	"IT="
#   define RD_DG	"DG="
#   define RD_CP	"CP="
#else
#   define RD_CD	LB_CD
#   define RD_JP	LB_JP
#   define RD_CH	LB_CH
#   define RD_RK	LB_RK
#   define RD_GB	LB_GB
#   define RD_NL	LB_NL
#   define RD_GE	LB_GE
#   define RD_FR	LB_FR
#   define RD_SV	LB_SV
#   define RD_IT	LB_IT
#   define RD_DG	LB_DG
#   define RD_CP	LB_CP
#endif

#define MAXDICTLINE	1024

/* #]define:		*/ 
/* #[typedef:		*/

typedef struct dict_node {
    struct dict_node *	dct_next;
    char *		dct_jp;		/* Japanese		*/
    char *		dct_gb;		/* English		*/
    char *		dct_ch;		/* Chinese		*/
    char *		dct_rk;		/* Korean		*/
    char *		dct_nl;		/* Dutch		*/
    char *		dct_ge;		/* German		*/
    char *		dct_fr;		/* French		*/
    char *		dct_sv;		/* Swedish		*/
    char *		dct_it;		/* Italian		*/
    char *		dct_dg;		/* Diagram		*/
    char *		dct_cp;		/* Caption		*/
    char *		dct_spec;	/* Only for clients	*/
    int			dct_type;	/* See defines		*/
} GODICT;

typedef struct found_info {
    int			fnd_lang;
    int			fnd_records;
    long		fnd_bytes;
} FOUND;

/* #]typedef:		*/ 
/* #[prototype:		*/

#ifdef __STDC__
#	define	PROTO(s) s
#else
#	define	PROTO(s) ()
#endif

GODICT  *	dict_load	PROTO((void));
void		dict_retrieve	PROTO((GODICT *d, char *p, FOUND *f));
void		dict_interact	PROTO((int mode));
void		dict_verbose	PROTO((int mode));
void		dict_types	PROTO((int t));
void		dict_langs	PROTO((int l));

/*
 * The next function should actually be moved into a seperate module
 */
char *		lstr		PROTO((char *s));

/* #]prototype:		*/ 
