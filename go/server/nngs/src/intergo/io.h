#ifndef IO_H
#define IO_H

/* #[info:		*/
/************************************************************************
*									*
*	    	  intergo --- An Online Go Dictionary			*
*									*
*			   Jan van der Steen				*
*		       Amsterdam, the Netherlands			*
*									*
*-----------------------------------------------------------------------*
* File    : io.h 						*
* Purpose : IO message types						*
* Version : 1.1 						*
* Modified: 1/13/93 18:23:40						*
* Author  : Jan van der Steen (jansteen@cwi.nl) 			*
************************************************************************/
/* #]info:		*/
/* #[define:		*/

#define	MSG_USR_OUT	0
#define	MSG_USR_ERR	1
#define	MSG_SYS_OUT	2
#define	MSG_SYS_ERR	3

/* #]define:		*/
/* #[prototype:		*/

#if defined(__STDC__)
# define PROTO(s) s
#else
# define PROTO(s) ()
#endif

extern	void	msg	PROTO((int kind, char *fmt, ...));

#undef PROTO

/* #]prototype:		*/ 

#endif
