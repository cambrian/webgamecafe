/* #[info:		*/
/************************************************************************
 *									*
 *	    	   intergo --- An online Go Dictionary			*
 *									*
 *			    Jan van der Steen				*
 *		       Amsterdam, the Netherlands			*
 *									*
 *----------------------------------------------------------------------*
 * File    : godict.c 						*
 * Purpose : Get search patterns and give language synonyms		*
 * Version : 1.10 						*
 * Modified: 9/10/93 17:19:22						*
 * Author  : Jan van der Steen (jansteen@cwi.nl) 			*
 ************************************************************************/
/* #]info:		*/ 
/* #[include:		*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "godict.h"
#include "io.h"

/* #]include:		*/ 
/* #[static:		*/

static int	interactive = 0;
static int	verbose	    = 0;
static int	types	    = 0;	/* No types	*/
static int	langs	    = 0;	/* No languages	*/

/* #]static:		*/ 
/* #[prototype:		*/

#ifdef __STDC__
#	define	PROTO(s) s
#else
#	define	PROTO(s) ()
#endif

static GODICT *	dict_search	PROTO((GODICT *d, char *p, FOUND *f));
static int	dict_print	PROTO((GODICT *d, int emit));
static void	dict_output	PROTO((GODICT *d, char *p, FOUND *f));
static void	dict_collect	PROTO((GODICT *d, char *p, FOUND *f));
static void	dict_report	PROTO((FOUND *f));
static void	dict_close	PROTO((GODICT *d));
static void	dict_store	PROTO((char **f, char *s));
static int	dict_fmt	PROTO((char *label, char *s, int emit));

static void	usage		PROTO((int exitcode));
/*static int	getopt		PROTO((int argc, char **argv, char *s));*/
static void	getoptions	PROTO((int argc, char **argv));
static void	addtype		PROTO((char *s));
static void	addlang		PROTO((char *s));

/* #]prototype:		*/ 

/* #[dict_load:		*/

#define LANGENTRY(line,label)	(!strncmp(line,label,strlen(label)))

GODICT *
dict_load()
{
    FILE   * fp;
    GODICT * d	      = (GODICT *) 0;
    GODICT * dp	      = (GODICT *) 0;
    char   * dictname = getenv("INTERGO");
    int	     linenr   = 0;
    int	     record   = 0;
    char     line[MAXDICTLINE];

    /*
     * If the environment variable INTERGO is not set we take the default
     */
    if (dictname == (char *) 0) dictname = DEFDICT;

    /*
     * Open the dictionary
     */
    if ((fp = fopen(dictname, "r")) == (FILE *) 0) {
	msg(MSG_SYS_ERR, "Can't open \"%s\"\n", dictname);
	return d;
    }

    /*
     * Read the dictionary
     */
    while (fgets(line, MAXDICTLINE, fp) != (char *) 0) {
	char *newline;

	/*
	 * Account line and move the \n (and possible \r)
	 */
	linenr++;
	if ((newline = strchr(line, '\n')) != (char *) 0) *newline = 0;
	if ((newline = strchr(line, '\r')) != (char *) 0) *newline = 0;

	/*
	 * Comment or empty lines?
	 */
	if (*line == 0 || *line == COMMENT) continue;

	/*
	 * Bad input lines?
	 */
	if (strlen(line) < strlen(RD_CD)) {
	    msg(MSG_SYS_ERR, "Line %d, Bad format: \"%s\"\n",
		    linenr,
		    line);
	    continue;
	}

	if (strncmp(line, RD_CD, strlen(RD_CD)) == 0) {
	    /*
	     * Deal with new dictionary entry after closing previous one
	     */
	    if (d != (GODICT *) 0) dict_close(d);

	    /*
	     * Allocate and insert new dictionary entry
	     */
	    dp = (GODICT *) calloc(1, sizeof(GODICT));
	    if (dp == (GODICT *) 0) {
		msg(MSG_SYS_ERR, "Out of memory in dict_load()\n");
		fclose(fp);
		return dp;
	    }
	    dp->dct_next = d;
	    d = dp;
	    switch(*(line+strlen(RD_CD))) {
		case 'n': d->dct_type = CD_NAME; break;
/*		case 'c': d->dct_type = CD_CHAM; break; */
		case 't': d->dct_type = CD_TECH; break;
		case 'p': d->dct_type = CD_POLI; break; 
		case 'd': d->dct_type = CD_DIGI; break;
		default : d->dct_type = CD_MISC; break; 
	    }
	    record++;

	    /*
	     * Report...
	     */
	    if (verbose && (record % 10) == 0) msg(MSG_USR_ERR, "[%3d]", record);

	    continue;
	}

	/*
	 * Check for language definition
	 */
	if (d == (GODICT *) 0) {
	    msg(MSG_SYS_ERR, "Line %d, Skipped: \"%s\"\n",
		    linenr,
		    line);
	}
	if LANGENTRY(line,RD_JP) { dict_store(&(d->dct_jp), line+strlen(RD_JP)); continue; }
	if LANGENTRY(line,RD_CH) { dict_store(&(d->dct_ch), line+strlen(RD_CH)); continue; }
	if LANGENTRY(line,RD_RK) { dict_store(&(d->dct_rk), line+strlen(RD_RK)); continue; }
	if LANGENTRY(line,RD_GB) { dict_store(&(d->dct_gb), line+strlen(RD_GB)); continue; }
	if LANGENTRY(line,RD_NL) { dict_store(&(d->dct_nl), line+strlen(RD_NL)); continue; }
	if LANGENTRY(line,RD_GE) { dict_store(&(d->dct_ge), line+strlen(RD_GE)); continue; }
	if LANGENTRY(line,RD_FR) { dict_store(&(d->dct_fr), line+strlen(RD_FR)); continue; }
	if LANGENTRY(line,RD_SV) { dict_store(&(d->dct_sv), line+strlen(RD_SV)); continue; }
	if LANGENTRY(line,RD_IT) { dict_store(&(d->dct_it), line+strlen(RD_IT)); continue; }
	if LANGENTRY(line,RD_DG) { dict_store(&(d->dct_dg), line+strlen(RD_DG)); continue; }
	if LANGENTRY(line,RD_CP) { dict_store(&(d->dct_cp), line+strlen(RD_CP)); continue; }
	msg(MSG_SYS_ERR, "Line %d, Unsupported language: \"%s\"\n",
		linenr,
		line);
    }

    /*
     * Close last dictionary entry
     */
    if (d != (GODICT *) 0) dict_close(d);

    /*
     * Report...
     */
    if (verbose) {
	if (record % 10 != 0) msg(MSG_USR_ERR, "[%3d]", record);
	msg(MSG_USR_ERR, "\n");
    }

    fclose(fp);
    return d;
}

/* #]dict_load:		*/ 
/* #[dict_store:	*/

static void
dict_store(f, s)
char **f;	/* Field */
char  *s;	/* Data  */
{
    int  more   = (*f != (char *) 0);
    long needed = strlen(s) + 1;

    /*
     * Don't store empty fields
     */
    if (!*s) return;

    if (more) {
	needed += strlen(*f) + 1;
	*f = (char *) realloc(*f, needed);
    }
    else {
	*f = (char *) malloc(needed);
    }

    if (*f == (char *) 0) {
	msg(MSG_SYS_ERR, "Out of memory in dict_store()\n");
	exit(1);
    }
    if (more) {
	strcat(*f, "\n");
	strcat(*f, lstr(s));
    }
    else {
	strcpy(*f, lstr(s));
    }
}

/* #]dict_store:	*/ 
/* #[dict_close:	*/

static void
dict_close(d)
/*
 * Close a dictionary entry --- handle CD_NAME entries and calculate the size
 */
GODICT *d;
{
    if (d == (GODICT *) 0) {
	msg(MSG_SYS_ERR, "Null pointer in dict_close()\n");
	exit(1);
    }

    /*
     * If a name entry is empty it's assumed to be
     * the same as the Japanese transcription
     * (which might also be empty of course)
     */
    if (d->dct_type == CD_NAME) {
	if (!d->dct_gb) d->dct_gb = d->dct_jp;
	if (!d->dct_nl) d->dct_nl = d->dct_jp;
	if (!d->dct_ge) d->dct_ge = d->dct_jp;
    }
}

/* #]dict_close:	*/ 
/* #[dict_search:	*/

#define SETL(f,l)		((f)->fnd_lang |= (l))
#define WORDINLANG(df,l,w)	((df) && (langs & (l)) && strstr((df),(w)))

static GODICT *
dict_search(d, s, f)
/*
 * Start querying the database for string s at entry point d
 */
GODICT	*d;
char	*s;
FOUND	*f;
{
    int found = 0;

    while (d) {
      if (types & d->dct_type) {
	if (WORDINLANG(d->dct_jp,LANG_JP,s)) { SETL(f,LANG_JP); found++; }
	if (WORDINLANG(d->dct_ch,LANG_CH,s)) { SETL(f,LANG_CH); found++; }
	if (WORDINLANG(d->dct_rk,LANG_RK,s)) { SETL(f,LANG_RK); found++; }
	if (WORDINLANG(d->dct_gb,LANG_GB,s)) { SETL(f,LANG_GB); found++; }
	if (WORDINLANG(d->dct_nl,LANG_NL,s)) { SETL(f,LANG_NL); found++; }
	if (WORDINLANG(d->dct_ge,LANG_GE,s)) { SETL(f,LANG_GE); found++; }
	if (WORDINLANG(d->dct_fr,LANG_FR,s)) { SETL(f,LANG_FR); found++; }
	if (WORDINLANG(d->dct_sv,LANG_SV,s)) { SETL(f,LANG_SV); found++; }
	if (WORDINLANG(d->dct_it,LANG_IT,s)) { SETL(f,LANG_IT); found++; }
	if (WORDINLANG(d->dct_dg,LANG_DG,s)) { SETL(f,LANG_DG); found++; }
	if (WORDINLANG(d->dct_cp,LANG_CP,s)) { SETL(f,LANG_CP); found++; }
	if (found) return d;
      }
      d = d->dct_next;
    }
    return d;
}

/* #]dict_search:	*/ 
/* #[dict_fmt:		*/

static int
dict_fmt(label, s, emit)
/*
 * Output a field value which might possibly contain newlines.
 * We output the label for each line.
 * If emit=0, we just return the number of bytes which *would* be emitted.
 */
char *label;
char *s;
int   emit;
{
    int n = 0;

    if (emit) {
	char def[MAXDICTLINE];
	char *t = def;

	while (*s) {
	    msg(MSG_USR_OUT, "%s", label);
	    while (*s) {
		*t++ = *s;
		if (*s++ == '\n') {
		    *t = 0;
		    msg(MSG_USR_OUT, "%s", def);
		    t = def;
		    break;
		}
	    }
	}
	*t = 0;
	msg(MSG_USR_OUT, "%s\n", def);
    }
    else {
	while (*s) {
	    n += (int) strlen(label);
	    while (*s) {
		n++;
		if (*s++ == '\n') break;
	    }
	}
	n++;
    }
    return n;
}

/* #]dict_fmt:		*/ 
/* #[dict_print:	*/ 

#define LANGDEF(df,l)	((langs & (l)) && (df) && *(df))

static int
dict_print(d, emit)
/*
 * Print a dictionary entry
 */
GODICT *d;
int	emit;
{
    int n = 0;

    if (d == (GODICT *) 0) {
	msg(MSG_SYS_ERR, "Null pointer in dict_print()\n");
	exit(1);
    }

#if SERVER
    switch(d->dct_type) {
	case CD_CHAM: n += dict_fmt(LB_CD, MSG_CHAM, emit); break;
	case CD_TECH: n += dict_fmt(LB_CD, MSG_TECH, emit); break;
	case CD_NAME: n += dict_fmt(LB_CD, MSG_NAME, emit); break;
	case CD_POLI: n += dict_fmt(LB_CD, MSG_POLI, emit); break; 
	case CD_DIGI: n += dict_fmt(LB_CD, MSG_DIGI, emit); break;
	default	    : n += dict_fmt(LB_CD, MSG_MISC, emit); break;
    }
#else
    if (d->dct_spec && *d->dct_spec) n += dict_fmt(LB_CD, d->dct_spec, emit);
#endif

    if (LANGDEF(d->dct_jp,LANG_JP)) n += dict_fmt(LB_JP, d->dct_jp, emit);
    if (LANGDEF(d->dct_ch,LANG_CH)) n += dict_fmt(LB_CH, d->dct_ch, emit);
    if (LANGDEF(d->dct_rk,LANG_RK)) n += dict_fmt(LB_RK, d->dct_rk, emit);
    if (LANGDEF(d->dct_gb,LANG_GB)) n += dict_fmt(LB_GB, d->dct_gb, emit);
    if (LANGDEF(d->dct_nl,LANG_NL)) n += dict_fmt(LB_NL, d->dct_nl, emit);
    if (LANGDEF(d->dct_ge,LANG_GE)) n += dict_fmt(LB_GE, d->dct_ge, emit);
    if (LANGDEF(d->dct_fr,LANG_FR)) n += dict_fmt(LB_FR, d->dct_fr, emit);
    if (LANGDEF(d->dct_sv,LANG_SV)) n += dict_fmt(LB_SV, d->dct_sv, emit);
    if (LANGDEF(d->dct_it,LANG_IT)) n += dict_fmt(LB_IT, d->dct_it, emit);
    if (LANGDEF(d->dct_dg,LANG_DG)) n += dict_fmt(LB_DG, d->dct_dg, emit);
    if (LANGDEF(d->dct_cp,LANG_CP)) n += dict_fmt(LB_CP, d->dct_cp, emit);

    return n;
}

/* #]dict_print:	*/ 
/* #[dict_report:	*/

static void
dict_report(f)
FOUND *f;
{
    if (f->fnd_records) {
	msg(MSG_USR_OUT,
	    "[%d records (%ld bytes) matched in ",
	    f->fnd_records, f->fnd_bytes);
	if (f->fnd_lang & LANG_JP) msg(MSG_USR_OUT, "[JP]");
	if (f->fnd_lang & LANG_CH) msg(MSG_USR_OUT, "[CH]");
	if (f->fnd_lang & LANG_GB) msg(MSG_USR_OUT, "[GB]");
	if (f->fnd_lang & LANG_RK) msg(MSG_USR_OUT, "[RK]");
	if (f->fnd_lang & LANG_NL) msg(MSG_USR_OUT, "[NL]");
	if (f->fnd_lang & LANG_GE) msg(MSG_USR_OUT, "[GE]");
	if (f->fnd_lang & LANG_FR) msg(MSG_USR_OUT, "[FR]");
	if (f->fnd_lang & LANG_SV) msg(MSG_USR_OUT, "[SV]");
	if (f->fnd_lang & LANG_IT) msg(MSG_USR_OUT, "[IT]");
	if (f->fnd_lang & LANG_DG) msg(MSG_USR_OUT, "[DG]");
	if (f->fnd_lang & LANG_CP) msg(MSG_USR_OUT, "[CP]");
	msg(MSG_USR_OUT, "]\n");
    } else msg(MSG_USR_OUT, "[No records matched]\n");
}

/* #]dict_report:	*/ 
/* #[dict_output:	*/

static void
dict_output(d, p, f)
/*
 * Output all dictionary entries which contain pattern "p"
 */
GODICT *d;		/* Dictionary		*/
char   *p;		/* Search pattern	*/
FOUND  *f;		/* What did we find?	*/
{
    GODICT *dp;
    int     next = 0;

    for (dp=dict_search(d, p, f); dp; dp=dict_search(dp->dct_next, p, f)) {
	if (next++) msg(MSG_USR_OUT, SEP);
	dict_print(dp, 1);
    }
#if SERVER
    if (!interactive) (void) dict_fmt(LB_EOT, " ", 1);
#endif
}

/* #]dict_output:	*/ 
/* #[dict_collect:	*/

static void
dict_collect(d, p, f)
/*
 * Calculate the number of records and bytes emited
 * by the hits of "p" in dictionary "d"
 */
GODICT *d;		/* Dictionary		*/
char   *p;		/* search pattern	*/
FOUND  *f;		/* What did we find?	*/
{
    GODICT *dp;

    memset(f, 0, sizeof(FOUND));

    for (dp = dict_search(d, p, f); dp; dp = dict_search(dp->dct_next, p, f)) {
	f->fnd_records += 1;
	f->fnd_bytes   += dict_print(dp, 0);
	f->fnd_bytes   += strlen(SEP);		/* record seperator */
    }
#if SERVER
    /*
     * search seperator
     */
    if (!interactive) f->fnd_bytes += dict_fmt(LB_EOT," ",0);
#endif
}

/* #]dict_collect:	*/ 
/* #[dict_retrieve:	*/

void
dict_retrieve(d, p, f)
GODICT *d;
char   *p;		/* Pattern to look for	*/
FOUND  *f;		/* Result of the query	*/
{
    /*
     * Retrieve information from the database
     */
    dict_collect(d, p, f);

    /*
     * Output the retrieval information
     */
/*
    if (MAXOUTPUT && f->fnd_bytes > MAXOUTPUT)	dict_report(f);
*/
    if (f->fnd_records == 0)		dict_report(f);
    else					dict_output(d, p, f);
}

/* #]dict_retrieve:	*/ 
/* #[dict_interact:	*/ 

void
dict_interact(mode)
int mode;
{
    interactive = mode;
}

/* #]dict_interact:	*/ 
/* #[dict_verbose:	*/ 

void
dict_verbose(mode)
int mode;
{
    verbose = mode;
}

/* #]dict_verbose:	*/ 
/* #[dict_types:	*/ 

void
dict_types(t)
/*
 * Select the types searched in the dictionary
 */
int t;
{
    types = t;
}

/* #]dict_types:	*/ 
/* #[dict_langs:	*/ 

void
dict_langs(l)
/*
 * Select the languages returned from the dictionary
 */
int l;
{
    langs = l;
}

/* #]dict_langs:	*/ 
/* #[lstr:		*/ 

char *
lstr(s)
/*
 * Convert a string to lowercase
 * We could use the strlow() library function but it's not 100% portable
 */
char *s;
{
    char *t;

    for (t = s; *t; t++) if (isupper(*t)) *t = tolower(*t);

    return s;
}

/* #]lstr:		*/ 
