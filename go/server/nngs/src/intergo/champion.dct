#########################################################################
#									#
#	    	       The Internet Go Dictionary			#
#									#
#				 by the					#
#		       The Internet Go Community			#
#									#
#-----------------------------------------------------------------------#
# File    : champion.dct 						#
# Purpose : Go related expressions and their translations		#
# Version : 1.11 						#
# Modified: 9/13/93 10:56:16						#
# E-mail  : Jim Yu            (jyu@eecs.nwu.edu)			#
# E-mail  : Jan van der Steen (jansteen@cwi.nl) 			#
#########################################################################
#
# The Go Dictionary is a joint effort by the Internet Go community.
# You would help your fellow country men and the Go community as a
# whole by adding definitions/translations to the list below.
# Please send them to either:
#
#	Jim Yu			jyu@eecs.nwu.edu
#	Jan van der Steen	jansteen@cwi.nl
#
# Explanation of the format of the dictionary:
#
# Empty lines and lines starting with a "hash" (like these lines) are ignored.
# All other lines are expected to have the format:
#
#	FIELD=VALUE
#
# where:
#
#	Field	Meaning
#	-------------------------
#	CD	Code (see below)
#	JP	Japanese
#	GB	English
#	CH	Chinese (pinyin)
#	RK	Republic of Korea
#	NL	Dutch
#	GE	German
#	FR	France
#	SV	Swedish
#       IT      Italy
#
#	Code	Classification
#	-------------------------
#	t	(T)echnical
#	n	(N)ame
#	c	(C)hampionship
#	d	(d)igits
#	p	(p)olite, courtesy phrases
#
# Contributions:
#
#   Name		E-mail				Language
#   -------------------------------------------------------------------
#   Barry Phease	BARRYP@otago.ac.nz		English/various
#   Jan van der Steen	jansteen@cwi.nl			Dutch/English
#   Jim Yu		jyu@eecs.nwu.edu		Chinese/English
#   Joachim Beggerow	job@shusaku.escape.de		German
#   Kim Juwhan		juwhan@eve.kaist.ac.kr		Korean
#   Kishiko Shimizu	jansteen@cwi.nl			Japanese
#   Nate Smith		nates@ll.mit.edu		English
#   Philippe Bizard	bizard@imag.fr			French
#   Roy Schmidt		schmidt@usthk.ust.hk		English/various
#   Ryu Kwangseon	juwhan@eve.kaist.ac.kr		Korean
#   Yang Wonsuk		osl2@mgt.kaist.ac.kr		Korean
#   Park Misa		juwhan@eve.kaist.ac.kr		Korean
#   Peter Smidt		smidt@fy.chalmers.se		Swedish
#   Sergio Martino      martino@minsky.csata.it         Italian
# 

# Jim Yu:
# (Note: I also included 1. pinyin without tone number; 2. the "meaning" of
# each title game, literally; 3. the level of the title match, either 5-game
# or 7-game.)

CD=c
RK=Gook-soo 
GB=Korean top title ("master of go";5-game title match)
SV=Koreansk m{sterskapstitel, (go-)m{stare ( b{st av 5 partier )
IT=Massimo titolo coreano (su 5 partite)

CD=c
RK=Wang-wee
GB=Korean top title( 7-game title match)
SV=Koreansk m{sterskapstitel ( b{st av 7 partier )
IT=Massimo titolo coreano (su 7 partite)

CD=c
RK=Gook-gi
GB=Korean top title( 5-game title match)
SV=Koreansk m{sterskapstitel ( b{st av 5 partier )
IT=Massimo titolo coreano (su 5 partite)

CD=c
JP=hon-inbo
GB=japanese top title ("ancient go family"; 7-game title match)
CH=ben3 ying fang3 (ben ying fang)
RK=Pon-in-bang
NL=japans top toernooi ('honinbo' = oude Japanse familie; best of seven)
FR=grand titre japonais (en 4 parties gagnantes)
FR=('honinbo' = l'une des anciennes familles de go japonaises)
SV=Honinbo, toppstatustitelmatch ( b{st av 7 partier ), ur}ldrigt
SV=japanskt familjenamn
IT=Massimo titolo giapponese (su 7 partite) (lett. antica famiglia del go)

CD=c
JP=meijin
GB=japanese top title ("master of go"; 7-game title match)
GB=korean top title(7-game title match)
CH=ming2 ren2 (ming ren)
RK=Myeong-in
NL=japans top toernooi ('meijin' = meester; best of seven)
FR=grand titre japonais ('meijin' = maitre de go ; 4 parties gagnantes)
SV=(go-)m{stare, japansk m{sterskapstitel ( b{st av 7 partier )
IT=Massimo titolo giapponese (su 7 partite) (lett. maestro del go)

CD=c
JP=kisei
GB=japanese top title ("board-game immortal"; 7-game title match)
GB=korean top title(7-game title match)
CH=qi2 sheng4 (qi sheng)
RK=Ki-seong
NL=japans top toernooi ('ki' = go, 'sei' = heilig persoon; best of seven)
FR=grand titre japonais (4 parties gagnantes)
SV=br{dets od|dlige, japansk m{sterskapstitel ( b{st av 7 partier )
IT=Massimo titolo giapponese (su 7 partite) (lett. gioco da tavola immortale)

CD=c
JP=judan
GB=japanese top title ("ten dan"; 5-game title match)
CH=shi2 duan4 (shi duan)
RK=Ship-dan
NL=japans top toernooi ('ju' = 10, dus 10de dan; best of five)
FR=grand titre japonais ('judan' = 10eme dan ; 3 parties gagnantes)
SV=10 dan, japansk m{sterskapstitel ( b{st av 5 partier )
IT=Massimo titolo giapponese (su 5 partite) (lett. decimo grado)

CD=c
JP=tengen
GB=japanese top title ("sky center"; 5-game title match)
CH=tian yuan2 (tian yuan)
RK=Cheon-won
NL=japans top toernooi ('tengen' = (10,10) punt; best of five)
FR=grand titre japonais ('tengen' = centre du ciel ; 3 parties gagnantes)
SV=himmelens mittpunkt, japansk m{sterskapstitel ( b{st av 5 partier )
IT=Massimo titolo giapponese (su 5 partite) (lett. centro del cielo)

CD=c
JP=gosei
GB=japanese top title ("go immortal"; 5-game title match)
CH=qi2 sheng4 (xiao3 qi2 sheng4; xiao qi sheng)
RK=Ki-wang
NL=japans top toernooi ('sei' = heilig meester; best of five)
FR=grand titre japonais (3 parties gagnantes)
SV=od|dlig go, japansk m{sterskapstitel ( b{st av 5 partier )
IT=Massimo titolo giapponese (su 3 partite) (lett. immortale del go)

CD=c
JP=oza
GB=japanese top title ("king seat"; 5-game title match)
CH=wang2 zuo4 (wang zuo)
RK=Wang-jwa
NL=japans top toernooi ('oza' = troon; best of five)
FR=grand titre japonais ('oza' = trone ; 3 parties gagnantes)
SV=kungstronen, japansk m{sterskapstitel ( b{st av 5 partier )
IT=Massimo titolo giapponese (su 5 partite) (lett. trono)

CD=c
JP=shinjin-o
GB=top new comers ("new men king")
CH=xin ren2 wang2 (xin reng wang)
RK=Shin-in-wang
NL=japans toernooi ('shinjin' = nieuwe, 'o' = koning; voor spelers <= 7p)
FR=titre japonais ("le roi des nouveaux visages")
SV=(]rets?) b{sta nya spelare
IT=primo dei principianti (lett. il re dei principianti)

CD=c
JP=kakusei
GB=
CH=
RK=Hak-seong
NL=Japan Airlines toernooi ('kaku' = kraanvogel, 'sei' = heilig persoon)
FR=titre japonais
SV=heliga tranan, den eviga f}geln, japansk m{sterskapstitel
IT=titolo giapponese (nel torneo della Japan Airlines)

CD=c
JP=nec cup
GB=
CH=
RK=NEC-bae
NL=NEC snelgo toernooi (met publiek)
FR=Coupe NEC (tournoi japonais) 
SV=NEC-cupen
IT=Coppa NEC (giapponese)

CD=c
JP=nhk cup
GB=
CH=
RK=NHK-bae
NL=NHK TV snelgo toernooi (wekelijks op TV)
FR=Coupe NHK (tournoi japonais)
SV=NHK-cupen
IT=Coppa NHK (giapponese)

CD=c
JP=ryusei
GB=
CH=
RK=Yong-seong
NL=Satelliet TV toernooi
SV=(satellit-)TV-turnering
IT=Torneo Satellite TV

CD=c
JP=fujitsu Cup
GB=
CH=
RK=Fujitsu-bae
NL=Fujitsu Toernooi
FR=Coupe Fujitsu
SV=Fujitsu-cupen
IT=Coppa Fujitsu (giapponese)

CD=c
JP=Haya-go senshuken
GB=
CH=
RK=
NL=Snelgo kampioenshap
FR=Championnat de blitz
SV=blixtm{sterskap
IT=Torneo lampo

CD=c
JP=O shoki hai
GB=Ing cup
CH=
RK=
NL=Ing toernooi
FR=Coupe Ing
SV=Ing-cupen
IT=Coppa Ing

