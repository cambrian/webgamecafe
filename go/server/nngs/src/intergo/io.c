/* #[info:		*/
/************************************************************************
*									*
*	    	  intergo --- An Online Go Dictionary			*
*									*
*			   Jan van der Steen				*
*		       Amsterdam, the Netherlands			*
*									*
*-----------------------------------------------------------------------*
* File    : io.c 						*
* Purpose : Handle IO details						*
* Version : 1.3 						*
* Modified: 2/27/93 12:58:18						*
* Author  : Jan van der Steen (jansteen@cwi.nl) 			*
************************************************************************/
/* #]info:		*/ 
/* #[include:		*/

#include <stdio.h>
#ifdef __STDC__
#  include <stdlib.h>
#endif
#include <string.h>
#include "io.h"

/* #]include:		*/ 
/* #[define:		*/

/*
 * Adjust the output to IGS standards?
 */
#ifndef IGS
#  define IGS 0
#endif

/* #]define:		*/ 
/* #[prototype:		*/

#if defined(__STDC__)
# define PROTO(s) s
#else
# define PROTO(s) ()
#endif

static void	do_error	PROTO((int kind, char *message));

#undef PROTO

/* #]prototype:		*/ 

/* #[msg:		*/

#if defined (__STDC__)
	/*#[ANSI-C:	*/

#include <stdarg.h>

void msg(int kind, char *fmt, ...)
{
    va_list argpoint;
    char msgbuffer[1024];

    va_start(argpoint, fmt);
	vsprintf(msgbuffer, fmt, argpoint);
    va_end(argpoint);

    do_error(kind, msgbuffer);
}

	/*#]ANSI-C:	*/ 
#else
	/*#[not ANSI-C:	*/

#include <varargs.h>

/*VARARGS1*/
void msg(va_alist)
va_dcl
{
    va_list	argpoint;
    int		kind;
    char *	fmt;
    char	msgbuffer[1024];

    va_start(argpoint);
	kind = va_arg(argpoint, int);
	fmt  = va_arg(argpoint, char *);
	vsprintf(msgbuffer, fmt, argpoint);
    va_end(argpoint);

    do_error(kind, msgbuffer);
}
	/*#]not ANSI-C:	*/ 
#endif

/* #]msg:		*/ 
/* #[do_error:		*/

static void do_error(int kind, char *message)
/*
 * We distinguish four kind of messages:
 *
 *	MSG_USR_OUT	Normal output to the user
 *	MSG_USR_ERR	Error message to the user
 *	MSG_SYS_OUT	Diagnostic output for the system
 *	MSG_SYS_ERR	Error message for the system
 */
{
    switch (kind) {
	case MSG_USR_OUT :
#if IGS
		/*SENDMESSAGE(message, player, type);*/
		/*pprintf(p, "%s%s", SendCode(p, INFO), message);*/
#else
		fprintf(stdout, "%s", message);
#endif
		break;
	case MSG_USR_ERR :
#if IGS
		/*SENDMESSAGE(message, player, type);*/
		/*pprintf(p, "%s%s", SendCode(p, ERROR), message);*/
#else
		fprintf(stderr, "%s", message);
#endif
		break;
	case MSG_SYS_OUT :
	case MSG_SYS_ERR :
#if IGS
#ifdef DEBUG
		fprintf(stderr, message);
#endif
#else
		fprintf(stderr, message);
#endif
		break;
	default		 :
#if IGS
#ifdef DEBUG
		fprintf(stderr,
			"Unexpected message type (%d) in do_error()\n",
			kind);
#endif
#else
		fprintf(stderr,
			"Unexpected message type (%d) in do_error()\n",
			kind);
#endif
		break;
    }
}

/* #]do_error:		*/ 
