/* playerdb.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995  Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "command.h"
#include "alias.h"
#include "censor.h"
#include "plan.h"

#ifndef _PLAYERDB_H
#define _PLAYERDB_H
#define MAX_PLAYER 500
#define MAX_PENDING 5
#define MAX_OBSERVE 10
#define MAX_PLAN 10
#define MAX_FORMULA 9
#define MAX_CENSOR 10
#define MAX_NOTIFY 25
#define MAX_ALIASES 30
#define MAX_MESSAGES 80
#define MAX_INCHANNELS 50

#define PLAYER_EMPTY    0
#define PLAYER_NEW      1
#define PLAYER_INQUEUE  2
#define PLAYER_LOGIN    3
#define PLAYER_PASSWORD 4
#define PLAYER_PROMPT   5
#define PLAYER_LOOKING  6

#define P_LOGIN 0
#define P_LOGOUT 1

#define SORT_ALPHA 0
#define SORT_LADDER9 1
#define SORT_LADDER19 2

#define PEND_MATCH 0
#define PEND_ADJOURN 1
#define PEND_PAUSE 2
#define PEND_GMATCH 3 /* params 1=color 2=size 3=time 4=byo-time */
#define PEND_KOMI 4
#define PEND_DONE 5
#define PEND_LADDER 6
#define PEND_PAIR 7
#define PEND_TEACH 8
#define PEND_AUTOMATCH 9
#define PEND_GOEMATCH 10
#define PEND_TMATCH 11

#define PEND_TO 0
#define PEND_FROM 1

/*************************************************************************
                   The are the player record item sizes
*************************************************************************/
#define MAX_NAME 10
#define MAX_PASSWORD 14
#define MAX_FULLNAME 80
#define MAX_EMAIL 60
#define MAX_PROMPT 10
#define MAX_REGDATE 36
#define MAX_LASTFILE 80
#define MAX_SRANK 12
#define MAX_RANK 60
#define MAX_RANKED 12

typedef struct _pending {
  int type;
  int whoto;
  int whofrom;
  int param1, param2, param3, param4, param5;
  float float1;
  char char1[20], char2[20];
} pending;

typedef struct _player {
  char login[MAX_NAME + 1];
  char name[MAX_NAME + 1];
  char passwd[MAX_PASSWORD + 1];
  char fullName[MAX_FULLNAME + 1];
  char emailAddress[MAX_EMAIL + 1];
  char prompt[MAX_PROMPT + 1];
  char RegDate[MAX_REGDATE + 1];
  char last_file[MAX_LASTFILE + 1];
  char srank[MAX_SRANK + 1];
  char rank[MAX_RANK + 1];  
  char ranked[MAX_RANKED + 1];  
  int pass_tries;
  int water;
  int extprompt;
  int registered;
  int socket;
  int status;
  int state; /* The current state of the user  (Probably unstable.  :) */
  int game;
  int gametype;
  int opponent; /* Only valid if game is >= 0 */
  int side;     /* Only valid if game is >= 0 */
  int last_tell;
  int last_pzz;
  int last_tell_from;
  int last_channel;
  int last_opponent;
  int logon_time;
  int last_command_time;
  int num_from;
  int num_to;
  int num_observe;
  int lastColor;
  int numgam;
  int d_height;
  int d_width;
  int last_file_line;
  int open;
  int looking;
  int rated;
  int rating;
  int orating;
  int ropen;
  int notifiedby;
  int bell;
  int client;
  int which_client;
  int i_login;
  int i_game;
  int i_shout;
  int i_gshout;
  int i_lshout;
  int last_problem;
  int i_tell;
  int i_robot;
  int i_kibitz;
  int i_verbose;
  int Private;
  int automail;
  int adminLevel;
  int teach;    /* A teaching account */
#if 0
  int num_plan;
  int num_censor;
#endif
  int nochannels;
  int gonum_white;
  int gonum_black;
  int gowins;
  int golose;
  int bmuzzled;
  int muzzled;
  int gmuzzled;
  int tmuzzled;
  int kmuzzled;
  int def_time;
  int def_size;
  int def_byo_time;
  int def_byo_stones;
  int num_logons;
  int match_type;
  int silent_login;
  unsigned int thisHost;
  unsigned int lastHost;
  pending p_from_list[MAX_PENDING];
  pending p_to_list[MAX_PENDING];
  int observe_list[MAX_OBSERVE];
  char busy[100]; /* more than enough */
  alias_t alias_list;
#if 1
  censor_t censor_list;
  plan_t plan_lines;
#else
  char *censorList[MAX_CENSOR];
  char *planLines[MAX_PLAN];
#endif
} player;

#define PARRAY_SIZE 260
extern player parray[PARRAY_SIZE];

extern int p_num;

extern void player_init(void);

extern int get_empty_slot(void);
extern int player_read(int, char *);
extern int player_free(int);

extern int player_new(void);
extern int player_remove(int);
extern int player_delete(int);
extern int player_markdeleted(int);
extern int player_save(int);
extern int player_find(int);
extern int player_find_bylogin(char *);
extern int player_find_part_login(char *);
extern int player_censored(int, int);
extern int check_censored(int, char *);
extern int player_notified(int, int);
extern int player_notified_departure(int);
extern int player_notify_present (int);
extern int player_notify(int, char *, char *);

extern int player_count(void);
extern int player_idle(int);
extern int player_ontime(int);

extern int player_zero(int, int);

extern int player_clear(int);

extern void player_write_login(int);
extern void player_write_logout(int);
extern int player_lastconnect(int);
extern int player_lastdisconnect(int);

extern int player_cmp(int, int, int);
extern void player_resort(void);

extern void player_pend_print(int, pending *);

extern int player_find_pendto(int, int, int);
extern int player_new_pendto(int);
extern int player_remove_pendto(int, int, int);

extern int player_find_pendfrom(int, int, int);
extern int player_new_pendfrom(int);
extern int player_remove_pendfrom(int, int, int);
extern int player_add_request(int, int, int, int);
extern int player_remove_request(int, int, int);
extern int player_decline_offers(int, int, int);
extern int player_withdraw_offers(int, int, int);

extern int player_is_observe(int, int);
extern int player_add_observe(int, int);
extern int player_remove_observe(int, int);
extern int player_game_ended(int);

extern int player_num_messages(int);
extern int player_add_message(int, int, char *);
extern int player_show_messages(int);
extern int player_clear_messages(int);

extern int player_search(int, char *);

extern int sort_blitz[];
extern int sort_stand[];
extern int sort_alpha[];
extern int sort_ladder9[];
extern int sort_ladder19[];

extern int player_kill(char *);
extern int player_rename(char *, char *);
extern int player_raise(char *);

/* For more information on these #$%^$%## defines, look at client source
   code, such as xigc.  What a horrible way to live.
*/

#define PROMPT    1   
#define BEEP      2  
#define DOWN      4  
#define ERROR     5  
#define FIL       6  
#define GAMES     7  
#define HELP      8  
#define INFO      9  
#define LAST     10  
#define KIBITZ   11  
#define LOAD     12 
#define LOOK_M   13
#define MESSAGE  14   
#define MOVE     15  
#define OBSERVE  16 
#define REFRESH  17
#define SAVED    18   
#define SAY      19  
#define SCORE_M  20 
#define SHOUT    21
#define STATUS   22   
#define STORED   23  
#define TELL     24 
#define THIST    25
#define TIM      26   
#define WHO      27  
#define UNDO     28 
#define SHOW     29
#define TRANS    30   
#define YELL     32  
#define TEACH    33 
#define VERSION  39
#define DOT      40   
#define CLIVRFY  41  
/* PEM: Removing stones. */
#define REMOVED  49
#define EMOTE    500
#define EMOTETO  501
#define PING     502

#define WAITING     5  
#define PLAYING_GO  6 
#define SCORING     7
#define OBSERVING   8  
#define TEACHING    9 
#define COMPLETE   10

#endif /* _PLAYERDB_H */

