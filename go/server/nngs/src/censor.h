#ifndef _censor_h_
#define _censor_h_

/* censor.h
** Per-Erik Martin (pem@nexus.se) 1999-05-08
**
*/

typedef struct censor *censor_t;

/* Creates a censored list. */
extern censor_t censor_init(void);

/* Returns the number of censored in list. */
extern size_t censor_count(censor_t);

/* Returns a pointer to the censored 'c' if found, NULL otherwise. */
extern char *censor_lookup(char *c, censor_t);

/* Adds the censored 'c'. Returns 1 if it replaced an old entry,
** or 0 if it was a new entry.
*/
extern int censor_add(char *c, censor_t);

/* Removes censored 'c' from list. Returns 1 if it was there,
** or 0 if it wasn't found.
*/
extern int censor_rem(char *c, censor_t);

/* Initiates an iteration through the list.
** A typical iteration (through the list 'cl') looks like this:
**
** {
**   char *c;
**
**   censor_start(cl);
**   while (censor_next(&c, cl))
**     do_something_with(c);
** }
*/
extern void censor_start(censor_t);

/* Gives the next censored in turn.
** Returns 1 if there was one, 0 if we reached the end.
*/
extern int censor_next(char **cp, censor_t);

/* Clear the list
 */
extern void censor_clear(censor_t);

/* Destroy the list
*/
extern void censor_free(censor_t);

#endif /* _censor_h_ */
