#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#define MAX_USERS 4096
#define MAX_RANKS 40

int rankarray[MAX_RANKS]; /* One for each rank */
int ratarray[MAX_USERS]; /* One for each user */

char *ranks[] = {"30k", "29k", "28k", "27k", "26k", "25k", "24k", "23k", "22k",
                 "21k", "20k", "19k", "18k", "17k", "16k", "15k", "14k", "13k",
                 "12k", "11k", "10k", " 9k", " 8k", " 7k", " 6k", " 5k", " 4k",
                 " 3k", " 2k", " 1k", " 1d", " 2d", " 3d", " 4d", " 5d", " 6d",
                 " 7d"};

int cmprat(const void *p1, const void *p2)
{
#define P1 (*(const int * const *)p1)
#define P2 (*(const int * const *)p2)

  return(P1 - P2);
}

main(int argc, char *argv[])
{
  char *SearchFor;
  FILE *fp;
  char name[11], rank[10];
  int rating, numgam, i, j, k, totrat;
  float percent;
  char tmp[64];

  for (i = 0; i < MAX_USERS; i++) {
    ratarray[i] = 0;
  }
  for (i = 0; i < MAX_RANKS; i++) {
    rankarray[i] = 0;
  }

  fp = fopen("/home/nngs/ratings/pett/results-rated", "r");
  if(!fp) {
    perror("Could not open file");
    exit(0);
  }
  i = totrat =  0;
  while((fscanf(fp, "%s %s %d %d", name, rank, &rating, &numgam)) == 4) {
    ratarray[i++] = rating;
    totrat += rating;
    if(rating > 3699) rating = 3699; 
    rankarray[(int)(rating / 100)]++;
  }
  fclose(fp);
  for (j = 0; j < MAX_RANKS; j++) {
    if(rankarray[j] > 0) {
      tmp[0] = NULL;
      percent = (float) ((float)rankarray[j] / (float)i) * 100;
      printf(" %s: %-4d |", ranks[j], rankarray[j]); 
      for(k = 0; k < percent; k++) strcat(tmp, "*");
      printf("%-30.30s| (%.2f%%)\n", tmp, 
              percent);
    }
  }
  qsort(ratarray, i, sizeof(int *), cmprat);
  printf("\n %d rated players.  Average rating: %.2f (%s)  Mean: %.2f\n", i, 
          (float) totrat / i, ranks[(int) (totrat / i) / 100],
          (float) ratarray[i / 2]);
} 

