/* channel.h
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997  Erik Van Riper (geek@midway.com)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _CHANNEL_H
#define _CHANNEL_H

/* Set the number of channels (100 is plenty) */
#define MAX_CHANNELS 100

/* Set the number of "other" channels, like shout, etc */
#define MAX_O_CHANNELS 6

#define YELL_STACK_SIZE 20

/* define the different "other" channel types */
#define CASHOUT 0  /* Admin shouts, for admins only */
#define CSHOUT  1  /* Regular shouts */
#define CGSHOUT 2  /* GoBot shouts */
#define CLSHOUT 3  /* Ladder shouts */
#define CLOGON  4  /* Player login announcments */
#define CGAME   5  /* Game announcments */

typedef struct _chan {
int locked;   /* If the channel is locked to further people */
int hidden;   /* If the people inside are hidden from view */
int dNd;      /* If the people inside do not want outside yells. */
char *Title;   /* The Channel title */
char *Yell_Stack[YELL_STACK_SIZE]; /* Last channel yells */
int Num_Yell;         /* Number of yells on stack */
} chan;

extern chan carray[MAX_CHANNELS];

extern int *channels[MAX_CHANNELS];
extern int numOn[MAX_CHANNELS];
/* extern char *Chan_Title[MAX_CHANNELS]; */

/* The following should be merged with the above */
extern int *Ochannels[MAX_O_CHANNELS];
extern int OnumOn[MAX_O_CHANNELS];
extern chan Ocarray[MAX_O_CHANNELS];

extern void channel_init(void);
extern int on_channel(int, int);
extern int channel_remove(int, int);
extern int channel_add(int, int);
extern int add_to_yell_stack(int, char *);
extern int add_to_Oyell_stack(int, char *);

extern void Ochannel_init(void);
extern int Oon_channel(int, int);
extern int Ochannel_remove(int, int);
extern int Ochannel_add(int, int);

#endif /* _CHANNEL_H */
