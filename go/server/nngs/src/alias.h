#ifndef _alias_h_
#define _alias_h_

/* alias.h
** Per-Erik Martin (pem@docs.uu.se) 1996-02-06
**
*/

typedef struct alias *alias_t;

/* Creates a alias list. */
extern alias_t alias_init(void);

/* Returns the number of aliases in list. */
extern size_t alias_count(alias_t);

/* Returns a pointe to the alias of cmd if found, NULL otherwise. */
extern char *alias_lookup(char *cmd, alias_t);

/* Adds the alias for cmd. Returns 1 if it replaced an old entry,
** or 0 if it was a new entry.
*/
extern int alias_add(char *cmd, char *alias, alias_t);

/* Removes alias for cmd from list. Returns 1 if it was there,
** or 0 if it wasn't found.
*/
extern int alias_rem(char *cmd, alias_t);

/* Initiates an iteration through the list.
** A typical iteration (through the list 'al') looks like this:
**
** {
**   char *c, *a;
**
**   alias_start(al);
**   while (alias_next(&c, &a, al))
**     do_something_with(c, a);
** }
*/
extern void alias_start(alias_t);

/* Gives the command/alias pair for the next entry in turn.
** Returns 1 if there was one, 0 if we reached the end.
** The pointers 'cp' and 'ap' may be NULL. A non-null pointer
** is updated to the next value if 1 was returned.
*/
extern int alias_next(char **cp, char **ap, alias_t);

/* Destroy the list
*/
extern void alias_free(alias_t);

#endif /* _alias_h_ */
