/* nngsmain.c
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"

#include "common.h"
#include "nngsmain.h"
#include "nngsconfig.h"
#include "network.h"
#include "command.h"
#include "channel.h"
#include "playerdb.h"
#include "utils.h"
#include "ladder.h"
#include "emote.h"
#include "mink.h"

/* Arguments */
PUBLIC int port, Ladder9, Ladder19, num_19, num_9, completed_games,
       num_logins, num_logouts, new_players, Debug;
#ifdef BYTE_COUNT
PUBLIC unsigned long byte_count;
#endif

void player_array_init(void);
void player_init(void);
static void usage(char *);

PRIVATE void usage(char *progname) {
  fprintf(stderr, "Usage: %s [-p port] [-h]\n", progname);
  fprintf(stderr, "\t\t-p port\t\tSpecify port. (Default=%d)\n", DEFAULT_PORT);
  fprintf(stderr, "\t\t-h\t\tDisplay this information.\n");
  exit(1);
}

PRIVATE void GetArgs(int argc, char *argv[])
{
  int i;

  port = DEFAULT_PORT;

  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'p':
	if (i == argc - 1)
	  usage(argv[0]);
	i++;
	if (sscanf(argv[i], "%d", &port) != 1)
	  usage(argv[0]);
	break;
      case 'h':
	usage(argv[0]);
	break;
      }
    } else {
      usage(argv[0]);
    }
  }
}

PRIVATE void main_event_loop(void) {
  net_select(1);
}

void TerminateServer(int sig)
{
  fprintf(stderr, "Got signal %d\n", sig);
  Logit("Got signal %d", sig);
  TerminateCleanup();
  net_closeAll();
  exit(1);
}

void BrokenPipe(int sig)
{
  signal(SIGPIPE, BrokenPipe);
  Logit("Got Broken Pipe\n");
}

PUBLIC int main(int argc, char *argv[])
{
  FILE *fp;

#ifdef SYSTEM_NEXT
#ifdef DEBUG
  malloc_debug(16);
#endif
#endif
  GetArgs(argc, argv);
  signal(SIGTERM, TerminateServer);
  signal(SIGINT, TerminateServer);
  signal(SIGPIPE, SIG_IGN);
  if (net_init(port)) {
    fprintf(stderr, "Network initialize failed on port %d.\n", port);
    exit(1);
  }
  if(port != 9999) {
    if (net_init(2929)) {
      fprintf(stderr, "Network initialize failed on port 2929.\n");
      exit(1);
    }
    if (net_init(7777)) {
      fprintf(stderr, "Network initialize failed on port 6969.\n");
      exit(1);
    }
    if (net_init(6969)) {
      fprintf(stderr, "Network initialize failed on port 6969.\n");
      exit(1);
    }
  }
  startuptime = time(0);
  player_high = 0;
  game_high = 0;
#ifdef BYTE_COUNT
  byte_count = (long) 0;
#endif
  srand(startuptime);

#ifdef SGI
  /*mallopt(100, 1);*/  /* Turn on malloc(3X) debugging (Irix only) */
#endif
  Logit("Initialized on port %d.", port);
  command_init();
  EmoteInit(emotes_file);
  /*Logit("commands_init()");*/
  commands_init();
  /*Logit("channel_init()");*/
  channel_init();
  Ochannel_init();
  /*Logit("player_array_init()");*/
  player_array_init();
  player_init();
  LadderInit(NUM_LADDERS);
  Ladder9 = LadderNew(LADDERSIZE);
  Ladder19 = LadderNew(LADDERSIZE);
  num_9 = 0;
  Debug = 0;
  completed_games = 0;
  num_logins = num_logouts = new_players = 0;
  fp = fopen(LADDER9, "r");
  if(fp) {
    num_9 = PlayerLoad(fp, Ladder9);
    Logit("%d players loaded from file %s", num_9, LADDER9);
    fclose(fp);
  }
  num_19 = 0;
  fp = fopen(LADDER19, "r");
  if(fp) {
    num_19 = PlayerLoad(fp, Ladder19);
    Logit("%d players loaded from file %s", num_19, LADDER19);
    fclose(fp);
  }
  initmink();
  Logit("Server up and running at");
  main_event_loop();
  Logit("Closing down.");
  net_closeAll();
  return 0;
}
