#include <stdio.h>

main(int argc, char *argv[])
{
  FILE *fp;
  char name[11], rank[10];
  int rating, numgam;
  unsigned int num_games = 0, rats = 0;
  float ave_rats;
  int num_play = 0;
  int xrat[2048];

  fp = fopen("/home/nngs/src/best.1", "r");
  if(!fp) {
    perror("Could not open file");
    exit(0);
  }
  while((fscanf(fp, "%s %s %d %d", name, rank, &rating, &numgam)) == 4) {
    num_games += numgam;
    rats += rating;
    xrat[num_play++] = rating;
    printf(" %-4d %-11s %-5s %d\n", num_play, name, rank, rating);
  }
  fclose(fp);
/*
  printf("\nGames: %d  Number of players: %d  Average rating: %f  Median: %d\n",
            num_games, num_play, (float) rats / num_play, xrat[num_play/2]);
*/
} 
