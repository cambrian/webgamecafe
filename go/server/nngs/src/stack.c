#include <stdio.h>

char *shout_stack[10];
char *chan_stack[10];

int num_on_shout_stack = 1;
int num_on_chan_stack = 1;

int add_to_stack(int, char *[], char *);

int add_to_stack(int num_on_stack, char *stack[], char *text)
{
  int i;

  /* First, check if shout stack full.   If so, release oldest, and
     shuffle the rest down */

  if(num_on_stack == 11) {
    free(stack[0]);
    for (i = 0; i <= 8; i++) {
     stack[i] = stack[i + 1];
    }
    num_on_stack = 10;
  }

  /* Now add the latest string of text */

  stack[num_on_stack - 1] = (char *) strdup(text);
  num_on_stack++;
  printf("%d\n", num_on_stack - 1);
  return(num_on_stack);
}

int display_shout_stack()
{
  int i;

  for (i = 1; i < num_on_shout_stack; i++) {
    printf("%s\n",shout_stack[i - 1]);
  }

  return 1;
}

int display_chan_stack()
{
  int i;

  for (i = 1; i < num_on_chan_stack; i++) {
    printf("%s\n", chan_stack[i - 1]);
  }

  return 1;
}

main()
{

  char text[80];

  while(1) {
    gets(text);
    if(!strcmp(text, "ooooo")) display_shout_stack();
    else num_on_shout_stack = add_to_stack(num_on_shout_stack, (char *)shout_stack, text);
  }
}
