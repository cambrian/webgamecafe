/*
    NNGS - The No Name Go Server
    Copyright (C) 1995  Erik Van Riper (geek@imageek.york.cuny.edu)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef _stdinclude_h
#define _stdinclude_h

/*
#include <sys/types.h>
*/

/* If you have a normal cc environment (post 1970's) */
#define GOOD_STDIO 1

#ifdef SYSTEM_SUN4
  #define USE_VARARGS
#endif

/* These are included into every .c file */
#ifdef SYSTEM_SUN5
  #define USE_RLIMIT
  #define USE_TIMES
  #define USE_WAITPID
  #define rindex strrchr
  #include <string.h>
  #include <dirent.h>
  #include <unistd.h>
  #include <fcntl.h>
  #include <sys/filio.h>
  #include <sys/systm.h>
  #include <netinet/in.h>
  #include <arpa/inet.h>
  #define direct dirent
#endif

#ifdef OLD_LINUX
#ifdef ERIK_TEST
#define __USE_BSD 1
#define __USE_SVID 1
#include <string.h>
#include <dirent.h>
#define direct dirent
#endif
#else
#include <strings.h>
#endif

#ifndef SYSTEM_SUN5
#include <sys/dir.h> 
#endif
#ifdef OLD_LINUX
#include <sys/stat.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/stat.h>
#ifndef SGI
/*#include <unistd.h>*/
#endif
#ifdef SGI
#include <string.h>
#endif
#include <signal.h>
#ifdef ERIK_TEST
#include <ctype.h>
#include <sys/errno.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#endif
#include "bm.h"

#ifdef AIX
  #include <dirent.h>
  #define direct dirent
#endif

#ifndef GOOD_STDIO
  extern int fclose(FILE *);
  extern int fscanf(FILE *, char *, ...);
  extern int fprintf(FILE *, char *, ...);
  extern int printf(char *, ...);
#endif

#ifndef LINUX
  extern void malloc_good_size(unsigned int size);
  extern int link();
  extern int unlink();
  extern int rename();
  extern int kill();
  extern int access();
  extern int getdtablesize();
  extern int write();
#endif

#ifdef OLD_LINUX
/* [PEM]: Why do linux include files suck? */

extern int atoi(const char *);

extern int rand(void);
extern void srand(unsigned int);

extern void *malloc(size_t);
extern void *realloc(void *, size_t);
extern void *calloc(size_t, size_t);
extern void free(void *);

extern char *strdup(const char *);

extern int system(const char *);

#endif /* LINUX */

#endif /* _stdinclude_h */
