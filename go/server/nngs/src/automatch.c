/**************************************************************************
 * automatch.c               by Erik Ekholm                Created 960305 *
 **************************************************************************/


/* KOMIn is the standard komi for an even game on an nxn board. The value of
   one stone is twice as much. */

#define KOMI19 5.5  /*standard komi = 5.5 => one stone is worth 11 points
		      From this follows that each komi point corresponds
		      to a 9 (100/11) point rating difference */

#define KOMI13 8.5  /* From the Go FAQ. (Note BTW that the table in the FAQ
		       isn't quite consistent; the gap between kyu diff.
		       3 and 4 is much larger than between 2 and 3.) */

#define KOMI9 5.5   /* 5.5 according to some pro-pro games. */



/* STONEPTSn is how many points in the rating system a stone on an nxn board
   corresponds to. The figures for smaller boards could be changed to fit
   statistical data better. */

#define STONEPTS19 100    /* 100 pts = one grade at NNGS */
#define STONEPTS13 300    /* 1 stone at 13x13 is 3 kyu grades, see Go FAQ. */
#define STONEPTS9  600    /* My best guess */


/* THRESHOLDn decides at which rating difference handicap is increased.
   Avoiding negative komi might be a good idea, since it makes it harder
   for Black to succeed with a mirror go strategy. */

#define THRESHOLD19 0  /* Values from 0 to 5 possible. 5 avoids neg. komi. */
#define THRESHOLD13 0  /* 0 - 8 possible. 8 avoids neg. komi */
#define THRESHOLD9  0  /* 0 - 5 possible. 5 avoids neg. komi */


/* OFFSETn is used for eliminating round-off errors */

#define OFFSET19 ((STONEPTS19 + 2 * KOMI19) / (KOMI19 * 4))
#define OFFSET13 ((STONEPTS13 + 2 * KOMI13) / (KOMI13 * 4))
#define OFFSET9  ((STONEPTS9  + 2 * KOMI9)  / (KOMI9  * 4))


/*---------------------------------------------------------------------------
 * AutoMatch sets stones and komi given positive ratingdiff (in NNGS rating
 * points) and boardsize (for 19, 13 and 9), so that both players have as
 * close to 50% chance of winning as possible.
 *
 * Please note that an even game is represented as a game with 1 stone 5.5
 * komi, thus NOT a zero stone game
 */

void AutoMatch(int ratingdiff, int boardsize, int *stones, double *komi)
{
  int units;  /* help variable, number of total komi units. */

  switch ( boardsize ) {
    
  case 19:
    units = (ratingdiff + OFFSET19) / (STONEPTS19 / (KOMI19 * 2.0));
    *stones = 1 + (units + THRESHOLD19) / (KOMI19 * 2);
    *komi = KOMI19 - units + (KOMI19 * 2) * (*stones - 1);
    break;

  case 13:
    units = (ratingdiff + OFFSET13) / (STONEPTS13 / (KOMI13 * 2.0));
    *stones = 1 + (units + THRESHOLD13) / (KOMI13 * 2);
    *komi = KOMI13 - units + (KOMI13 * 2) * (*stones - 1);
    break;

  case 9:
    units = (ratingdiff + OFFSET9) / (STONEPTS9 / (KOMI9 * 2.0));
    *stones = 1 + (units + THRESHOLD9) / (KOMI9 * 2);
    *komi = KOMI9 - units + (KOMI9 * 2) * (*stones - 1);
    break;
  }
}
  
#ifdef NOTUSED
/* Just a simple test */

void main(void)
  {
    int d;
    int stones;
    double komi;

    for(d=0; d<900; d++) {
      AutoMatch(d, 19, &stones, &komi);
      printf("diff:%4d,  stones:%2d,  komi :%5.1f\n", d, stones, komi); 
    }
  }
#endif /* NOTUSED */

/* I have not approached the question of how to deal with rating differences
   of more than 9 stones.  Solutions: 1) Allowed, use free or default
   placement.  2) Use max 9 stones, but larger komi. 3) Truncate at 9
   stones. 4) Let the match command reject such automatch requests.  */

