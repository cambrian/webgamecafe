/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997 John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef __MINK_H_
#define __MINK_H_

#include "glue.h"
#define GO_EMPTY	0		/* these 3 colors must be in [0..3] */
#define GO_BLACK	1
#define GO_WHITE	2

#define EDGE	-1
#define PASS	-1
#define KOMI 5.5		/* default value for komi */

#define FIXEDHCAP 1             /* If you want fixed handicaps by default */

typedef struct {
  int index, value;		/* logs every change to uf for later undo */
} uf_log;

typedef struct {
  int point,ko;			/* point played (or PASS), forbidden nextmove */
  unsigned int left:1, right:1, up:1, down:1, self:1;	/* captures made inc. suicide */
  ulong hash;
} move;

typedef struct {
  int width, height;	/* board dimensions */
  int rules;		/* what rules are we playing by? */
  move *moves;		/* move history */
  int mvsize,movenr;	/* size of moves and number of moves played */
  int *board;		/* current state of go-board */
  int *uf;		/* union-find structure */
  uf_log *uflog;	/* log of changes to uf */
  int logsize,logid;	/* size of uflog & number of changes to uf */
#ifdef MINKKOMI
  float komi;
#endif
  /*int nocaps; */		/* don't make captures, e.g. to play other games */
  int handicap;         /* handicap */
  int caps[4];		/* # stones captured */
  ulong hash;
  int root,kostat;	/* root is temporary variable used in group merging */
} minkgame;

/* public functions */
extern void initmink();
extern minkgame *initminkgame(int, int, int);	/* initialize game record */
extern void savegame(FILE *fp, minkgame *g, mvinfo *mi, int nmvinfos);
extern int loadgame(FILE *fp, minkgame *g);	/* returns player_to_move if OK */
extern void freeminkgame(minkgame *g);	/* frees up game record */
extern int sethcap(minkgame *g, int n);     /* returns 1 if succesful */
extern void setnocaps(minkgame *g, int val);     /* returns 1 if succesful */
extern ulong gethash(minkgame *g); /* get a hashcode for current board */
#ifdef MINKKOMI
extern void setkomi(minkgame *g, float k);      /* set the komi */
extern float getkomi(minkgame *g);              /* set the komi */
#endif
extern int movenum(minkgame *g);    /* return move number (#moves played in game) */
extern int back(minkgame *g);	        /* take back last move */
extern void forward(minkgame *g);	/* move forward one move */
extern int go_move(minkgame *g, char *s);   /* return point != 0 if s is go_move */
extern void listmove(minkgame *g, int i, char *buf);  /* list move i in game g */
extern int point(minkgame *g, int x, int y);	/* convert coords to point */
extern int play(minkgame *g, int p, int ko);	/* play at position p */
extern int removedead(minkgame *g, int p, int c);
extern void replay(minkgame *g);   /* replay game so as to undo all removes */
extern int pass(minkgame *g);   /* if pass is i'th consecutive one, return i */
extern char file(int i);	/* convert file number 0-18 to character */
extern void printboard(minkgame *g, twodstring buf); /* print ascii representation of go board */
extern void statusdims(minkgame *g, int *width, int *height);
extern void boardstatus(minkgame *g, twodstring buf);
extern void countscore(minkgame *g, twodstring buf, int *wt, int *bt, int *wo, int *bo);	/* count w/b territory and w/b occupied */
extern void getcaps(minkgame *g, int *wh, int *bl); /* #w/b stones captured */
extern int loadpos(FILE *, minkgame *);

/* private functions */
/* none of yer business:) */

#endif
