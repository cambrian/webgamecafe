#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 4242

int
main(int argc, char **argv)
{
  int x, s;
  struct sockaddr_in addr;
  unsigned a[4];
  unsigned char ab[4];

  if (argc != 2 ||
      sscanf(argv[1], "%u.%u.%u.%u", a+0, a+1, a+2, a+3) != 4)
  {
    fprintf(stderr, "Usage: ctst IP\n");
    exit(1);
  }
  for (x = 0 ; x < 4 ; x++)
    ab[x] = a[x];

  if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    fprintf(stderr, "socket() failed\n");
    exit(1);
  }

  memset((void *)&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  memcpy(&addr.sin_addr.s_addr, ab, 4);
  addr.sin_port = htons(PORT);

  if (connect(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
  {
    fprintf(stderr, "connect() failed\n");
    exit(1);
  }

  x = 4;
  while (x--)
  {
    char buf[32];

    if (read(s, buf, 32) < 0)
    {
      fprintf(stderr, "read() failed\n");
      exit(1);
    }
    printf("read() => %s\n", buf);
    sleep(3);
  }

  close(s);

  exit(0);
}
