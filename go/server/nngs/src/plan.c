/* plan.c
** Per-Erik Martin (pem@nexus.se) 1999-05-08
**
*/

#include <stdlib.h>
#include <assert.h>

/* For utils.h */
#include <time.h>
#include "bm.h"

#include "salloc.h"

#include "utils.h"

#include "plan.h"

#define INITSIZE 10

struct plan
{
  char **lines;
  size_t size, count, iter;
};

static char *Blank_line = "";

size_t
plan_count(plan_t p)
{
  return (p ? p->count : 0);
}

plan_t
plan_init(void)
{
  plan_t p;

  p = (plan_t)malloc(sizeof(struct plan));
  assert(p != NULL);
  p->size = p->count = 0;
  p->lines = (char **)malloc(INITSIZE*sizeof(char *));
  assert(p->lines != NULL);
  p->size = INITSIZE;
  return p;
}

char *
plan_get(size_t i, plan_t p)
{
  return (i < p->count ? NULL : p->lines[i]);
}

void
plan_insert(char *s, size_t i, plan_t p)
{
  size_t max = (i > p->count ? i+1 : p->count+1);

  while (max >= p->size)
  {
    p->size *= 2;
    p->lines = (char **)realloc(p->lines, p->size*sizeof(char *));
    assert(p->lines != NULL);
  }
  if (i < p->count)
  {
    size_t j;

    for (j = p->count ; j > i ; j--) /* Shift */
      p->lines[j] = p->lines[j-1];
  }
  else
  {
    size_t j;

    for (j = p->count ; j < i ; j++) /* Fill with "" */
      p->lines[j] = Blank_line;
  }
  if (s == NULL || *s == '\0')
    p->lines[i] = Blank_line;
  else
    p->lines[i] = (char *)strdup(s);
  p->count = max;
}

void
plan_set(char *s, size_t i, plan_t p)
{
  if (i >= p->count)
    plan_insert(s, i, p);
  else
  {
    if (p->lines[i] != Blank_line)
      free(p->lines[i]);
    if (s == NULL || *s == '\0')
      p->lines[i] = Blank_line;
    else
      p->lines[i] = (char *)strdup(s);
  }
}

void
plan_add(char *s, plan_t p)
{
  plan_insert(s, p->count, p);
}

void
plan_rem(size_t i, plan_t p)
{
  if (i < p->count && p->count > 0)
  {
    size_t j;

    if (p->lines[i] != Blank_line)
      free(p->lines[i]);
    for (j = i ; j < p->count-1 ; j++) /* Shift */
      p->lines[j] = p->lines[j+1];
    p->count -= 1;
    p->lines[p->count] = NULL;
  }
}

void
plan_start(plan_t p)
{
  if (p)
    p->iter = 0;
}

int
plan_next(char **lp, plan_t p)
{
  if (p)
  {
    if (p->iter < p->count)
    {
      if (lp)
	*lp = p->lines[p->iter];
      p->iter += 1;
      return 1;
    }
    p->iter = 0;
  }
  return 0;
}

void
plan_clear(plan_t p)
{
  if (p && p->lines)
  {
    size_t i = p->count;

    while (i--)
    {
      if (p->lines[i] != Blank_line)
	free(p->lines[i]);
      p->lines[i] = NULL;
    }
    p->count = 0;
  }
}

void
plan_free(plan_t p)
{
  if (p)
  {
    if (p->lines)
    {
      plan_clear(p);
      free(p->lines);
      p->lines = NULL;
    }
    free(p);
  }
}
