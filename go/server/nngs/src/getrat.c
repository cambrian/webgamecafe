#include <stdio.h>

main(int argc, char *argv[])
{
  char line[180];
  char SearchFor[20];
  FILE *fp;
  char name[11], rank[10];
  char junk1[5], junk2[5];
  int rating, numgam;
  float numeric_rank, confidence, range_high, range_low;
  int all_rated_wins, all_rated_losses, 
      rated_wins_vs_rated, rated_losses_vs_rated,
      high_confidence_wins, high_confidence_losses;
  int found = 0;    

  strcpy(SearchFor, argv[1]);
  fp = fopen("/home/nngs/ratings/pett/results-rated", "r");
  if(!fp) {
    perror("Could not open file");
    exit(0);
  }
  while((fscanf(fp, "%s %s %d %d", name, rank, &rating, &numgam)) == 4) {
    if(!strcasecmp(name, SearchFor)) {
      printf(".%s: %s (%d) Rated Games: %d\n", name, rank, rating, numgam);
      break;
    }
  }
  fclose(fp);
  fp = fopen("/home/nngs/ratings/pett/results", "r");
  if(!fp) {
    perror("Could not open file");
    exit(0);
  }
  while (fgets(line, 179, fp)) {
    if(sscanf(line, "%s %f %s %f %f/%f %d-%d %d-%d %d-%d", 
                     name, &numeric_rank,
                     rank, &confidence,
                     &range_high, &range_low,
                     &all_rated_wins, &all_rated_losses,
                     &rated_wins_vs_rated, &rated_losses_vs_rated,
                     &high_confidence_wins, &high_confidence_losses) == 12) {
      if(!strcasecmp(name, SearchFor)) {
        found = 1;
        printf(".confidence = %.2f%%\n\
.All rated wins: %d\n\
.All rated losses: %d\n\
.Rated wins versus rated players: %d\n\
.Rated losses versus rated players: %d\n\
.High confidence wins: %d\n\
.High confidence losses: %d\n", 
                confidence * 100, all_rated_wins, all_rated_losses,
                rated_wins_vs_rated, rated_losses_vs_rated,
                high_confidence_wins, high_confidence_losses);
        break;
      }
    }
    else if(sscanf(line, "%s %s %s %f %s %d-%d %d-%d %d-%d",
                     name, junk1,
                     rank, &confidence,
                     junk2,
                     &all_rated_wins, &all_rated_losses,
                     &rated_wins_vs_rated, &rated_losses_vs_rated,
                     &high_confidence_wins, &high_confidence_losses) == 11) {
      if(!strcasecmp(name, SearchFor)) {
        found = 1;
        printf(".%s is Not Rated\n\
.All rated wins: %d\n\
.All rated losses: %d\n\
.Rated wins versus rated players: %d\n\
.Rated losses versus rated players: %d\n\
.High confidence wins: %d\n\
.High confidence losses: %d\n", 
                name,
                all_rated_wins, all_rated_losses,
                rated_wins_vs_rated, rated_losses_vs_rated,
                high_confidence_wins, high_confidence_losses);
        break;
      }
    }
  }
  fclose(fp);
  if(!found) printf("No rating information for %s\n", SearchFor);
} 
