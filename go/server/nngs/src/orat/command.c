/* command.c
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"
#include <assert.h>

#include "nngsconfig.h"
#include "common.h"
#include "command.h"
#include "command_list.h"
#include "nngsmain.h"
#include "utils.h"
#include "playerdb.h"
#include "gamedb.h"
#include "gameproc.h"
#include "vers.h"
#include "network.h"
#include "channel.h"
#include "alias.h"
#ifdef SGI
#include <crypt.h>
#endif

PUBLIC char *mess_dir = DEFAULT_MESS;
PUBLIC char *help_dir = DEFAULT_HELP;
PUBLIC char *info_dir = DEFAULT_INFO;
PUBLIC char *adhelp_dir = DEFAULT_ADHELP;
PUBLIC char *stats_dir = DEFAULT_STATS;
PUBLIC char *player_dir = DEFAULT_PLAYERS;
PUBLIC char *game_dir = DEFAULT_GAMES;
PUBLIC char *problem_dir = DEFAULT_PROBLEMS;
PUBLIC char *def_prompt = DEFAULT_PROMPT;
PUBLIC char *lists_dir = DEFAULT_LISTS;
PUBLIC char *news_dir = DEFAULT_NEWS;
PUBLIC char *server_name = SERVER_NAME;
PUBLIC char *server_address = SERVER_ADDRESS;
PUBLIC char *server_email = SERVER_EMAIL;
PUBLIC char *ratings_file = RATINGS_FILE;
PUBLIC char *intergo_file = INTERGO_FILE;
PUBLIC char *results_file = RESULTS_FILE;
PUBLIC char *emotes_file = EMOTES_FILE;
PUBLIC char *note_file = DEFAULT_NOTE;

PUBLIC int startuptime;
PUBLIC int player_high;
PUBLIC int game_high;
PUBLIC int MailGameResult;
PUBLIC char orig_command[MAX_STRING_LENGTH];

PUBLIC struct stat RatingsBuf1, RatingsBuf2;

PUBLIC int commanding_player = -1;	/* The player whose command your in */

PRIVATE int lastCommandFound = -1;

PUBLIC char *version_string = UTS_VERSION;


/* Copies command into comm, and returns pointer to parameters in
 * parameters
 */
PRIVATE int parse_command(char *com_string,
			   char **comm,
			   char **parameters)
{
  *comm = com_string;
  *parameters = eatword(com_string);
  if (**parameters != '\0') {
    **parameters = '\0';
    (*parameters)++;
    *parameters = eatwhite(*parameters);
  }
  if (strlen(*comm) >= MAX_COM_LENGTH) {
    return COM_BADCOMMAND;
  }
  return COM_OK;
}

/* Puts alias substitution into alias_string */
PRIVATE void alias_substitute(int p, char *com_str, char **alias_str)
{
  char *save_str = com_str;
  char tmp[MAX_COM_LENGTH];
  static char outalias[MAX_STRING_LENGTH];
  int i = 0, done = 0;
  static alias_t i_alias_list = NULL; /* The internal, sorted alias list. */
  char *alias;

  if (!i_alias_list)		/* The first time, update from global list. */
    {
      alias_type *ap;

      i_alias_list = alias_init();
      assert(i_alias_list != NULL);
      for (ap = g_alias_list ; ap->comm_name ; ap++)
	alias_add(ap->comm_name, ap->alias, i_alias_list);
    }

  while (com_str && !iswhitespace(*com_str) && !done) {
    if (i >= MAX_COM_LENGTH) {	/* Too long for an alias */
      *alias_str = save_str;
      return;
    }
    tmp[i] = *com_str;
    com_str++;
    if (ispunct(tmp[i]))
      done = 1;
    i++;
  }
  tmp[i] = '\0';
  if ((alias = alias_lookup(tmp, parray[p].alias_list))) {
    if (com_str)
      sprintf(outalias, "%s%s", alias, com_str);
    else
      sprintf(outalias, "%s", alias);
    *alias_str = outalias;
  } else if ((alias = alias_lookup(tmp, i_alias_list))) {
    if (com_str)
      sprintf(outalias, "%s%s", alias, com_str);
    else
      sprintf(outalias, "%s", alias);
    *alias_str = outalias;
  } else
    *alias_str = save_str;
}

/* Returns pointer to command that matches */
PRIVATE int match_command(char *comm)
{
  int left, right;
  int len = strlen(comm);

  /* Fix this! 'right' is init to the number of commands. This
  ** should really be in command_list.h or counted when starting.
  ** (But as it is here, it's only done once, at the first call.)
  */
  static int noofcomms = 0;
  if (!noofcomms)
  {
    while (command_list[noofcomms].comm_name)
      noofcomms += 1;
  }

  /* Binary search. */
  left = 0;                     /* Left most element. */
  right = noofcomms;            /* Beyond right most element. */
  while (left < right)
  {
    int x, i = (left+right)/2; /* Check the middle element. */

    x = strncmp(comm, command_list[i].comm_name, len);
    if (x < 0)
      right = i;              /* It's on the left side. */
    else if (x > 0)
      left = i+1;             /* It's on the right side. */
    else
    {                       /* Found it. */
      /* Check if it's ambiguous. */
      if (i > 0 &&
          !strncmp(comm, command_list[i-1].comm_name, len))
        return -COM_AMBIGUOUS;
      if (i+1 < noofcomms &&
          !strncmp(comm, command_list[i+1].comm_name, len))
        return -COM_AMBIGUOUS;
      lastCommandFound = i;
      return i;
    }
  }
  return -COM_FAILED;
}

#ifdef WHY_TWO

/* Returns pointer to command that matches */
PRIVATE int match_command(char *comm)
{
  int left, right;
  int len = strlen(comm);

  /* Fix this! 'right' is init to the number of commands. This
  ** should really be in command_list.h or counted when starting.
  ** (But as it is here, it's only done once, at the first call.)
  */
  static int noofcomms = 0;
  if (!noofcomms)
  {
    while (command_list[noofcomms].comm_name)
      noofcomms += 1;
  }

  /* Binary search. */
  left = 0;                     /* Left most element. */
  right = noofcomms;            /* Beyond right most element. */
  while (left < right)
  {
    int x, i = (left+right)/2; /* Check the middle element. */

    x = strncmp(comm, command_list[i].comm_name, len);
    if (x < 0)
      right = i;              /* It's on the left side. */
    else if (x > 0)
      left = i+1;             /* It's on the right side. */
    else
    {                       /* Found it. */
      /* Check if it's ambiguous. */
      if (i > 0 &&
          !strncmp(comm, command_list[i-1].comm_name, len))
        return -COM_AMBIGUOUS;
      if (i+1 < noofcomms &&
          !strncmp(comm, command_list[i+1].comm_name, len))
        return -COM_AMBIGUOUS;
      lastCommandFound = i;
      return i;
    }
  }
  return -COM_FAILED;
}

#endif

/* Gets the parameters for this command */
PRIVATE int get_parameters(int command, char *parameters, param_list params)
{
  int i, parlen;
  int paramLower;
  char c;
  static char punc[2];

  punc[1] = '\0';		/* Holds punc parameters */
  for (i = 0; i < MAXNUMPARAMS; i++)
    (params)[i].type = (int) TYPE_NULL;	/* Set all parameters to NULL */
  parlen = strlen(command_list[command].param_string);
  for (i = 0; i < parlen; i++) {
    c = command_list[command].param_string[i];
    if (isupper(c)) {
      paramLower = 0;
      c = tolower(c);
    } else {
      paramLower = 1;
    }
    switch (c) {
    case 'w':			/* word */
      parameters = eatwhite(parameters);
      if (!*parameters)
	return COM_BADPARAMETERS;
    case 'o':			/* optional word */
      parameters = eatwhite(parameters);
      if (!*parameters)
	return COM_OK;
      (params)[i].val.word = parameters;
      (params)[i].type = TYPE_WORD;
      if (ispunct(*parameters)) {
	punc[0] = *parameters;
	(params)[i].val.word = punc;
	parameters++;
      } else {
	parameters = eatword(parameters);
	if (*parameters != '\0') {
	  *parameters = '\0';
	  parameters++;
	}
      }
      if (paramLower)
	stolower((params)[i].val.word);
      break;
    case 'd':			/* integer */
      parameters = eatwhite(parameters);
      if (!*parameters)
	return COM_BADPARAMETERS;
    case 'p':			/* optional integer */
      parameters = eatwhite(parameters);
      if (!*parameters)
	return COM_OK;
      if (sscanf(parameters, "%d", &(params)[i].val.integer) != 1)
	return COM_BADPARAMETERS;
      (params)[i].type = TYPE_INT;
      parameters = eatword(parameters);
      if (*parameters != '\0') {
	*parameters = '\0';
	parameters++;
      }
      break;
    case 'f':
      parameters = eatwhite(parameters);
      if (!*parameters)
        return COM_OK;
      if (sscanf(parameters, "%f", &(params)[i].val.f) != 1)
        return COM_BADPARAMETERS;
      (params)[i].type = TYPE_FLOAT;
      parameters = eatword(parameters);
      if (*parameters != '\0') {
        *parameters = '\0';
        parameters++;
      }
      break;
    case 'i':			/* word or integer */
      parameters = eatwhite(parameters);
      if (!*parameters)
	return COM_BADPARAMETERS;
    case 'n':			/* optional word or integer */
      parameters = eatwhite(parameters);
      if (!*parameters)
	return COM_OK;
      if (sscanf(parameters, "%d", &(params)[i].val.integer) != 1) {
	(params)[i].val.word = parameters;
	(params)[i].type = TYPE_WORD;
      } else {
	(params)[i].type = TYPE_INT;
      }
      if (ispunct(*parameters)) {
	punc[0] = *parameters;
	(params)[i].val.word = punc;
	(params)[i].type = TYPE_WORD;
	parameters++;
      } else {
	parameters = eatword(parameters);
	if (*parameters != '\0') {
	  *parameters = '\0';
	  parameters++;
	}
      }
      if ((params)[i].type == TYPE_WORD)
	if (paramLower)
	  stolower((params)[i].val.word);
      break;
    case 's':			/* string to end */
      if (!*parameters)
	return COM_BADPARAMETERS;
    case 't':			/* optional string to end */
      if (!*parameters)
	return COM_OK;
      (params)[i].val.string = parameters;
      (params)[i].type = TYPE_STRING;
      while (*parameters)
	parameters = nextword(parameters);
      if (paramLower)
	stolower((params)[i].val.string);
      break;
    }
  }
  if (*parameters)
    return COM_BADPARAMETERS;
  else
    return COM_OK;
}

PRIVATE void printusage(int p, char *command_str)
{
  int i, parlen;
  int command;
  char c;

  if ((command = match_command(command_str)) < 0) {
    pprintf(p, "%s  UNKNOWN COMMAND\n", SendCode(p, ERROR));
    return;
  }
  parlen = strlen(command_list[command].param_string);
  for (i = 0; i < parlen; i++) {
    c = command_list[command].param_string[i];
    if (isupper(c))
      c = tolower(c);
    switch (c) {
    case 'w':			/* word */
      pprintf(p, " word");
      break;
    case 'o':			/* optional word */
      pprintf(p, " [word]");
      break;
    case 'd':			/* integer */
      pprintf(p, " integer");
      break;
    case 'p':			/* optional integer */
      pprintf(p, " [integer]");
      break;
    case 'i':			/* word or integer */
      pprintf(p, " {word, integer}");
      break;
    case 'n':			/* optional word or integer */
      pprintf(p, " [{word, integer}]");
      break;
    case 's':			/* string to end */
      pprintf(p, " string");
      break;
    case 't':			/* optional string to end */
      pprintf(p, " [string]");
      break;
    }
  }
}

PUBLIC int process_command(int p, char *com_string)
{
  int which_command, retval;
  param_list params;
  char *alias_string;
  char *comm, *parameters;

  if (!com_string)
    return COM_FAILED;
  if (Debug > 8) {
    Logit("DEBUG: %s (%d/%d): > %s <", 
           parray[p].name, parray[p].socket, p, com_string);
  }
  KillTrailWhiteSpace(com_string);
  alias_substitute(p, com_string, &alias_string);
  if (Debug > 9) {
    if (com_string != alias_string)
      Logit("DEBUG: %s -alias-: > %s <", parray[p].name, alias_string);
  }
  if ((retval = parse_command(alias_string, &comm, &parameters)))
    return retval;
  stolower(comm);               /* All commands are case-insensitive */
  if(parray[p].game >= 0) {
    if(go_move(garray[parray[p].game].GoGame, comm)) {
      return COM_ISMOVE;
    }
  }
  if ((which_command = match_command(comm)) < 0)
    return -which_command;
  if (parray[p].adminLevel < command_list[which_command].adminLevel) {
    return COM_RIGHTS;
  }
  if ((retval = get_parameters(which_command, parameters, params)))
    return retval;
  return command_list[which_command].comm_func(p, params);
}

PRIVATE void process_login(int p, char *loginname)
{
  int problem = 1;
  int i;

  loginname = eatwhite(loginname);

  if (!*loginname) {		/* do something in here? */
  } else {
    char *loginnameii = (char *) strdup(loginname);
    stolower(loginname);

    if ((!alphastring(loginname)) || (!printablestring(loginname))) {
      pprintf(p, "\nSorry, names can only consist of lower and upper case letters.  Try again.\n");
      Logit("LOGIN: Bad userid: %s", loginname);
    } else if (strlen(loginname) < 2) {
      pprintf(p, "\nA name should be at least two characters long!  Try again.\n");
      Logit("LOGIN: Bad userid: %s", loginname);
    } else if (strlen(loginname) > 10) {
      pprintf(p, "\nSorry, names may be at most 10 characters long.  Try again.\n");
      Logit("LOGIN: Bad userid: %s", loginname);
    } else {
      problem = 0;
      if (player_read(p, loginname)) {
        do_copy(parray[p].name, loginnameii, MAX_NAME); 
	pprintf(p, "\n\"%s\" is not a registered name.  You may use this name to play unrated games.\n(After logging in, do \"help register\" for more info on how to register.)\n\nThis is a guest account.\nYour account name is %s.\n", 
                         parray[p].name,
			 parray[p].name);
        Ochannel_add(CSHOUT, p);
        Ochannel_add(CGSHOUT, p);
        Ochannel_add(CLSHOUT, p);
        Ochannel_add(CLOGON, p);
        Ochannel_add(CGAME, p);
      } else {
        pprintf(p, "\n%s",parray[p].client ? "1 1\n" : "Password: ");
      }
      parray[p].status = PLAYER_PASSWORD;
      /* player_resort(); */
      net_echoOff(parray[p].socket);
      free(loginnameii);
    }
  }

  if (problem) {
    for (i = 0; i < MAX_CHANNELS; i++) channel_remove(i, p);
    for (i = 0; i < MAX_O_CHANNELS; i++) Ochannel_remove(i, p);

    pprintf(p, "Login: ");
  }
  if((!parray[p].registered) && (!problem))  pcommand(p, "\n");
  return;
}

void boot_out (int p,int p1)
{
  int fd;
  pprintf (p, "\n **** %s is already logged in - kicking other copy out. ****\n", parray[p].name);
  pprintf (p1, "**** %s has arrived - you can't both be logged in. ****\n", parray[p].name);
  fd = parray[p1].socket;
#ifdef ALREADY_LOGGED_ON_DEBUG
  Logit("Already logged on: name=%s [login=%s] fd = %d, (name=%s [login=%s], fd = %d)", parray[p].name, parray[p].login, parray[p].socket, 
        parray[p1].name, parray[p1].login, parray[p1].socket);
#endif  
  process_disconnection(fd);
  net_close(fd);
  player_remove(p1);
}

PRIVATE int process_password(int p, char *password)
{
  int p1;
  char salt[3];
  int fd;
  unsigned int fromHost;
  int messnum, i, j;
  char tmptext[256];
  char tmp[256];
  char ctmptext[256];
  int len, clen;

  net_echoOn(parray[p].socket);

  if (parray[p].passwd[0] && parray[p].registered) {
    salt[0] = parray[p].passwd[0];
    salt[1] = parray[p].passwd[1];
    salt[2] = '\0';
    if(strlen(password) < 2) {
      pprintf(p, "\n%s", parray[p].client ? "1 1\n" : "Password: ");
      parray[p].status = PLAYER_PASSWORD;
      net_echoOff(parray[p].socket);
      return COM_OKN;
    }

    if (strcmp(crypt(password, salt), parray[p].passwd)) {
        Logit("%s tried to log in from %s with a bad password!", 
			parray[p].name,
			dotQuad(parray[p].thisHost));
      fd = parray[p].socket;
      fromHost = parray[p].thisHost;
      for (i=0; i < MAX_CHANNELS; i++) {
        if (on_channel(i, p1)) {
          channel_remove(i, p);
        }
      }
      player_clear(p);
      parray[p].logon_time = time(0);
      parray[p].status = PLAYER_LOGIN;
      /* player_resort(); */
      parray[p].socket = fd;
      parray[p].thisHost = fromHost;
      if (*password) {
	pprintf(p, "\n\nInvalid password!\n\n");
      }
      pprintf(p, "Login: ");
      if(parray[p].pass_tries++ > 3) {
        return COM_LOGOUT;
      }
      return COM_OKN;
    }
  }
  /* This should really be a hash! */
  for(p1 = 0; p1 < p_num; p1++) {
    if(parray[p1].name != NULL) {
      if((!strcasecmp (parray[p].name, parray[p1].name)) && (p != p1)) {
        if(parray[p].registered == 0) {
          pprintf (p, "\n*** Sorry %s is already logged in ***\n", 
                       parray[p].name);
          return COM_LOGOUT;
        }
        boot_out (p,p1);
      }
    }
  }
  /* Check if really an admin */
  if(parray[p].adminLevel > 0)
    if(!in_list("admin", parray[p].name)) 
      parray[p].adminLevel = 0;

  parray[p].status = PLAYER_PROMPT;
  pprintf(p, "%s\n", parray[p].client ? "1 5" : "");
  if (parray[p].adminLevel > 0) {
    pprintf(p, "%s\n", parray[p].client ? "9 File" : "");
    psend_raw_file(p, mess_dir, MESS_ADMOTD);
  } else {
    pprintf(p, "%s\n", parray[p].client ? "9 File" : "");
    psend_raw_file(p, mess_dir, MESS_MOTD);
  }
  if(Debug) Logit("About to do user MOTD");
  /* User specific MOTD */
  sprintf(tmptext, "%s.%s", MESS_MOTD, parray[p].name);
  psend_raw_file(p, mess_dir, tmptext);
  pprintf(p, "\n");
  pprintf(p, "%s\n", parray[p].client ? "9 File" : "");
  if(Debug) Logit("Done with user motd");

  if (!parray[p].passwd && parray[p].registered)
    pprintf(p, "%s*** You have no password. Please set one with the password command.\n", SendCode(p, ERROR));
  if (!parray[p].registered)
    psend_raw_file(p, mess_dir, MESS_UNREGISTERED);
  if(Debug) Logit("About to resort");
  player_resort();
  if(Debug) Logit("About to write login");
  player_write_login(p);
  if(Debug) Logit("Made it to announce");
  sprintf(tmptext, "{%s [%3.3s%s] has connected.}\n",
                parray[p].name,
                parray[p].srank,
                parray[p].rated ? "*" : " ");
  sprintf(ctmptext, "%d {%s [%3.3s%s] has connected.}\n",
                SHOUT,
                parray[p].name,
                parray[p].srank,
                parray[p].rated ? "*" : " ");

  len = strlen(tmptext);
  clen = strlen(ctmptext);

/*  for (p1 = 0; p1 < p_num; p1++) { */
  for (i = 0; i < OnumOn[CLOGON]; i++) {
    p1 = Ochannels[CLOGON][i];
    if ((p1 == p) ||
        (parray[p1].status != PLAYER_PROMPT) ||
        (!parray[p1].i_login))
      continue;
    if (parray[p1].adminLevel > 0) {
      pprintf_prompt(p1, "%s{%s [%3.3s%s] (%s%s: %s) has connected.}\n", 
                SendCode(p1, SHOUT),
                parray[p].name,
                parray[p].srank,
                parray[p].rated ? "*" : " ",
		(parray[p].registered ? "R" : "U"),
		(parray[p].adminLevel ? "*" : ""),
		(dotQuad(parray[p].thisHost)));
    } else {
      if(parray[p1].client) {
        net_send(parray[p1].socket, ctmptext, clen);
        sprintf(tmp, "1 %d\n", parray[p1].state);
        net_sendStr(parray[p1].socket, tmp);
      }
      else {
        net_send(parray[p1].socket, tmptext, len);
        if(parray[p1].state == SCORING) {
          net_send(parray[p1].socket, "Enter Dead Group: ", 18);
        }
        else { 
          if(parray[p1].extprompt) {
            sprintf(tmp, "|%s/%d%s| %s ",
            parray[p1].last_tell >= 0 ? parray[parray[p1].last_tell].name : "",
            parray[p1].last_channel,
            parray[p1].busy[0] == '\0' ? "" : "(B)",
            parray[p1].prompt);
          }
          else {
            sprintf(tmp, "%s",parray[p1].prompt);
          }
          net_sendStr(parray[p1].socket, tmp);
        } 
      }
    }
  }
  for (i = 0; i < MAX_CHANNELS; i++) {
    if (on_channel(i, p)) {
      pprintf(p, "%sChannel %d Topic: %s\n%s", SendCode(p, INFO),
                  i, carray[i].Title, SendCode(p, INFO));
      for (j = 0; j < numOn[i]; j++) {
        if(channels[i][j] == p) continue;
        if ((parray[channels[i][j]].status == PLAYER_PASSWORD)
             || (parray[channels[i][j]].status == PLAYER_LOGIN))
         continue;
        pprintf_prompt(channels[i][j], "\n%s%s has joined channel %d.\n",
                   SendCode(channels[i][j], INFO), parray[p].name, i);
      }
      for (j = 0; j < numOn[i]; j++) {
        pprintf(p, " %s", parray[channels[i][j]].name);
      }
      pprintf(p, "\n");
    }
  }
  if (parray[p].adminLevel) {
    pprintf(p, "%sWelcome to the Admin Channel.  Topic is: %s\n",
            SendCode(p, INFO), Ocarray[CASHOUT].Title);
    for(j = 0; j < OnumOn[CASHOUT]; j++) {
      if(Ochannels[CASHOUT][j] == p) continue;
      if ((parray[Ochannels[CASHOUT][j]].status == PLAYER_PASSWORD)
       || (parray[Ochannels[CASHOUT][j]].status == PLAYER_LOGIN))
        continue;
      pprintf_prompt(Ochannels[CASHOUT][j], "\n%s## --- ##: %s has joined the ADMIN channel.\n",
        SendCode(Ochannels[CASHOUT][j], INFO), 
        parray[p].name);
    }
  }

  num_logins++;
  if(Debug) Logit("Doing messages....");
  messnum = player_num_messages(p);
  if(Debug) Logit("Don with messages....");
  if (messnum) {
    pprintf(p, "%sYou have %d messages.  Type \"messages\" to display them\n", 
                SendCode(p, INFO),
                messnum);
  }
#ifdef NOTIFY
  player_notify_present(p);
  player_notify(p, "arrived", "arrival"); 
#endif
#ifdef CHECK_LAST_HOST
  /*  No real need to check this anymore */
  if (parray[p].registered && (parray[p].lastHost != 0) &&
      (parray[p].lastHost != parray[p].thisHost)) {
    Logit("Player %s: Last login: %s ", parray[p].name,
	    dotQuad(parray[p].lastHost));
  }
#endif
  parray[p].lastHost = parray[p].thisHost;
  parray[p].state = WAITING;
  pprintf_prompt(p, "%sNNGS %s (entry on [Ignore this])\n", SendCode(p, VERSION), version_string);
  if(parray[p].registered == 0) parray[p].water = 0;
  return 0;
}

PRIVATE int process_prompt(int p, char *command)
{
  int retval;

  command = eatwhite(command);
  if (!*command) {
    if(!parray[p].client) pprintf(p, "%s", parray[p].prompt);
    else pprintf_prompt(p, "\n");
    return COM_OK;
  }
  retval = process_command(p, command);
  switch (retval) {
  case COM_OKN:
    retval = COM_OK;
    pprintf_prompt(p, "");
    break;
  case COM_OK:
    retval = COM_OK;
    pprintf_prompt(p, "\n");
    break;
  case COM_NOSUCHGAME:
    pprintf(p, "%sThere is no such game.\n", SendCode(p, ERROR));
    pprintf_prompt(p, "\n");
    retval = COM_OK;
    break;
  case COM_OK_NOPROMPT:
    retval = COM_OK;
    break;
  case COM_ISMOVE:
    retval = COM_OK;
#ifdef PAIR
    process_move(p, command, 1);
#else
    process_move(p, command);
#endif
    break;
  case COM_RIGHTS:
    if(!parray[p].client) pprintf(p, "%s", parray[p].prompt);
    else pprintf(p, "1 %d\n",parray[p].state);
    retval = COM_OK;
    break;
  case COM_AMBIGUOUS:
    {
      int len = strlen(command);
      int i = 0;
      pprintf( p, "%sAmbiguous command. Matches:", SendCode(p, ERROR));
      while (command_list[i].comm_name) {
        if (!strncmp(command_list[i].comm_name, command, len)) {
          pprintf( p, " %s", command_list[i].comm_name );
        }
        i++;
      }
    }
    if(!parray[p].client) pprintf(p, "\n%s", parray[p].prompt);
    else pprintf(p, "\n1 %d\n",parray[p].state);
    retval = COM_OK;
    break;
  case COM_BADPARAMETERS:
    pprintf(p, "%sUsage: \n%s%s", SendCode(p, ERROR), SendCode(p, ERROR),
            command_list[lastCommandFound].comm_name);
    printusage(p, command_list[lastCommandFound].comm_name);
    pprintf(p, "\n%sSee 'help %s' for a complete description.\n", SendCode(p, ERROR), command_list[lastCommandFound].comm_name);
    if(!parray[p].client) pprintf(p, "%s", parray[p].prompt);
    else pprintf(p, "1 %d\n",parray[p].state);
    retval = COM_OK;
    break;
  case COM_FAILED:
  case COM_BADCOMMAND:
    pprintf_prompt(p, "%s%s: Unknown command.\n", SendCode(p, ERROR), command);
    retval = COM_OK;
    break;
  case COM_LOGOUT:
    retval = COM_LOGOUT;
    break;
  }
  return retval;
}

/* Return 1 to disconnect */
PUBLIC int process_input(int fd, char *com_string)
{
  int p = player_find(fd);
  int retval = 0;

  if (p < 0) {
    Logit("Input from a player not in array!");
    return -1;
  }
  commanding_player = p;
  parray[p].last_command_time = time(0);
  strcpy(orig_command, com_string);
  switch (parray[p].status) {
  case PLAYER_PROMPT:
    retval = process_prompt(p, com_string);
    break;
  case PLAYER_LOGIN:
    process_login(p, com_string);
    break;
  case PLAYER_PASSWORD:
    retval = process_password(p, com_string);
    break;
  case PLAYER_EMPTY:
    Logit("Command from an empty player!");
    break;
  case PLAYER_NEW:
    Logit("Command from a new player!");
    break;
  case PLAYER_INQUEUE:
    /* Ignore input from player in queue */
    break;
  }
  commanding_player = -1;
  return retval;
}

PUBLIC int process_new_connection(int fd, unsigned int fromHost)
{
  int p;

  p = player_new();

  parray[p].status = PLAYER_LOGIN;  /* Set status to LOGIN */
  parray[p].socket = fd;            /* track the FD */
  parray[p].logon_time = time(0);   /* Set the login time */
  parray[p].thisHost = fromHost;    /* set the host FROM field */
  parray[p].pass_tries = 0;         /* Clear number of password attempts */
  psend_raw_file(p, mess_dir, MESS_LOGIN); /* Send the login file */
  pprintf(p, "Login: ");            /* Give them a prompt */
  return 0;                         /* return */
}

PUBLIC int process_disconnection(int fd)
{
  int p = player_find(fd);
  int p1, i, j;

  if (p < 0) {
    Logit("Disconnect from a player not in array!");
    return -1;
  }
  if (parray[p].status == PLAYER_PROMPT) {
/*    for (p1 = 0; p1 < p_num; p1++) { */
    for (i = 0; i < OnumOn[CLOGON]; i++) {
      p1 = Ochannels[CLOGON][i];
      if ((p1 == p) || 
          (parray[p1].status != PLAYER_PROMPT) || 
          (!parray[p1].i_login)) 
        continue;
      pprintf_prompt(p1, "%s{%s has disconnected}\n", 
		SendCode(p1, SHOUT),
		parray[p].name);
    }
#ifdef NOTIFY
    player_notify(p, "departed", "departure");
    player_notify_departure(p); 
#endif
    player_write_logout(p);
    num_logouts++;
    if (parray[p].registered)
      player_save(p);
  }
  for (i = 0; i < MAX_CHANNELS; i++) {
    if (on_channel(i, p)) {
      for (j = 0; j < numOn[i]; j++) {
        if((channels[i][j] == p) ||
          (parray[channels[i][j]].status != PLAYER_PROMPT) ||
          (parray[p].status != PLAYER_PROMPT))
        continue;

        pprintf_prompt(channels[i][j], "\n%s%s has left channel %d.\n",
                   SendCode(channels[i][j], INFO), parray[p].name, i);
      }
    }
  }
  player_remove(p);
  return 0;
}

PUBLIC int process_incomplete(int fd, char *com_string)
{
  int p = player_find(fd);
  char last_char;

  if (com_string[0])
    last_char = com_string[strlen(com_string) - 1];
  else
    last_char = '\0';

  if (p < 0) {
    Logit("Incomplete command from a player not in array!");
    return -1;
  }
  if ((strlen(com_string) == 1) && (com_string[0] == '\4')) {	/* ctrl-d */
    if (parray[p].status == PLAYER_PROMPT)
      process_input(fd, "quit");
    return COM_LOGOUT;
  }
  if (last_char == '\3') {	/* ctrl-c */
    if (parray[p].status == PLAYER_PROMPT) {
      if(!parray[p].client) pprintf(p, "\n%s", parray[p].prompt);
      else pprintf(p, "\n1 %d\n",parray[p].state);
      return COM_FLUSHINPUT;
    } else {
      return COM_LOGOUT;
    }
  }
  return COM_OK;
}

/* Called every few seconds */
PUBLIC int process_heartbeat(int *fd)
{
  static int last_ratings = 0;
  static int lastcalled = 0;
  static int last_results = 0;
  int time_since_last;
  int p;
  static int last_idle_check;
  static int resu = 0;
  int now = time(0);
  FILE *fp;
  int rat, gm, orat;
  char rnk[10], nm[11];

  /*game_update_times(); */

  if (lastcalled == 0)
    time_since_last = 1;
  else
    time_since_last = now - lastcalled;
  lastcalled = now;
  if (time_since_last == 0)
    return(COM_OK);

  /* Check for timed out connections */
  if(last_idle_check > 60) {  /* Now only check every minute for idle */
    for (p = 0; p < p_num; p++) {
      if (((parray[p].status == PLAYER_LOGIN)     ||
            (parray[p].status == PLAYER_PASSWORD)) &&
  	  (player_idle(p) > MAX_LOGIN_IDLE))    { 
        *fd = parray[p].socket;
        return COM_LOGOUT;
      }
      if ((parray[p].adminLevel == 0) &&
          (player_idle(p) > MAX_IDLE)  &&
          (parray[p].state == WAITING)  &&
          (parray[p].status == PLAYER_PROMPT)) {
        pcommand(p, "quit");
      }
    }
    last_idle_check = 0;
  } else {
    last_idle_check++;
  }
  if (last_ratings == 0) {
    last_ratings = (now - (5 * 60 * 60)) + 120;	/* Do one in 2 minutes */
  }
  else {
    if (last_ratings + 6 * 60 * 60 < now) { /* Every 6 hours */
      last_ratings = now;
#ifdef LADDERSIFT
      Logit("Sifting 19x19 ladder");
      PlayerSift(Ladder19, 14);  /* Do the ladder stuff, 19x19 is 14 days */
      Logit("Sifting 9x9 ladder");
      PlayerSift(Ladder9, 7); /* Do the ladder stuff, 9x9 is 7 days */
#endif
    }
  }
#ifdef NNGSRATED
  if (last_results == 0) {
    last_results = now;
    stat(ratings_file, &RatingsBuf1);  /* init the stat buf */
    stat(ratings_file, &RatingsBuf2);
  }
  else { 
    if(last_results + (60 * 2) < now) { /* every 2 minutes */
      last_results = now;
      stat(ratings_file, &RatingsBuf2);
      resu = RatingsBuf2.st_mtime - RatingsBuf1.st_mtime;
      if(resu > 0) {
        stat(ratings_file, &RatingsBuf1);  /* re-init the stat buf */
        stat(ratings_file, &RatingsBuf2);  /* re-init the stat buf */
        Logit("Time to re-read the ratings file.  :)");
        fp = fopen(ratings_file, "r");
        for (p = 0; p < p_num; p++) {
          if ((parray[p].status == PLAYER_LOGIN)     ||
              (parray[p].status == PLAYER_PASSWORD)  ||
              (strcmp(parray[p].ranked, "NR") == 0)) {
            continue;
          }
          if (parray[p].registered == 0) {
            pprintf_prompt(p, "%sPlease see \"Help register\"\n", SendCode(p, INFO));
            continue;
          }
          if(fp) {
            fseek(fp, SEEK_SET, 0);
            while((fscanf(fp, "%s %s %d %d", nm, rnk, &rat, &gm)) == 4) {
              if(!strcmp(parray[p].name, nm)) {
                if(rnk[strlen(rnk) - 1] == '*') {
                  rnk[strlen(rnk) - 1] = '\0';
                  do_copy(parray[p].srank, rnk, MAX_SRANK);
                  parray[p].rated = 1;
                }
                else 
                  do_copy(parray[p].srank, rnk, MAX_SRANK);
                orat = parray[p].rating;
                parray[p].rating = rat;
                parray[p].orating = rat;
                parray[p].numgam = gm;
                if(orat == parray[p].rating) {
                  pprintf_prompt(p, "%sRatings update: You are still %s%s (%d, %d rated games)\n",
                            SendCode(p, INFO),
                            parray[p].srank,
                            parray[p].rated ? "*" : "",
                            parray[p].rating,
                            parray[p].numgam);
                } else {
                  pprintf_prompt(p, "%sRatings update: You are now %s%s (%d (were %d), %d rated games)\n", 
                            SendCode(p, INFO), 
                            parray[p].srank, 
                            parray[p].rated ? "*" : "", 
                            parray[p].rating, 
                            orat, 
                            parray[p].numgam);
                }
                break;
              }  
            }
          }
        }
        fclose(fp);
      }
    }
  }
#endif /* NNGSRATED */
  ShutHeartBeat();
  return COM_OK;
}

PUBLIC void commands_init()
{
  FILE *fp, *afp;
  char fname[MAX_FILENAME_SIZE];
  int i = 0;

  sprintf(fname, "%s/commands", help_dir);
  fp = fopen(fname, "w");
  if (!fp) {
    Logit("Could not write commands help file.");
    return;
  }
  sprintf(fname, "%s/admin_commands", adhelp_dir);
  afp = fopen(fname, "w");
  if (!afp) {
    Logit("Could not write admin_commands help file.");
    fclose(fp);
    return;
  }
  while (command_list[i].comm_name) {
    if (command_list[i].adminLevel >= ADMIN_ADMIN) {
      fprintf(afp, "%s\n", command_list[i].comm_name);
    } else {
      fprintf(fp, "%s\n", command_list[i].comm_name);
    }
    i++;
  }
  fclose(fp);
  fclose(afp);
}

PUBLIC void TerminateCleanup()
{
  int p1;
  int g;

  for (g = 0; g < g_num; g++) {
    if (garray[g].status != GAME_ACTIVE)
      continue;
    game_ended(g, NEITHER, END_ADJOURN);
  }
  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status == PLAYER_EMPTY)
      continue;
    pprintf(p1, "%s    **** Server shutting down immediately. ****\n", SendCode(p1, DOWN));
    if (parray[p1].status != PLAYER_PROMPT) {
      close(parray[p1].socket);
    } else {
      pprintf(p1, "%sLogging you out.\n",SendCode(p1, DOWN));
      psend_raw_file(p1, mess_dir, MESS_LOGOUT);
      player_write_logout(p1);
      if (parray[p1].registered)
	player_save(p1);
    }
  }
}

