/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996 Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"
#include <assert.h>

#include "common.h"
#include "ladder.h"
#include "command.h"
#include "comproc.h"
#include "playerdb.h"
#include "utils.h"
#include "network.h"
#include "nngsmain.h"
#include "nngsconfig.h"
#include "channel.h"
#include "gamedb.h"
extern long time();

PUBLIC player parray[PARRAY_SIZE]; 
PUBLIC int p_num = 0;

PUBLIC int sort_alpha[256];
PUBLIC int sort_ladder9[256];
PUBLIC int sort_ladder19[256];

int get_empty_slot();

PUBLIC int get_empty_slot()
{
  int i;

  for (i = 0; i < p_num; i++) {
    if (parray[i].status == PLAYER_EMPTY) {
      return i;
    }
  }
  p_num++;
  parray[p_num - 1].status = PLAYER_EMPTY;
  return p_num - 1;
}

PUBLIC void player_array_init()
{
  int i;

  for (i = 0; i < PARRAY_SIZE; i++) {  /* Added {}, bug number 476890 */
    player_zero(i);                    /* Found by pem, squished by geek */
    parray[i].status = PLAYER_EMPTY;
  }
}

PUBLIC void player_init()
{
  int i;

  for (i = 0; i < net_maxConn; i++) {
    sort_alpha[i] = sort_ladder9[i] = sort_ladder19[i] = i;
  }
}

PUBLIC int player_cmp(int p1, int p2, int sorttype)
{
  const Player *LP1, *LP2;
  int a, b;

  if (parray[p1].status != PLAYER_PROMPT) {
    if (parray[p2].status != PLAYER_PROMPT)
      return 0;
    else
      return -1;
  }
  if (parray[p2].status != PLAYER_PROMPT)
    return 1;
  switch (sorttype) {
  case SORT_LADDER9:
    LP1 = PlayerNamed(Ladder9, parray[p1].name);
    LP2 = PlayerNamed(Ladder9, parray[p2].name);
    if(LP1 == NULL) a = num_9 + 1; else a = LP1->idx;
    if(LP2 == NULL) b = num_9 + 1; else b = LP2->idx;
    if(a > b) return 1;
    else return -1;
    break;
  case SORT_LADDER19:
    LP1 = PlayerNamed(Ladder19, parray[p1].name);
    LP2 = PlayerNamed(Ladder19, parray[p2].name);
    if(LP1 == NULL) a = num_19 + 1; else a = LP1->idx;
    if(LP2 == NULL) b = num_19 + 1; else b = LP2->idx;
    if(a > b) return 1;
    else return -1;
    break;
  case SORT_ALPHA:
    if(parray[p1].rating == parray[p2].rating)
      return strcmp(parray[p1].login, parray[p2].login);
    return(parray[p1].rating > parray[p2].rating);
    break;
  default:
    break;
  }
  return 0;
}

PRIVATE void player_swap(int p1, int p2, int *sort_array)
{
  int tmp;

  tmp = sort_array[p1];
  sort_array[p1] = sort_array[p2];
  sort_array[p2] = tmp;
}

/* Yuck bubble sort! */
PUBLIC void player_resort(void)
{
  int i, j;
  int anychange = 1;
  int count = 0;

  for (i = 0; i < p_num; i++) {
    sort_alpha[i] = sort_ladder9[i] = sort_ladder19[i] = i;
    count++;
  }
  for (i = 0; (i < count - 1) && anychange; i++) {
    anychange = 0;
    for (j = 0; j < count - 1; j++) {
      if (player_cmp(sort_alpha[j], sort_alpha[j + 1], SORT_ALPHA) > 0) {
	player_swap(j, j + 1, sort_alpha);
	anychange = 1;
      }
      if (player_cmp(sort_ladder9[j], sort_ladder9[j + 1], SORT_LADDER9) > 0) {
	player_swap(j, j + 1, sort_ladder9);
	anychange = 1;
      }
      if (player_cmp(sort_ladder19[j], sort_ladder19[j+1], SORT_LADDER19) > 0) {
	player_swap(j, j + 1, sort_ladder19);
	anychange = 1;
      }
    }
  }
}

PUBLIC int player_new()
{
  int i;

  i = get_empty_slot();
  player_zero(i);
  parray[i].status = PLAYER_NEW; /* PEM */
  return i;
}


PUBLIC int player_zero(int p)
{
  int i;

  parray[p].name[0] = '\0';
  parray[p].login[0] = '\0';
  parray[p].fullName[0] = '\0';
  parray[p].emailAddress[0] = '\0';
  strncpy(parray[p].prompt,"#> ", 3);
  parray[p].passwd[0] = '\0';
  parray[p].RegDate[0] = '\0';
  parray[p].socket = -1;
  parray[p].registered = 0;
  parray[p].water = 3;
  parray[p].busy[0]='\0';
  /* [PEM]: This is set to 0 (PLAYER_EMPTY) further down.
    parray[p].status = PLAYER_NEW;
    */
  parray[p].d_height = 24;
  parray[p].d_width = 79;
  parray[p].last_file[0] = '\0';
  parray[p].last_file_line = 0;
  parray[p].open = 1;
  parray[p].numgam = 0;
  parray[p].client = 0;
  parray[p].which_client = 0;
  parray[p].rated = 0;
  parray[p].ropen = 1;
  parray[p].bell = 0;
  parray[p].extprompt = 0;
  parray[p].notifiedby = 0;
  parray[p].i_login = 1;
  parray[p].i_game = 1;
  parray[p].i_verbose = 1;
  parray[p].i_shout = 1;
  parray[p].i_gshout = 1;
  parray[p].i_lshout = 1;
  parray[p].i_tell = 1;
  parray[p].i_robot = 0;
  parray[p].i_kibitz = 1;
  parray[p].looking = 0;
  parray[p].Private = 0;
  parray[p].automail = 0;
  parray[p].game = -1;
  parray[p].last_tell = -1;
  parray[p].last_pzz = -1;
  parray[p].last_tell_from = -1;
  parray[p].last_channel = -1;
  parray[p].logon_time = 0;
  parray[p].last_command_time = time(0);
  parray[p].num_from = 0;
  parray[p].num_to = 0;
  parray[p].adminLevel = 0;
  parray[p].num_plan = 0;
  parray[p].num_censor = 0;
  parray[p].num_logons = 0;
  parray[p].nochannels = 0;
  parray[p].num_observe = 0;
  parray[p].def_time = 90;
  parray[p].def_size = 19;
  parray[p].def_byo_time = 10;
  parray[p].def_byo_stones = 25;
  parray[p].bmuzzled = 0;
  parray[p].muzzled = 0;
  parray[p].gmuzzled = 0;
  parray[p].tmuzzled = 0;
  parray[p].kmuzzled = 0;
  parray[p].last_problem = 0;
  parray[p].thisHost = 0;
  parray[p].lastHost = 0;
  parray[p].lastColor = WHITE;
  parray[p].rating = 0;
  parray[p].orating = 0;
  parray[p].alias_list = alias_init();
  assert(parray[p].alias_list != NULL);
  parray[p].opponent = -1;
  parray[p].last_opponent = -1;
  do_copy(parray[p].ranked, "NR", MAX_RANKED);
  do_copy(parray[p].rank, " ", MAX_RANK);
  do_copy(parray[p].srank, "NR", MAX_SRANK);
  parray[p].gowins = 0;
  parray[p].golose = 0;
  parray[p].match_type = 0;
  parray[p].gonum_white = 0;
  parray[p].gonum_black = 0;
  parray[p].status = PLAYER_EMPTY; /* [PEM]: Was 0 (same thing). */
  for(i = 0; i < MAX_PLAN; i++) {
    parray[p].planLines[i] = (char *) strdup("-");
  }
  for(i = 0; i < MAX_CENSOR; i++) {
    parray[p].censorList[i] = (char *) strdup("-");
  }
  for (i = 0; i < MAX_CHANNELS; i++) channel_remove(i, p);
  for (i = 0; i < MAX_O_CHANNELS; i++) Ochannel_remove(i, p);

  return 0;
}

PUBLIC int player_free(int p)
{
  int i;

  for (i = 0; i < MAX_PLAN; i++) { 
    if(parray[p].planLines[i] == (char *) NULL) continue;
    free(parray[p].planLines[i]);
  }

  for (i = 0; i < MAX_CENSOR; i++) {
    if(parray[p].censorList[i] == (char *) NULL) continue;
    free(parray[p].censorList[i]);
  }
  alias_free(parray[p].alias_list);
  for (i = 0; i < MAX_CHANNELS; i++) channel_remove(i, p);
  for (i = 0; i < MAX_O_CHANNELS; i++) Ochannel_remove(i, p);
  return 0;
}

PUBLIC int player_clear(int p)
{
  player_free(p);
  player_zero(p);
  parray[p].status = PLAYER_EMPTY; /* PEM */
  return 0;
}

PUBLIC int player_remove(int p)
{
  int i;

  player_decline_offers(p, -1, -1);
  player_withdraw_offers(p, -1, -1);

  if (parray[p].game >= 0) {	/* Player disconnected in the middle of a
				   game! */
    game_disconnect(parray[p].game, p);
  }

  /* [PEM]: Skip these loops if the p is empty or new. */
  if (parray[p].status != PLAYER_EMPTY &&
      parray[p].status != PLAYER_NEW)
  {    
    ReallyRemoveOldGamesForPlayer(p);
  }
  for (i = 0; i < p_num; i++) {
    if (parray[i].status == PLAYER_EMPTY)
      continue;
      if (parray[i].last_tell_from == p)
	parray[i].last_tell_from = -1;
      if (parray[i].last_tell == p)
	parray[i].last_tell = -1;
      if (parray[i].last_pzz == p)
	parray[i].last_pzz = -1;
      if (parray[i].last_opponent == p)
	parray[i].last_opponent = -1;
    }
  for (i = 0; i < MAX_CHANNELS; i++)
    channel_remove(i, p);
  for (i = 0; i < MAX_O_CHANNELS; i++)
    Ochannel_remove(i, p);
  player_clear(p);
  parray[p].status = PLAYER_EMPTY;
  if (Debug) Logit( "Removed parray[%d/%d]", p, p_num-1);
  player_resort();
  return 0;
}

PRIVATE int got_attr_value(int p, char *attr, char *value, FILE * fp, char *fname)
{
  int i, len;
  char *s;
  char tmp[MAX_LINE_SIZE], *tmp1;

  if (!strcmp(attr, "vars:")) {
    s = strtok(value, ":");
    if(s == NULL) return 0;
    parray[p].open = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].water = atoi(s);
    if(parray[p].water < 0) parray[p].water = 3;
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].client = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].ropen = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].bell = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_login = atoi(s);
    if(parray[p].i_login) Ochannel_add(CLOGON, p);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_game = atoi(s);
    if(parray[p].i_game) Ochannel_add(CGAME, p);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_shout = atoi(s);
    if(parray[p].i_shout) Ochannel_add(CSHOUT, p);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_gshout = atoi(s);
    if(parray[p].i_gshout) Ochannel_add(CGSHOUT, p);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_lshout = atoi(s);
    if(parray[p].i_lshout) Ochannel_add(CLSHOUT, p);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_tell = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_robot = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_kibitz = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].i_verbose = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].looking = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].Private = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].automail = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].adminLevel = atoi(s);
    if(parray[p].adminLevel) {
      Ochannel_add(CASHOUT, p);
    }
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].bmuzzled = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].muzzled = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].gmuzzled = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].tmuzzled = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].kmuzzled = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].last_problem = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].gonum_white = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].gonum_black = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].gowins = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].golose = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].def_time = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].def_size = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].def_byo_time = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].def_byo_stones = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].last_channel = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].extprompt = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].which_client = atoi(s);
    s = strtok(NULL, ":");
    if(s == NULL) return 0;
    parray[p].num_logons = atoi(s);
  } else if (!strcmp(attr, "name:")) {
    do_copy(parray[p].name, value, MAX_NAME);
  } else if (!strcmp(attr, "password:")) {
    do_copy(parray[p].passwd, value, MAX_PASSWORD);
  } else if (!strcmp(attr, "fullname:")) {
    do_copy(parray[p].fullName, value, MAX_FULLNAME);
  } else if (!strcmp(attr, "email:")) {
    do_copy(parray[p].emailAddress, value, MAX_EMAIL);
  } else if (!strcmp(attr, "regdate:")) {
    do_copy(parray[p].RegDate, value, MAX_REGDATE);
  } else if (!strcmp(attr, "rank:")) {
    do_copy(parray[p].rank, value, MAX_RANK);
  } else if (!strcmp(attr, "ranked:")) {
    do_copy(parray[p].ranked, value, MAX_RANKED);
  } else if (!strcmp(attr, "prompt:")) {
    do_copy(parray[p].prompt, value, MAX_PROMPT);
  } else if (!strcmp(attr, "lasthost:")) {
    parray[p].lastHost = atoi(value);
  } else if (!strcmp(attr, "channel:")) {
    channel_add(atoi(value), p);
  } else if (!strcmp(attr, "num_plan:")) {
    parray[p].num_plan = atoi(value);
    if (parray[p].num_plan < 0) parray[p].num_plan = 0;
    if (parray[p].num_plan > 0) {
      for (i = 0; i < parray[p].num_plan; i++) {
        fgets(tmp, MAX_LINE_SIZE, fp);
        if (!(len = strlen(tmp))) {
          Logit( "Error bad plan in file %s", fname);
        } else {
          tmp[len - 1] = '\0';  /* Get rid of '\n' */
          free(parray[p].planLines[i]);
          parray[p].planLines[i] = (char *) strdup(tmp);
        }
      }
    }
  } else if (!strcmp(attr, "num_alias:")) {
    i = atoi(value);
    while (i-- > 0) {
      fgets(tmp, MAX_LINE_SIZE, fp);
      if (!(len = strlen(tmp))) {
	Logit( "Error bad alias in file %s\n", fname);
      } else {
	tmp[len - 1] = '\0';	/* Get rid of '\n' */
	tmp1 = tmp;
	tmp1 = eatword(tmp1);
	*tmp1 = '\0';
	tmp1++;
	tmp1 = eatwhite(tmp1);
	alias_add(tmp, tmp1, parray[p].alias_list);
      }
    }
  } else if (!strcmp(attr, "num_censor:")) {
    parray[p].num_censor = atoi(value);
    if (parray[p].num_censor < 0) parray[p].num_censor = 0;
    if (parray[p].num_censor > 0) {
      for (i = 0; i < parray[p].num_censor; i++) {
	fgets(tmp, MAX_LINE_SIZE, fp);
	if (!(len = strlen(tmp))) {
	  Logit( "Error bad censor in file %s", fname);
	} else {
	  tmp[len - 1] = '\0';	/* Get rid of '\n' */
          free(parray[p].censorList[i]);
	  parray[p].censorList[i] = (char *) strdup(tmp);
	}
      }
    }
  } else if (!strcmp(attr, "water:")) {
    parray[p].water = atoi(value);
    if(parray[p].water < 0) parray[p].water = 3;
  } else if (!strcmp(attr, "open:")) {
    parray[p].open = atoi(value);
  } else if (!strcmp(attr, "client:")) {
    parray[p].client = atoi(value);
  } else if (!strcmp(attr, "ropen:")) {
    parray[p].ropen = atoi(value);
  } else if (!strcmp(attr, "bell:")) {
    parray[p].bell = atoi(value);
  } else if (!strcmp(attr, "i_login:")) {
    parray[p].i_login = atoi(value);
  } else if (!strcmp(attr, "i_game:")) {
    parray[p].i_game = atoi(value);
  } else if (!strcmp(attr, "i_shout:")) {
    parray[p].i_shout = atoi(value);
  } else if (!strcmp(attr, "i_lshout:")) {
    parray[p].i_lshout = atoi(value);
  } else if (!strcmp(attr, "i_gshout:")) {
    parray[p].i_gshout = atoi(value);
  } else if (!strcmp(attr, "i_tell:")) {
    parray[p].i_tell = atoi(value);
  } else if (!strcmp(attr, "i_robot:")) {
    parray[p].i_robot = atoi(value);
  } else if (!strcmp(attr, "i_verbose:")) {
    parray[p].i_verbose = atoi(value);
  } else if (!strcmp(attr, "i_kibitz:")) {
    parray[p].i_kibitz = atoi(value);
  } else if (!strcmp(attr, "looking:")) {
    parray[p].looking = atoi(value);
  } else if (!strcmp(attr, "private:")) {
    parray[p].Private = atoi(value);
  } else if (!strcmp(attr, "automail:")) {
    parray[p].automail = atoi(value);
  } else if (!strcmp(attr, "admin_level:")) {
    parray[p].adminLevel = atoi(value);
  } else if (!strcmp(attr, "goblack_games:")) {
    parray[p].gonum_black = atoi(value);
  } else if (!strcmp(attr, "gowhite_games:")) {
    parray[p].gonum_white = atoi(value);
  } else if (!strcmp(attr, "gowins:")) {
    parray[p].gowins = atoi(value);
  } else if (!strcmp(attr, "golose:")) {
    parray[p].golose = atoi(value);
  } else if (!strcmp(attr, "def_time:")) {
    parray[p].def_time = atoi(value);
  } else if (!strcmp(attr, "def_size:")) {
    parray[p].def_size = atoi(value);
  } else if (!strcmp(attr, "def_byo_time:")) {
    parray[p].def_byo_time = atoi(value);
  } else if (!strcmp(attr, "last_chan:")) {
    parray[p].last_channel = atoi(value);
  } else if (!strcmp(attr, "def_byo_stones:")) {
    parray[p].def_byo_stones = atoi(value);
  } else if (!strcmp(attr, "bmuzzled:")) {
    parray[p].bmuzzled = atoi(value);
  } else if (!strcmp(attr, "muzzled:")) {
    parray[p].muzzled = atoi(value);
  } else if (!strcmp(attr, "tmuzzled:")) {
    parray[p].tmuzzled = atoi(value);
  } else if (!strcmp(attr, "kmuzzled:")) {
    parray[p].kmuzzled = atoi(value);
  } else if (!strcmp(attr, "last_problem:")) {
    parray[p].last_problem = atoi(value);
  } else if (!strcmp(attr, "gmuzzled:")) {
    parray[p].gmuzzled = atoi(value);
  } else {
    Logit( "Error bad attribute >%s< from file %s", attr, fname);
  }
  return 0;
}

PUBLIC int player_read(int p, char *name)
{
  char fname[MAX_FILENAME_SIZE];
  char line[MAX_LINE_SIZE];
  char *attr, *value;
  FILE *fp;
  int len, rat, gm;
  int shuttime = time(0);
  char rnk[10], nm[11], trnk;
  char low_name[MAX_NAME];

  gm = 0;

  do_copy(low_name, name, MAX_NAME);
  stolower(low_name); 
  
  do_copy(parray[p].login, low_name, MAX_NAME);

  sprintf(fname, "%s/%c/%s", player_dir, parray[p].login[0], parray[p].login);
  fp = fopen(fname, "r");
  if (!fp) {
    do_copy(parray[p].name, name, MAX_NAME);
    parray[p].registered = 0;
    parray[p].rated = 0;
    return -1;
  }
  parray[p].registered = 1;
  while (!feof(fp)) {
    fgets(line, MAX_LINE_SIZE, fp);
    if (feof(fp))
      break;
    if ((len = strlen(line)) <= 1) continue;
    line[len - 1] = '\0';
    attr = eatwhite(line);
    if (attr[0] == '#') continue;			/* Comment */
    value = eatword(attr);
    if (!*value) {
      Logit( "Error reading file %s (%s)", fname, attr);
      continue;
    }
    *value = '\0';
    value++;
    value = eatwhite(value);
    if (!*value) {
      Logit( "Error reading file %s", fname);
      continue;
    }
    stolower(attr);
/*    Logit("attr: %s    value: %s", attr, value); */
    got_attr_value(p, attr, value, fp, fname);
  }
  fclose(fp);
  if (!parray[p].name) {
    do_copy(parray[p].name, name, MAX_NAME);
    pprintf(p, "%s\n%s*** WARNING: Your Data file is corrupt. Please tell an admin ***\n", SendCode(p, ERROR), SendCode(p, ERROR));
  }
  if(!parray[p].RegDate) {
    do_copy(parray[p].RegDate, strltime((time_t *) &shuttime), MAX_REGDATE);
  }
#ifdef NNGSRATED
  if((!strcmp(parray[p].ranked, "NR")) || (!strcmp(parray[p].ranked, "nr"))) {
    parray[p].rating = 0;
    parray[p].orating = 0;
    parray[p].rated = 0;
  } else if(!strcmp(parray[p].ranked, "???")) {
    parray[p].rating = 1;
    parray[p].orating = 1;
  } else {
    fp = fopen(ratings_file, "r");
    if(fp) {
      while((fscanf(fp, "%s %s %d %d", nm, rnk, &rat, &gm)) == 4) {
        if(!strcmp(parray[p].name, nm)) {
          if(rnk[strlen(rnk) - 1] == '*') {
            rnk[strlen(rnk) - 1] = '\0';
            do_copy(parray[p].srank, rnk, MAX_SRANK);
            parray[p].rated = 1;
          }
          else {
            do_copy(parray[p].srank, rnk, MAX_SRANK);
          }
          parray[p].rating = rat;
          parray[p].orating = rat;
          parray[p].numgam = gm;
          break;
        }
      }
      fclose(fp);
    }
    else {
      Logit("Cannot open ratings file");
    }
  }
  if(parray[p].rating == 0) {
    do_copy(parray[p].srank, parray[p].ranked, MAX_SRANK);
    if(strcmp(parray[p].srank, "NR")) {
      sscanf(parray[p].srank, "%u%c", &rat, &trnk);
      switch(trnk) {
        case 'k': case 'K':
        rat = 31 - rat;
        break;
        case 'd': case 'D':
        rat += 30;
        break;
        case 'p': case 'P':
        rat += 40;
        break;
      }
      parray[p].orating = rat * 100;
    }
    else parray[p].orating = 0;
  } 
#else
  parray[p].rating == 0;
  parray[p].srank = (char *) strdup(parray[p].ranked);
  if(strcmp(parray[p].srank, "NR") {
    sscanf(parray[p].srank, "%u%c", &rat, &trnk);
    switch(trnk) {
      case 'k': case 'K':
      rat = 31 - rat;
      break;
      case 'd': case 'D':
      rat += 30;
      break;
      case 'p': case 'P':
      rat += 40;
      break;
    }
    parray[p].orating = rat * 100;
  }
  else parray[p].orating = 0;
#endif /* NNGSRATED */
  return 0;
}

PUBLIC int player_delete(int p)
{
  char fname[MAX_FILENAME_SIZE];

  if (!parray[p].registered) {	/* Player must not be registered */
    return -1;
  }
  sprintf(fname, "%s/%c/%s", player_dir, parray[p].login[0], parray[p].login);
  unlink(fname);
  return 0;
}

PUBLIC int player_markdeleted(int p)
{
  FILE *fp;
  char fname[MAX_FILENAME_SIZE], fname2[MAX_FILENAME_SIZE];

  if(parray[p].adminLevel)
    if(!in_list("admin", parray[p].name))
      return -1;  /* Not an admin, corruption, refuse to save. */

  if (!parray[p].registered) {	/* Player must not be registered */
    return -1;
  }

  sprintf(fname, "%s/%c/%s", player_dir, parray[p].login[0], parray[p].login);
  sprintf(fname2, "%s/%c/%s.delete", player_dir, parray[p].login[0], parray[p].login);
  rename(fname, fname2);
  fp = fopen(fname2, "a");	/* Touch the file */
  if (fp) {
    fprintf(fp, "\n");
    fclose(fp);
  }
  return 0;
}

PUBLIC int player_save(int p)
{
  FILE *fp;
  int i;
  char fname[MAX_FILENAME_SIZE];

  if (!parray[p].registered) {	/* Player must not be registered */
    return -1;
  }
  sprintf(fname, "%s/%c/%s", player_dir, parray[p].login[0], parray[p].login);
  fp = fopen(fname, "w");
  if (!fp) {
    Logit( "Problem opening file %s for write\n", fname);
    return -1;
  }
  if(parray[p].name) fprintf(fp, "Name: %s\n", parray[p].name);
  if(parray[p].fullName) fprintf(fp, "Fullname: %s\n", parray[p].fullName);
  if(parray[p].passwd) fprintf(fp, "Password: %s\n", parray[p].passwd);
  if(parray[p].emailAddress) fprintf(fp, "Email: %s\n", parray[p].emailAddress);
  if(parray[p].rank) fprintf(fp, "rank: %s\n", parray[p].rank);
  if(parray[p].ranked) fprintf(fp, "ranked: %s\n", parray[p].ranked);
  if(parray[p].prompt != NULL) fprintf(fp, "Prompt: %s\n", parray[p].prompt);
  fprintf(fp, "RegDate: %s\n",  parray[p].RegDate);

  fprintf(fp, "LastHost: %d\n", parray[p].lastHost);
  fprintf(fp, "VARS: %d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d\n",
  parray[p].open,
  parray[p].water,
  parray[p].client,
  parray[p].ropen,
  parray[p].bell,
  parray[p].i_login,
  parray[p].i_game,
  parray[p].i_shout,
  parray[p].i_gshout,
  parray[p].i_lshout,
  parray[p].i_tell,
  parray[p].i_robot,
  parray[p].i_kibitz,
  parray[p].i_verbose,
  parray[p].looking,
  parray[p].Private,
  parray[p].automail,
  parray[p].adminLevel,
  parray[p].bmuzzled,
  parray[p].muzzled,
  parray[p].gmuzzled,
  parray[p].tmuzzled,
  parray[p].kmuzzled,
  parray[p].last_problem,
  parray[p].gonum_white,
  parray[p].gonum_black,
  parray[p].gowins,
  parray[p].golose,
  parray[p].def_time,
  parray[p].def_size,
  parray[p].def_byo_time,
  parray[p].def_byo_stones,
  parray[p].last_channel,
  parray[p].extprompt,
  parray[p].which_client,
  parray[p].num_logons);

  for (i = 0; i < MAX_CHANNELS; i++) {
    if (on_channel(i, p))
      fprintf(fp, "Channel: %d\n", i);
  }

  fprintf(fp, "Num_plan: %d\n", parray[p].num_plan);
  for (i = 0; i < parray[p].num_plan; i++)
    fprintf(fp, "%s\n", (parray[p].planLines[i] ? parray[p].planLines[i] : ""));

  if(parray[p].num_censor < 0) parray[p].num_censor = 0;
  fprintf(fp, "Num_censor: %d\n", parray[p].num_censor);
  for (i = 0; i < parray[p].num_censor; i++)
    fprintf(fp, "%s\n", parray[p].censorList[i]);

  {
    char *c, *a;

    fprintf(fp, "Num_alias: %d\n", alias_count(parray[p].alias_list));
    alias_start(parray[p].alias_list);
    while (alias_next(&c, &a, parray[p].alias_list))
      fprintf(fp, "%s %s\n", c, a);
  }
  fclose(fp);
  return 0;
}

PUBLIC int player_find(int fd)
{
  int i;

  for (i = 0; i < p_num; i++) {
    if (parray[i].status == PLAYER_EMPTY)
      continue;
    if (parray[i].socket == fd)
      return i;
  }
  return -1;
}

PUBLIC int player_find_bylogin(char *name)
{
  int i;

  for (i = 0; i < p_num; i++) {
    if ((parray[i].status == PLAYER_EMPTY) || 
       (parray[i].status == PLAYER_LOGIN) ||
       (parray[i].status == PLAYER_PASSWORD))
      continue;
    if (!parray[i].login)
      continue;
    if (!strcmp(parray[i].login, name))
      return i;
  }
  return -1;
}

PUBLIC int player_find_part_login(char *name)
{
  int i;
  int found = -1;

  i = player_find_bylogin(name);
  if (i >= 0)
    return i;
  for (i = 0; i < p_num; i++) {
    if ((parray[i].status == PLAYER_EMPTY) || 
       (parray[i].status == PLAYER_LOGIN) ||
       (parray[i].status == PLAYER_PASSWORD))
      continue;
    if (!parray[i].login)
      continue;
    if (!strncmp(parray[i].login, name, strlen(name))) {
      if (found >= 0) {		/* Ambiguous */
	return -2;
      }
      found = i;
    }
  }
  return found;
}

PUBLIC int check_censored(int p, char *name) 
{
  int i;

  for (i = 0; i < parray[p].num_censor; i++) {
    if(parray[p].censorList[i]) {
      if ((strcasecmp(parray[p].censorList[i], name)) == 0)
        return 1;
    }
  }
  return 0;
}

PUBLIC int player_censored(int p, int p1)
{
  int i;

  for (i = 0; i < parray[p].num_censor; i++) {
    if(parray[p].censorList[i]) {
      if ((strcasecmp(parray[p].censorList[i], parray[p1].login)) == 0)
        return 1;
    }
  }
  return 0;
}

PUBLIC int player_count()
{
  int count = 0;
  int i;

  for (i = 0; i < p_num; i++) {
    if (parray[i].status != PLAYER_PROMPT)
      continue;
    count++;
    if (count > player_high)
      player_high++;
  }
  return count;
}

PUBLIC int player_idle(int p)
{
  if (parray[p].status != PLAYER_PROMPT)
    return time(0) - parray[p].logon_time;
  else
    return time(0) - parray[p].last_command_time;
}

PUBLIC int player_ontime(int p)
{
  return time(0) - parray[p].logon_time;
}

PRIVATE void write_p_inout(int inout, int p, char *fname, int maxlines)
{
  FILE *fp;

  fp = fopen(fname, "a");
  if (fp == NULL)
    return;
  fprintf(fp, "%d %s %d %d %s\n", inout, parray[p].name, (int) time(0), 
                  parray[p].registered, 
                  dotQuad(parray[p].thisHost));
  fclose(fp);
  if(parray[p].num_logons % 100 == 0) {
    if(Debug) Logit("About to truncate");
    if(maxlines >= 1) truncate_file(fname, maxlines);
    if(Debug) Logit("done with  truncate");
  }
}

PUBLIC void player_write_login(int p)
{
  char fname[MAX_FILENAME_SIZE];

  parray[p].num_logons++;

  if (parray[p].registered) {
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_LOGONS);
    write_p_inout(P_LOGIN, p, fname, 24);
  }
  sprintf(fname, "%s/%s", stats_dir, STATS_LOGONS);
  write_p_inout(P_LOGIN, p, fname, 100);
  Logit("Login : %s %d/%s%s %s", 
                  parray[p].name, 
		  p,
                  parray[p].registered ? "R" : "U", 
                  parray[p].adminLevel ? "***" : "", 
                  dotQuad(parray[p].thisHost));
}

PUBLIC void player_write_logout(int p)
{
  char fname[MAX_FILENAME_SIZE];

  if (parray[p].registered) {
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_LOGONS);
    write_p_inout(P_LOGOUT, p, fname, 24);
  }
  sprintf(fname, "%s/%s", stats_dir, STATS_LOGONS);
  write_p_inout(P_LOGOUT, p, fname, 0);
  Logit("Logout: %s %s %s (%d/%d)", 
                  parray[p].name, 
                  parray[p].registered ? "R" : "U", 
                  dotQuad(parray[p].thisHost), player_count() - 1, game_count());
}

PUBLIC int player_lastconnect(int p)
{
  char fname[MAX_FILENAME_SIZE];
  FILE *fp;
  int inout, thetime, registered;
  int last = 0;
  char loginName[MAX_LOGIN_NAME];
  char ipstr[20];

  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, 
          parray[p].login[0], parray[p].login, STATS_LOGONS);
  fp = fopen(fname, "r");
  if (!fp)
    return 0;
  inout=1; 
  while (!feof(fp)) {
    if (inout == P_LOGIN)
      last = thetime;
    if (fscanf(fp, "%d %s %d %d %s\n", &inout, loginName, &thetime, 
                       &registered, ipstr) != 5) {
      Logit( "Error in login info format. %s", fname);
      fclose(fp);
      return 0;
    }
  }
  fclose(fp);
  return last;
}

PUBLIC int player_lastdisconnect(int p)
{
  char fname[MAX_FILENAME_SIZE];
  FILE *fp;
  int inout, thetime, registered;
  int last = 0;
  char ipstr[20];
  char loginName[MAX_LOGIN_NAME];

  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_LOGONS);
  fp = fopen(fname, "r");
  if (!fp)
    return 0;
  while (!feof(fp)) {
    if (fscanf(fp, "%d %s %d %d %s\n", &inout, loginName, &thetime, &registered, ipstr) != 5) {
      Logit( "Error in login info format. %s", fname);
      fclose(fp);
      return 0;
    }
    if (inout == P_LOGOUT)
      last = thetime;
  }
  fclose(fp);
  return last;
}

PUBLIC void player_pend_print(int p, pending * pend)
{
  char outstr[512];
  char tmp[512];

  if (p == pend->whofrom) {
    sprintf(outstr, "You are offering ");
  } else {
    sprintf(outstr, "%s is offering ", parray[pend->whofrom].name);
  }
  if (p == pend->whoto) {
    strcpy(tmp, "");
  } else {
    sprintf(tmp, "%s ", parray[pend->whoto].name);
  }
  strcat(outstr, tmp);
  switch (pend->type) {
  case PEND_GOEMATCH:
    sprintf(tmp, "%d %d.", pend->param3, pend->param2);
    break;
  case PEND_TMATCH:
  case PEND_GMATCH:
  case PEND_MATCH:
    sprintf(tmp, "%s.", ggame_str(pend->param3, pend->param2, pend->param4));
    break;
  case PEND_PAUSE:
    sprintf(tmp, "to pause the clock.");
    break;
  case PEND_TEACH:
    sprintf(tmp, "to make this a teaching game.");
    break;
  case PEND_ADJOURN:
    sprintf(tmp, "an adjournment.");
    break;
  case PEND_PAIR:
    sprintf(tmp, "to pair.");
    break;
  case PEND_DONE:
    sprintf(tmp, "that the board is acceptable for scoring.");
    break;
  }
  strcat(outstr, tmp);
  pprintf(p, "%s\n", outstr);
}

PUBLIC int player_find_pendto(int p, int p1, int type)
{
  int i;

  for (i = 0; i < parray[p].num_to; i++) {
    if (((parray[p].p_to_list[i].whoto == p1) || (p1 == -1)) &&
	((type < 0) || (parray[p].p_to_list[i].type == type))) {
      return i;
    }
  }
  return -1;
}

PUBLIC int player_new_pendto(int p)
{
  if (parray[p].num_to >= MAX_PENDING)
    return -1;
  parray[p].num_to++;
  return parray[p].num_to - 1;
}

PUBLIC int player_remove_pendto(int p, int p1, int type)
{
  int w;
  if ((w = player_find_pendto(p, p1, type)) < 0)
    return -1;
  for (; w < parray[p].num_to - 1; w++)
    parray[p].p_to_list[w] = parray[p].p_to_list[w + 1];
  parray[p].num_to = parray[p].num_to - 1;
  return 0;
}

PUBLIC int player_find_pendfrom(int p, int p1, int type)
{
  int i;

  for (i = 0; i < parray[p].num_from; i++) {
    if (((parray[p].p_from_list[i].whofrom == p1) || (p1 == -1)) &&
	((type < 0) || (parray[p].p_from_list[i].type == type)))
      return i;
  }
  return -1;
}

PUBLIC int player_new_pendfrom(int p)
{
  if (parray[p].num_from >= MAX_PENDING)
    return -1;
  parray[p].num_from++;
  return parray[p].num_from - 1;
}

PUBLIC int player_remove_pendfrom(int p, int p1, int type)
{
  int w;
  if ((w = player_find_pendfrom(p, p1, type)) < 0)
    return -1;
  for (; w < parray[p].num_from - 1; w++)
    parray[p].p_from_list[w] = parray[p].p_from_list[w + 1];
  parray[p].num_from = parray[p].num_from - 1;
  return 0;
}

PUBLIC int player_add_request(int p, int p1, int type, int param)
{
  int pendt;
  int pendf;

  if (player_find_pendto(p, p1, type) >= 0)
    return -1;			/* Already exists */
  pendt = player_new_pendto(p);
  if (pendt == -1) {
    return -1;
  }
  pendf = player_new_pendfrom(p1);
  if (pendf == -1) {
    parray[p].num_to--;		/* Remove the pendto we allocated */
    return -1;
  }
  parray[p].p_to_list[pendt].type = type;
  parray[p].p_to_list[pendt].whoto = p1;
  parray[p].p_to_list[pendt].whofrom = p;
  parray[p].p_to_list[pendt].param1 = param;
  parray[p1].p_from_list[pendf].type = type;
  parray[p1].p_from_list[pendf].whoto = p1;
  parray[p1].p_from_list[pendf].whofrom = p;
  parray[p1].p_from_list[pendf].param1 = param;
  return 0;
}

PUBLIC int player_remove_request(int p, int p1, int type)
{
  int to = 0, from = 0;

  while (to != -1) {
    to = player_find_pendto(p, p1, type);
    if (to != -1) {
      for (; to < parray[p].num_to - 1; to++)
	parray[p].p_to_list[to] = parray[p].p_to_list[to + 1];
      parray[p].num_to = parray[p].num_to - 1;
    }
  }
  while (from != -1) {
    from = player_find_pendfrom(p1, p, type);
    if (from != -1) {
      for (; from < parray[p1].num_from - 1; from++)
	parray[p1].p_from_list[from] = parray[p1].p_from_list[from + 1];
      parray[p1].num_from = parray[p1].num_from - 1;
    }
  }
  return 0;
}


PUBLIC int player_decline_offers(int p, int p1, int offerType)
{
  int offer;
  int type, p2;
  int count = 0;

  while ((offer = player_find_pendfrom(p, p1, offerType)) >= 0) {
    type = parray[p].p_from_list[offer].type;
    p2 = parray[p].p_from_list[offer].whofrom;
    switch (type) {
    case PEND_GMATCH:
    case PEND_TMATCH:
    case PEND_GOEMATCH:
    case PEND_MATCH:
      pprintf_prompt(p2, "%s%s declines your request for a match.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou decline the match offer from %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_TEACH:
      pprintf_prompt(p2, "%s%s declines your request for a teaching match.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou decline the teaching match offer from %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_PAUSE:
      pprintf_prompt(p2, "%s%s declines pause request.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou decline the pause request from %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_ADJOURN:
      pprintf_prompt(p2, "%s%s declines the adjourn request.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou decline the adjourn request from %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_PAIR:
      pprintf_prompt(p2, "%s%s declines your pair request.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou decline the pair request from %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_DONE:
      pprintf_prompt(p2, "%s%s was not finished removing stones.  Please type \"done\" again.\n", SendCode(p2, INFO), parray[p].name);
      break;
    }
    player_remove_request(p2, p, type);
    count++;
  }
  return count;
}


PUBLIC int player_withdraw_offers(int p, int p1, int offerType)
{
  int offer;
  int type, p2;
  int count = 0;

  while ((offer = player_find_pendto(p, p1, offerType)) >= 0) {
    type = parray[p].p_to_list[offer].type;
    p2 = parray[p].p_to_list[offer].whoto;
    switch (type) {
    case PEND_GMATCH:
    case PEND_TMATCH:
    case PEND_GOEMATCH:
    case PEND_MATCH:
      pprintf_prompt(p2, "%s%s withdraws the match offer.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou withdraw the match offer to %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_TEACH:
      pprintf_prompt(p2, "%s%s withdraws the teaching match offer.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou withdraw the teaching match offer to %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_PAUSE:
      pprintf_prompt(p2, "%s%s withdraws pause request.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou withdraw the pause request to %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_ADJOURN:
      pprintf_prompt(p2, "%s%s withdraws the adjourn request.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou withdraw the adjourn request to %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_PAIR:
      pprintf_prompt(p2, "%s%s withdraws the pair request.\n", 
                     SendCode(p2, INFO), parray[p].name);
      pprintf(p, "%sYou withdraw the pair request to %s.\n", 
                     SendCode(p, INFO), parray[p2].name);
      break;
    case PEND_DONE:
      pprintf_prompt(p, "%sYou will need to type \"done\" again.\n", 
                     SendCode(p, INFO));
      break;
    }
    player_remove_request(p, p2, type);
    count++;
  }
  return count;
}

PUBLIC int player_is_observe(int p, int g)
{
  int i;

  for (i = 0; i < parray[p].num_observe; i++) {
    if (parray[p].observe_list[i] == g)
      break;
  }
  if (i == parray[p].num_observe)
    return 0;
  else
    return 1;
}

PUBLIC int player_add_observe(int p, int g)
{
  if (parray[p].num_observe == MAX_OBSERVE)
    return -1;
  parray[p].observe_list[parray[p].num_observe] = g;
  parray[p].num_observe++;
  parray[p].state = OBSERVING;
  return 0;
}

PUBLIC int player_remove_observe(int p, int g)
{
  int i;

  for (i = 0; i < parray[p].num_observe; i++) {
    if (parray[p].observe_list[i] == g)
      break;
  }
  if (i == parray[p].num_observe)
    return -1;			/* Not found! */
  for (; i < parray[p].num_observe - 1; i++) {
    parray[p].observe_list[i] = parray[p].observe_list[i + 1];
  }
  parray[p].num_observe--;
  if(parray[p].num_observe == 0) {
    parray[p].state = WAITING;
    parray[p].observe_list[0] = -1;
  }
  parray[p].last_command_time = time(0);
  return 0;
}

PUBLIC int player_game_ended(int g)
{
  int p;

  for (p = 0; p < p_num; p++) {
    if (parray[p].status == PLAYER_EMPTY)
      continue;
    player_remove_observe(p, g);
  }
  player_remove_request(garray[g].white, garray[g].black, -1);
  player_remove_request(garray[g].black, garray[g].white, -1);
  return 0;
}

PUBLIC int player_num_messages(int p)
{
  char fname[MAX_FILENAME_SIZE];

  if (!parray[p].registered)
    return 0;
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_MESSAGES);
  return lines_file(fname);
}

PUBLIC int player_add_message(int top, int fromp, char *message)
{
  char fname[MAX_FILENAME_SIZE];
  FILE *fp;
  int t = time(0);

  if (!parray[top].registered) return -1;
  if (!parray[fromp].registered) return -1;
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, 
                  parray[top].login[0], parray[top].login, STATS_MESSAGES);
  if ((lines_file(fname) >= MAX_MESSAGES) && (parray[top].adminLevel == 0))
    return -1;
  fp = fopen(fname, "a");
  if (!fp) return -1;
  fprintf(fp, "%s at %s GMT: %s\n", parray[fromp].name, strgtime((time_t *) &t), message);
  fclose(fp);
  return 0;
}

PUBLIC int player_show_messages(int p)
{
  char fname[MAX_FILENAME_SIZE];

  if (!parray[p].registered) return -1;
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_MESSAGES);
  if (lines_file(fname) <= 0) return -1;
  psend_file(p, NULL, fname);
  pprintf(p, "%sPlease type \"erase\" to erase your messages after reading\n", SendCode(p, INFO));
  return 0;
}

PUBLIC int player_clear_messages(int p)
{
  char fname[MAX_FILENAME_SIZE];

  if (!parray[p].registered)
    return -1;
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_MESSAGES);
  unlink(fname);
  return 0;
}

PUBLIC int player_search(int p, char *name)
/*
 * Find player matching the given string. First looks for exact match
 *  with a logged in player, then an exact match with a registered player,
 *  then a partial unique match with a logged in player, then a partial
 *  match with a registered player.
 *  Returns player number if the player is connected, negative (player number)
 *  if the player had to be connected, and 0 if no player was found
 */
{
  static char buffer[16384];
  int i, p1, count;
  char pdir[MAX_FILENAME_SIZE];

  /* exact match with connected player? */
  if ((p1 = player_find_bylogin(name)) >= 0) {
    return p1 + 1;
  }
  /* exact match with registered player? */
  sprintf(pdir, "%s/%c", player_dir, name[0]);
  count = search_directory(pdir, name, buffer, 8000);
  if (count > 0 && !strcmp(name, buffer)) {
    goto ReadPlayerFromFile;	/* found an unconnected registered player */
  }
  /* partial match with connected player? */
  if ((p1 = player_find_part_login(name)) >= 0) {
    return p1 + 1;
  }
  /* partial match with registered player? */
  if (count < 1) {
    pprintf(p, "%sThere is no player matching that name.\n", SendCode(p, ERROR));
    return 0;
  }
  if (count > 1) {
    char *s = buffer;
    multicol *m = multicol_start(2000); /* max. of 2000 names */
    pprintf(p, "-- Matches: %d names --", count);
    for (i = 0; i < count; i++) {
      multicol_store( m, s );
      s += strlen(s) + 1;
    }
    multicol_pprint( m, p, 78, 1 );
    multicol_end(m);
    return 0;
  }
ReadPlayerFromFile:
  p1 = player_new();
  if (player_read(p1, buffer)) {
    player_remove(p1);
    pprintf(p, "%sERROR: a player named %s was expected but not found!\n", SendCode(p, ERROR), buffer);
    pprintf(p, "%sPlease tell an admin about this incident. Thank you.\n", SendCode(p, ERROR));
    return 0;
  }
  return (-p1) - 1;		/* negative to indicate player was not
				   connected */
}

PUBLIC int player_kill(char *name)
{
  char fname[MAX_FILENAME_SIZE], fname2[MAX_FILENAME_SIZE];

  sprintf(fname, "%s/%c/%s", player_dir, name[0], name);
  sprintf(fname2, "%s/%c/.rem.%s", player_dir, name[0], name);
  rename(fname, fname2);
  sprintf(fname, "%s/player_data/%c/%s.games", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/.rem.%s.games", stats_dir, name[0], name);
  rename(fname, fname2);
  sprintf(fname, "%s/player_data/%c/%s.logons", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/.rem.%s.logons", stats_dir, name[0], name);
  rename(fname, fname2);
  sprintf(fname, "%s/player_data/%c/%s.messages", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/.rem.%s.messages", stats_dir, name[0], name);
  rename(fname, fname2);
  return 0;
}

PUBLIC int player_rename(char *name, char *newname)
{
  char fname[MAX_FILENAME_SIZE], fname2[MAX_FILENAME_SIZE];

  sprintf(fname, "%s/%c/%s", player_dir, name[0], name);
  sprintf(fname2, "%s/%c/%s", player_dir, newname[0], newname);
  rename(fname, fname2);
  sprintf(fname, "%s/player_data/%c/%s.games", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/%s.games", stats_dir, name[0], newname);
  rename(fname, fname2);
  sprintf(fname, "%s/player_data/%c/%s.logons", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/%s.logons", stats_dir, name[0], newname);
  rename(fname, fname2);
  sprintf(fname, "%s/player_data/%c/%s.messages", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/%s.messages", stats_dir, name[0], newname);
  rename(fname, fname2);
  return 0;
}

PUBLIC int player_raise(char *name)
{
  char fname[MAX_FILENAME_SIZE], fname2[MAX_FILENAME_SIZE];

  sprintf(fname, "%s/%c/%s", player_dir, name[0], name);
  sprintf(fname2, "%s/%c/.rem.%s", player_dir, name[0], name);
  rename(fname2, fname);
  sprintf(fname, "%s/player_data/%c/%s.games", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/.rem.%s.games", stats_dir, name[0], name);
  rename(fname2, fname);
  sprintf(fname, "%s/player_data/%c/%s.logons", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/.rem.%s.logons", stats_dir, name[0], name);
  rename(fname2, fname);
  sprintf(fname, "%s/player_data/%c/%s.messages", stats_dir, name[0], name);
  sprintf(fname2, "%s/player_data/%c/.rem.%s.messages", stats_dir, name[0], name);
  rename(fname2, fname);
  return 0;
}

