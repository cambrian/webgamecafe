#!/bin/sh

cd /home/nngs/src/orat

for f in *.[ch]; do
  rm -f ../$f
  echo "ln -s orat/$f ../"
  ln -s orat/$f ../
done
