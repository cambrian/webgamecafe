/* comproc.c
 *
 */

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1997 Erik Van Riper (geek@nngs.cosmic.org)
    and John Tromp (tromp@daisy.uwaterloo.ca/tromp@cwi.nl)

    Adapted from:
    fics - An internet chess server.
    Copyright (C) 1993  Richard V. Nash

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "stdinclude.h"

#include "command.h"
#include "common.h"
#include "comproc.h"
#include "gameproc.h"
#include "bm.h"
#include "utils.h"
#include "nngsmain.h"
#include "nngsconfig.h"
#include "playerdb.h"
#include "network.h"
#include "channel.h"
#include "variable.h"
#include "gamedb.h"
#include "multicol.h"
#include "glue.h"
#include "mink.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#ifdef SGI
#include <crypt.h>
#endif
#include "ladder.h"
#include "emote.h"
#include "alias.h"
#ifdef NNGSRATED
#include "rdbm.h"		/* PEM */
#endif

PUBLIC int com_register(int p, param_list param)
{
  char text[2048];
  char *newplayer = param[0].val.word;
  char *newemail = param[1].val.word;
  char *newname = param[2].val.string;
  char password[PASSLEN + 1];
  char newplayerlower[MAX_LOGIN_NAME];
  char tmp[200];
  char salt[3];
  int p1, p2, lookup;
  int i;
  int shuttime = time(0);

  if ((int) strlen(newplayer) > MAX_NAME) {
    pprintf(p, "%sPlayer name is too long", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((int) strlen(newplayer) < 2) {
    pprintf(p, "%sPlayer name is too short",  SendCode(p, ERROR));
    return COM_OK;
  }
  if ((strncasecmp(newplayer, "guest", 5)) == 0) {
    pprintf(p, "%sIt would not be nice to register guest", SendCode(p, ERROR));
    return COM_OK;
  }
  if (!alphastring(newplayer)) {
    pprintf(p, "%sIllegal characters in player name. Only A-Z a-z allowed.",  SendCode(p, ERROR));
    return COM_OK;
  }
  if(param[2].type == TYPE_STRING)
    newname = (char *) strdup(param[2].val.string);
  else
    newname = (char *) strdup("Not Provided"); 
  strcpy(newplayerlower, newplayer);
  stolower(newplayerlower);
  p1 = player_new();
  lookup = player_read(p1, newplayerlower);
  player_remove(p1);
  if (!lookup) {
    pprintf(p, "%sA player by the name %s is already registered.", 
		 SendCode(p, ERROR), newplayerlower);
    free(newname);
    return COM_OK;
  }
  if ((strcmp(newemail, "none") == 0) ||
      (strstr(newemail, ",") != (char *) NULL) ||
      (strstr(newemail, " ") != (char *) NULL) ||
      (strstr(newemail, "'") != (char *) NULL) ||
      (strstr(newemail, ";") != (char *) NULL) ||
      (strstr(newemail, "`") != (char *) NULL) ||
      (strlen(newemail) < 4) ||
      (!printablestring(newemail)) ||
      (strchr(newemail, '@') == (char *) NULL)) {
    pprintf(p, "%sInvalid email address\n", SendCode(p, ERROR));
    return COM_OK;
  }
  p1 = player_new();
  do_copy(parray[p1].name, newplayer, MAX_NAME);
  do_copy(parray[p1].login, newplayerlower, MAX_NAME);
  do_copy(parray[p1].rank, "NR", MAX_RANK);
  do_copy(parray[p1].ranked, "NR", MAX_RANKED);
  do_copy(parray[p1].srank, "NR", MAX_SRANK);
  do_copy(parray[p1].fullName, newname, MAX_FULLNAME);
  do_copy(parray[p1].emailAddress, newemail, MAX_EMAIL);
  do_copy(parray[p1].RegDate, strltime((time_t *) &shuttime), MAX_REGDATE);
  for (i = 0; i < PASSLEN; i++) {
    password[i] = 'a' + rand() % 26;
  }
  password[i] = '\0';
  salt[0] = 'a' + rand() % 26;
  salt[1] = 'a' + rand() % 26;
  salt[2] = '\0';
  do_copy(parray[p1].passwd, crypt(password, salt), MAX_PASSWORD);
  parray[p1].registered = 1;
  player_save(p1);
  player_remove(p1);

  sprintf(text, "\nWelcome to %s!  Here is your account info:\n\n\
  Login Name: %s\n\
  Full Name: %s\n\
  Email Address: %s\n\
  Initial Password: %s\n\n\
  Host registered from: %s as %s \n\n\
If any of this information is incorrect, please contact the administrator\n\
to get it corrected at %s.\n\n\
Please write down your password, as it will be your initial password\n\
To access the server, telnet to %s\n\
Please change your password after logging in.  See \"help password\"\n\
For additional help, type \"help welcome\" while on the server.\n\n\
On WWW, please try http://nngs.cosmic.org/\n\n\
Regards,\n\n\
NNGS admins\n\n--", 
          server_name,
          newplayer, newname, 
          newemail, password, 
          dotQuad(parray[p].thisHost),
          parray[p].name,
          server_email, server_address);
  sprintf(tmp, "%s Account Created (%s)", server_name, newplayer);
  mail_string_to_address(newemail, tmp, text);
  sprintf(text, "\n\
  Login Name: %s\n\
  Full Name: %s\n\
  Email Address: %s\n\
  Initial Password: %s\n\
  Host registered from: %s \n\n--", 
          newplayer, newname, newemail, password, 
          dotQuad(parray[p].thisHost));
  /* Mail a copy to geek for testing / verification.  */
  mail_string_to_address("geek", tmp, text); 
  Logit("NewPlayer: %s [%s] %s (%s) by user %s",
                   newplayer, newemail, newname, password, parray[p].name);
  pprintf_prompt(p, "%sYou are now registered! Confirmation together with password is sent to your\n%semail address: %s\n",
                       SendCode(p, INFO), SendCode(p, INFO), newemail);
  for (p2 = 0; p2 < p_num; p2++) { /* Announce to all online admins */
    if (!parray[p2].adminLevel) continue;
    if (parray[p2].status != PLAYER_PROMPT) continue;
    pprintf_prompt(p2, "%s*** New Player: %s [%s] by guest %s\n", 
                        SendCode(p2, SHOUT),
			newplayer, newemail, parray[p].name);
  }
  new_players++;
  return COM_OK;
}

PUBLIC int com_review(int p, param_list param)
{
  pprintf(p, "%sThe currently loaded games:\n", SendCode(p, INFO));
  pprintf(p, "%sYou cannot review multiple SGF files.\n", SendCode(p, INFO));
  pprintf(p, "%sYou need review a game. 'review <loaded game>'.  File is a loaded file.", SendCode(p, INFO));

  return COM_OK;
}

PUBLIC int com_more(int p, param_list param)
{
  pmore_file( p );
  return COM_OK;
}

PUBLIC int com_ayt(int p, param_list param)
{
  /*pprintf(p, "%syes",  SendCode(p, INFO));*/
  pprintf2(p, INFO, "yes");
  return COM_OK;
}

PUBLIC int com_news(int p, param_list param)
{
  FILE *fp;
  char filename[MAX_FILENAME_SIZE];
  char junk[MAX_LINE_SIZE];
  char *junkp;
  int crtime;
  char count[10];
  int flag, len;

  if (((param[0].type==0) || (!strcmp(param[0].val.word,"all")))) {

/* no params - then just display index over news */
   
    pprintf(p, "%s    **** BULLETIN BOARD ****\n",  SendCode(p, INFO));
    sprintf(filename, "%s/news.index", news_dir);
    fp = fopen(filename, "r");
    if (!fp) {
      Logit("Cant find news index.");
      return COM_OK; 
    }
    flag=0;
    while (!feof(fp)) {
      junkp=junk; 
      fgets(junk, MAX_LINE_SIZE, fp);
      if (feof(fp))
        break;
      if ((len = strlen(junk))>1) {
        junk[len-1]='\0';
        sscanf(junkp, "%d %s", &crtime, count);
        junkp=nextword(junkp); 
        junkp=nextword(junkp);
        if (((param[0].type==TYPE_WORD) && (!strcmp(param[0].val.word,"all")))) {
          pprintf(p, "%s%3s (%s) %s\n",  SendCode(p, INFO), count, strltime((time_t *) &crtime), junkp);
          flag=1;
        } else {
          if ((crtime - player_lastconnect(p))>0) {
	    pprintf(p, "%s%3s (%s) %s\n",  SendCode(p, INFO), count, strltime((time_t *) &crtime), junkp);
	    flag=1;
	  }
	}
      } 
    }
    fclose(fp);
    crtime=player_lastconnect(p);
    if (!flag) {
      pprintf(p, "%sThere is no news since your last login (%s).\n",  SendCode(p, INFO), strltime((time_t *) &crtime));
    } else {
      pprintf(p, "%s\n",  SendCode(p, INFO));
    }
  } else {

/* check if the specific news file exist in index */

    sprintf(filename, "%s/news.index", news_dir);
    fp = fopen(filename, "r");
    if (!fp) {
      Logit("Cant find news index.");
      return COM_OK; 
    }
    flag=0;
    while ((!feof(fp)) && (!flag)) {
      junkp=junk; 
      fgets(junk, MAX_LINE_SIZE, fp);
      if (feof(fp))
        break;
      if ((len = strlen(junk))>1) {
        junk[len-1]='\0';
        sscanf(junkp, "%d %s", &crtime, count);
        if (!strcmp(count,param[0].val.word)) {
          flag=1;
          junkp=nextword(junkp); 
          junkp=nextword(junkp);
          pprintf(p, "%sNEWS %3s (%s)\n\n         %s\n",  SendCode(p, INFO), count, strltime((time_t *) &crtime), junkp);
        }
      }
    }
    fclose(fp);
    if (!flag) {
      pprintf(p, "%sBad index number!\n",  SendCode(p, ERROR));
      return COM_OK;
    }

/* file exists - show it */

    sprintf(filename, "%s/news.%s", news_dir, param[0].val.word);
    fp = fopen(filename, "r");
    if (!fp) {
      pprintf(p, "%sNo more info.\n",  SendCode(p, ERROR));
      return COM_OK; 
    }
    fclose(fp);   
    sprintf(filename, "news.%s", param[0].val.word);
    if (psend_file(p, news_dir, filename) < 0) {
      pprintf(p, "%sInternal error - couldn't send news file!\n",  SendCode(p, ERROR));
    }
  }
  return COM_OK;
}

/* Next function is for public bulletin */
/* add by Syncanph */

PUBLIC int com_note(int p, param_list param)
{
  FILE *fp;
  int t = time(0);
  char filename[MAX_FILENAME_SIZE];
 
  sprintf(filename, note_file);
  fp = fopen(filename,"a");
  if(fp == NULL) {
    pprintf(p, "%sError.  Note not saved\n", SendCode(p, ERROR));
    return COM_OK;
  }
  fputs(parray[p].name, fp);
  fputs(" on ",fp);
  fputs(strgtime((time_t *) & t),fp);
  fputs(": \n '",fp);
  fputs(param[0].val.string, fp);
  fputs("'\n\n",fp);
  fclose(fp);

#ifdef LANGUAGE
  switch (parray[p].language) {
    case CHINESE: pprintf(p, "%s您的話已紀錄在留言板上.\n",SendCode(p, INFO));
                  break;
    default: pprintf(p, "%sYour note saved.\n",SendCode(p, INFO));
  }
#else
  pprintf(p, "%sYour note saved.\n", SendCode(p, INFO));
#endif
  return COM_OK;
}

PUBLIC int com_shownote(int p, param_list param)
{
  psend_file (p, NULL, note_file);
  return COM_OK;
}

PUBLIC int in_list(char *listname, char *member)
{
  FILE *fp;
  char listmember[20];
  char filename[MAX_FILENAME_SIZE];

  sprintf(filename, "%s/%s", lists_dir, listname);
  fp = fopen(filename, "r");
  if (!fp) return 0;
  while (!feof(fp)) {
    if (fscanf(fp, "%s\n", listmember) != 1) {
      fclose(fp);
      return 0;
    }
    if (!strcasecmp(member, listmember)) {
      fclose(fp);
      return 1; 
    }
  }
  fclose(fp);
  return 0;
}  

PUBLIC int com_addlist(int p, param_list param)
{
  FILE *fp;
  char filename[MAX_FILENAME_SIZE];
  char *listname = param[0].val.word;
  char *member;
  char lmember[20];
  int rights, flag;
  int p1, connected;

/* Check for a match in the index file */  

  sprintf(filename, "%s/index", lists_dir);
  fp = fopen(filename, "r");
  if (!fp) {
    Logit("Internal error - cant open list index!");
    return COM_OK;
  }
  flag=0;
  while ((!feof(fp)) && (!flag)) {
    if (fscanf(fp, "%s %d\n", lmember, &rights) != 2) {
      Logit("Internal error - bad format in index!");
      fclose(fp);
      return COM_OK;
    }
    if (!strncmp(lmember, listname, strlen(listname))) {
      if ((rights < 2) && (parray[p].adminLevel < 10)) {
	pprintf(p, "%sYou are not allowed to edit this list!\n", SendCode(p, ERROR));
	fclose(fp);
	return COM_OK;
      }
      flag = 1; 
      listname = lmember;
    }
  }
  fclose(fp);
  if (!flag) {
    pprintf(p, "%sNo such list!\n", SendCode(p, ERROR));
    return COM_OK;
  }

/* get exact member name */

  member = param[1].val.word;
  p1 = player_search(p, member);
  if (!p1) return COM_OK;
  if (p1 < 0) {		/* player had to be connected and will be removed 
later */
    connected = 0;
    p1 = (-p1) - 1;
  } else {
    connected = 1;
    p1 = p1 - 1;
  }
  member=parray[p1].name;
  
/* listname is now the exact list name. --- Now check if member is already in 
the list */ 

  if (in_list(listname, member)) {
    pprintf(p, "%s[%s] is already in the %s list.\n", SendCode(p, ERROR), member,listname);
    if (!connected) player_remove(p1);
    return COM_OK;
  }

/* expand list with member */

  pprintf(p, "%s[%s] added to the %s list.\n", SendCode(p, INFO), member,listname);
  sprintf(filename, "%s/%s", lists_dir, listname);
  fp = fopen(filename, "a");
  fprintf(fp, "%s\n", member);
  fclose(fp);
  if (!connected) player_remove(p1);
  return COM_OK;
}

PUBLIC int com_sublist(int p, param_list param)
{
  FILE *fp;
  FILE *fp1;
  FILE *fp2;
  char filename[MAX_FILENAME_SIZE];
  char filename1[MAX_FILENAME_SIZE];
  char filename2[MAX_FILENAME_SIZE];
  char *listname = param[0].val.word;
  char lmember[20];
  char *member;
  int rights, flag;
  int p1, connected;

/* Check for a match in the index file */  

  sprintf(filename, "%s/index", lists_dir);
  fp = fopen(filename, "r");
  if (!fp) {
    Logit("Internal error - cant open index file!");
    return COM_OK;
  }
  flag=0;
  while ((!feof(fp)) && (!flag)){
    if (fscanf(fp, "%s %d\n", lmember, &rights) != 2) {
      Logit("Internal error - bad format in index!");
      fclose(fp);
      return COM_OK;
    }
    if (!strncmp(lmember, listname, strlen(listname))) {
      if ((rights < 2) && (parray[p].adminLevel < 10)) {
	pprintf(p, "%sYou are not allowed to edit this list!\n", SendCode(p, ERROR));
	fclose(fp);
	return COM_OK;
      }
      flag = 1;
      listname=lmember;
    }
  }
  fclose(fp);
  if (!flag) {
    pprintf(p, "%sNo such list!\n", SendCode(p, ERROR));
    return COM_OK;
  }

/* get exact member name */

  member = param[1].val.word;
  p1 = player_search(p, member);
  if (!p1) return COM_OK;
  if (p1 < 0) {		/* player had to be connected and will be removed later 
*/
    connected = 0;
    p1 = (-p1) - 1;
  } else {
    connected = 1;
    p1 = p1 - 1;
  }
  member=parray[p1].name;
  
/* listname is now the exact list name. --- Now check if member is 
already in the list */ 

  if (!in_list(listname, member)) {
    pprintf(p, "%s[%s] is not in the %s list.\n", SendCode(p, ERROR), member,listname);
    if (!connected) player_remove(p1);
    return COM_OK;
  }

/* remove member from the list */

  pprintf(p, "%s[%s] has been removed from the %s list.\n", SendCode(p, INFO), member,listname);
  sprintf(filename1, "%s/%s", lists_dir, listname);
  sprintf(filename2, "%s/%s.old", lists_dir, listname);
  rename(filename1, filename2);
  fp2 = fopen(filename2, "r");
  fp1 = fopen(filename1, "w");
  while (!feof(fp2)) {
    if (fscanf(fp2, "%s\n", lmember) != 1) {
      Logit("Internal error - cant make copy!");
      fclose(fp1);
      fclose(fp2);
      if (!connected) player_remove(p1);
      return COM_OK;
    }
    if (strcasecmp(lmember, member)) fprintf(fp1, "%s\n", lmember);
  }
  fclose(fp1);
  fclose(fp2);
  if (!connected) player_remove(p1);
  return COM_OK;
}

PUBLIC int com_showlist(int p, param_list param)
{
  FILE *fp;
  char filename[MAX_FILENAME_SIZE];
  char *listname = param[0].val.word;
  char member[20];
  int rights, flag;

  if (param[0].type == 0) {

/* no args -> show all lists */

    pprintf(p, "%sLists:\n",  SendCode(p, INFO));
    sprintf(filename, "%s/index", lists_dir);
    fp = fopen(filename, "r");
    if (!fp) {
      Logit("Internal error - cant open list index!");
      return COM_OK;
    }
    while (!feof(fp)) {
      if (fscanf(fp, "%s %d\n", member, &rights) != 2) {
	Logit("Internal error - bad format in index!");
	fclose(fp);
	return COM_OK;
      }
      pprintf(p, "%s%-20s is %s\n",  SendCode(p, INFO), member, (rights == 0) ? "SECRET" : 
(rights == 1) ? "READ ONLY" : "READ/WRITE");
    }
    fclose(fp);
    return COM_OK;
  }

/* find match in index */

  sprintf(filename, "%s/index", lists_dir);
  fp = fopen(filename, "r");
  if (!fp) {
    Logit("Internal error - cant open list file!");
    return COM_OK;
  }
  flag=0;
  while ((!feof(fp)) && (!flag)) {
    if (fscanf(fp, "%s %d\n", member, &rights) != 2) {
      Logit("Internal error - bad format in list!");
      fclose(fp);
      return COM_OK;
    }
    if (!strncmp(member, listname,strlen(listname))) {
      if ((rights < 1) && (parray[p].adminLevel < 10)) {
	pprintf(p, "%sYou are not allowed to see this list!",  SendCode(p, ERROR));
	fclose(fp);
	return COM_OK;
      }
      flag = 1;
      listname=member;
    }
  }
  fclose(fp);
  if (!flag) {
    pprintf(p, "%sNo such list!",  SendCode(p, ERROR));
    return COM_OK;
  }

/* display the file */

/*  pprintf(p, "-- The %s list: --",listname); */
  {
    char s[80];
    char fname[MAX_FILENAME_SIZE];
    FILE *fp2;
    int count = 0;
    multicol *m = multicol_start(1000);
    /* if a list has >1000 names, we're in trouble! */

    sprintf(fname, "%s/%s", lists_dir, listname);
    fp2 = fopen(fname, "r");
    if (!fp2) {
      pprintf( p, "Error: couldn't find the list named %s!\n", listname);
      return COM_OK;
    }
    fgets( s, 80, fp2 );
    while (!feof(fp2)) {
      s[strlen(s)-1] = '\0';
      multicol_store_sorted( m, s );
      count++;
      fgets( s, 80, fp2 );
    }
    fclose(fp2);
    if(parray[p].client) pprintf(p, "8 File\n");
    pprintf( p, "-- The %s list: %d names --", listname, count );
    multicol_pprint( m, p, 78, 2 );
    multicol_end( m );
    if(parray[p].client) pprintf(p, "8 File\n");
  }
  return COM_OK;
}

PUBLIC int com_quit( int p, param_list param)
{
  if (parray[p].game >= 0) {
    pprintf_prompt(parray[p].opponent, 
    "%sYour opponent typed quit.  Perhaps they had an emergency.\n", 
                    SendCode(p, ERROR));
    Logit("ESCAPE: %s quit from game with %s", 
           parray[p].name, parray[parray[p].opponent].name);
  }
  psend_file(p, mess_dir, MESS_LOGOUT);
  return COM_LOGOUT;
}

PUBLIC int com_best(int p, param_list param)
{
  const Player *LadderPlayer;
  int i, type, low, high;

  i = low = high = 0;

  if(param[0].type == TYPE_WORD) {
    i = sscanf(param[0].val.word, "%d-%d", &low, &high);
    if(i == 1) high = low;
    type = 19;
  }
  else if (param[0].type == TYPE_INT)
    type = param[0].val.integer;
  else type = 0;

  if (param[1].type == TYPE_INT)
    low = high = param[1].val.integer;
  else if(param[1].type == TYPE_WORD) {
    i = sscanf(param[1].val.word, "%d-%d", &low, &high);
    if(i == 1) high = low;
  }
  else {
    if((type == 9) && (high == 0)) high = num_9;
    else if((type == 19) && (high == 0)) high = num_19;
  }

  if(low) low--;
  if(low < 0) low = 0;
  if(low > high) { i = high; high = low; low = i;}
  if(high <= 0) high = 1;
  if(type == 9) {
    if(high > num_9) high = num_9;
    if(low > high) low = 0;
  }
  else {
    if(high > num_19) high = num_19;
    if(low > high) low = 0;
  }

  pprintf(p, "%sPosition      Name       W    L       Date last played\n", SendCode(p, INFO));
  pprintf(p, "%s--------  -----------   ---  ---  -------------------------\n", SendCode(p, INFO));
  if(type == 9) {
    for(i = low; i < high; i++)
    {
      LadderPlayer = PlayerAt(Ladder9, i);
      pprintf(p, "%s %3d      %-10s    %3d  %3d  %11.11s\n",
                 SendCode(p, INFO), i+1,
                 LadderPlayer->szName,
                 LadderPlayer->nWins, LadderPlayer->nLosses,
                 LadderPlayer->tLast ? strgtime((time_t *) &LadderPlayer->tLast) : "---");
    }
  }
  else {
    for(i = low; i < high; i++)
    {
      LadderPlayer = PlayerAt(Ladder19, i);
      pprintf(p, "%s %3d      %-10s    %3d  %3d  %s\n",
                 SendCode(p, INFO), i+1, 
                 LadderPlayer->szName,
                 LadderPlayer->nWins, LadderPlayer->nLosses,
                 LadderPlayer->tLast ? strgtime((time_t *) &LadderPlayer->tLast) : "---");
    }
  }
  return COM_OKN;
}

PUBLIC int com_join(int p, param_list param)
{
  FILE *fp;
  const Player *LadderPlayer;
  time_t now;

  now = time(0);

  if(parray[p].registered == 0) {
    pprintf(p, "%sSorry, you must register to play on the ladder.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  if(parray[p].game >= 0) {
    pprintf(p, "%sSorry, you cannot join a ladder while playing a game.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[0].type == 0) {
  /* no args -> show all ladders */
    pprintf(p, "%sLadders:\n",  SendCode(p, INFO));
    pprintf(p, "%sLadder9        9x9 Ladder\n", SendCode(p, INFO));
    pprintf(p, "%sLadder19       19x19 Ladder", SendCode(p, INFO));
    return COM_OK;
  }
  if (param[0].val.string != (char *) NULL) {
    if(!strcmp(param[0].val.string, "ladder9")) {
      if((PlayerNamed(Ladder9, parray[p].name)) != NULL) {
        pprintf(p, "%sYou are already a member of the 9x9 ladder.", 
                    SendCode(p, ERROR));
        return COM_OK;
      }
      PlayerNew(Ladder9, parray[p].name);
      LadderPlayer = PlayerNamed(Ladder9, parray[p].name);
      PlayerUpdTime(Ladder9, LadderPlayer->idx, now);
      fp = fopen(LADDER9, "w");
      if(fp == (FILE *) NULL) {
        Logit("Error opening %s for write!!!", LADDER9);
        pprintf(p, "%sThere was an internal error.  Please notify an admin!",
                    SendCode(p, ERROR));
        return COM_OK;
      }
      num_9 = PlayerSave(fp, Ladder9);
      fclose(fp);
      pprintf(p, "%sYou are at position %d in the 9x9 ladder.  Good Luck!",
                  SendCode(p, INFO), (LadderPlayer->idx) + 1);
      player_resort(); 
      return COM_OK;
    }
    else if(!strcmp(param[0].val.string, "ladder19")) {
      if((PlayerNamed(Ladder19, parray[p].name)) != NULL) {
        pprintf(p, "%sYou are already a member of the 19x19 ladder.", 
                    SendCode(p, ERROR));
        return COM_OK;
      }
      PlayerNew(Ladder19, parray[p].name);
      LadderPlayer = PlayerNamed(Ladder19, parray[p].name);
      PlayerUpdTime(Ladder19, LadderPlayer->idx, now);
      fp = fopen(LADDER19, "w");
      if(fp == (FILE *) NULL) {
        Logit("Error opening %s for write!!!", LADDER19);
        pprintf(p, "%sThere was an internal error.  Please notify an admin!",
                    SendCode(p, ERROR));
        return COM_OK;
      }
      num_19 = PlayerSave(fp, Ladder19);
      fclose(fp);
      pprintf(p, "%sYou are at position %d in the 19x19 ladder.  Good Luck!",
                  SendCode(p, INFO), (LadderPlayer->idx) + 1);
      player_resort();
      return COM_OK;
    }
    else return COM_BADPARAMETERS;
  }     
  return COM_OK;
}

PUBLIC int com_drop(int p, param_list param)
{
  FILE *fp;
  const Player *LadderPlayer;

  if(parray[p].registered == 0) {
    pprintf(p, "%sSorry, you must register to play on the ladder.",
                SendCode(p, ERROR));
    return COM_OK;
  }

  if(parray[p].game >= 0) {
    pprintf(p, "%sSorry, you cannot drop a ladder while playing a game.",
                SendCode(p, ERROR));
    return COM_OK;
  }

  if (param[0].type == 0) {
  /* no args -> show all ladders */
    pprintf(p, "%sYou are in ladders:\n",  SendCode(p, INFO));
    LadderPlayer = PlayerNamed(Ladder9, parray[p].name);
    if(LadderPlayer != NULL)
      pprintf(p, "%sLadder9        Position %d\n", 
                  SendCode(p, INFO), (LadderPlayer->idx) + 1);
    LadderPlayer = PlayerNamed(Ladder19, parray[p].name);
    if(LadderPlayer != NULL)
      pprintf(p, "%sLadder19       Position %d", 
                  SendCode(p, INFO), (LadderPlayer->idx) + 1);
    return COM_OK;
  }
  if (param[0].val.string != (char *) NULL) {
    if(!strcmp(param[0].val.string, "ladder9")) {
      if((LadderPlayer = PlayerNamed(Ladder9, parray[p].name)) == NULL) {
        pprintf(p, "%sYou are not a member of the 9x9 ladder.",
                    SendCode(p, ERROR));
        return COM_OK;
      }
      PlayerKillAt(Ladder9, LadderPlayer->idx);
      fp = fopen(LADDER9, "w");
      if(fp == NULL) {
        Logit("Error opening %s for write!!!", LADDER9);
        pprintf(p, "%sThere was an internal error.  Please notify an admin!\n",
                    SendCode(p, ERROR));
        return COM_OK;
      }
      num_9 = PlayerSave(fp, Ladder9);
      fclose(fp);
      pprintf(p, "%sYou have been removed from the 9x9 ladder.",
                  SendCode(p, INFO));
      player_resort();
      return COM_OK;
    }
    if(!strcmp(param[0].val.string, "ladder19")) {
      if((LadderPlayer = PlayerNamed(Ladder19, parray[p].name)) == NULL) {
        pprintf(p, "%sYou are not a member of the 19x19 ladder.",
                    SendCode(p, ERROR));
        return COM_OK;
      }
      PlayerKillAt(Ladder19, LadderPlayer->idx);
      fp = fopen(LADDER19, "w");
      if(fp == NULL) {
        Logit("Error opening %s for write!!!", LADDER19);
        pprintf(p, "%sThere was an internal error.  Please notify an admin!\n",
                    SendCode(p, ERROR));
        return COM_OK;
      }
      num_19 = PlayerSave(fp, Ladder19);
      fclose(fp);
      pprintf(p, "%sYou have been removed from the 19x19 ladder.",
                  SendCode(p, INFO));
      player_resort();
      return COM_OK;
    }
  }
  return COM_OK;
}

PUBLIC int com_clntvrfy(int p, param_list param)
{
  char tmp[MAX_STRING_LENGTH];
  char *atmp;
  int len, i;
  
  i = 0;
  if (param[0].type == 0) { return COM_BADPARAMETERS; }
  if(((int) strlen(param[0].val.string)) < 2) { return COM_BADPARAMETERS; }

  atmp = getword(param[0].val.string);  

  if(param[0].val.string != (char *) NULL) {
    /*sprintf(tmp, "%s%c\n", SendCode(p, CLIVRFY), param[0].val.string[0]);*/
    sprintf(tmp, "%s%s\n", SendCode(p, CLIVRFY), atmp);
    pprintf(p, tmp);
    len = strlen(atmp);
    atmp = param[0].val.string + len;
    while(*atmp) {
      if(*atmp == '%') { tmp[i++] = '%'; tmp[i++] = '%';}
      else tmp[i++] = *atmp;
      tmp[i] = '\0';
      atmp++;
    }
    pcommand(p, tmp);
  }
  return COM_OKN;
}

PUBLIC int com_choice(int p, param_list param)
{
  pprintf(p, "%sGame set to go.", SendCode(p, INFO));
  return COM_OK;
}


PUBLIC int com_shout(int p, param_list param)
{
  int p1, i;
  int len, clen = 0;

  char szBuf[1024];
  char szBufc[1024];
  char tmp[256];

#ifdef UNREGS_CANNOT_SHOUT
  if (!parray[p].registered) {
    pprintf(p, "%sOnly registered players can use the shout command.",  SendCode(p, ERROR));
    return COM_OK;
  }
#endif /* UNREGS_CANNOT_SHOUT */
  if (parray[p].muzzled) {
    return COM_OK;
  }

  if(Ocarray[CSHOUT].locked && parray[p].adminLevel == 0) {
    pprintf(p, "%sSorry, shouts are turned off right now.  :)", SendCode(p, ERROR));
    return COM_OK;
  }

  sprintf(szBuf, "!%s!: %s\n", parray[p].name, param[0].val.string);
  sprintf(szBufc, "%d !%s!: %s\n", SHOUT, parray[p].name, param[0].val.string);

  len = strlen(szBuf);
  clen = strlen(szBufc);

  for (i = 0; i < OnumOn[CSHOUT]; i++) {
    p1 = Ochannels[CSHOUT][i];
    if(p1 == p) continue;
    if (player_censored(p1, p)) continue;
    if ((parray[p1].status == PLAYER_PASSWORD)
        || (parray[p1].status == PLAYER_LOGIN))
      continue;
    if(parray[p1].client) {
      net_send(parray[p1].socket, szBufc, clen);
      sprintf(tmp, "1 %d\n", parray[p1].state);
      net_sendStr(parray[p1].socket, tmp);
    }
    else {
      net_send(parray[p1].socket, szBuf, len);
      if(parray[p1].state == SCORING) {
        net_send(parray[p1].socket, "Enter Dead Group: ", 18);
      }
      else {
        if(parray[p1].extprompt) {
          sprintf(tmp, "|%s/%d%s| %s ",
          parray[p1].last_tell >= 0 ? parray[parray[p1].last_tell].name : "",
          parray[p1].last_channel,
          parray[p1].busy[0] == '\0' ? "" : "(B)",
          parray[p1].prompt);
        }
        else {
          sprintf(tmp, "%s",parray[p1].prompt);
        }
        net_sendStr(parray[p1].socket, tmp);
      }
    }
  }
  return COM_OK;
}

PUBLIC int com_lashout(int p, param_list param)
{
  int i;

  for (i = 1; i < Ocarray[CASHOUT].Num_Yell; i++) {
    pprintf(p, "%s%s", SendCode(p, INFO), Ocarray[CASHOUT].Yell_Stack[i - 1]);
  }
  return COM_OK;
}

PUBLIC int com_lchan(int p, param_list param)
{
  int ch, i;

  ch = parray[p].last_channel;

  for (i = 1; i < carray[ch].Num_Yell; i++) {
    pprintf(p, "%s%s", SendCode(p, INFO), carray[ch].Yell_Stack[i - 1]);
  }
  return COM_OK;
}


PUBLIC int com_admins(int p, param_list param)
{
  int p1, count = 0;

  pprintf(p, "%sAdmins available to help you:\n%s", SendCode(p, INFO),
          SendCode(p, INFO));
  for (p1 = 0; p1 < p_num; p1++) {
    if (parray[p1].status != PLAYER_PROMPT) continue;
    if (!parray[p1].adminLevel) continue;
    if (parray[p1].game >= 0) continue;
    if (((player_idle(p1)%3600)/60) > 45) continue;
    pprintf(p, " %13s", parray[p1].name);
    count++;
    if (count %3 == 0) 
      pprintf(p, "\n%s", SendCode(p, INFO));
  }
  pprintf(p, "\n%sFound %d admin%s to help you.",
              SendCode(p, INFO), count, (count > 1) ? "s" : "");
  return COM_OK;
}

PUBLIC int com_ashout(int p, param_list param)
{
  int p1, i;
  char text[MAX_STRING_LENGTH];

  if (!parray[p].adminLevel) {
    return COM_OK;
  }

  for (i = 0; i < OnumOn[CASHOUT]; i++) {
    p1 = Ochannels[CASHOUT][i];
    if(p1 == p) continue;
    if (player_censored(p1, p)) continue;
    if ((parray[p1].status == PLAYER_PASSWORD)
        || (parray[p1].status == PLAYER_LOGIN))
      continue;
    pprintf_prompt(p1, "%s##%s##: %s\n", 
                   SendCode(p1, SHOUT),
                   parray[p].name,
		   param[0].val.string);
  }
  sprintf(text, "<%s> %s\n", parray[p].name, param[0].val.string);
  add_to_Oyell_stack(CASHOUT, text);

  return COM_OK;
}

PUBLIC int com_gshout(int p, param_list param)
{
  int p1, i;
  int len, clen = 0;

  char szBuf[1024];
  char szBufc[1024];
  char tmp[256];

#ifdef UNREGS_CANNOT_SHOUT
  if (!parray[p].registered) {
    pprintf(p, "%sOnly registered players can use the gshout command.", 
           SendCode(p, ERROR));
    return COM_OK;
  }
#endif /* UNREGS_CANNOT_SHOUT */

  if (parray[p].gmuzzled) {
    return COM_OK;
  }
  if(Ocarray[CSHOUT].locked && parray[p].adminLevel == 0) {
    pprintf(p, "%sSorry, shouts are turned off right now.  :)", SendCode(p, ERROR));
    return COM_OK;
  }
  sprintf(szBuf, "!%s!: %s\n", parray[p].name, param[0].val.string);
  sprintf(szBufc, "%d !%s!: %s\n", SHOUT, parray[p].name, param[0].val.string);

  len = strlen(szBuf);
  clen = strlen(szBufc);

  for (i = 0; i < OnumOn[CGSHOUT]; i++) {
    p1 = Ochannels[CGSHOUT][i];
    if(p1 == p) continue;
    if (player_censored(p1, p)) continue;
    if ((parray[p1].status == PLAYER_PASSWORD)
        || (parray[p1].status == PLAYER_LOGIN))
      continue;
    if(parray[p1].client) {
      net_send(parray[p1].socket, szBufc, clen);
      sprintf(tmp, "1 %d\n", parray[p1].state);
      net_sendStr(parray[p1].socket, tmp);
    }
    else {
      net_send(parray[p1].socket, szBuf, len);
      if(parray[p1].state == SCORING) {
        net_send(parray[p1].socket, "Enter Dead Group: ", 18);
      }
      else {
        if(parray[p1].extprompt) {
          sprintf(tmp, "|%s/%d%s| %s ",
          parray[p1].last_tell >= 0 ? parray[parray[p1].last_tell].name : "",
          parray[p1].last_channel,
          parray[p1].busy[0] == '\0' ? "" : "(B)",
          parray[p1].prompt);
        }
        else {
          sprintf(tmp, "%s",parray[p1].prompt);
        }
        net_sendStr(parray[p1].socket, tmp);
      }
    }
  }

/*  for (p1 = 0; p1 < p_num; p1++) {
    if (p1 == p) continue;
    if (parray[p1].status != PLAYER_PROMPT) continue;
    if (!parray[p1].i_gshout) continue;
    if (player_censored(p1, p)) continue;
    pprintf_prompt(p1, "%s!%s!: %s\n", 
                   SendCode(p1, SHOUT),
                   parray[p].name,
		   param[0].val.string);
  }
*/
  return COM_OK;
}

PUBLIC int com_it(int p, param_list param)
{
  int p1;

#ifdef UNREGS_CANNOT_SHOUT
  if (!parray[p].registered) {
    pprintf(p, "%sOnly registered players can use the it command.", 
           SendCode(p, ERROR));
    return COM_OK;
  }
#endif /* UNREGS_CANNOT_SHOUT */
  if (parray[p].muzzled) {
    return COM_OK;
  }
  for (p1 = 0; p1 < p_num; p1++) {
    if (p1 == p) continue;
    if (parray[p1].status != PLAYER_PROMPT) continue;
    if (!parray[p1].i_shout) continue;
    if (player_censored(p1, p)) continue;
    if ((!strncmp(param[0].val.string,"\'",1)) || 
       (!strncmp(param[0].val.string,",",1)) ||
       (!strncmp(param[0].val.string,".",1)))
       {
       pprintf_prompt(p1, "%s--> %s%s\n", 
                   SendCode(p1, SHOUT),
                   parray[p].name,
		   param[0].val.string);
       }
    else
       {
       pprintf_prompt(p1, "%s--> %s %s\n", 
		   SendCode(p1, SHOUT),
                   parray[p].name,
		   param[0].val.string);
       }
  }
  return COM_OK;
}

PUBLIC int com_git(int p, param_list param)
{
  int p1;

#ifdef UNREGS_CANNOT_SHOUT
  if (!parray[p].registered) {
    pprintf(p, "%sOnly registered players can use the it command.",
           SendCode(p, ERROR));
    return COM_OK;
  }
#endif /* UNREGS_CANNOT_SHOUT */
  if (parray[p].muzzled) {
    return COM_OK;
  }
  for (p1 = 0; p1 < p_num; p1++) {
    if (p1 == p) continue;
    if (parray[p1].status != PLAYER_PROMPT) continue;
    if (!parray[p1].i_gshout) continue;
    if (player_censored(p1, p)) continue;
    if ((!strncmp(param[0].val.string,"\'",1)) ||
       (!strncmp(param[0].val.string,",",1)) ||
       (!strncmp(param[0].val.string,".",1)))
       {
       pprintf_prompt(p1, "%s--> %s%s\n",
                   SendCode(p1, SHOUT),
                   parray[p].name,
                   param[0].val.string);
       }
    else
       {
       pprintf_prompt(p1, "%s--> %s %s\n",
                   SendCode(p1, SHOUT),
                   parray[p].name,
                   param[0].val.string);
       }
  }
  return COM_OK;
}

PUBLIC int com_emote(int p, param_list param)
{
  const char *tmp;
  char args[MAX_STRING_LENGTH];
  int p1;

  if ((param[0].type == (int) TYPE_NULL) || (param[1].type == (int) TYPE_NULL))
    return COM_BADPARAMETERS;
  if (param[0].type == TYPE_WORD) {
    stolower(param[1].val.word);
  }
  p1 = player_find_part_login(param[1].val.word);
  if ((p1 < 0) ||
     (parray[p1].status == PLAYER_PASSWORD) ||
     (parray[p1].status == PLAYER_LOGIN)) {
     pprintf(p, "%sNo user named \"%s\" is logged in.", SendCode(p, ERROR),
                 param[1].val.word);
     return COM_OK;
  }

  if (player_censored(p1, p)) {
    pprintf(p, "%sPlayer \"%s\" is censoring you.",
                SendCode(p, ERROR), parray[p1].name);
    return COM_OK;
  }
  if (param[2].type != (int) TYPE_NULL) {
    strcpy(args, param[2].val.string);
  } else {
    args[0] = 0;
  }

  if(!strcmp(param[0].val.word, "balloon")) {
    if(parray[p].water == 0) {
      pprintf(p, "%sYou are out of water balloons.  Go win some games.  :)",
                  SendCode(p, ERROR));
      return COM_OK;
    } else { 
      if(parray[p].adminLevel <= 0) {
        parray[p].water--;
      }
    }
  }

  tmp = EmoteMkStr(param[0].val.word, parray[p].name, args, parray[p1].client);

  if(tmp != NULL) {
    pprintf_prompt(p1, "%s%s^ %s ^\n", 
                        parray[p1].client ? "" : "\n",
                        SendCode(p1, EMOTE), tmp);
    tmp = EmoteMkStr(param[0].val.word, parray[p].name, args, parray[p].client);
    pprintf(p, "%s^ %s ^", SendCode(p, EMOTE), tmp);
  }
                 
  else
    pprintf(p, "%sNo such emote", SendCode(p, ERROR));

  return COM_OK;
}

PUBLIC int com_pme(int p, param_list param)
{
  char *tmp;
  int p1;

  if (param[0].type == (int) TYPE_NULL)
    return COM_BADPARAMETERS;
  if (param[0].type == TYPE_WORD) {
    stolower(param[0].val.word);
  }
  if (!strcmp(param[0].val.word, "*")) {
    if (parray[p].last_pzz < 0) {
      pprintf(p, "%sNo one to tell anything to.", SendCode(p, ERROR));
      return COM_OK;
    } else {
      p1 = parray[p].last_pzz;
    }
  } else {
    p1 = player_find_part_login(param[0].val.word);
    if ((p1 < 0) ||
       (parray[p1].status == PLAYER_PASSWORD) ||
       (parray[p1].status == PLAYER_LOGIN)) {
       pprintf(p, "%sNo user named \"%s\" is logged in.", SendCode(p, ERROR),
                   param[0].val.word);
       return COM_OK;
    }
  }
  if (player_censored(p1, p)) {
    pprintf(p, "%sPlayer \"%s\" is censoring you.",
                SendCode(p, ERROR), parray[p1].name);
    return COM_OK;
  }
  tmp = eatwhite(param[1].val.string);
 
  pprintf_prompt(p1, "%s%s^ %s %s ^\n", 
                 parray[p1].client ? "" : "\n",
                 SendCode(p1, EMOTE), parray[p].name,
                 tmp);
  if(parray[p].last_pzz != p1) {
    parray[p].last_pzz = p1;
    pprintf(p, "%sSetting your * to %s", SendCode(p, EMOTETO), parray[p1].name);
  } else {
    pprintf(p, "%s%s", SendCode(p, EMOTETO), parray[p1].name);
  }
  return COM_OK;
}

PUBLIC int com_invite(int p, param_list param)
{
  int ch, p1;

  if (param[0].type == (int) TYPE_NULL)
    return COM_BADPARAMETERS;
  if (param[0].type == TYPE_WORD) {
    stolower(param[0].val.word);
  }
  p1 = player_find_part_login(param[0].val.word);
  if ((p1 < 0) ||
     (parray[p1].status == PLAYER_PASSWORD) ||
     (parray[p1].status == PLAYER_LOGIN)) {
     pprintf(p, "%sNo user named \"%s\" is logged in.", SendCode(p, ERROR),
                 param[0].val.word);
     return COM_OK;
  }
  
  if (param[1].type == (int) TYPE_NULL) {
    if(parray[p].last_channel < 0) {
      return COM_BADPARAMETERS;
    } else {
      ch = parray[p].last_channel;
    }
  } else {
    if(param[1].val.integer < 0 || param[1].val.integer > MAX_CHANNELS) {
      return COM_BADPARAMETERS;
    } else {
      ch = param[1].val.integer;
    }
  }

  if(!on_channel(ch, p)) {
    pprintf(p, "%sYou are not on channel %d\n", SendCode(p, ERROR), ch);
    return COM_OK;
  }
  if(on_channel(ch, p1)) {
    pprintf(p, "%s%s is already on channel %d\n", SendCode(p, ERROR), 
                parray[p1].name, ch);
    return COM_OK;
  }


  pprintf_prompt(p1, "%s%s invites you to channel %d\n", SendCode(p1, INFO),
                    parray[p].name, ch);
  pprintf(p, "%sInvited %s to channel %d", SendCode(p, INFO),
              parray[p1].name, ch);
  return COM_OK;
}

PUBLIC int com_me(int p, param_list param)
{
  int ch, i, p1;

  if(parray[p].last_channel < 0) {
    pprintf(p, "%sNo previous channel.", SendCode(p, ERROR));
    return COM_OK;
  }

  ch = parray[p].last_channel;

  for (i = 0; i < numOn[ch]; i++) {
    p1 = channels[ch][i];
    if (player_censored(p1, p)) continue;
    if ((parray[p1].status == PLAYER_PASSWORD)
        || (parray[p1].status == PLAYER_LOGIN))
      continue;
    if(p1 == p || (parray[p1].last_channel != ch)) {
      pprintf(p1, "\n%s(%d)-- %s %s --\n", 
                   SendCode(p1, INFO),
                   ch,
                   parray[p].name,
                   param[0].val.string);
    }
    else {
      pprintf_prompt(p1, "\n%s-- %s %s --\n", 
                   SendCode(p1, INFO),
                   parray[p].name,
                   param[0].val.string);
    }
  }
  return COM_OK;
}

#define TELL_TELL 0
#define TELL_SAY 1
#define TELL_KIBITZ 3
#define TELL_CHANNEL 4

PRIVATE int tell(int p, int p1, char *msg, int why, int ch)
{
  char tmp[MAX_LINE_SIZE];

  if ((!parray[p1].i_tell) && (!parray[p].registered)) {
    pprintf(p, "%sPlayer \"%s\" isn't listening to unregistered tells.",
	    SendCode(p, ERROR),
	    parray[p1].name);
    return COM_OK;
  }
  if((parray[p1].i_robot) && (parray[p].i_robot)) {
    return COM_OK;
  }
  if ((player_censored(p1, p)) && (parray[p].adminLevel==0)) {
    pprintf(p, "%sPlayer \"%s\" is censoring you.", 
            SendCode(p, ERROR), parray[p1].name);
    return COM_OK;
  }
  if (((parray[p1].game >= 0 && (parray[p1].game != parray[p].game)) &&
       garray[parray[p1].game].gotype >= TYPE_TNETGO) || (parray[p].match_type == TYPE_TNETGO && parray[p1].game != parray[p].game)) {
    pprintf(p, "%sPlayer %s is currently involved in a tournement match.\n%sPlease send a message instead.", SendCode(p, ERROR), parray[p1].name, SendCode(p, ERROR));
    return COM_OK;
  }
  switch (why) {
  case TELL_SAY:
    pprintf_prompt(p1, "%s*%s*: %s\n", 
                  SendCode(p1, SAY), parray[p].name, msg);
    break;
  case TELL_KIBITZ:
    pprintf_prompt(p1, "%s   %s\n", SendCode(p1, KIBITZ), msg);
    break;
  case TELL_CHANNEL:
    if(parray[p1].client) {
      pprintf_prompt(p1, "%s%d:%s: %s\n", 
            SendCode(p1, YELL), ch, parray[p].name, msg);
    }
    else {
      if(parray[p1].last_channel == ch)
        pprintf_prompt(p1, "\n<%s> %s\n", parray[p].name, msg);
      else
        pprintf_prompt(p1, "\n<%s/%d> %s\n", parray[p].name, ch, msg);
    }
    break;
  case TELL_TELL:
  default:
    pprintf(p1, "%s%s\n\n", 
        parray[p1].bell ? SendCode(p1, BEEP) : "", parray[p1].bell ? "" : "");
    pprintf_prompt(p1, "%s*%s*: %s\n", SendCode(p1, TELL), parray[p].name, msg);
    if(parray[p1].last_tell != p) parray[p1].last_tell_from = p;
    break;
  }
  if (!(parray[p1].busy[0]=='\0')) {
    sprintf(tmp,"  [%s] (idle: %d minutes)", 
                   parray[p1].busy, ((player_idle(p1)%3600)/60));
  } else {
    if (((player_idle(p1)%3600)/60) > 2) {
      sprintf(tmp," who has been idle %d minutes", ((player_idle(p1)%3600)/60));
    } 
    else sprintf(tmp," ");
  }  
  if (why == TELL_TELL) {
    if(parray[p].last_tell != p1) {
      pprintf(p, "%sSetting your . to %s%s", 
           SendCode(p, INFO),
  	   parray[p1].name, 
           (parray[p1].game >= 0 && (parray[p1].game != parray[p].game)) 
           ? " (who is playing a game)" : tmp);
      parray[p].last_tell = p1;
    }
    else {
      pprintf(p, "%s%s%s", 
           SendCode(p, DOT),
  	   parray[p1].name, 
           (parray[p1].game >= 0 && (parray[p1].game != parray[p].game)) 
           ? " (who is playing a game)" : tmp);
    }
  }
  return COM_OK;
}

PRIVATE int beep(int p, int p1)
{
  char tmp[MAX_LINE_SIZE];

  if ((player_censored(p1, p)) && (parray[p].adminLevel==0)) {
    pprintf(p, "%sPlayer %s is censoring you.", 
            SendCode(p, ERROR), parray[p1].name);
    return COM_OK;
  }
  if((parray[p].bmuzzled)) {
    pprintf(p, "%sBeep: oops.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (!parray[p].registered) {
    pprintf(p, "%sOnly registered players can use the beep command.",
            SendCode(p, ERROR));
    return COM_OK;
  }

  if(parray[p1].bell) pprintf(p1, "%s\n", SendCode(p1, BEEP));
  else pprintf(p1, "%s\n", SendCode(p1, BEEP));
  pprintf_prompt(p1, "%s%s is beeping you.\n", 
		SendCode(p1, INFO),
      		parray[p].name);
  if (!(parray[p1].busy[0]=='\0')) {
    sprintf(tmp,"%sbeeped %s's console who %s (idle: %s)", 
		SendCode(p, INFO),
            parray[p1].name, 
            parray[p1].busy, 
            hms(player_idle(p1), 1, 0, 0));
  } else {
    if (((player_idle(p1)%3600)/60) > 2) {
      sprintf(tmp,"%s%s has been idle %s", 
		SendCode(p, INFO),
              parray[p1].name,
              hms(player_idle(p1), 1, 0, 0));
    } 
    else sprintf(tmp,"%sbeeped %s`s console.",
		SendCode(p, INFO),
		parray[p1].name);
  }  
  pprintf(p, "%s", tmp);
  return COM_OK;
}

PRIVATE int chtell(int p, int ch, char *msg)
{
  int p1, i;
  char text[MAX_STRING_LENGTH];

  if ((ch == 0) && (parray[p].adminLevel == 0)) {
    pprintf(p, "%sInvalid channel number.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (ch < 0) {
    pprintf(p, "%sThe lowest channel number is 1.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (ch >= MAX_CHANNELS) {
    pprintf(p, "%sThe maximum channel number is %d.", SendCode(p, ERROR), MAX_CHANNELS - 1);
    return COM_OK;
  }
  if((carray[ch].dNd == 1) && (!on_channel(ch, p))) {
    pprintf(p, "%sThe users of channel %d would prefer you to be in that channel before\n%sspeaking in it.  See \"help channel\"", 
        SendCode(p, ERROR), ch, SendCode(p, ERROR));
    return COM_OK;
  }
  for (i = 0; i < numOn[ch]; i++) {
    p1 = channels[ch][i];
    if (p1 == p) continue;
    if (player_censored(p1, p)) continue;
    if (!parray[p1].i_tell) continue;
    if ((parray[p1].status == PLAYER_PASSWORD)
	|| (parray[p1].status == PLAYER_LOGIN))
      continue;
    tell(p, p1, msg, TELL_CHANNEL, ch);
  }
  if(ch != parray[p].last_channel) {
    pprintf(p, "%sSetting your ; to channel %d", SendCode(p, INFO), ch);
    parray[p].last_channel = ch;
  } else {
    pprintf(p, "%s[%d]", SendCode(p, INFO), ch);
  }
  sprintf(text, "<%s> %s\n", parray[p].name, msg);
  add_to_yell_stack(ch, text);
  return COM_OK;
}

PUBLIC int com_kibitz(int p, param_list param)
{
  int g, i, otherg;
  int p1;
  int count = 0;
  char tmp2[MAX_LINE_SIZE];
  char tmp3[MAX_LINE_SIZE];

  if (parray[p].muzzled) {
    return COM_OK;
  }
  if ((parray[p].num_observe == 0) && (parray[p].game) < 0) {
    pprintf(p, "%sYou are not playing or observing a game.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[0].type == (int) TYPE_NULL) return COM_BADPARAMETERS;
  if (param[0].type == TYPE_INT) {
    if(param[0].val.integer == 0) return COM_BADPARAMETERS;
    g = param[0].val.integer - 1;
    for (i = 0; i < parray[p].num_observe; i++) {
      if(g == parray[p].observe_list[i]) {
        count = 1;
        continue;
      }
    }
    if((!count) && (g != parray[p].game)) {
      pprintf(p, "%sYou are not playing or observing that game.", SendCode(p, ERROR));
      return COM_OK;
    }
  }

  else {
    if (parray[p].game >= 0)
      g = parray[p].game;
    else
      g = parray[p].observe_list[0];
  }
  if (param[0].type == TYPE_INT) {
    sprintf(tmp2, "%s (%d)", param[1].type == (int) TYPE_NULL ? "" : param[1].val.string, movenum(garray[g].GoGame));
  } else {
    sprintf(tmp2, "%s %s (%d)", param[0].val.word, 
            param[1].type == (int) TYPE_NULL ? "" : param[1].val.string,
            movenum(garray[g].GoGame));
  }

  for (p1 = 0; p1 < p_num; p1++) {
    if (p1 == p) continue;
    if (parray[p1].status != PLAYER_PROMPT) continue;
    if (player_is_observe(p1, g)) {
      if(player_censored(p1, p)) continue;
      if(parray[p1].client) {
        if(parray[p1].bell) {
          pprintf(p1, "%s\n\n", SendCode(p1, BEEP));
        }
      }
      pprintf(p1, "%sKibitz %s [%3.3s%s]: Game %s vs %s [%d]\n",
                   SendCode(p1, KIBITZ),
 		   parray[p].name,
 		   parray[p].srank,
 		   parray[p].rated ? "*" : " ",
		   parray[garray[g].white].name,
		   parray[garray[g].black].name,
		   g + 1);
      tell(p, p1, tmp2, TELL_KIBITZ, 0);
    }
#ifdef PAIR
    if(paired(g)) { 
      otherg = garray[g].pairwith;
      if (player_is_observe(p1, otherg)) {
        if(player_censored(p1, p)) continue;
        if(parray[p1].client) {
          if(parray[p1].bell) {
            pprintf(p1, "%s\n\n", SendCode(p1, BEEP));
          }
        }
        pprintf(p1, "%sKibitz %s [%3.3s%s]: Game %s vs %s [%d]\n",
                     SendCode(p1, KIBITZ),
 		   parray[p].name,
 		   parray[p].srank,
 		   parray[p].rated ? "*" : " ",
		   parray[garray[g].white].name,
		   parray[garray[g].black].name,
		   otherg + 1);
        tell(p, p1, tmp2, TELL_KIBITZ, 0);
      }
    }
#endif
  }
  if(garray[g].Teach2 == 1) {
    p1 = garray[g].white;
    pprintf(p1, "%sKibitz %s [%3.3s%s]: Game %s vs %s [%d]\n",
                   SendCode(p1, KIBITZ),
                   parray[p].name,
                   parray[p].srank,
                   parray[p].rated ? "*" : " ",
                   parray[garray[g].white].name,
                   parray[garray[g].black].name,
                   g + 1);
      tell(p, p1, tmp2, TELL_KIBITZ, 0);
  }
  if((garray[g].Teach == 1) || (garray[g].Teach2 == 1)) {
    p1 = garray[g].black;
    pprintf(p1, "%sKibitz %s [%3.3s%s]: Game %s vs %s [%d]\n",
                   SendCode(p1, KIBITZ),
                   parray[p].name,
                   parray[p].srank,
                   parray[p].rated ? "*" : " ",
                   parray[garray[g].white].name,
                   parray[garray[g].black].name,
                   g + 1);
      tell(p, p1, tmp2, TELL_KIBITZ, 0);
  }

  sprintf(tmp3, " %s %s%s: %s", 
                parray[p].name, 
                parray[p].srank, 
                parray[p].rated ? "*" : " ", 
                tmp2); 
  add_kib(&garray[g], movenum(garray[g].GoGame), tmp3);
#ifdef PAIR
  if(paired(g)) { 
    otherg = garray[g].pairwith;
    add_kib(&garray[otherg], movenum(garray[otherg].GoGame), tmp3);
  }
#endif
  return COM_OK;
}

PUBLIC int com_beep(int p, param_list param)
{
  int p1;

  if (param[0].type == (int) TYPE_NULL)
    return COM_BADPARAMETERS;
  if (param[0].type == TYPE_WORD) {
    stolower(param[0].val.word);
    }
    p1 = player_find_part_login(param[0].val.word);
    if ((p1 < 0) || 
       (parray[p1].status == PLAYER_PASSWORD) || 
       (parray[p1].status == PLAYER_LOGIN)) {
         pprintf(p, "%sNo user named \"%s\" is logged in.", SendCode(p, ERROR), param[0].val.word);
         return COM_OK;
    }
    return beep(p, p1);
}

PUBLIC int com_tell(int p, param_list param)
{
  int p1;

  if (param[0].type == (int) TYPE_NULL)
    return COM_BADPARAMETERS;
  if (param[0].type == TYPE_WORD) {
    stolower(param[0].val.word);
    if (!strcmp(param[0].val.word, ".")) {
      if (parray[p].last_tell < 0) {
	pprintf(p, "%sNo one to tell anything to.", SendCode(p, ERROR));
	return COM_OK;
      } else {
	return tell(p, parray[p].last_tell, param[1].val.string, TELL_TELL, 0);
      }
    }
    if (!strcmp(param[0].val.word, "^")) {
      if (parray[p].last_tell_from < 0) {
	pprintf(p, "%sNo one to tell anything to.", SendCode(p, ERROR));
	return COM_OK;
      } else {
	return tell(p, parray[p].last_tell_from, param[1].val.string, TELL_TELL, 0);
      }
    }
    if (!strcmp(param[0].val.word, ",")) {
      if (parray[p].last_channel < 0) {
	pprintf(p, "%sNo previous channel.", SendCode(p, ERROR));
	return COM_OK;
      } else {
	return chtell(p, parray[p].last_channel, param[1].val.string);
      }
    }
    p1 = player_find_part_login(param[0].val.word);
    if ((p1 < 0) || (parray[p1].status == PLAYER_PASSWORD)
	|| (parray[p1].status == PLAYER_LOGIN)) {
      pprintf(p, "%sNo user named \"%s\" is logged in.", SendCode(p, ERROR), param[0].val.word);
      return COM_OK;
    }
    return tell(p, p1, param[1].val.string, TELL_TELL, 0);
  } else {			/* Channel */
    return chtell(p, param[0].val.integer, param[1].val.string);
  }
}

PUBLIC int com_say(int p, param_list param)
{
  int g;
  char tmp2[MAX_LINE_SIZE];

  if (parray[p].opponent < 0) {
    if (parray[p].last_opponent < 0) {
      pprintf(p, "%sNo one to say anything to, try tell.", SendCode(p, ERROR));
      return COM_OK;
    } else {
      return tell(p, parray[p].last_opponent, param[0].val.string, TELL_SAY, 0);
    }
  }
  g = parray[p].game;
  sprintf(tmp2, " %s %s%s: %s", 
                 parray[p].name, 
                 parray[p].srank, 
                 parray[p].rated ? "*" : " ", 
                 param[0].val.string);
  add_kib(&garray[g], movenum(garray[g].GoGame), tmp2);

  return tell(p, parray[p].opponent, param[0].val.string, TELL_SAY, 0);
}

PUBLIC int com_set(int p, param_list param)
{
  int result;
  int which;
  char *val;

  if (param[1].type == (int) TYPE_NULL)
    val = (char *) NULL;
  else
    val = param[1].val.string;
  result = var_set(p, param[0].val.word, val, &which);
  switch (result) {
  case VAR_OK:
    break;
  case VAR_BADVAL:
    pprintf(p, "%sBad value given for variable %s.", SendCode(p, ERROR), param[0].val.word);
    break;
  case VAR_NOSUCH:
    pprintf(p, "%sUnknown value for toggling.", SendCode(p, ERROR), param[0].val.word);
    break;
  case VAR_AMBIGUOUS:
    pprintf(p, "%sAmbiguous variable name %s.", SendCode(p, ERROR), param[0].val.word);
    break;
  }
  player_save(p);
  return COM_OK;
}

PUBLIC int com_stats(int p, param_list param)
{
  int p1, connected;
  int i, t;
  const Player *LadderPlayer;

  if (param[0].type == TYPE_WORD) {
    p1 = player_search(p, param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  } else {
    p1 = p;
    connected = 1;
  }
  pprintf(p, "%sPlayer:      %s\n%sGame:        go (1)\n", 
              SendCode(p, INFO), parray[p1].name, SendCode(p, INFO));
  pprintf(p, "%sRating:     %3.3s%s   %d\n%sRated Games:      %d\n%sRank:  %s   %d\n",
              SendCode(p, INFO), parray[p1].srank, 
              parray[p1].rated ? "*" : " ", parray[p1].rating,
              SendCode(p, INFO), parray[p1].numgam, 
              SendCode(p, INFO), parray[p1].ranked,
              parray[p1].orating);
  pprintf(p, "%sWins:         %d\n%sLosses:       %d\n",
	      SendCode(p, INFO), parray[p1].gowins,
	      SendCode(p, INFO), parray[p1].golose);
  if(!connected) {
    t = player_lastdisconnect(p1);
    pprintf(p, "%sLast Access(GMT):   (Not on)    %s\n",
                SendCode(p, INFO),
                t ? strgtime((time_t *) &t) : "Never connected.");
    pprintf(p, "%sLast Access(local): (Not on)    %s\n",
                SendCode(p, INFO),
                t ? strltime((time_t *) &t) : "Never connected.");
  }
  else {
    pprintf(p, "%sIdle Time:  (On server) %s\n", 
                SendCode(p, INFO),
                hms(player_idle(p1), 1, 1, 0));
    if(parray[p1].game >= 0)
      pprintf(p, "%sPlaying in game:  %d (I)\n", 
                SendCode(p, INFO),
                (parray[p1].game) + 1);
    else if(parray[p1].num_observe > 0) 
      pprintf(p, "%sObserving game:  %d\n", 
                SendCode(p, INFO),
                (parray[p1].observe_list[0]) + 1);
  }

  if (((parray[p].adminLevel > 0) || (parray[p1].registered == 0)) && 
       (connected)) {
    pprintf(p, "%sAddress: %s [%s]\n", 
          SendCode(p, INFO), 
          (parray[p1].emailAddress ? parray[p1].emailAddress : "UNREGISTERED"),
	    dotQuad(connected ? parray[p1].thisHost : parray[p1].lastHost));
  }
  else if((parray[p].adminLevel > 0) && !(connected)) {
    pprintf(p, "%sAddress: %s [Last Con From: %s]\n",
          SendCode(p, INFO),
          parray[p1].emailAddress,
          dotQuad(parray[p1].lastHost));
  }
  else pprintf(p, "%sAddress: %s \n", 
          SendCode(p, INFO), 
          (parray[p1].emailAddress ? parray[p1].emailAddress : "UNREGISTERED"));
  if(parray[p1].registered)
  pprintf(p, "%sReg date: %s\n", SendCode(p, INFO), parray[p1].RegDate);
  if((LadderPlayer = PlayerNamed(Ladder9, parray[p1].name)) != NULL) {
    if(LadderPlayer->idx == 0) {
      pprintf(p, "%sLadder9 position: Number One\n", SendCode(p, INFO));
    } else {
      pprintf(p, "%sLadder9 position: %d\n", 
                  SendCode(p, INFO), (LadderPlayer->idx) +1);
    }
  }
  if((LadderPlayer = PlayerNamed(Ladder19, parray[p1].name)) != NULL) {
    if(LadderPlayer->idx == 0) {
      pprintf(p, "%sLadder19 position: Number One\n", SendCode(p, INFO));
    } else {
      pprintf(p, "%sLadder19 position: %d\n", 
                  SendCode(p, INFO), (LadderPlayer->idx) +1);
    }
  }
  if (parray[p1].game >= 0) {
    int g = parray[p1].game;
    pprintf(p, "%s(playing game %d: %s vs. %s)\n", SendCode(p, INFO), g + 1,
      parray[garray[g].white].name, parray[garray[g].black].name );
  }

  if ((parray[p1].busy[0]) && (connected)) {
    pprintf( p, "%sBusy: [%s]\n", 
	SendCode(p, INFO), parray[p1].busy );
  }

  if (!parray[p1].registered) {
    pprintf(p, "%sUnreg: %s is NOT a registered player.", 
                SendCode(p, INFO), parray[p1].name);
  } else {
    if(parray[p1].rank) {
      pprintf(p, "%sRank Info: %s\n", SendCode(p, INFO), parray[p1].rank);
    }
    pprintf(p, "%sGames as B:  %-4d  Games as W : %-4d\n",
                   SendCode(p, INFO),
                   parray[p1].gonum_black,
                   parray[p1].gonum_white);
  }
  if ((parray[p1].adminLevel > 0) && (parray[p].adminLevel > 0)) {
    pprintf(p, "%sAdmin Level: Administrator.\n", SendCode(p, INFO));
  }

  if(parray[p1].registered)
    pprintf(p, "%sFull Name:   %s", SendCode(p, INFO), parray[p1].fullName);

  if (parray[p].adminLevel > 0) {
    pprintf(p, "\n%sMuzzled: %s  GMuzzled: %s  BMuzzled: %s  TMuzzled: %s  KMuzzled: %s", 
	    SendCode(p, INFO),
            parray[p1].muzzled ? "Yes" : "No",
            parray[p1].gmuzzled ? "Yes" : "No",  
            parray[p1].bmuzzled ? "Yes" : "No",
            parray[p1].tmuzzled ? "Yes" : "No",
            parray[p1].kmuzzled ? "Yes" : "No");  
  }

  if (parray[p1].num_plan) {
    for (i = 0; i < parray[p1].num_plan; i++)
      pprintf(p, "\n%sInfo: %s", SendCode(p, INFO), 
            (parray[p1].planLines[i] != (char *) NULL) ? parray[p1].planLines[i] : "");
  }
  if(p == p1)
  pprintf(p, "\n%sSee also: \"variables %s\"", 
                 SendCode(p, INFO), parray[p1].name);
  if (!connected)
    player_remove(p1);
  return COM_OK;
}

PUBLIC int com_variables(int p, param_list param)
{
  int p1, connected;
  int i;
  int count = 0;

  if (param[0].type == TYPE_WORD) {
    p1 = player_search(p, param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0) {		/* player had to be connected and will be
				   removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
  } else {
    p1 = p;
    connected = 1;
  }
  if(parray[p].client) pprintf(p, "8 File\n");
  pprintf(p, "Variable settings of %s:\n", parray[p1].name);
  pprintf(p, "\
go shouts (gshout)       = %-3.3s       kibitz (kibitz)              = %-3.3s\n\
shout (shout)            = %-3.3s       open (open)                  = %-3.3s\n\
bell (bell)              = %-3.3s       tell (tell)                  = %-3.3s\n\
robot (robot)            = %-3.3s       player inform (pin)          = %-3.3s\n\
looking (looking)        = %-3.3s       verbose (verbose)            = %-3.3s\n\
private (private)        = %-3.3s       ropen (ropen)                = %-3.3s\n\
automail (automail)      = %-3.3s       game inform (gin)            = %-3.3s\n\
client (client)          = %-3.3s       ladder shouts (lshout)       = %-3.3s\n\
time (time)              = %2.2d        byo-yomi stones (byo_stones) = %2d\n\
size (size)              = %2.2d        notifiedby (notified)        = %d\n\
width (width)            = %2.2d        height (height)              = %2d\n\
byo-yomi time (byo_time) = %2.2d        Problem number               = %d\n\
Water Balloons           = %3.3d       Extended Prompt (extprompt)  = %-3.3s\n\
\n",
	parray[p1].i_gshout ? "Yes" : "No", 
        parray[p1].i_kibitz ? "Yes" : "No",
	parray[p1].i_shout ? "Yes" : "No",  
        parray[p1].open ? "Yes" : "No",
	parray[p1].bell ? "Yes" : "No",     
        parray[p1].i_tell ? "Yes" : "No",
	parray[p1].i_robot ? "Yes" : "No",    
        parray[p1].i_login ? "Yes" : "No",
        parray[p1].looking ? "Yes" : "No",  
        parray[p1].i_verbose ? "Yes" : "No",
        parray[p1].Private ? "Yes" : "No",  
        parray[p1].ropen ? "Yes" : "No", 
        parray[p1].automail ? "Yes" : "No", 
        parray[p1].i_game ? "Yes" : "No", 
        parray[p1].client ? "Yes" : "No",   
        parray[p1].i_lshout ? "Yes" : "No", 
        parray[p1].def_time, 
        parray[p1].def_byo_stones,
        parray[p1].def_size, 
        parray[p1].notifiedby,
	parray[p1].d_width,  
        parray[p1].d_height,
        parray[p1].def_byo_time,
        parray[p1].last_problem,
        parray[p1].water,
        parray[p1].extprompt ? "Yes" : "No");

  pprintf(p, "Prompt: %s\n", parray[p1].prompt);
  for (i=0; i < MAX_CHANNELS; i++) {
    if (on_channel(i, p1)) {
      if (!count) pprintf( p, "Channels:");
      pprintf( p, " %d", i );
      count++;
    }
  }
  if (count) pprintf( p, "\n" );
  if(parray[p1].last_channel >= 0) {
    pprintf(p, "Last Channel: (;) %d\n", 
                parray[p1].last_channel);
  }
  if((parray[p1].last_tell >= 0) && ((p == p1) || 
    (parray[p].adminLevel > 0))) {
    pprintf(p, "Last Tell: (.) %s\n", 
                parray[parray[p1].last_tell].name);
  }
  if (alias_count(parray[p1].alias_list) && (p == p1)) {
    char *c, *a;

    pprintf(p, "Aliases:\n");
    alias_start(parray[p1].alias_list);
    while (alias_next(&c, &a, parray[p1].alias_list)) {
      pprintf(p, "      %s %s\n", c, a);
    }
  }

  if(parray[p].adminLevel > 0) {
    pprintf(p, "Socket: %d p: %d\n", parray[p1].socket, p1);
  }
  pprintf(p, "Client Type: ");
  switch(parray[p1].which_client) {
    case UNKNOWN_CLIENT:
    pprintf(p, "Unknown\n");
    break;
    
    case TELNET:
    pprintf(p, "Telnet\n");
    break;
    
    case XIGC:
    pprintf(p, "Xigc\n");
    break;
    		
    case WINIGC:
    pprintf(p, "WinIGC\n");
    break;
    		
    case WIGC:
    pprintf(p, "WIGC\n");
    break;
    		
    case CGOBAN:
    pprintf(p, "CGoban\n");
    break;
    		
    case JAVA:
    pprintf(p, "Java\n");
    break;

    case JAGOCLIENT:
    pprintf(p, "JagoClient\n");
    break;
    		
    case TGIGC:
    pprintf(p, "Tgigc\n");
    break;
    		
    case TGWIN:
    pprintf(p, "TgWin\n");
    break;
    		
    case FIGC:
    pprintf(p, "Figc\n");
    break;
    		
    case PCIGC:
    pprintf(p, "PCigc\n");
    break;
    	
    case GOSERVANT:
    pprintf(p, "GoServant\n");
    break;
    	
    case MACGO:
    pprintf(p, "MacGo\n");
    break;
    		
    case AMIGAIGC:
    pprintf(p, "AmigaIgc\n");
    break;
    	
    case HAICLIENT:
    pprintf(p, "Hai Client\n");
    break;
    	
    case IGC:
    pprintf(p, "IGC\n");
    break;
    		
    case KGO:
    pprintf(p, "Kgo\n");
    break;
    		
    case NEXTGO:
    pprintf(p, "NextGo\n");
    break;
    		
    case OS2IGC:
    pprintf(p, "OS/2igc\n");
    break;
    		
    case STIGC:
    pprintf(p, "StIgc\n");
    break;
    		
    case XGOSPEL:
    pprintf(p, "XGospel\n");
    break;

    case TKGC:
    pprintf(p, "TkGc\n");
    break;

    default:
    pprintf(p, "Unknown\n");
    break;
  } 
    
  if(parray[p].client) pprintf(p, "8 File");

  if (!connected)
    player_remove(p1);
  return COM_OK;
}

PUBLIC int com_password(int p, param_list param)
{
  char *oldpassword = param[0].val.word;
  char *newpassword = param[1].val.word;
  char salt[3];

  if (!parray[p].registered) {
    pprintf(p, "%sSetting a password is only for registered players.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (parray[p].passwd) {
    salt[0] = parray[p].passwd[0];
    salt[1] = parray[p].passwd[1];
    salt[2] = '\0';
    if (strcmp(crypt(oldpassword, salt), parray[p].passwd)) {
      pprintf(p, "%sIncorrect password, password not changed!", SendCode(p, ERROR));
      return COM_OK;
    }
    parray[p].passwd[0] = '\0'; 
  }
  salt[0] = 'a' + rand() % 26;
  salt[1] = 'a' + rand() % 26;
  salt[2] = '\0';
  do_copy(parray[p].passwd, crypt(newpassword, salt), MAX_PASSWORD);
  pprintf(p, "%sPassword changed to \"%s\".", SendCode(p, INFO), newpassword);
  player_save(p);
  return COM_OK;
}

PUBLIC int com_uptime(int p, param_list param)
{
  unsigned long uptime, now;
  const Player *LPlayer;

  now = time(0);

  uptime = time(0) - startuptime;

  pprintf(p, "%sThe current time (GMT) is:  %s\n", 
              SendCode(p, INFO), strgtime((time_t *) &now));
  pprintf(p, "%sThe current local time is:  %s\n", 
              SendCode(p, INFO), strltime((time_t *) &now));
  pprintf(p, "%sThe world has existed since %s GMT.\n", 
              SendCode(p, INFO), strltime((time_t *) &startuptime));
  pprintf(p, "%sIt is Morning GST (Geek Standard Time).\n",
              SendCode(p, INFO));
  pprintf(p, "%sUp for: %s\n", SendCode(p, INFO), strhms(uptime));
  pprintf(p, "%sPlayer limit: %d\n", SendCode(p, INFO), net_maxConn);
  pprintf(p, "%sMove limit: 2,147,483,648\n", SendCode(p, INFO));
  pprintf(p, "%sGames played since restart: %d\n", SendCode(p, INFO), 
              completed_games);
  pprintf(p, "%sLogins: %d   Logouts: %d   New Players: %d\n", 
              SendCode(p, INFO), num_logins, num_logouts, new_players);
  pprintf(p, "%sThere are currently %d players, with a high of %d since last restart.\n", SendCode(p, INFO), player_count(), player_high);
  pprintf(p, "%sThere are currently %d games, with a high of %d since last restart.\n", SendCode(p, INFO), game_count(), game_high);
  LPlayer = PlayerAt(Ladder9, 0);
  pprintf(p, "%sConnected to: %s\n", SendCode(p, INFO), server_name);
  pprintf(p, "%sBytes sent: %ld\n", SendCode(p, INFO), byte_count);
  pprintf(p, "%sThere are currently %d players in the  9x9  ladder.  %s #1!\n", 
              SendCode(p, INFO), num_9, LPlayer->szName);
  LPlayer = PlayerAt(Ladder19, 0);
  pprintf(p, "%sThere are currently %d players in the 19x19 ladder.  %s #1!\n", 
              SendCode(p, INFO), num_19, LPlayer->szName);
  pprintf(p, "%sSee http://nngs.cosmic.org/", 
              SendCode(p, INFO));
  return COM_OK;
}

PUBLIC int com_date(int p, param_list param)
{
  int t = time(0);
  pprintf(p, "%sLocal time     - %s\n", SendCode(p, INFO), strltime((time_t *) &t));
  pprintf(p, "%sGreenwich time - %s\n", SendCode(p, INFO), strgtime((time_t *) &t));
  return COM_OK;
}

char *inout_string[] = {
  "login", "logout"
};

PUBLIC int plogins(int p, char * fname)
{
  FILE *fp;
  int inout, thetime, registered;
  char loginName[MAX_LOGIN_NAME + 1];
  char ipstr[50];

  fp = fopen(fname, "r");
  if (!fp) {
    pprintf(p, "%sSorry, no login information available.",
                     SendCode(p, ERROR));
    return COM_OK;
  }
  while (!feof(fp)) {
    if (fscanf(fp, "%d %s %d %d %s\n", &inout, loginName, &thetime, 
                &registered, ipstr) != 5) {
      Logit("Error in login info format. %s", fname);
      fclose(fp);
      return COM_OK;
    }
    pprintf(p, "%s%s: %-10s %-6s", 
               SendCode(p, INFO), strltime((time_t *) &thetime), 
               loginName, inout_string[inout]);
    if (parray[p].adminLevel > 0) {
      pprintf( p, " from %s\n", ipstr );
    } else  pprintf( p, "\n" );
  }
  fclose(fp);
  return COM_OKN;
}

PUBLIC int com_llogons(int p, param_list param)
{
  char fname[MAX_FILENAME_SIZE];

  sprintf(fname, "%s/%s", stats_dir, STATS_LOGONS);
  return plogins(p, fname);
}

PUBLIC int com_logons(int p, param_list param)
{
  char fname[MAX_FILENAME_SIZE];

  if (param[0].type == TYPE_WORD) {
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, param[0].val.word[0], param[0].val.word, STATS_LOGONS);
  } else {
    sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_LOGONS);
  }
  return plogins(p, fname);
}

#define WHO_OPEN 0x01
#define WHO_CLOSED 0x02
#define WHO_RATED 0x04
#define WHO_UNRATED 0x08
#define WHO_FREE 0x10
#define WHO_LOOKING 0x20
#define WHO_REGISTERED 0x40
#define WHO_UNREGISTERED 0x80

PRIVATE int who_ok(int p, unsigned int sel_bits, int from, int to)
{
  if (parray[p].status != PLAYER_PROMPT) return 0;
    
  if((from < 0) && (sel_bits == 0xff)) {
    return(1);
  }

  if((sel_bits == 0xff) && (from > -1)) {
    if((parray[p].orating >= from) &&
       (parray[p].orating <= to)) 
      return 1;
  }

  if (from > -1) {
    if (parray[p].orating < from || parray[p].orating > to) {
      return 0;
    }  
    if (sel_bits & WHO_OPEN) if ((!parray[p].open) || (parray[p].game >= 0))
        return 0;
    if (sel_bits & WHO_CLOSED) if (parray[p].open) return 0;
    if (sel_bits & WHO_RATED) if(!parray[p].rated) return 0;
    if (sel_bits & WHO_UNRATED) if (parray[p].rated) return 0;
    if (sel_bits & WHO_FREE) if (parray[p].game >= 0) return 0;
    if (sel_bits & WHO_LOOKING) if (parray[p].looking == 0) return 0;
    if (sel_bits & WHO_REGISTERED) if (!parray[p].registered) return 0;
    if (sel_bits & WHO_UNREGISTERED) if (parray[p].registered) return 0;
  }
  else {
    if (sel_bits & WHO_OPEN) if ((!parray[p].open) || (parray[p].game >= 0))
        return 0;
    if (sel_bits & WHO_CLOSED) if (parray[p].open) return 0;
    if (sel_bits & WHO_RATED) if(!parray[p].rated) return 0;
    if (sel_bits & WHO_UNRATED) if (parray[p].rated) return 0;
    if (sel_bits & WHO_FREE) if (parray[p].game >= 0) return 0;
    if (sel_bits & WHO_LOOKING) if (parray[p].looking == 0) return 0;
    if (sel_bits & WHO_REGISTERED) if (!parray[p].registered) return 0;
    if (sel_bits & WHO_UNREGISTERED) if (parray[p].registered) return 0;
  }      
  return 1;
}

PUBLIC int com_awho(int p, param_list param)
{
  int style = 0;
  int *sortarray = sort_ladder19;
  unsigned int sel_bits = 0xff;

  int plist[256];
  int i, len;
  
  char c;
  int p1, count, num_who;
  

  if (param[0].type == TYPE_WORD) {
    len = strlen(param[0].val.word);
    for (i = 0; i < len; i++) {
      c = param[0].val.word[i];
      switch (c) {
        case 'o':
          if (sel_bits == 0xff)
            sel_bits = WHO_OPEN;
          else
            sel_bits |= WHO_OPEN;
          break;
        case 'r':
        case '*':
          if (sel_bits == 0xff)
            sel_bits = WHO_RATED;
          else
            sel_bits |= WHO_RATED;
          break;
        case 'l':
          if (sel_bits == 0xff)
            sel_bits = WHO_LOOKING;
          else
            sel_bits |= WHO_LOOKING;
          break;
        case 'f':
          if (sel_bits == 0xff)
            sel_bits = WHO_FREE;
          else
            sel_bits |= WHO_FREE;
          break;
        case 'R':
          if (sel_bits == 0xff)
            sel_bits = WHO_REGISTERED;
          else
            sel_bits |= WHO_REGISTERED;
          break;
        case 'A':               /* Sort order */
          sortarray = sort_alpha;
          break;
        case '9':               /* Sort order */
          sortarray = sort_ladder9;
          break;
        case 't':               /* format */
          style = 0;
          break;
        case 'a':               /* format */
          style = 1;
          break;
        case 'U':
          if (sel_bits == 0xff)
            sel_bits = WHO_UNREGISTERED;
          else
            sel_bits |= WHO_UNREGISTERED;
          break;
        default:
          return COM_BADPARAMETERS;
          break;
        }
    }
  }
  num_who = count = 0;
  for (p1 = 0; p1 < p_num; p1++) {
    if (!who_ok(sortarray[p1], sel_bits, -1, -1))
      continue;
    plist[num_who++] = sortarray[p1];
    count++;
  }
/*  num_who = 0;
  count = 0;
  for (p1 = 0; p1 < p_num; p1++) {
    if (!who_ok(sortarray[p1], sel_bits, -1, -1))
      continue;
    plist[num_who++] = sortarray[p1];
    count++;
  } */
  if (num_who == 0) {
    pprintf(p, "%sThere are no players logged in that match your flag set.", 
             SendCode(p, ERROR));
    return COM_OK;
  }
  switch (style) {
  case 0:                       /* terse */
  case 1:
    player_resort();
    a_who(p, num_who, plist);
    break;
  default:
    return COM_BADPARAMETERS;
    break;
  }
  return COM_OKN;
}

PUBLIC int a_who(int p, int num, int *plist)
{
  char ptmp[180];	
  int i, i2, p1, pos9, pos19;
  char obtemp[3], gametemp[3];
  const Player *LadderPlayer9;
  const Player *LadderPlayer19;
  char tmp[80];
  
  pprintf(p, "%s Info     Name       Rank  19  9  Idle Rank Info\n", SendCode(p, INFO));
  pprintf(p, "%s -------- ---------- ---- --- --- ---- ---------------------------------\n",SendCode(p, INFO));
  for(i2 = 0; i2 < num; i2++) {
    p1 = plist[i2];
    if (parray[p1].status != PLAYER_PROMPT) continue;
    LadderPlayer19 = PlayerNamed(Ladder19, parray[p1].name);
    LadderPlayer9 = PlayerNamed(Ladder9, parray[p1].name);
    if(LadderPlayer9 != NULL) pos9 = LadderPlayer9->idx + 1;
    else pos9 = 0;
    if(LadderPlayer19 != NULL) pos19 = LadderPlayer19->idx + 1;
    else pos19 = 0;
    sprintf(ptmp, "%s%s",
                   parray[p1].i_shout ? " " : "S",
                   (parray[p1].i_login && parray[p1].i_game) ? " " : "Q");
    if((parray[p1].game >= 0) && 
       (garray[parray[p1].game].Ladder19 || garray[parray[p1].game].Ladder9)) {
      strcat(ptmp, "*");
#ifdef PAIR
    } else if ((parray[p1].game >= 0) &&
       (paired(parray[p1].game))) {
      strcat(ptmp, "@");
#endif
    } else if ((!parray[p1].open) && (parray[p1].game < 0)) {
      strcat(ptmp, "X");
    } else if (parray[p1].looking && (parray[p1].game < 0)) {
      strcat(ptmp, "!");
    } else {
      strcat(ptmp, " ");
    }
    if(parray[p1].num_observe > -1)
      sprintf(obtemp, "%2d", parray[p1].observe_list[0] + 1);
    if(parray[p1].game > -1)
      sprintf(gametemp, "%2d", parray[p1].game + 1);
    if(parray[p1].rank) {
      i = strlen(parray[p1].rank);
      if(i > 0) {
        strcpy(tmp, parray[p1].rank);
        tmp[i] = '\0';
      }
    }
    else strcpy(tmp, "None");
    sprintf(ptmp, "%s %s %s %-10s %3.3s%s %3d %3d %3s  %-38.38s",
              ptmp,
              parray[p1].num_observe ? obtemp : "--",
              (parray[p1].game >= 0) ? gametemp : "--",
              parray[p1].name,
              parray[p1].srank,
              parray[p1].rated ? "*" : parray[p1].rating > 1 ? "?" : " ",
              pos19 == 0 ? 0 : pos19,
              pos9 == 0 ? 0 : pos9,
              newhms(player_idle(p1)),
              tmp);
    pprintf(p, "%s%s\n", SendCode(p, INFO), ptmp);
  }
  return COM_OK;
}

PRIVATE void who_terse(int p, int num, int *plist, int type)
{
  char ptmp[180], ptmp2[180];	
  int i, p1, left;
  char obtemp[3], gametemp[3];

  left = 1;

  pprintf(p, "%s Info       Name       Idle   Rank |  Info       Name       Idle   Rank\n",SendCode(p, WHO));
  for (i = 0; i < num; i++) {
    p1 = plist[i];
/*    if((parray[p1].invisable) && (parray[p].adminLevel < 1)) continue; */
    sprintf(ptmp, " %s",
              (parray[p1].i_shout) ? ((parray[p1].i_login) ? " " : "Q") : "S");
    if (!parray[p1].open) {
      strcat(ptmp, "X");
    } else if (parray[p1].looking && (parray[p1].game < 0)) {
      strcat(ptmp, "!");
    } else {
      strcat(ptmp, " "); 
    }

    if(parray[p1].num_observe > -1) 
      sprintf(obtemp, "%2d", parray[p1].observe_list[0] + 1);
    if(parray[p1].game > -1)
      sprintf(gametemp, "%2d", parray[p1].game + 1);

    sprintf(ptmp, "%s %s   %s %-10s %3s    %3.3s%s %s", 
              ptmp,
	      parray[p1].num_observe ? obtemp : "--",
	      (parray[p1].game >= 0) ? gametemp : "--",
              parray[p1].name,
	      newhms(player_idle(p1)),
              parray[p1].srank,
              parray[p1].rated ? "*" : " ",
	      left ? "|" : " ");
    if((left) && (i + 1 == num)) {  /* Occurs when we have a L, but no R */
      pprintf(p, "%s%s                                   \n", 
                SendCode(p, WHO), ptmp);
    }
    else if(left) {
      strcpy(ptmp2, ptmp);
      left = 0;
    }
    else {
      left = 1;
      pprintf(p, "%s%s %s\n", SendCode(p, WHO), ptmp2, ptmp);
    }

  }
  pprintf(p, "%s                ******** %3d Players %d Total Games ********", SendCode(p, WHO), player_count(), game_count());
}

/* This is the of the most compliclicated commands in terms of parameters */
PUBLIC int com_who(int p, param_list param)
{
  int style = 0;
  int *sortarray = sort_alpha;
/*  float stop_perc = 1.0;
  float start_perc = 0;
*/
  unsigned int sel_bits = 0xff;

  unsigned int to, from;
  char kord1, kord2;
  char options[MAX_STRING_LENGTH];
  
  int plist[256];
  int tmpint;
  
  char *s;
  int p1, count, num_who;
#ifdef SGI
  int sort_type = (const int) 0;
#else
  int sort_type =  0;
#endif

  int all = 0;

  from = to = -1;  /* initialize from and to -1 (show all) */
  
  if (param[0].type != TYPE_STRING) {
    all = 1;  /* Assume that they want everyone */
    style = 1;
    sortarray = sort_alpha;
  }
  else {
    strcpy(options, param[0].val.string);
    s = strtok(options, " ");
    while(s != NULL) {
      if (s == NULL) break;
      if ((sscanf(s, "%u%c-%u%c", &from, &kord1, &to, &kord2) != 4) &&
           (sscanf(s, "%u%c",      &from, &kord1) != 2)) {
         /* It's not a rating or rating interval. */
        if (!strcasecmp(s, "ALL")) {
          all = 1;
          sortarray = sort_alpha;
        }
        else switch(s[0]) {
          case 'k': case 'K':
            from = 0; to = 3000;
          break;
          case 'd': case 'D':
            from = 3100; to = 3900;
          break;
          case 'p': case 'P':
            from = 4000; to = 5000;
          break;
	  case 'o':
	    if (sel_bits == 0xff) sel_bits = WHO_OPEN;
            else sel_bits |= WHO_OPEN;
            all = 0;
          break;
          case 'r':
	    if (sel_bits == 0xff) sel_bits = WHO_RATED;
	    else sel_bits |= WHO_RATED;
	    all = 0;
	  break;
	  case 'l':
	    if (sel_bits == 0xff) sel_bits = WHO_LOOKING;
	    else sel_bits |= WHO_LOOKING;
	    all = 0;
	  break;
	  case 'f':
	    if (sel_bits == 0xff) sel_bits = WHO_FREE;
	    else sel_bits |= WHO_FREE;
	    all = 0;
	  break;
	  case 'R':
	    if (sel_bits == 0xff) sel_bits = WHO_REGISTERED;
	    else sel_bits |= WHO_REGISTERED;
	    all = 0;
	  break;
	  case 'A': case 'a':		/* ALL */
	    style = all = 1;
	    sortarray = sort_alpha;
	  break;
	  case 'U':
	    if (sel_bits == 0xff) sel_bits = WHO_UNREGISTERED;
	    else sel_bits |= WHO_UNREGISTERED;
	    all = 0;
          break;
	  default:
	    return COM_BADPARAMETERS;
          break;
	}
      }
      else {
	if (to == -1) {
          to = from;          /* Not interval, make to == from. */
          kord2 = kord1;
	}
	switch (kord1) {
          case 'k': case 'K':
            from = 31 - from;
            break;
          case 'd': case 'D':
            from += 30;
            break;
          case 'p': case 'P':
            from += 40;
            break;
          default:
            /* Bad rating. */
            ;
	}
	switch (kord2) {
          case 'k': case 'K':
            to = 31 - to;
            break;
          case 'd': case 'D':
            to += 30;
            break;
          case 'p': case 'P':
            to += 40;
            break;
          default:
            /* Bad rating. */
            ;
	} 
	if (to < from) { /* wrong order, swap them */
          tmpint = from;
          from = to;
          to = tmpint;
	}
      }
      s = strtok(NULL, " ");
    }
  }
/* Original from 9/24/97.  "who 2k-5k" gives 3k - 6k 
  from = (from * 100) - 100;
  to = (to * 100) - 100;
*/
/* I think I messed this up, let's see if this fixes it. -- EVR 9/24/97 */
  from = (from * 100);
  to = (to * 100);

  if(from == to) to += 99;
  num_who = count = 0;
  for (p1 = 0; p1 < p_num; p1++) {
    if (!who_ok(sortarray[p1], sel_bits, from, to))
      continue;
    plist[num_who++] = sortarray[p1];
    count++;
  }

/*  startpoint = floor((float) count * start_perc);
  stoppoint = ceil((float) count * stop_perc) - 1;
  num_who = 0;
  count = 0;
  for (p1 = 0; p1 < p_num; p1++) {
    if (!who_ok(sortarray[p1], sel_bits, (from * 100) - 100, (to * 100) - 100))
      continue; 
    if ((count >= startpoint) && (count <= stoppoint)) {
      plist[num_who++] = sortarray[p1];
    }
    count++;
  }
*/
  if (num_who == 0) {
    pprintf(p, "%sThere are no players logged in that match your flag set.\n", SendCode(p, ERROR));
    return COM_OK;
  }
  switch (style) {
  case 0:			/* terse */
    who_terse(p, num_who, plist, sort_type);
    break;
  case 1:
    who_terse(p, num_who, plist, sort_type);
    break;
  default:
    return COM_BADPARAMETERS;
    break;
  }
  return COM_OK;
}

PUBLIC int com_censor(int p, param_list param)
{
  int i;
  int p1;

  if (param[0].type != TYPE_WORD) {
    if (!parray[p].num_censor) {
      pprintf(p, "%sYou have no one censored.", SendCode(p, ERROR));
      return COM_OK;
    } else
      pprintf(p, "%sYou have censored:", SendCode(p, INFO));
    for (i = 0; i < parray[p].num_censor; i++) {
      pprintf(p, " %s", parray[p].censorList[i]);
    }
    pprintf(p, ".\n");
    return COM_OKN;
  }
  if (parray[p].num_censor >= MAX_CENSOR) {
    pprintf(p, "%sYou are already censoring the maximum number of players.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  if(check_censored(p, param[0].val.word)) {
    pprintf(p, "%sYou are already censoring %s.",
                SendCode(p, ERROR), param[0].val.word);
    return COM_OK;
  }
  if ((p1 = player_find_part_login( param[0].val.word )) >= 0) {
    if (p1 == p) {
      pprintf(p, "%sYou can't censor yourself.", SendCode(p, ERROR));
      return COM_OK;
    }
  }
  parray[p].censorList[parray[p].num_censor++] = (char *) strdup(param[0].val.word);
  pprintf(p, "%s%s censored.\n", SendCode(p, INFO), param[0].val.word);
  return COM_OK;
}

PUBLIC int com_uncensor(int p, param_list param)
{
  char *pname = (char *) NULL;
  int i;
  int unc = 0;

  if (param[0].type == TYPE_WORD) {
    pname = param[0].val.word;
  }
  for (i = 0; i < parray[p].num_censor; i++) {
    if (!pname || !strcasecmp(pname, parray[p].censorList[i])) {
      pprintf(p, "%s%s uncensored.",SendCode(p, INFO), parray[p].censorList[i]);
      free(parray[p].censorList[i]);
      parray[p].censorList[i] = (char *) NULL;
      unc++;
    }
  }
  if (unc) {
    for (i = 0; i < parray[p].num_censor; i++) {
      if (!parray[p].censorList[i]) {
	parray[p].censorList[i] = parray[p].censorList[i + 1];
	i = i - 1;
	parray[p].num_censor = parray[p].num_censor - 1;
      }
    }
  } else {
    pprintf(p, "%sNo one was uncensored.", SendCode(p, ERROR));
  }
  return COM_OK;
}

PUBLIC int com_channel(int p, param_list param)
{
  int i, j, err;

  if (param[0].type == (int) TYPE_NULL) {	/* Turn off all channels */
    pcommand(p, "inchannel");
  } else {
    i = param[0].val.integer;
    if ((i == 0) && (parray[p].adminLevel == 0)) {
      pprintf(p, "%sInvalid channel number.", SendCode(p, ERROR));
      return COM_OK;
    }
    if (i < 0) {
      pprintf(p, "%sThe lowest channel number is 1.", SendCode(p, ERROR));
      return COM_OK;
    }
    if (i >= MAX_CHANNELS) {
      pprintf(p, "%sThe maximum channel number is %d.", SendCode(p, ERROR), MAX_CHANNELS - 1);
      return COM_OK;
    }
    if (on_channel(i, p)) {
      if (!channel_remove(i, p))
	pprintf(p, "%sChannel %d turned off.\n", SendCode(p, INFO), i);
      for (j = 0; j < numOn[i]; j++) {
	if(channels[i][j] == p) continue;
	pprintf_prompt(channels[i][j], "%s%s has left channel %d.\n",
		       SendCode(channels[i][j], INFO), parray[p].name, i);
      }
      /* [PEM]: Only zap last_channel if it's the one we just left. */
      if (parray[p].last_channel == i)
	parray[p].last_channel = -1;
    } else {
      if (!(err=channel_add(i,p))) {
	pprintf(p, "%sChannel %d turned on.\n", SendCode(p, INFO), i);
        pprintf(p, "%sChannel %d Title:\n%s%s\n", SendCode(p, INFO), i,
                    SendCode(p, INFO), carray[i].Title);
        parray[p].last_channel = i;
        pprintf(p, "%sSetting your ; to channel %d\n", SendCode(p, INFO), i);
        for (j = 0; j < numOn[i]; j++) {
          if(channels[i][j] == p) continue;
          pprintf_prompt(channels[i][j], "%s%s has joined channel %d.\n",
                     SendCode(channels[i][j], INFO), parray[p].name, i);
        }
      } else {
        if (err == 1)
	pprintf(p, "%sChannel %d is already full.\n", SendCode(p, ERROR), i);
        if (err == 2)
        pprintf(p, "%sMaximum channel number exceeded.\n",SendCode(p, ERROR));
        if (err == 3)
        pprintf(p, "%sInvalid Channel.\n",SendCode(p, ERROR));
        if (err == 4)
        pprintf(p, "%sSorry, that channel is locked.\n",SendCode(p, ERROR));
        if (err == 5)
        pprintf(p, "%sSorry, that channel is closed.\n",SendCode(p, ERROR));
      }
    }
  }
  return COM_OKN;
}

PUBLIC int com_unlock(int p, param_list param)
{
  int i, j;

  if((param[0].val.integer > MAX_CHANNELS - 1) || (param[0].val.integer < 0)) {
    return COM_OK;
  }
   
  i = param[0].val.integer;

  if((on_channel(i, p)) || (parray[p].adminLevel > 0)) {
    for (j = 0; j < numOn[i]; j++) {
      pprintf_prompt(channels[i][j], "%s%s has unlocked channel %d\n",
      SendCode(channels[i][j], INFO), parray[p].name, i);
    }
    carray[i].locked = 0;
  }
  return COM_OK;
}

PUBLIC int com_lock(int p, param_list param)
{
  int i, j;

  if((param[0].val.integer > MAX_CHANNELS - 1) || (param[0].val.integer < 0)) {
    return COM_OK;
  }
   
  i = param[0].val.integer;

  if((on_channel(i, p)) || (parray[p].adminLevel > 0)) {
    for (j = 0; j < numOn[i]; j++) {
      pprintf_prompt(channels[i][j], "%s%s has locked channel %d\n",
      SendCode(channels[i][j], INFO), parray[p].name, i);
    }
    carray[i].locked = 1;
  }
  return COM_OK;
}

PUBLIC int com_dnd(int p, param_list param)
{
  int i, j;

  if((param[0].val.integer > MAX_CHANNELS - 1) || (param[0].val.integer < 0)) {
    return COM_OK;
  }

  i = param[0].val.integer;

  if((on_channel(i, p)) || (parray[p].adminLevel > 0)) {
    if(carray[i].dNd == 0) {
      for (j = 0; j < numOn[i]; j++) {
        pprintf_prompt(channels[i][j], "%s%s has do not disturbed channel %d\n",
        SendCode(channels[i][j], INFO), parray[p].name, i);
      }
      carray[i].dNd = 1;
    }
    else if(carray[i].dNd == 1) {
      for (j = 0; j < numOn[i]; j++) {
        pprintf_prompt(channels[i][j], "%s%s has removed the do not disturb on channel %d\n",
        SendCode(channels[i][j], INFO), parray[p].name, i);
      }
      carray[i].dNd = 0;
    }
  }
  return COM_OK;
}

PUBLIC int com_ctitle(int p, param_list param)
{
  int i, j;
  char szBuf[1024];

  if((param[0].val.integer > MAX_CHANNELS - 1) ||
     (param[0].val.integer < 0)) {
    return COM_OK;
  }
   
  i = param[0].val.integer;

  if((int) strlen(param[1].val.string) > 200) return COM_OK;

  sprintf(szBuf, "[%s] %s", parray[p].name, param[1].val.string);
  if(on_channel(i, p)) {
    free(carray[i].Title);
    carray[i].Title = (char *) strdup(szBuf);
    for (j = 0; j < numOn[i]; j++) {
      pprintf_prompt(channels[i][j], 
                     "%s%s has changed the title of channel %d to:\n%s%s\n",
      SendCode(channels[i][j], INFO), parray[p].name, i,
      SendCode(channels[i][j], INFO), carray[i].Title);
    }
  }
  return COM_OK;
}


PUBLIC int com_inchannel(int p, param_list param)
{
  int c1, c2;
  int i, j, count = 0;

  if (param[0].type == (int) TYPE_NULL) {  /* List everyone on every channel */
    c1 = -1;
    c2 = -1;
  } else if (param[1].type == (int) TYPE_NULL) {	/* One parameter */
    c1 = param[0].val.integer;
    if (c1 < 0) {
      pprintf(p, "%sThe lowest channel number is 1.", SendCode(p, ERROR));
      return COM_OK;
    }
    c2 = -1;
  } else {			/* Two parameters */
    c1 = param[0].val.integer;
    c2 = param[2].val.integer;
    if ((c1 < 0) || (c2 < 0)) {
      pprintf(p, "%sThe lowest channel number is 1.",SendCode(p, ERROR));
      return COM_OK;
    }
    pprintf(p, "%sTwo parameter inchannel is not implemented.", 
                 SendCode(p, ERROR));
    return COM_OK;
  }
  if ((c1 >= MAX_CHANNELS) || (c2 >= MAX_CHANNELS)) {
    pprintf(p, "%sThe maximum channel number is %d.", 
                SendCode(p, ERROR), MAX_CHANNELS - 1);
    return COM_OK;
  }
  for (i = 0; i < MAX_CHANNELS; i++) {
    if((carray[i].hidden == 1) && (parray[p].adminLevel == 0)) continue;
    if (numOn[i] && ((c1 < 0) || (i == c1))) {
      pprintf(p, "%sChannel %d: (%s%s%s) %s\n%s", SendCode(p, INFO), i, 
                  carray[i].locked ? "L" : "-", 
                  carray[i].hidden ? "H" : "", 
                  carray[i].dNd ? "D" : "", 
                  carray[i].Title, 
                  SendCode(p, INFO));
      for (j = 0; j < numOn[i]; j++) {
	pprintf(p, " %s", parray[channels[i][j]].name);
      }
      count++;
      pprintf(p, "\n");
    }
  }
  if (!count) {
    if (c1 < 0)
      pprintf(p, "%sNo channels in use.\n", SendCode(p, ERROR));
    else
      pprintf(p, "%sChannel not in use.\n", SendCode(p, ERROR));
  }
  return COM_OKN;
}

/* For GO matches */

PUBLIC int com_tmatch(int p, param_list param)
{
  int p1;
  int pendfrom, pendto;
  int ppend, p1pend;
  
  int start_time = -1;		/* start time */
  int byo_time = -1;		/* byo time */
  int size = -1;                /* size of board */
  int challenger = -1;    /* color of challenger */
  int challenged = -1;    /* color of challenged */
  int wp, bp;
  char parsebuf[100];
  char *val;
  
 
  /* Things we need:

     player_to_challenge challenger_color size_of_board time byo-yomi_time
     
     looks like:

     match daveg b 19 30 10
  */
 
  if (parray[p].game >= 0) {
    pprintf(p, "%sYou can't challenge while you are playing a game.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (parray[p].open == 0) {
    parray[p].open = 1;
    pprintf(p, "%sSetting you open for matches.\n", SendCode(p, INFO));
  }
  stolower(param[0].val.word);
  stolower(param[1].val.word);
  
  p1 = player_find_part_login(param[0].val.word);
  if (p1 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.\n", 
                SendCode(p, ERROR),
                param[0].val.word);
    return COM_OK;
  }
  if (p1 == p) {
    pprintf(p, "%sUse \"Teach\" for Teaching games.\n", 
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_censored(p1, p)) {
    pprintf(p, "%sPlayer \"%s\" is censoring you.\n", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (player_censored(p, p1)) {
    pprintf(p, "%sYou are censoring \"%s\".\n", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (!parray[p1].open) {
    pprintf(p, "%sPlayer \"%s\" is not open to match requests.\n", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (parray[p1].game >= 0) {
    pprintf(p, "%sPlayer \"%s\" is involved in another game.\n", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  challenger = challenged = -1;
  pendto = player_find_pendto(p, p1, PEND_TMATCH);
  pendfrom = player_find_pendfrom(p, p1, PEND_TMATCH);
  /* parse the match string */
  val = param[1].val.string;
  while (sscanf(val, " %99s", parsebuf) == 1) {
    val = eatwhite(val);
    val = eatword(val);
    if (challenger == -1) {
      /* [PEM]: Changed the if's to a switch and added the nigiri. */
      switch (parsebuf[0]) {
      case 'b':
	challenger = BLACK;
	break;
      case 'w':
	challenger = WHITE;
	break;
      case '?':			/* Nigiri! */
	challenger = (rand()&1 ? BLACK : WHITE);
	break;
      default:
	return COM_BADPARAMETERS;
      }
      if(challenger == WHITE) 
        challenged = BLACK;
      else challenged = WHITE;
    }
  }
  if ((size = param[2].val.integer) < 2)
  {
    pprintf(p, "%sBoardsize must be >= 2\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((start_time = param[3].val.integer) < 0)
  {
    pprintf(p, "%sStart time must be >= 0\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((byo_time = param[4].val.integer) < 0)
  {
    pprintf(p, "%sByo-yomi time must be >= 0\n", SendCode(p, ERROR));
    return COM_OK;
  }

  /* Ok match offer will be made */

  if (pendto >= 0) {
    pprintf(p, "%sUpdating offer already made to \"%s\".\n", 
                SendCode(p, INFO), parray[p1].name);
  }
  if (pendfrom >= 0) {
    if (pendto >= 0) {
      pprintf(p, "%sInternal error Report circumstances and bug PENDFROMTO\n", 
                  SendCode(p, ERROR));
      Logit("This shouldn't happen. You can't have a match pending from and to the same person.");
      return COM_OK;
    }
    if ((size == parray[p].p_from_list[pendfrom].param2) &&
	(start_time == parray[p].p_from_list[pendfrom].param3) &&
	(byo_time == parray[p].p_from_list[pendfrom].param4)) {
      /* Identical match, should accept! */
      challenged = parray[p].p_from_list[pendfrom].param1;
      if (challenged == BLACK) { 
        challenger = WHITE;
        bp = p; 
      } else {
        bp = p1;
        challenger = BLACK;
      }
      if (bp == p) wp = p1;
      else (wp = p);
      if(size == 0) return COM_BADPARAMETERS;
      if ((create_new_gomatch(wp, bp, challenger, 
           size, start_time, byo_time, 0, RULES_NET, TYPE_TNETGO)) == COM_FAILED) {
	pprintf(p, "%sThere was a problem creating the new match.",
                     SendCode(p, ERROR));
	pprintf_prompt(p1, "%sThere was a problem creating the new match.\n",
                     SendCode(p, ERROR));
      }
      parray[p].match_type = TYPE_TNETGO;
      parray[p1].match_type = TYPE_TNETGO;
      printf ("Set to TYPE_TNETGO\n");
      return COM_OK;
    } else {
      player_remove_pendfrom(p, p1, PEND_TMATCH);
      player_remove_pendto(p1, p, PEND_TMATCH);
    }
  }
  if (pendto < 0) {
    ppend = player_new_pendto(p);
    if (ppend < 0) {
      pprintf(p, "%sSorry, you can't have any more pending matches.",
                  SendCode(p, ERROR));
      return COM_OK;
    }
    p1pend = player_new_pendfrom(p1);
    if (p1pend < 0) {
      pprintf(p, "%sSorry, %s can't have any more pending matches.", 
                  SendCode(p, ERROR),
                  parray[p1].name);
      parray[p].num_to = parray[p].num_to - 1;
      return COM_OK;
    }
  } else {
    ppend = pendto;
    p1pend = player_find_pendfrom(p1, p, PEND_TMATCH);
  }
  parray[p].p_to_list[ppend].param1 = challenger;
  parray[p].p_to_list[ppend].param2 = size;
  parray[p].p_to_list[ppend].param3 = start_time;
  parray[p].p_to_list[ppend].param4 = byo_time;
  parray[p].p_to_list[ppend].type = PEND_TMATCH;
  parray[p].p_to_list[ppend].whoto = p1;
  parray[p].p_to_list[ppend].whofrom = p;

  parray[p1].p_from_list[p1pend].param1 = challenged;
  parray[p1].p_from_list[p1pend].param2 = size;
  parray[p1].p_from_list[p1pend].param3 = start_time;
  parray[p1].p_from_list[p1pend].param4 = byo_time;
  parray[p1].p_from_list[p1pend].type = PEND_TMATCH;
  parray[p1].p_from_list[p1pend].whoto = p1;
  parray[p1].p_from_list[p1pend].whofrom = p;

  if (pendfrom >= 0) {
    pprintf(p, "%sDeclining offer from %s and offering new match parameters.\n",SendCode(p, INFO),  parray[p1].name);
    pprintf_prompt(p1, "%s%s declines your request for a match.\n", SendCode(p1, INFO), parray[p].name);
  }
  if (pendto >= 0) {
    pprintf(p, "%sUpdating match request", SendCode(p, INFO));
    pprintf(p1, "%s%s updates the match request.\n", SendCode(p1, INFO), parray[p].name);
  } else {
    pprintf(p, "%sRequesting match in %d min with %s as %s.\n", 
                SendCode(p, INFO),    
		start_time,
                parray[p1].name,
                challenger == BLACK ? "White" : "Black");
  }
    pprintf(p1, "%sMatch [%dx%d] in %d minutes requested with %s as %s.\n",
                 SendCode(p1, INFO),
                 size, size,
                 start_time,
                 parray[p].name,
                 challenger == BLACK ? "Black" : "White");
    pprintf_prompt(p1, "%sUse <tmatch %s %s %d %d %d> or <decline %s> to respond.\n",
                 SendCode(p1, INFO),
                 parray[p].name,
                 challenged == BLACK ? "B" : "W",
                 size,
                 start_time,
                 byo_time,
                 parray[p].name);
  return COM_OK;
}

/* For GO matches */

PUBLIC int com_gmatch(int p, param_list param)
{
  int p1;
  int pendfrom, pendto;
  int ppend, p1pend;
  
  int start_time = -1;		/* start time */
  int byo_time = -1;		/* byo time */
  int size = -1;                /* size of board */
  int challenger = -1;    /* color of challenger */
  int challenged = -1;    /* color of challenged */
  int wp, bp;
  char parsebuf[100];
  char *val;
  
 
  /* Things we need:

     player_to_challenge challenger_color size_of_board time byo-yomi_time
     
     looks like:

     match daveg b 19 30 10
  */
 
  if (parray[p].game >= 0) {
    pprintf(p, "%sYou can't challenge while you are playing a game.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (parray[p].open == 0) {
    parray[p].open = 1;
    pprintf(p, "%sSetting you open for matches.\n", SendCode(p, INFO));
  }
  stolower(param[0].val.word);
  stolower(param[1].val.word);
  
  p1 = player_find_part_login(param[0].val.word);
  if (p1 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.", 
                SendCode(p, ERROR),
                param[0].val.word);
    return COM_OK;
  }
  if (p1 == p) {
    pprintf(p, "%sUse \"Teach\" for Teaching games.", 
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_censored(p1, p)) {
    pprintf(p, "%sPlayer \"%s\" is censoring you.", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (player_censored(p, p1)) {
    pprintf(p, "%sYou are censoring \"%s\".", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (!parray[p1].open) {
    pprintf(p, "%sPlayer \"%s\" is not open to match requests.", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (parray[p1].game >= 0) {
    pprintf(p, "%sPlayer \"%s\" is involved in another game.", 
                SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  challenger = challenged = -1;
  pendto = player_find_pendto(p, p1, PEND_GMATCH);
  pendfrom = player_find_pendfrom(p, p1, PEND_GMATCH);
  /* parse the match string */
  val = param[1].val.string;
  while (sscanf(val, " %99s", parsebuf) == 1) {
    val = eatwhite(val);
    val = eatword(val);
    if (challenger == -1) {
      /* [PEM]: Changed the if's to a switch and added the nigiri. */
      switch (parsebuf[0]) {
      case 'b':
	challenger = BLACK;
	break;
      case 'w':
	challenger = WHITE;
	break;
      case '?':			/* Nigiri! */
	challenger = (rand()&1 ? BLACK : WHITE);
	break;
      default:
	return COM_BADPARAMETERS;
      }
      if(challenger == WHITE) 
        challenged = BLACK;
      else challenged = WHITE;
    }
  }
  if ((size = param[2].val.integer) < 2)
  {
    pprintf(p, "%sBoardsize must be >= 2\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((start_time = param[3].val.integer) < 0)
  {
    pprintf(p, "%sStart time must be >= 0\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((byo_time = param[4].val.integer) < 0)
  {
    pprintf(p, "%sByo-yomi time must be >= 0\n", SendCode(p, ERROR));
    return COM_OK;
  }

  /* Ok match offer will be made */

  if (pendto >= 0) {
    pprintf(p, "%sUpdating offer already made to \"%s\".\n", 
                SendCode(p, INFO), parray[p1].name);
  }
  if (pendfrom >= 0) {
    if (pendto >= 0) {
      pprintf(p, "%sInternal error Report circumstances and bug PENDFROMTO\n", 
                  SendCode(p, ERROR));
      Logit("This shouldn't happen. You can't have a match pending from and to the same person.");
      return COM_OK;
    }
    if ((size == parray[p].p_from_list[pendfrom].param2) &&
	(start_time == parray[p].p_from_list[pendfrom].param3) &&
	(byo_time == parray[p].p_from_list[pendfrom].param4)) {
      /* Identical match, should accept! */
      challenged = parray[p].p_from_list[pendfrom].param1;
      if (challenged == BLACK) { 
        challenger = WHITE;
        bp = p; 
      } else {
        bp = p1;
        challenger = BLACK;
      }
      if (bp == p) wp = p1;
      else (wp = p);
      if(size == 0) return COM_BADPARAMETERS;
      if ((create_new_gomatch(wp, bp, challenger, 
           size, start_time, byo_time, 0, RULES_NET, TYPE_GO)) == COM_FAILED) {
	pprintf(p, "%sThere was a problem creating the new match.",
                     SendCode(p, ERROR));
	pprintf_prompt(p1, "%sThere was a problem creating the new match.\n",
                     SendCode(p, ERROR));
      }
      return COM_OK;
    } else {
      player_remove_pendfrom(p, p1, PEND_GMATCH);
      player_remove_pendto(p1, p, PEND_GMATCH);
    }
  }
  if (pendto < 0) {
    ppend = player_new_pendto(p);
    if (ppend < 0) {
      pprintf(p, "%sSorry, you can't have any more pending matches.",
                  SendCode(p, ERROR));
      return COM_OK;
    }
    p1pend = player_new_pendfrom(p1);
    if (p1pend < 0) {
      pprintf(p, "%sSorry, %s can't have any more pending matches.", 
                  SendCode(p, ERROR),
                  parray[p1].name);
      parray[p].num_to = parray[p].num_to - 1;
      return COM_OK;
    }
  } else {
    ppend = pendto;
    p1pend = player_find_pendfrom(p1, p, PEND_GMATCH);
  }
  parray[p].p_to_list[ppend].param1 = challenger;
  parray[p].p_to_list[ppend].param2 = size;
  parray[p].p_to_list[ppend].param3 = start_time;
  parray[p].p_to_list[ppend].param4 = byo_time;
  parray[p].p_to_list[ppend].type = PEND_GMATCH;
  parray[p].p_to_list[ppend].whoto = p1;
  parray[p].p_to_list[ppend].whofrom = p;

  parray[p1].p_from_list[p1pend].param1 = challenged;
  parray[p1].p_from_list[p1pend].param2 = size;
  parray[p1].p_from_list[p1pend].param3 = start_time;
  parray[p1].p_from_list[p1pend].param4 = byo_time;
  parray[p1].p_from_list[p1pend].type = PEND_GMATCH;
  parray[p1].p_from_list[p1pend].whoto = p1;
  parray[p1].p_from_list[p1pend].whofrom = p;

  if (pendfrom >= 0) {
    pprintf(p, "%sDeclining offer from %s and offering new match parameters.\n",SendCode(p, INFO),  parray[p1].name);
    pprintf_prompt(p1, "%s%s declines your request for a match.\n", SendCode(p1, INFO), parray[p].name);
  }
  if (pendto >= 0) {
    pprintf(p, "%sUpdating match request", SendCode(p, INFO));
    pprintf(p1, "%s%s updates the match request.\n", SendCode(p1, INFO), parray[p].name);
  } else {
    pprintf(p, "%sRequesting match in %d min with %s as %s.\n", 
                SendCode(p, INFO),    
		start_time,
                parray[p1].name,
                challenger == BLACK ? "White" : "Black");
  }
    pprintf(p1, "%sMatch [%dx%d] in %d minutes requested with %s as %s.\n",
                 SendCode(p1, INFO),
                 size, size,
                 start_time,
                 parray[p].name,
                 challenger == BLACK ? "Black" : "White");
    pprintf_prompt(p1, "%sUse <match %s %s %d %d %d> or <decline %s> to respond.\n",
                 SendCode(p1, INFO),
                 parray[p].name,
                 challenged == BLACK ? "B" : "W",
                 size,
                 start_time,
                 byo_time,
                 parray[p].name);
  return COM_OK;
}


/* For GOE matches */

PUBLIC int com_goematch(int p, param_list param)
{
  int p1;
  int pendfrom, pendto;
  int ppend, p1pend;
  
  int start_time = -1;		/* start time */
  int overtime = -1;		/* overtime */
  int size = -1;                /* size of board */
  int challenger = -1;    /* color of challenger */
  int challenged = -1;    /* color of challenged */
  int wp, bp;
  char parsebuf[100];
  char *val;
  
 
  /* Things we need:

     player_to_challenge challenger_color size_of_board time
     
     looks like:

     match daveg b 19 30

     komi is 8
     byo yomi is 1/2 of the initial time param
     you get 3 periods
     each of 1/6 of initial time
     there are 2 pts penalty for entering each period
     at the end of the third, you lose on time
  */
 
  if (parray[p].game >= 0) {
    pprintf(p, "%sYou can't challenge while you are playing a game.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (parray[p].open == 0) {
    parray[p].open = 1;
    pprintf(p, "%sSetting you open for matches.\n", SendCode(p, INFO));
  }
  stolower(param[0].val.word);
  stolower(param[1].val.word);
  
  p1 = player_find_part_login(param[0].val.word);
  if (p1 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.", SendCode(p, ERROR),
                param[0].val.word);
    return COM_OK;
  }
  if (p1 == p) {
    pprintf(p, "%sUse \"Teach\" for Teaching games.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (player_censored(p1, p)) {
    pprintf(p, "%sPlayer \"%s\" is censoring you.", SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (player_censored(p, p1)) {
    pprintf(p, "%sYou are censoring \"%s\".", SendCode(p, ERROR),
                parray[p1].name);
    return COM_OK;
  }
  if (!parray[p1].open) {
    pprintf(p, "%sPlayer \"%s\" is not open to match requests.", 
                SendCode(p, ERROR), parray[p1].name);
    return COM_OK;
  }
  if (parray[p1].game >= 0) {
    pprintf(p, "%sPlayer \"%s\" is involved in another game.", 
                SendCode(p, ERROR), parray[p1].name);
    return COM_OK;
  }
  challenger = challenged = -1;
  pendto = player_find_pendto(p, p1, PEND_GOEMATCH);
  pendfrom = player_find_pendfrom(p, p1, PEND_GOEMATCH);
  /* parse the match string */
  val = param[1].val.string;
  while (sscanf(val, " %99s", parsebuf) == 1) {
    val = eatwhite(val);
    val = eatword(val);
    if (challenger == -1) {
      /* [PEM]: Changed the if's to a switch and added the nigiri. */
      switch (parsebuf[0]) {
      case 'b':
	challenger = BLACK;
	break;
      case 'w':
	challenger = WHITE;
	break;
      case '?':			/* Nigiri! */
	challenger = (rand()&1 ? BLACK : WHITE);
	break;
      default:
	return COM_BADPARAMETERS;
      }
      if(challenger == WHITE) 
        challenged = BLACK;
      else challenged = WHITE;
    }
  }
  if ((size = param[2].val.integer) < 2)
  {
    pprintf(p, "%sBoardsize must be >= 2\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if ((start_time = param[3].val.integer) < 0)
  {
    pprintf(p, "%sStart time must be >= 0\n", SendCode(p, ERROR));
    return COM_OK;
  }

  /* Ok match offer will be made */

  if (pendto >= 0) {
    pprintf(p, "%sUpdating offer already made to \"%s\".\n", 
                SendCode(p, INFO), parray[p1].name);
  }
  if (pendfrom >= 0) {
    if (pendto >= 0) {
      pprintf(p, "%sInternal error Report circumstances and bug PENDFROMTO\n", 
                  SendCode(p, ERROR));
      return COM_OK;
    }
    if ((size == parray[p].p_from_list[pendfrom].param2) &&
	(start_time == parray[p].p_from_list[pendfrom].param3)) {
      /* Identical match, should accept! */
      challenged = parray[p].p_from_list[pendfrom].param1;
      if (challenged == BLACK) { 
        challenger = WHITE;
        bp = p; 
      } else {
        bp = p1;
        challenger = BLACK;
      }
      if (bp == p) wp = p1;
      else (wp = p);
      if(size == 0) return COM_BADPARAMETERS;
      if ((create_new_gomatch(wp, bp, challenger, 
           size, start_time, overtime, 0, RULES_ING, TYPE_GOEGO)) == COM_FAILED) {
	pprintf(p, "%sThere was a problem creating the new match.",
                     SendCode(p, ERROR));
	pprintf_prompt(p1, "%sThere was a problem creating the new match.\n",
                     SendCode(p, ERROR));
      }
      return COM_OK;
    } else {
      player_remove_pendfrom(p, p1, PEND_GOEMATCH);
      player_remove_pendto(p1, p, PEND_GOEMATCH);
    }
  }
  if (pendto < 0) {
    ppend = player_new_pendto(p);
    if (ppend < 0) {
      pprintf(p, "%sSorry, you can't have any more pending matches.",
                  SendCode(p, ERROR));
      return COM_OK;
    }
    p1pend = player_new_pendfrom(p1);
    if (p1pend < 0) {
      pprintf(p, "%sSorry, %s can't have any more pending matches.", 
                  SendCode(p, ERROR),
                  parray[p1].name);
      parray[p].num_to = parray[p].num_to - 1;
      return COM_OK;
    }
  } else {
    ppend = pendto;
    p1pend = player_find_pendfrom(p1, p, PEND_GOEMATCH);
  }
  parray[p].p_to_list[ppend].param1 = challenger;
  parray[p].p_to_list[ppend].param2 = size;
  parray[p].p_to_list[ppend].param3 = start_time;
  parray[p].p_to_list[ppend].type = PEND_GOEMATCH;
  parray[p].p_to_list[ppend].whoto = p1;
  parray[p].p_to_list[ppend].whofrom = p;

  parray[p1].p_from_list[p1pend].param1 = challenged;
  parray[p1].p_from_list[p1pend].param2 = size;
  parray[p1].p_from_list[p1pend].param3 = start_time;
  parray[p1].p_from_list[p1pend].type = PEND_GOEMATCH;
  parray[p1].p_from_list[p1pend].whoto = p1;
  parray[p1].p_from_list[p1pend].whofrom = p;

  if (pendfrom >= 0) {
    pprintf(p, "%sDeclining offer from %s and offering new match parameters.\n",SendCode(p, INFO),  parray[p1].name);
    pprintf_prompt(p1, "%s%s declines your request for a match.\n", SendCode(p1, INFO), parray[p].name);
  }
  if (pendto >= 0) {
    pprintf(p, "%sUpdating match request", SendCode(p, INFO));
    pprintf(p1, "%s%s updates the match request.\n", SendCode(p1, INFO), parray[p].name);
  } else {
    pprintf(p, "%sRequesting match in %d min with %s as %s.\n", 
                SendCode(p, INFO),    
		start_time,
                parray[p1].name,
                challenger == BLACK ? "White" : "Black");
  }
    pprintf(p1, "%sMatch [%dx%d] in %d minutes requested with %s as %s.\n",
                 SendCode(p1, INFO),
                 size, size,
                 start_time,
                 parray[p].name,
                 challenger == BLACK ? "Black" : "White");
    pprintf_prompt(p1, "%sUse <goematch %s %s %d %d> or <decline %s> to respond.\n",
                 SendCode(p1, INFO),
                 parray[p].name,
                 challenged == BLACK ? "B" : "W",
                 size,
                 start_time,
                 parray[p].name);
  return COM_OK;
}
#ifndef SGI

/* [PEM]: Set *p to player p's rating. Returns 1 on success, 0 otherwise. */
static int
get_nrating(int p, double *ratp)
{
#ifdef NNGSRATED
  rdbm_t db;
  rdbm_player_t rp;

  if ((db = rdbm_open("/home/nngs/src/ratdb", 0)) == NULL)
    return 0;
  if (rdbm_fetch(db, parray[p].name, &rp))
  {
    *ratp = rp.rating;
    rdbm_close(db);
    return 1;
  }
  rdbm_close(db);
#endif /* NNGSRATED */
  return 0;
}

static void
aux_suggest(int p, int ratdiff, int stronger)
{
  int stones;
  float komi;

  /* PEM: Ugly attempt to NaN-bug workaround. */
  AutoMatch(ratdiff, 19, &stones, &komi);
  /*  AutoMatch(ratdiff, 19, &stones, &komi);*/
  switch (stones)
  {
  case 0: case 1:
    pprintf(p, "%sFor 19x19:  Play an even game and set komi to %.1f.\n", 
	    SendCode(p, INFO), komi);
    break;

  case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
    pprintf(p, "%sFor 19x19:  %s %d handicap stones and set komi to %.1f.\n", 
	    SendCode(p, INFO), stronger ? "Give" : "Take", stones, komi);
    break;

  default:
    pprintf(p, "%sFor 19x19:  %s 9 handicap stones and set komi to 0.5\n", 
	    SendCode(p, INFO), stronger ? "Give" : "Take");
    break;
  }

  /* PEM: Ugly attempt to NaN-bug workaround. */
  AutoMatch(ratdiff, 13, &stones, &komi);
  /* AutoMatch(ratdiff, 13, &stones, &komi);*/
  switch (stones)
  {
  case 0: case 1:
    pprintf(p, "%sFor 13x13:  Play an even game and set komi to %.1f.\n", 
	    SendCode(p, INFO), komi);
    break;

  case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
    pprintf(p, "%sFor 13x13:  %s %d handicap stones and set komi to %.1f.\n",
	    SendCode(p, INFO), stronger ? "Give" : "Take", stones, komi);
    break;

  default:
    pprintf(p, "%sFor 13x13:  %s 9 handicap stones and set komi to 0.5\n",
	    SendCode(p, INFO), stronger ? "Give" : "Take");
    break;
  }

  /* PEM: Ugly attempt to NaN-bug workaround. */
  AutoMatch(ratdiff, 9, &stones, &komi);
  /*  AutoMatch(ratdiff, 9, &stones, &komi);*/
  switch (stones)
  {
  case 0: case 1:
    pprintf(p, "%sFor   9x9:  Play an even game and set komi to %.1f.\n", 
	    SendCode(p, INFO), komi);
    break;

  case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
    pprintf(p, "%sFor   9x9:  %s %d handicap stones and set komi to %.1f.\n",
	    SendCode(p, INFO), stronger ? "Give" : "Take", stones, komi);
    break;

  default:
    pprintf(p, "%sFor   9x9:  %s 9 handicap stones and set komi to 0.5\n",
	    SendCode(p, INFO), stronger ? "Give" : "Take");
    break;
  }
}

PUBLIC int com_nsuggest(int p, param_list param)
{
  int p1, p2, conn1, conn2;
  int stronger;
  double rat1, rat2;

  /* Have at least one argument, maybe two. If only one,
  ** we use p1 as the caller and p2 as the only argument.
  */
  if (param[1].type == TYPE_NULL)
    /* One argument. */
    if (get_nrating(p, &rat1))
    {				/* Have rating. */
      p1 = p;
      conn1 = 1;		/* Obviously connected aleady. */
    }
    else
    {
      pprintf(p, "%sYou are not rated, so I cannot suggest.\n%sPlease discuss handicaps with your opponent.\n",
	      SendCode(p, INFO), SendCode(p, INFO));
      return COM_OK;
    }
  else
  {				/* Two arguments. */
    p1 = player_search(p, param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0)
    {				/* Player was connected, will be removed. */
      conn1 = 0;
      p1 = (-p1) - 1;
    }
    else
    {				/* Player connected already. */
      conn1 = 1;
      p1 -= 1;
    }
  }

  if (param[1].type == TYPE_NULL)
    p2 = player_search(p, param[0].val.word);
  else
    p2 = player_search(p, param[1].val.word);
  if (!p2)
    return COM_OK;
  if (p2 < 0)
  {				/* Player was connected, will be removed. */
    conn2 = 0;
    p2 = (-p2) - 1;
  }
  else
  {				/* Player connected already. */
    conn2 = 1;
    p2 -= 1;
  } 

  if (p1 == p2)
  {
    pprintf(p, "%sPlease type \"help teach\"\n", SendCode(p, INFO));
    if (!conn1)
      player_remove(p1);
    if (!conn2)
      player_remove(p2);
    return COM_OK;
  }

  if (p == p2)
  {
    /* Caller's name given as 2nd argument, swap p1 - p2. */
    int px = p1;
    int connx = conn1;

    p1 = p2;
    conn1 = conn2;
    p2 = px;
    conn2 = connx;
  }

  if (!get_nrating(p1, &rat1))
  {
    if (p == p1)
      pprintf(p, "%sYou are not rated, so I cannot suggest.\n%sPlease discuss handicaps with your opponent.\n",
	      SendCode(p, INFO), SendCode(p, INFO));
    else
      pprintf(p, "%s%s is not rated, so I cannot suggest.\n",
	      SendCode(p, INFO), parray[p1].name);
    if (!conn1)
      player_remove(p1);
    return COM_OK;
  }
  if (!get_nrating(p2, &rat2))
  {
    if (p == p1)
      pprintf(p, "%s%s is not rated, so I cannot suggest.\n%sPlease discuss handicaps with your opponent.\n",
	      SendCode(p, INFO), parray[p2].name, SendCode(p, INFO));
    else
      pprintf(p, "%s%s is not rated, so I cannot suggest.\n",
	      SendCode(p, INFO), parray[p2].name);
    if (!conn2)
      player_remove(p2);
    return COM_OK;
  }

  if (rat1 > rat2) 
    stronger = 1; 
  else 
    stronger = 0;

  pprintf(p, "%sI suggest that %s play %s against %s:\n",
	  SendCode(p, INFO),
	  p == p1 ? "you" : parray[p1].name,
	  stronger ? "White" : "Black",
	  parray[p2].name);

  aux_suggest(p, abs((int)(100*(rat1 - rat2 + 0.005))), stronger);

  if (!conn1)
    player_remove(p1);
  if (!conn2)
    player_remove(p2);

  return COM_OK;
}
#endif
/* [PEM]: A new version of com_suggest() that takes at one or two arguments.
** (Old version deleted.)
*/
PUBLIC int com_suggest(int p, param_list param)
{
  int p1, p2, conn1, conn2;
  int stronger;

  /* Have at least one argument, maybe two. If only one,
  ** we use p1 as the caller and p2 as the only argument.
  */
  if (param[1].type == TYPE_NULL)
    /* One argument. */
    if (parray[p].rating != 0)
    {				/* Have rating. */
      p1 = p;
      conn1 = 1;		/* Obviously connected aleady. */
    }
    else
    {
      pprintf(p, "%sYou are not rated, so I cannot suggest.\n%sPlease discuss handicaps with your opponent.\n",
	      SendCode(p, INFO), SendCode(p, INFO));
      return COM_OK;
    }
  else
  {				/* Two arguments. */
    p1 = player_search(p, param[0].val.word);
    if (!p1)
      return COM_OK;
    if (p1 < 0)
    {				/* Player was connected, will be removed. */
      conn1 = 0;
      p1 = (-p1) - 1;
    }
    else
    {				/* Player connected already. */
      conn1 = 1;
      p1 -= 1;
    }
  }

  if (param[1].type == TYPE_NULL)
    p2 = player_search(p, param[0].val.word);
  else
    p2 = player_search(p, param[1].val.word);
  if (!p2)
    return COM_OK;
  if (p2 < 0)
  {				/* Player was connected, will be removed. */
    conn2 = 0;
    p2 = (-p2) - 1;
  }
  else
  {				/* Player connected already. */
    conn2 = 1;
    p2 -= 1;
  } 

  if (p1 == p2)
  {
    pprintf(p, "%sPlease type \"help teach\"\n", SendCode(p, INFO));
    if (!conn1)
      player_remove(p1);
    if (!conn2)
      player_remove(p2);
    return COM_OK;
  }

  if (p == p2)
  {
    /* Caller's name given as 2nd argument, swap p1 - p2. */
    int px = p1;
    int connx = conn1;

    p1 = p2;
    conn1 = conn2;
    p2 = px;
    conn2 = connx;
  }

  if (parray[p1].rating == 0)
  {
    if (p == p1)
      pprintf(p, "%sYou are not rated, so I cannot suggest.\n%sPlease discuss handicaps with your opponent.\n",
	      SendCode(p, INFO), SendCode(p, INFO));
    else
      pprintf(p, "%s%s is not rated, so I cannot suggest.\n",
	      SendCode(p, INFO), parray[p1].name);
    if (!conn1)
      player_remove(p1);
    return COM_OK;
  }
  if (parray[p2].rating == 0)
  {
    if (p == p1)
      pprintf(p, "%s%s is not rated, so I cannot suggest.\n%sPlease discuss handicaps with your opponent.\n",
	      SendCode(p, INFO), parray[p2].name, SendCode(p, INFO));
    else
      pprintf(p, "%s%s is not rated, so I cannot suggest.\n",
	      SendCode(p, INFO), parray[p2].name);
    if (!conn2)
      player_remove(p2);
    return COM_OK;
  }

  if (parray[p1].rating > parray[p2].rating) 
    stronger = 1; 
  else 
    stronger = 0;

  pprintf(p, "%sI suggest that %s play %s against %s:\n",
	  SendCode(p, INFO),
	  p == p1 ? "you" : parray[p1].name,
	  stronger ? "White" : "Black",
	  parray[p2].name);

  aux_suggest(p, abs(parray[p1].rating - parray[p2].rating), stronger);

  if (!conn1)
    player_remove(p1);
  if (!conn2)
    player_remove(p2);

  return COM_OK;
}

PUBLIC int com_teach(int p, param_list param)
{
  int t;

  if(parray[p].game >= 0) {
    pprintf(p, "%sYou are already playing a game.", SendCode(p, ERROR));
    return COM_OK;
  }

  if(param[0].val.integer <= 0) t = COM_FAILED;
  else t = create_new_gomatch(p, p, WHITE, param[0].val.integer, 1, 1, 1, RULES_NET, TYPE_GO);
  if(t == COM_FAILED) return COM_FAILED;
  garray[parray[p].game].Teach = 1;
  garray[parray[p].game].rated = 0;
  return COM_OK;
}

PUBLIC int create_new_gomatch(int wp, int bp,
			     int challenger, int size,
			     int start_time, int byo_time,
			     int teaching, int rules, int type)
{
  int g = game_new(TYPE_GO,size), p;
  char outStr[1024];
  
  const Player *LadderPlayer;
  int bpos, wpos;

  if (g < 0) return COM_FAILED;
  if(size >= NUMLINES) return COM_FAILED;
  garray[g].GoGame = initminkgame(size,size, rules);
  switch(type) {
    case TYPE_GO:
    player_remove_request(wp, bp, PEND_GMATCH);
    player_remove_request(bp, wp, PEND_GMATCH);
    player_decline_offers(wp, -1, PEND_GMATCH);
    player_withdraw_offers(wp, -1, PEND_GMATCH);
    player_decline_offers(bp, -1, PEND_GMATCH);
    player_withdraw_offers(bp, -1, PEND_GMATCH);
    garray[g].gotype = TYPE_GO;
    break;

    case TYPE_GOEGO:
    player_remove_request(wp, bp, PEND_GOEMATCH);
    player_remove_request(bp, wp, PEND_GOEMATCH);
    player_decline_offers(wp, -1, PEND_GOEMATCH);
    player_withdraw_offers(wp, -1, PEND_GOEMATCH);
    player_decline_offers(bp, -1, PEND_GOEMATCH);
    player_withdraw_offers(bp, -1, PEND_GOEMATCH);
    garray[g].gotype = TYPE_GOEGO;
    break;

    case TYPE_TNETGO:
    player_remove_request(wp, bp, PEND_TMATCH);
    player_remove_request(bp, wp, PEND_TMATCH);
    player_decline_offers(wp, -1, PEND_TMATCH);
    player_withdraw_offers(wp, -1, PEND_TMATCH);
    player_decline_offers(bp, -1, PEND_TMATCH);
    player_withdraw_offers(bp, -1, PEND_TMATCH);
    garray[g].gotype = TYPE_TNETGO;
    break;
  }

  if((strcmp(parray[wp].srank, parray[bp].srank)) == 0)
    garray[g].komi = 5.5;
  if(rules == RULES_ING) garray[g].komi = 8.0;
  if(start_time == 0) {
    garray[g].time_type = TYPE_UNTIMED;
    start_time = 480;
    byo_time = 480;
  }
  else {
    start_time = start_time * 60;			/* To Seconds */
    if(rules == RULES_NET || type == TYPE_TNETGO) byo_time = byo_time * 60;
    else byo_time = (start_time) / 6;
    garray[g].time_type = TYPE_TIMED;
  }
  garray[g].white = wp;
  garray[g].black = bp;
  garray[g].status = GAME_ACTIVE;
  garray[g].Teach = teaching;
  if(rules == RULES_NET || type == TYPE_TNETGO) garray[g].type = TYPE_GO;
  else garray[g].type = TYPE_GOEGO;
#ifdef USING_PRIVATE_GAMES
  garray[g].Private = parray[wp].Private || parray[bp].Private; 
#endif
  garray[g].wTime = start_time;
  garray[g].wInByo = 0;
  garray[g].bTime = start_time;
  garray[g].bInByo = 0;
  garray[g].B_penalty = 0;
  garray[g].num_Bovertime = 0;
  garray[g].W_penalty = 0;
  garray[g].num_Wovertime = 0;
  if(parray[wp].registered && parray[bp].registered) garray[g].rated = 1;
  else garray[g].rated = 0;
  if(!strcmp(parray[wp].srank, "NR")) garray[g].rated = 0;
  if(!strcmp(parray[bp].srank, "NR")) garray[g].rated = 0;
  garray[g].Byo = byo_time;
  garray[g].ByoS = 25;    /* Making an assumption here */
  garray[g].onMove = BLACK;
  garray[g].timeOfStart = tenth_secs();
  garray[g].startTime = tenth_secs();
  garray[g].lastMoveTime = garray[g].startTime;
  garray[g].lastDecTime = garray[g].startTime;
  garray[g].clockStopped = 0;
  garray[g].rules = rules;
  if(garray[g].Teach == 0)
  /*pprintf_prompt(wp, "%sMatch [%2d] with %s in %d accepted.\n",*/
  pprintf_prompt(wp, "%sMatch [%d] with %s in %d accepted.\n",
          SendCode(wp, INFO),
          g + 1,
          parray[bp].name,
          start_time / 60);
  if(garray[g].Teach == 0)
  /*pprintf_prompt(wp, "%sCreating match [%2d] with %s.\n",*/
  pprintf_prompt(wp, "%sCreating match [%d] with %s.\n",
          SendCode(wp, INFO),
          g + 1,
          parray[bp].name);
  /*pprintf_prompt(bp, "%sMatch [%2d] with %s in %d accepted.\n",*/
  pprintf_prompt(bp, "%sMatch [%d] with %s in %d accepted.\n",
          SendCode(bp, INFO),
          g + 1,
          parray[wp].name,
          start_time / 60);
  /*pprintf_prompt(bp, "%sCreating match [%2d] with %s.\n",*/
  pprintf_prompt(bp, "%sCreating match [%d] with %s.\n",
          SendCode(bp, INFO),
          g + 1,
          parray[wp].name);
  sprintf(outStr, "Game %d %s: %s (0 %d %d) vs %s (0 %d %d)",
        g + 1,
        "I",
        parray[wp].name,
        garray[g].wTime,
        garray[g].wByoStones,
        parray[bp].name,
	garray[g].bTime,
	garray[g].bByoStones);
  if(garray[g].Teach == 0)
    if(parray[wp].client) pprintf(wp, "%s%s\n", 
      SendCode(wp,MOVE), outStr);
  if(garray[g].Teach == 0)
  if(parray[bp].client) pprintf(bp, "%s%s\n", 
      SendCode(bp,MOVE), outStr);
  Logit("%s", outStr);
  sprintf(outStr, "{Match %d: %s [%3.3s%s] vs. %s [%3.3s%s] }\n",
	  g + 1, parray[wp].name,
          parray[wp].srank,
          parray[wp].rated ? "*" : " ",
	  parray[bp].name,
          parray[bp].srank,
          parray[bp].rated ? "*" : " ");
  for (p = 0; p < p_num; p++) {
    if (parray[p].status != PLAYER_PROMPT) continue;
    if (!parray[p].i_game) continue;
    pprintf_prompt(p, "%s%s", SendCode(p,SHOUT), outStr);
  }
  parray[wp].state = PLAYING_GO;
  parray[bp].state = PLAYING_GO;
  parray[wp].game = g;
  parray[wp].opponent = bp;
  parray[wp].side = WHITE;
  parray[bp].game = g;
  parray[bp].opponent = wp;
  parray[bp].side = BLACK;
  send_go_boards(g, 0);
  if(garray[g].Teach == 1) return COM_OKN;
  if(size == 19) {
    if((LadderPlayer=PlayerNamed(Ladder19,parray[bp].name)) != NULL) {
      bpos = LadderPlayer->idx;
    }
    else return COM_OKN;
    if((LadderPlayer=PlayerNamed(Ladder19,parray[wp].name)) != NULL) {
      wpos = LadderPlayer->idx;
    }
    else return COM_OKN;
    if(wpos < bpos) {
      garray[g].Ladder_Possible = 1;
      pprintf(wp, "%sThis can be a Ladder19 rated game.\n%sType 'ladder' BEFORE your first move to make it ladder rated.\n",
                    SendCode(wp, INFO), SendCode(wp, INFO));
      pprintf(bp, "%sThis can be a Ladder19 rated game.\n%sType 'ladder' BEFORE your first move to make it ladder rated.\n",
                    SendCode(bp, INFO), SendCode(bp, INFO));
    }
  }
  else if(size == 9) {
    if((LadderPlayer=PlayerNamed(Ladder9,parray[bp].name)) != NULL) {
      bpos = LadderPlayer->idx;
    }
    else return COM_OKN;
    if((LadderPlayer=PlayerNamed(Ladder9,parray[wp].name)) != NULL) {
      wpos = LadderPlayer->idx;
    }
    else return COM_OKN;
    if(wpos < bpos) {
      garray[g].Ladder_Possible = 1;
      pprintf(wp, "%sThis can be a Ladder9 rated game.\n%sType 'ladder' BEFORE your first move to make it rated.\n", 
                    SendCode(wp, INFO), SendCode(wp, INFO));
      pprintf(bp, "%sThis can be a Ladder9 rated game.\n%sType 'ladder' BEFORE your first move to make it rated.\n", 
                    SendCode(bp, INFO), SendCode(bp, INFO));
    }
  }
  return COM_OKN;
}

PUBLIC int com_accept(int p, param_list param)
{
  int acceptNum = -1;
  int type = -1;
  int p1;

  if (parray[p].num_from == 0) {
    pprintf(p, "%sYou have no offers to accept.", SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].num_from != 1) {
      pprintf(p, "%sYou have more than one offer to accept.\n%sUse \"pending\" to see them and \"accept n\" to choose which one.", 
                 SendCode(p, ERROR), SendCode(p, ERROR));
      return COM_OK;
    }
    acceptNum = 0;
  } else if (param[0].type == TYPE_INT) {
    if ((param[0].val.integer < 1) || 
        (param[0].val.integer > parray[p].num_from)) {
      pprintf(p, "%sOut of range. Use \"pending\" to see the list of offers.",
                    SendCode(p, ERROR));
      return COM_OK;
    }
    acceptNum = param[0].val.integer - 1;
  /* } else if (param[0].type == TYPE_WORD) { */
  } else if (!strcmp(param[0].val.word, "pause")) {
      type = PEND_PAUSE;
  } else if (!strcmp(param[0].val.word, "adjourn")) {
      type = PEND_ADJOURN;
      if (!strcmp(param[0].val.word, "all")) {
        while (parray[p].num_from != 0) {
        pcommand(p, "accept 1");
      }
      return COM_OK;
    }
    if (type > 0) {
      if ((acceptNum = player_find_pendfrom(p, -1, type)) < 0) {
	pprintf(p, "%sThere are no pending %s offers.", 
                 SendCode(p, ERROR), param[0].val.word);
	return COM_OK;
      }
    } else {			/* Word must be a name */
      p1 = player_find_part_login(param[0].val.word);
      if (p1 < 0) {
	pprintf(p, "%sNo user named \"%s\" is logged in.\n", 
                      SendCode(p, ERROR), param[0].val.word);
	return COM_OK;
      }
      if ((acceptNum = player_find_pendfrom(p, p1, -1)) < 0) {
	pprintf(p, "%sThere are no pending offers from %s.", 
                    SendCode(p, ERROR), parray[p1].name);
	return COM_OK;
      }
    }
  }
  switch (parray[p].p_from_list[acceptNum].type) {
/* loon: let's see if this works :) */
  case PEND_MATCH:
  case PEND_GMATCH:
    pcommand(p, "match %s", parray[parray[p].p_from_list[acceptNum].whofrom].name);
    break;
  case PEND_PAUSE:
    pcommand(p, "pause");
    break;
  case PEND_ADJOURN:
    pcommand(p, "adjourn");
    break;
  }
  return COM_OK_NOPROMPT;
}

/* AT AROUND LINE 2600 */

PUBLIC int com_decline(int p, param_list param)
{
  int declineNum;
  int p1 = -1, type = -1;
  int count;

  if (parray[p].num_from == 0) {
    pprintf(p, "%sYou have no pending offers from other players.", 
                SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].num_from == 1) {
      p1 = parray[p].p_from_list[0].whofrom;
      type = parray[p].p_from_list[0].type;
    } else {
      pprintf(p, "%sYou have more than one pending offer.\n%sPlease specify which one you wish to decline.\n%s'Pending' will give you the list.", 
             SendCode(p, ERROR), SendCode(p, ERROR), SendCode(p, ERROR));
      return COM_OK;
    }
  } else {
    if (param[0].type == TYPE_WORD) {
      /* Draw adjourn match takeback abort or <name> */

      if (!strcmp(param[0].val.word, "match")) {
	type = PEND_MATCH;
      } else if (!strcmp(param[0].val.word, "pause")) {
	type = PEND_PAUSE;
      } else if (!strcmp(param[0].val.word, "adjourn")) {
	type = PEND_ADJOURN;
      /* } else if (!strcmp(param[0].val.word, "all")) { */
      } else {
	p1 = player_find_part_login(param[0].val.word);
	if (p1 < 0) {
	  pprintf(p, "%sNo user named \"%s\" is logged in.", 
                  SendCode(p, ERROR), param[0].val.word);
	  return COM_OK;
	}
      }
    } else {			/* Must be an integer */
      declineNum = param[0].val.integer - 1;
      if (declineNum >= parray[p].num_from || declineNum < 0) {
	pprintf(p, "%sInvalid offer number. Must be between 1 and %d.", 
                    SendCode(p, ERROR), parray[p].num_from);
	return COM_OK;
      }
      p1 = parray[p].p_from_list[declineNum].whofrom;
      type = parray[p].p_from_list[declineNum].type;
    }
  }
  count = player_decline_offers(p, p1, type);
  if (count != 1)
    pprintf(p, "%s%d offers declined", SendCode(p, ERROR), count);
  return COM_OK;
}

PUBLIC int com_withdraw(int p, param_list param)
{
  int withdrawNum;
  int p1 = -1, type = -1;
  int count;

  if (parray[p].num_to == 0) {
    pprintf(p, "%sYou have no pending offers to other players.", 
            SendCode(p, ERROR));
    return COM_OK;
  }
  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].num_to == 1) {
      p1 = parray[p].p_to_list[0].whoto;
      type = parray[p].p_to_list[0].type;
    } else {
      pprintf(p, "%sYou have more than one pending offer.\n%sPlease specify which one you wish to withdraw.\n%s'Pending' will give you the list.", 
              SendCode(p, ERROR), SendCode(p, ERROR), SendCode(p, ERROR));
      return COM_OK;
    }
  } else {
    if (param[0].type == TYPE_WORD) {
      /* Draw adjourn match takeback abort or <name> */
      if (!strcmp(param[0].val.word, "match")) {
	type = PEND_MATCH;
      } else if (!strcmp(param[0].val.word, "pause")) {
	type = PEND_PAUSE;
      } else if (!strcmp(param[0].val.word, "adjourn")) {
	type = PEND_ADJOURN;
      /* } else if (!strcmp(param[0].val.word, "all")) { */
      } else {
	p1 = player_find_part_login(param[0].val.word);
	if (p1 < 0) {
	  pprintf(p, "%sNo user named \"%s\" is logged in.", 
                  SendCode(p, ERROR), param[0].val.word);
	  return COM_OK;
	}
      }
    } else {			/* Must be an integer */
      withdrawNum = param[0].val.integer - 1;
      if (withdrawNum >= parray[p].num_to || withdrawNum < 0) {
	pprintf(p, "%sInvalid offer number. Must be between 1 and %d.", 
                    SendCode(p, ERROR), parray[p].num_to);
	return COM_OK;
      }
      p1 = parray[p].p_to_list[withdrawNum].whoto;
      type = parray[p].p_to_list[withdrawNum].type;
    }
  }
  count = player_withdraw_offers(p, p1, type);
  if (count != 1)
    pprintf(p, "%s%d offers withdrawn", SendCode(p, ERROR), count);
  return COM_OK;
}

PUBLIC int com_pending(int p, param_list param)
{
  int i;

  if (!parray[p].num_to) {
    pprintf(p, "%sThere are no offers pending TO other players.\n", 
                    SendCode(p, ERROR));
  } else {
    pprintf(p, "%sOffers TO other players:\n",SendCode(p, INFO));
    for (i = 0; i < parray[p].num_to; i++) {
      pprintf(p, "   ");
      player_pend_print(p, &parray[p].p_to_list[i]);
    }
  }
  if (!parray[p].num_from) {
    pprintf(p, "%sThere are no offers pending FROM other players.\n", SendCode(p, ERROR));
  } else {
    pprintf(p, "%sOffers FROM other players:\n", SendCode(p, INFO));
    for (i = 0; i < parray[p].num_from; i++) {
      pprintf(p, " %d: ", i + 1);
      player_pend_print(p, &parray[p].p_from_list[i]);
    }
    pprintf(p, "%sIf you wish to accept any of these offers type 'accept n'\n%sor just 'accept' if there is only one offer.\n", 
               SendCode(p, INFO), SendCode(p, INFO));
  }
  return COM_OKN;
}

PUBLIC int com_watching(int p, param_list param)
{
  int g, first;
  first = 1;

  if (parray[p].num_observe) {
    pprintf(p, "%sGames currently being observed: ", SendCode(p, INFO));
    for (g = 0; g < parray[p].num_observe; g++) {
      if(first) {
        pprintf(p, "%3d", 1 + (parray[p].observe_list[g]));
        first = 0;
      } else {
        pprintf(p, ",%3d", 1 + (parray[p].observe_list[g]));
      }
    }
    pprintf(p, "\n");
  }
  else {
    pprintf(p, "%sGames currently being observed:  None.\n",
                SendCode(p, INFO));
  }
  return COM_OKN;
}

PUBLIC int com_score(int p, param_list param)
{
  int g, wterr, bterr, wocc, bocc, bc, wc;
  twodstring statstring;
  float wscore, bscore;

  bterr = 0; /* B territory */
  wterr = 0; /* W territory */
  bocc = 0; /* B Occupied */
  wocc = 0; /* W Occupied */
  bc = 0; /* B Captured */
  wc = 0; /* W Captured */

  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) {
      g = parray[p].game;
    } else {			/* Do observing in here */
      if (parray[p].num_observe) {
        g = parray[p].observe_list[0];
      } else { 
        pprintf(p, "%sYou are neither playing nor observing a game.", 
                SendCode(p, ERROR));
        return COM_OK;
      }
    }

    countscore(garray[g].GoGame, statstring, &wterr, &bterr, &wocc, &bocc);
    getcaps(garray[g].GoGame, &wc, &bc);
    if(garray[g].komi > 0) {
      wscore = wterr + wocc + garray[g].komi;
      bscore = bterr + bocc;
    } else {
      wscore = wterr + wocc;
      bscore = bterr + bocc - garray[g].komi; /* PEM: komi <= 0 */
    }
    pprintf(p, "%s(Chinese) White: %.1f, Black: %.1f\n",  
           SendCode(p, INFO), wscore, bscore);

    if(garray[g].komi > 0) {
      wscore = wterr + bc + garray[g].komi;
      bscore = bterr + wc;
    } else {
      wscore = wterr + bc;
      bscore = bterr + wc - garray[g].komi; /* PEM: komi <= 0 */
    }
    pprintf(p, "%s(Japanese) White: %.1f, Black: %.1f\n",  
             SendCode(p, INFO), wscore, bscore);

  } else if (param[0].type == TYPE_INT) {
    g = param[0].val.integer - 1;
    if (g < 0) {
      pprintf(p, "Game numbers must be >= 1.");
      return COM_OK;
    } 

    if ((g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
      return COM_NOSUCHGAME;
    }

    countscore(garray[g].GoGame, statstring, &wterr, &bterr, &wocc, &bocc);
    getcaps(garray[g].GoGame, &wc, &bc);
    if(garray[g].komi > 0) {
      wscore = wterr + wocc + garray[g].komi;
      bscore = bterr + bocc;
    } else {
      wscore = wterr + wocc;
      bscore = bterr + bocc - garray[g].komi; /* PEM: komi <= 0 */
    }
    pprintf(p, "%s(Chinese) White: %.1f, Black: %.1f\n", 
            SendCode(p, INFO), wscore, bscore);

    if(garray[g].komi > 0) {
      wscore = wterr + bc + garray[g].komi;
      bscore = bterr + wc;
    } else {
      wscore = wterr + bc;
      bscore = bterr + wc - garray[g].komi; /* PEM: komi <= 0 */
    }
    pprintf(p, "%s(Japanese) White: %.1f, Black: %.1f\n", 
           SendCode(p, INFO), wscore, bscore);
  } else {
    return COM_BADPARAMETERS;
  }
  return COM_OKN;
}

PUBLIC int com_refresh(int p, param_list param)
{
  int g;
  if (param[0].type == (int) TYPE_NULL) {
    if (parray[p].game >= 0) {
      if (garray[parray[p].game].gotype >= TYPE_GO) {
        if((!parray[p].client) || (parray[p].i_verbose))
          send_go_board_to(parray[p].game, p);
        if(parray[p].client) pcommand(p, "moves %d", (parray[p].game) + 1);
      }
    } else {			/* Do observing in here */
      if (parray[p].num_observe) {
	for (g = 0; g < parray[p].num_observe; g++) {
          if (garray[parray[p].observe_list[g]].gotype >= TYPE_GO) {
            if((!parray[p].client) || (parray[p].i_verbose))
              send_go_board_to(parray[p].observe_list[g], p);
	    if(parray[p].client) 
              pcommand(p, "moves %d", (parray[p].observe_list[g]) + 1);
          }
	}
      } else
	pprintf(p, "%sYou are neither playing nor observing a game.\n", 
                 SendCode(p, ERROR));
    }
  } else if (param[0].type == TYPE_INT) {
    g = param[0].val.integer - 1;
    if (g < 0) {
      pprintf(p, "%sGame numbers must be >= 1.\n", SendCode(p, ERROR));
    } else if ((g >= g_num) || (garray[g].status != GAME_ACTIVE)) {
      pprintf(p, "%sNo such game.\n", SendCode(p, ERROR));
    } else {
        if((!parray[p].client) || (parray[p].i_verbose)) send_go_board_to(g, p);
	if(parray[p].client) pcommand(p, "moves %d", g +1);
    }
  } else {
    return COM_BADPARAMETERS;
  }
  return COM_OKN;
}

PUBLIC int com_open(int p, param_list param)
{
  int retval;
  
  if ((retval = pcommand(p, "set open")) != COM_OK)
    return retval;
  else
    return COM_OK_NOPROMPT;
}

PUBLIC int com_rank(int p, param_list param)
{
  int retval;
 
  if ((retval = pcommand(p, "set rank %s", param[0].val.string)) != COM_OK)
    return retval;
  else
    return COM_OK_NOPROMPT;
}

PUBLIC int com_ranked(int p, param_list param)
{
  int len, level, rat;
  char c[10], temp[10], trnk;

  len = strlen(param[0].val.string);
  if((len > 3) || (len < 2)) {
    pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
                SendCode(p, ERROR));
    return COM_OK;
  }
  strcpy(temp, param[0].val.string);
  stolower(temp);
  stolower(param[0].val.string);
  
  if(((temp[1]==' ')||(!isdigit(temp[0]))) && (!strcmp(temp,"nr")) && (!strcmp(temp, "???"))) { 
    pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
    SendCode(p, ERROR));
    return COM_OK;
  }
  /* Check for stupid ranks aka Hell kk */
  if((!strcmp(temp, "kk")) || (!strcmp(temp, "dd"))) {
    pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
    SendCode(p, ERROR));
    return COM_OK;
  }

  if(len == 2) {
    if(temp[1] != 'k') {
      if(temp[1] != 'd') {
        if(temp[1] != 'r') {
          if(temp[1] != '?') {
            pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
                      SendCode(p, ERROR));
            return COM_OK;
          } else {
            do_copy(parray[p].ranked, "???", MAX_RANKED);
            do_copy(parray[p].srank, "???", MAX_SRANK);
            parray[p].rating = 1;
            parray[p].orating = 1;
            return COM_OK;
          }
        } else {
          do_copy(parray[p].srank, "NR", MAX_SRANK);
          do_copy(parray[p].ranked, "NR", MAX_RANKED);
          parray[p].rating = 0;
          parray[p].orating = 0;
          parray[p].rated = 0;
          return COM_OK;
        }
      } else {
        sscanf(param[0].val.string, "%d%c", &level, c);
        if((level < 1) || (level > 6) || (isdigit(temp[0]) == 0)) {
          pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
                        SendCode(p, ERROR));
          return COM_OK;
        }
        if(parray[p].rated == 0) 
          do_copy(parray[p].srank, param[0].val.string, MAX_SRANK);
        do_copy(parray[p].ranked, param[0].val.string, MAX_RANKED);
      }
    } else {
      sscanf(param[0].val.string, "%d%c", &level, c);
      if((level > 9) || (level < 1)) {
        pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
                      SendCode(p, ERROR));
        return COM_OK;
      }
      if(parray[p].rated == 0) 
        do_copy(parray[p].srank, param[0].val.string, MAX_SRANK);
      do_copy(parray[p].ranked, param[0].val.string, MAX_RANKED);
    }
  }
  if(len == 3) {
    if(temp[2] != 'k') {
      if(temp[2] != 'r') {
        if(temp[2] != '?') {
          pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
                    SendCode(p, ERROR));
          return COM_OK;
        } else {
          do_copy(parray[p].ranked, "???", MAX_RANKED);
          do_copy(parray[p].srank, "???", MAX_SRANK);
          parray[p].rating = 1;
          parray[p].orating = 1;
          return COM_OK;
        }
      } else {
        do_copy(parray[p].srank, "NR", MAX_SRANK);
        do_copy(parray[p].ranked, "NR", MAX_RANKED);
        parray[p].rating = 0;
        parray[p].orating = 0;
        parray[p].rated = 0;
        return COM_OK;
      }
    } else {
      sscanf(param[0].val.string, "%d%c", &level, c);
      if((level < 1) || (level > 30) || (isdigit(temp[1]) == 0)) {
        pprintf(p, "%sInvalid rank.  Valid ranks are: 30K - 1K, 1D - 6D.",
                      SendCode(p, ERROR));
        return COM_OK;
      }
      if(parray[p].rated == 0) 
        do_copy(parray[p].srank, param[0].val.string, MAX_SRANK);
      do_copy(parray[p].ranked, param[0].val.string, MAX_RANKED);
    }
  }
  pprintf(p, "%sRank set to %s\n", SendCode(p, INFO), parray[p].ranked);
  pprintf(p, "%sPlease explain your rank by using \"set rank\".", 
                SendCode(p, INFO));
  if(strcmp(parray[p].ranked, "NR")) {
    sscanf(parray[p].ranked, "%u%c", &rat, &trnk);
    switch(trnk) {
      case 'k': case 'K':
      rat = 31 - rat;
      break;
      case 'd': case 'D':
      rat += 30;
      break;
      case 'p': case 'P':
      rat += 40;
      break;
    }
    parray[p].orating = (rat * 100) - 100;
  }
  else parray[p].orating = 0;

  return COM_OK;
}

PUBLIC int com_info(int p, param_list param)
{
  int retval;
  
  if ((retval = pcommand(p, "set 0 %s", param[0].val.string)) != COM_OK)
    return retval;
  else
    return COM_OK_NOPROMPT;
}

PUBLIC int com_bell(int p, param_list param)
{
  int retval;
  
  if ((retval = pcommand(p, "set bell")) != COM_OK)
    return retval;
  else
    return COM_OK_NOPROMPT;
}

PUBLIC int com_alias(int p, param_list param)
{
  char *c, *a;

  if (param[0].type == (int) TYPE_NULL) {
    pprintf(p, "%sYOUR ALIASES\n%sCode Command\n%s---- -------\n",
                SendCode(p, INFO),SendCode(p, INFO),SendCode(p, INFO));
    alias_start(parray[p].alias_list);
    while (alias_next(&c, &a, parray[p].alias_list)) {
      pprintf(p, "%s%4s %s\n", SendCode(p, INFO), c, a);
    }
    return COM_OKN;
  }
  a = alias_lookup(param[0].val.word, parray[p].alias_list);
  if (param[1].type == (int) TYPE_NULL) {
    if (!a) {
      pprintf(p, "%sYou have no alias named '%s'.\n", 
                   SendCode(p, ERROR), param[0].val.word);
    } else {
      pprintf(p, "%s%s -> %s\n", 
                   SendCode(p, INFO), param[0].val.word, a);
    }
  } else {
    if (alias_count(parray[p].alias_list) >= MAX_ALIASES - 1) {
      pprintf(p, "%sYou have your maximum of %d aliases.\n", 
	      SendCode(p, ERROR), MAX_ALIASES - 1);
    } else {

      if (!strcmp(param[0].val.string, "quit") ||
	  !strcmp(param[0].val.string, "unalias")) {	/* making sure they cant
							   alias some commands */
	pprintf(p, "%sYou can't alias this command.\n", SendCode(p, ERROR));
      } else {
	if (alias_add(param[0].val.word, param[1].val.string,
		      parray[p].alias_list))
	  pprintf(p, "%sAlias replaced.\n", SendCode(p, INFO));
	else
	  pprintf(p, "%sAlias set.\n", SendCode(p, INFO));
      }
    }
  }
  return COM_OKN;
}

PUBLIC int com_unalias(int p, param_list param)
{
  int x;

  x = alias_rem(param[0].val.word, parray[p].alias_list);
  if (!x) {
    pprintf(p, "%sYou have no alias named '%s'.", 
               SendCode(p, ERROR), param[0].val.word);
  } else {
    pprintf(p, "%sAlias removed.", SendCode(p, INFO));
  }
  return COM_OK;
}


PUBLIC int com_sendmessage(int p, param_list param)
{
  int p1, connected = 1;

  if (!parray[p].registered) {
    pprintf(p, "%sYou are not registered and cannot send messages.", 
                 SendCode(p, ERROR));
    return COM_OK;
  }
  if ((param[0].type == (int) TYPE_NULL) || (param[1].type == (int) TYPE_NULL)) {
    pprintf(p, "%sNo message sent.", SendCode(p, ERROR));
    return COM_OK;
  }
  p1 = player_search(p, param[0].val.word);
  if (!p1) return COM_OK;
  if (p1 < 0) {			/* player had to be connected and will be
				   removed later */
    connected = 0;
    p1 = (-p1) - 1;
  } else {
    connected = 1;
    p1 = p1 - 1;
  }
  if (player_add_message(p1, p, param[1].val.string)) {
    pprintf(p, "%sCouldn't send message to %s. Message buffer full.",
	    SendCode(p, ERROR), 
	    parray[p1].name);
  } else {
    pprintf(p, "%sMessage sent to %s.", SendCode(p, INFO), parray[p1].name);
    if (connected)
      pprintf_prompt(p1, "%s%s just sent you a message.\n", 
                SendCode(p1, INFO), parray[p].name);
  }
  if (!connected) player_remove(p1);
  return COM_OK;
}

PUBLIC int com_messages(int p, param_list param)
{
  if (param[0].type != (int) TYPE_NULL) {
    return com_sendmessage(p, param);
  }
  if (player_num_messages(p) <= 0) {
    pprintf(p, "%sYou have no messages.", SendCode(p, INFO));
    return COM_OK;
  }
  pprintf(p, "%sMessages:\n", SendCode(p, INFO));
  player_show_messages(p);
  return COM_OKN;
}

PUBLIC int com_clearmess(int p, param_list param)
{
  if (player_num_messages(p) <= 0) {
    pprintf(p, "%sYou have no messages.", SendCode(p, INFO));
    return COM_OK;
  }
  pprintf(p, "%sMessages cleared.", SendCode(p, INFO));
  player_clear_messages(p);
  return COM_OK;
}

PUBLIC int com_help(int p, param_list param)
{
  char command[MAX_FILENAME_SIZE];

  if (param[0].type == (int) TYPE_NULL) {
    sprintf(command, "ls -C %s", help_dir);
    if(parray[p].client) pprintf(p, "8 File\n");
    psend_command(p, command, NULL);
    pprintf(p, "[Type \"help overview\" for a list of NNGS general information files.]\n");
    if(parray[p].client) pprintf(p, "8 File");
    return COM_OK;
  }
  if (safestring(param[0].val.word)) {
#define HELP_BUFFER_SIZE 8000
    char filenames[HELP_BUFFER_SIZE];	/* enough for all helpfile names */
    int count;

    count = search_directory(help_dir, param[0].val.word,
			     filenames, HELP_BUFFER_SIZE);
    if (count > 0) {
      if ((count > 1) && (strcmp(filenames, param[0].val.word))) {
	char *s = filenames;
	int i;
	pprintf(p, "%sMatches: %s", SendCode(p, INFO), s);
	for (i = 1; i < count; i++) {
	  s += strlen(s) + 1;
	  pprintf(p, ", %s", s);
	}
	pprintf(p, ".\n");
	return COM_OK;
      }
      if (psend_file(p, help_dir, filenames)) {
	/* we should never reach this unless the file was just deleted */
	pprintf(p, "%sHelpfile %s could not be found!", 
                      SendCode(p, ERROR), filenames);
	pprintf(p, "%sPlease inform an admin of this. Thank you.\n", 
                      SendCode(p, ERROR));
      }
    } else {
      /* if we get here no file was found */
      pprintf(p, "%sNo help available on %s.\n", 
                    SendCode(p, ERROR), param[0].val.word);
    }
  } else {
    pprintf(p, "%sIllegal character in command %s.\n", 
                    SendCode(p, ERROR), param[0].val.word);
  }
  return COM_OKN;
}

PUBLIC int com_infor(int p, param_list param)
{
  char command[MAX_FILENAME_SIZE];

  sprintf(command, "ls -C %s", info_dir);
  psend_command(p, command, NULL);
  return COM_OK;
}

PUBLIC int com_reset(int p, param_list param)
{
  parray[p].gowins = 0;
  parray[p].golose = 0;
  parray[p].gonum_white = 0;
  parray[p].gonum_black = 0;
  pprintf(p, "%sReset your stats to 0", SendCode(p, INFO));
  return COM_OK;
}

PUBLIC int com_adhelp(int p, param_list param)
{
  char command[MAX_FILENAME_SIZE];

  if (param[0].type == (int) TYPE_NULL) {
    sprintf(command, "ls -C %s", adhelp_dir);
    psend_command(p, command, NULL);
    return COM_OK;
  }
  if (safestring(param[0].val.word)) {
#define ADHELP_BUFFER_SIZE 8000
    char filenames[ADHELP_BUFFER_SIZE];	/* enough for all helpfile names */
    int count;

    count = search_directory(adhelp_dir, param[0].val.word,
			     filenames, ADHELP_BUFFER_SIZE);
    if (count > 0) {
      if (count > 1) {
	char *s = filenames;
	int i;
	pprintf(p, "%sMatches: %s", SendCode(p, INFO), s);
	for (i = 1; i < count; i++) {
	  s += strlen(s) + 1;
	  pprintf(p, ", %s", s);
	}
	pprintf(p, ".\n");
	return COM_OK;
      }
      if (psend_file(p, adhelp_dir, filenames)) {
	/* we should never reach this unless the file was just deleted */
	pprintf(p, "%sHelpfile %s could not be found!", 
                    SendCode(p, ERROR), filenames);
	pprintf(p, "%sPlease inform a site-admin of this. Thank you.\n", 
                    SendCode(p, ERROR));
      }
    } else {
      /* if we get here no file was found */
      pprintf(p, "%sNo help available on %s.\n", 
                    SendCode(p, ERROR), param[0].val.word);
    }
  } else {
    pprintf(p, "%sIllegal character in command %s.\n", 
                    SendCode(p, ERROR), param[0].val.word);
  }
  return COM_OKN;
}

PUBLIC int com_mailmess(int p, param_list param)
{
  char fname[MAX_FILENAME_SIZE];

  if(!player_num_messages(p)) {
    pprintf(p, "%sYou have no messages to mail.\n", SendCode(p, INFO));
    return COM_OK;
  }
  sprintf(fname, "%s/player_data/%c/%s.%s", stats_dir, parray[p].login[0], parray[p].login, STATS_MESSAGES);

  pmail_file(p, NULL, fname);
  pprintf(p, "%sYour messages were sent to %s\n", 
                SendCode(p, INFO), parray[p].emailAddress);
  return COM_OKN;
}

PUBLIC int com_mailhelp(int p, param_list param)
{				/* Sparky  */
  /* FILE *fp; char tmp[MAX_LINE_SIZE]; char fname[MAX_FILENAME_SIZE]; */
  char command[MAX_FILENAME_SIZE];
  char buffer[10000];
  char hdir[MAX_FILENAME_SIZE];
  int count;

  if (param[0].type == (int) TYPE_NULL) {
    sprintf(command, "ls -C %s", help_dir);
    psend_command(p, command, NULL);
    return COM_OK;
  }

  sprintf(hdir, "%s/", help_dir);
  count = search_directory(hdir, param[0].val.word, buffer, 8000);


  if (count == 1) {
    sprintf(command, "/bin/mail -s \"NNGS helpfile: %s\" %s < %s%s&", 
            param[0].val.word, parray[p].emailAddress, hdir, param[0].val.word);
    system(command);
    pprintf(p, "%sThe file %s was sent to %s\n", 
                SendCode(p, INFO), param[0].val.word, parray[p].emailAddress);
  } else
    pprintf(p, "%sFound %d files matching that.\n", SendCode(p, ERROR), count);
  return COM_OKN;

}

PUBLIC int com_mailme(int p, param_list param)
{
  int i;
  char dname[MAX_FILENAME_SIZE];

  if((strcmp(param[0].val.word, "me")) != 0)
    return COM_BADPARAMETERS;
  
  sprintf(dname, "%s/%s", stats_dir, STATS_CGAMES);
  
  i = pmail_file(p, dname, getword(eatwhite(param[1].val.string)));
  if(i == 0) {
    pprintf(p, "%s%s mailed to %s.", SendCode(p, INFO), 
                param[1].val.string,
                parray[p].emailAddress);
  }
  else {
    pprintf(p, "%sEither the file was not found, or your email address is invalid.", SendCode(p, ERROR));
  }
  return COM_OK;
}

PUBLIC int com_handles(int p, param_list param)
{
  char buffer[8000];
  char pdir[MAX_FILENAME_SIZE];
  int count;

  sprintf(pdir, "%s/%c", player_dir, param[0].val.word[0]);
  count = search_directory(pdir, param[0].val.word, buffer, 8000);
  pprintf(p, "%sFound %d names.", SendCode(p, INFO), count);
  if (count > 0) {
    display_directory(p, buffer, count);
  }
  if(count == 0) pprintf(p, "\n");
  return COM_OKN;
}

PUBLIC int com_which_client(int p, param_list param)
{
  parray[p].which_client = param[0].val.integer;

  pprintf(p, "%sClient type changed to: ", SendCode(p, INFO));
  switch(parray[p].which_client) {
    case UNKNOWN_CLIENT: pprintf(p, "Unknown\n"); break;
    case TELNET: pprintf(p, "Telnet\n"); break;
    case XIGC: pprintf(p, "Xigc\n"); break;
    case WINIGC: pprintf(p, "WinIGC\n"); break;
    case WIGC: pprintf(p, "WIGC\n"); break;
    case CGOBAN: pprintf(p, "CGoban\n"); break;
    case JAVA: pprintf(p, "Java\n"); break;
    case TGIGC: pprintf(p, "Tgigc\n"); break;
    case TGWIN: pprintf(p, "TgWin\n"); break;
    case FIGC: pprintf(p, "Figc\n"); break;
    case PCIGC: pprintf(p, "PCigc\n"); break;
    case GOSERVANT: pprintf(p, "GoServant\n"); break;
    case MACGO: pprintf(p, "MacGo\n"); break;
    case AMIGAIGC: pprintf(p, "AmigaIgc\n"); break;
    case HAICLIENT: pprintf(p, "Hai Client\n"); break;
    case IGC: pprintf(p, "IGC\n"); break;
    case KGO: pprintf(p, "Kgo\n"); break;
    case NEXTGO: pprintf(p, "NextGo\n"); break;
    case OS2IGC: pprintf(p, "OS/2igc\n"); break;
    case STIGC: pprintf(p, "StIgc\n"); break;
    case XGOSPEL: pprintf(p, "XGospel\n"); break;
    default: pprintf(p, "Unknown\n"); break;
  } 
  return COM_OK;
}


PUBLIC int com_qtell(int p, param_list param)
{
  int p1;
  char tmp[MAX_STRING_LENGTH];
  char *ptmp = tmp;

  if ((p1 =  player_find_bylogin(param[0].val.word)) < 0) {
    pprintf(p, "%s*qtell %s 1*", SendCode(p, INFO), param[0].val.word);
    return COM_OK;
  }

  /* Exchange "\n" in string to a '\n' char */ 
  
  sprintf(tmp, "%s", param[1].val.string);
  while (!(*ptmp=='\0')) {
    if ((*ptmp=='\\') && (*(++ptmp)=='n')) {
      *ptmp='\n';
      *(--ptmp)=' ';
    }
    ++ptmp;
  }
  pprintf_prompt(p1, "%s>%s<%s", SendCode(p1, INFO), parray[p].name, tmp);
  pprintf(p, "%s*qtell %s 0*", SendCode(p, INFO), parray[p1].name);
  return COM_OK;
}

PUBLIC int com_spair(int p, param_list param)
{
  int p1, p2, p3, p4;

  p1 = player_find_part_login(param[0].val.word);
  if (p1 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.",
                SendCode(p, ERROR),
                param[0].val.word);
    return COM_OK;
  }
  p2 = player_find_part_login(param[1].val.word);
  if (p2 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.",
                SendCode(p, ERROR),
                param[1].val.word);
    return COM_OK;
  }
  p3 = player_find_part_login(param[2].val.word);
  if (p3 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.",
                SendCode(p, ERROR),
                param[2].val.word);
    return COM_OK;
  }
  p4 = player_find_part_login(param[3].val.word);
  if (p4 < 0) {
    pprintf(p, "%sNo user named \"%s\" is logged in.",
                SendCode(p, ERROR),
                param[3].val.word);
    return COM_OK;
  }
  
  if((p1 == p2) || (p1 == p3) || (p1 == p4) ||
     (p2 == p3) || (p2 == p4) ||
     (p3 == p4)) {
    pprintf(p, "%sYou must list 4 unique player names to play pair go.",
                SendCode(p, ERROR));
    return COM_OK;
  }

  if ((strcmp(parray[p].name, parray[p1].name) != 0) && 
      (strcmp(parray[p].name, parray[p2].name) != 0) && 
      (strcmp(parray[p].name, parray[p3].name) != 0) && 
      (strcmp(parray[p].name, parray[p4].name) != 0))  {
    pprintf(p, "%sYou must be one of the four players listed in the spair command", SendCode(p, ERROR));
    return COM_OK;
  }

  if ((parray[p1].game >= 0) ||
     (parray[p2].game >= 0) ||
     (parray[p3].game >= 0) ||
     (parray[p4].game >= 0)) {
    pprintf(p, "%sOne of the 4 listed players are already playing a game.",
                SendCode(p, ERROR));
    return COM_OK;
  }

  pprintf(p1, "%sWelcome to NNGS' Pair Go!\n", SendCode(p1, INFO));
  pprintf(p2, "%sWelcome to NNGS' Pair Go!\n", SendCode(p2, INFO));
  pprintf(p3, "%sWelcome to NNGS' Pair Go!\n", SendCode(p3, INFO));
  pprintf(p4, "%sWelcome to NNGS' Pair Go!\n", SendCode(p4, INFO));

  pprintf(p1, "%sYou, and %s are the White team.\n", 
                SendCode(p1, INFO), parray[p2].name);
  pprintf(p2, "%sYou, and %s are the White team.\n", 
                SendCode(p2, INFO), parray[p1].name);
  pprintf(p3, "%sYou, and %s are the Black team.\n", 
                SendCode(p3, INFO), parray[p4].name);
  pprintf(p4, "%sYou, and %s are the Black team.\n", 
                SendCode(p4, INFO), parray[p3].name);

  pprintf(p3, "%sImportant to remember: do not move until after the games are paired.\n", SendCode(p3, INFO));
  pprintf(p4, "%sImportant to remember: do not move until after the games are paired.\n", SendCode(p4, INFO));

  pprintf(p1, "%sFirst, create a match against your opponent, %s.\n", 
               SendCode(p1, INFO), parray[p3].name);
  pprintf(p2, "%sFirst, create a match against your opponent, %s.\n", 
               SendCode(p2, INFO), parray[p4].name);

  pprintf(p3, "%sYour opponent %s will first request a match with you.\n",
               SendCode(p3, INFO), parray[p1].name);
  pprintf(p4, "%sYour opponent %s will first request a match with you.\n",
               SendCode(p4, INFO), parray[p2].name);

  pprintf(p1, "%sType: match %s w size time byo-time\n",
               SendCode(p1, INFO), parray[p3].name);
  pprintf(p2, "%sType: match %s w size time byo-time\n",
               SendCode(p2, INFO), parray[p4].name);

  pprintf(p1, "%sOnce your match is created, check to see what %s's game number is.\n", SendCode(p1, INFO), parray[p2].name);         
  pprintf(p2, "%sOnce your match is created, check to see what %s's game number is.\n", SendCode(p2, INFO), parray[p1].name);         

  pprintf(p1, "%sThen type \"pair #\" where # is %s's game number.\n", 
               SendCode(p1, INFO), parray[p2].name);
  pprintf(p2, "%sThen type \"pair #\" where # is %s's game number.\n", 
               SendCode(p2, INFO), parray[p1].name);
  
  pprintf(p3, "%sSit back and wait for the message \"Game has been paired\", then move.\n", SendCode(p3, INFO));
  pprintf(p4, "%sSit back and wait for the message \"Game has been paired\", then move.\n", SendCode(p4, INFO));

  pprintf_prompt(p1, "%sGood Luck!\n", SendCode(p1, INFO));
  pprintf_prompt(p2, "%sGood Luck!\n", SendCode(p2, INFO));
  pprintf_prompt(p3, "%sGood Luck!\n", SendCode(p3, INFO));
  pprintf_prompt(p4, "%sGood Luck!\n", SendCode(p4, INFO));
  
  return COM_OKN;
}

#ifndef SGI
PUBLIC int com_nrating(int p, param_list param)
{
#ifdef NNGSRATED
  char name[20];
  rdbm_t db;
  rdbm_player_t rp;

  if (param[0].type == (int)TYPE_NULL)
    strncpy(name, parray[p].name, 20);
  else
    strncpy(name, param[0].val.word, 20);
  name[19] = '\0';

  if ((db = rdbm_open("/home/nngs/src/ratdb",0)) == NULL)
  {
    pprintf(p, "%sAn unlucky chance occured\n", SendCode(p, ERROR));
    return COM_OK;
  }
  if (!rdbm_fetch(db, name, &rp))
    pprintf(p, "%sNo rating information for %s\n",
	    SendCode(p, INFO), name);
  else
    pprintf(p, "%s%s: %s%c (%.2f)\n\
%sRange: %.2f stones (%.2f %+.2f)\n\
%sRated games: %u (%u wins, %u losses)\n",
	    SendCode(p, INFO), rp.name, rp.rank, (rp.star ? '*' : ' '),
	    rp.rating,
	    SendCode(p, INFO), rp.high-rp.low, rp.low, rp.high,
	    SendCode(p, INFO), rp.wins+rp.losses, rp.wins, rp.losses);
  rdbm_close(db);
#endif /* NNGSRATED */
  return COM_OK;
}

#endif
#ifdef NNGSRATED
PUBLIC int com_rating(int p, param_list param)
{
  char line[180];
  char SearchFor[20];
  FILE *fp;
  char name[11], rank[10];
  char junk1[5], junk2[5];
  int rating, numgam;
  float numeric_rank, confidence, range_high, range_low;
  int all_rated_wins, all_rated_losses,
      rated_wins_vs_rated, rated_losses_vs_rated,
      high_confidence_wins, high_confidence_losses;
  int found = 0;

  if (param[0].type == (int) TYPE_NULL) {
    strncpy(SearchFor, parray[p].name, 20);
  }
  else strncpy(SearchFor, param[0].val.word, 20);
  SearchFor[19] = '\0';

  fp = fopen(ratings_file, "r");
  if(!fp) {
    pprintf(p, "%sAn unknown error occured\n", SendCode(p, ERROR));
    return COM_OK;
  }
  while((fscanf(fp, "%s %s %d %d", name, rank, &rating, &numgam)) == 4) {
    if(!strcasecmp(name, SearchFor)) {
      pprintf(p,"%s%s: %s (%d) Rated Games: %d\n", SendCode(p, INFO),
                name, rank, rating, numgam);
      break;
    }
  }
  fclose(fp);
  fp = fopen("/home/nngs/ratings/pett/results", "r");
  if(!fp) {
    pprintf(p, "%sAn unknown error occured\n", SendCode(p, ERROR));
    return COM_OK;
  }

  while (fgets(line, 179, fp)) {
    if(sscanf(line, "%s %f %s %f %f/%f %d-%d %d-%d %d-%d",
                     name, &numeric_rank,
                     rank, &confidence,
                     &range_high, &range_low,
                     &all_rated_wins, &all_rated_losses,
                     &rated_wins_vs_rated, &rated_losses_vs_rated,
                     &high_confidence_wins, &high_confidence_losses) == 12) {
      if(!strcasecmp(name, SearchFor)) {
        found = 1;
        pprintf(p, "%sConfidence = %.2f percent\n\
%sAll rated wins: %d\n\
%sAll rated losses: %d\n\
%sRated wins versus rated players: %d\n\
%sRated losses versus rated players: %d\n\
%sHigh confidence wins: %d\n\
%sHigh confidence losses: %d\n",
                SendCode(p, INFO), confidence * 100, 
                SendCode(p, INFO), all_rated_wins, 
                SendCode(p, INFO), all_rated_losses,
                SendCode(p, INFO), rated_wins_vs_rated, 
                SendCode(p, INFO), rated_losses_vs_rated,
                SendCode(p, INFO), high_confidence_wins, 
                SendCode(p, INFO), high_confidence_losses);
        break;
      }
    }
    else if(sscanf(line, "%s %s %s %f %s %d-%d %d-%d %d-%d",
                     name, junk1,
                     rank, &confidence,
                     junk2,
                     &all_rated_wins, &all_rated_losses,
                     &rated_wins_vs_rated, &rated_losses_vs_rated,
                     &high_confidence_wins, &high_confidence_losses) == 11) {
      if(!strcasecmp(name, SearchFor)) {
        found = 1;
        pprintf(p, "%s%s is Not Rated\n\
%sAll rated wins: %d\n\
%sAll rated losses: %d\n\
%sRated wins versus rated players: %d\n\
%sRated losses versus rated players: %d\n\
%sHigh confidence wins: %d\n\
%sHigh confidence losses: %d\n",
                SendCode(p, INFO), name,
                SendCode(p, INFO), all_rated_wins, 
                SendCode(p, INFO), all_rated_losses,
                SendCode(p, INFO), rated_wins_vs_rated, 
                SendCode(p, INFO), rated_losses_vs_rated,
                SendCode(p, INFO), high_confidence_wins, 
                SendCode(p, INFO), high_confidence_losses);
        break;
      }
    }
  }
  fclose(fp);
  if(!found) pprintf(p, "%sNo rating information for %s\n", SendCode(p, INFO),
                      SearchFor);
  return COM_OK;
}
#endif

PUBLIC int com_translate(int p, param_list param)
{
#define BUFSIZE 1024 
  char cmd[1000];
  char buf[BUFSIZE];
  FILE *ptr;

  if(!safestring(param[0].val.word)) {
    pprintf(p, "%sSorry, your query contains invalid characters.",
                SendCode(p, ERROR));
    return COM_OK;
  }

  sprintf(cmd, "%s %s", intergo_file, param[0].val.word);

  if ((ptr = (FILE *)popen(cmd, "r")) != NULL) {
    while (fgets(buf, BUFSIZE, ptr) != NULL)
      pprintf(p, "%s%s", SendCode(p, TRANS), buf);
    pclose(ptr);
  }
  return COM_OKN;
}

PUBLIC int com_ping(int p, param_list param)
{
  pprintf(p, "%sPONG", SendCode(p, PING));
  return COM_OK;
}

PUBLIC int com_find(int p, param_list param)
{
  char buf[MAX_LINE_SIZE];
  int i;
  FILE *fp;
  
  bldSD(param[0].val.string);

  if((fp = fopen(DEFAULT_FIND, "r")) != NULL) {
    while (fgets(buf, MAX_LINE_SIZE, fp)) {
      i = strlen(buf);
      buf[i - 1] = '\0';       
      if(blank(buf)) {
        continue;
      } else {

        const SearchResult *psr = (const SearchResult *) search(buf);

        if (psr) {
          pprintf(p, "%sMatched player: %-10s at address %s\n", 
                        SendCode(p, INFO), psr->szPlayer, psr->szMailAddr);
        }
      }
    }
    fclose(fp);
  }
  return COM_OKN;
}

#ifdef NOTIFY
PUBLIC int com_notify(int p, param_list param)
{
  int i, p1, connected;

  if (param[0].type != TYPE_WORD) {
    if (!parray[p].num_notify) {
      pprintf(p, "%sYour notify list is empty.\n", SendCode(p, ERROR));
      return COM_OK;
    }
    else {
      pprintf(p, "%s-- Your notify list contains %d names: --\n",
                  SendCode(p, INFO),
                  parray[p].num_notify);

      for(i = 0; i < parray[p].num_notify; i++) {
        pprintf(p, "%s%s\n",SendCode(p, INFO), parray[p].notifyList[i]);
      }
      return COM_OK;
    }
  }
  else if(param[0].type == TYPE_WORD) {
    if(parray[p].num_notify >= MAX_NOTIFY) {
      pprintf(p, "%sSorry, your notify list is already full.\n", 
                   SendCode(p, ERROR));
      return COM_OK;
    }
    p1 = player_search(p, param[0].val.word);
    if(!p1) return COM_OK;
    if(p1 < 0) {  /* player had to be connected and will be removed later */
      connected = 0;
      p1 = (-p1) - 1;
    } else {
      connected = 1;
      p1 = p1 - 1;
    }
    for (i = 0; i < parray[p].num_notify; i++) {
      if (!strcasecmp(parray[p].notifyList[i], parray[p1].name)) {
        pprintf(p, "%sYour notify list already includes %s.\n", 
                      SendCode(p, ERROR), parray[p1].name);
        if(!connected) player_remove(p1);
        return COM_OK;
      }
    }
    if(p1 == p) {
      pprintf(p, "%sYou can't notify yourself.\n", SendCode(p, ERROR));
      return COM_OK;
    }
    parray[p].notifyList[parray[p].num_notify++] = (char *) strdup(parray[p1].name);
    pprintf(p, "%s%s is now on your notify list.\n", 
                  SendCode(p, INFO), parray[p1].name);
    if(!connected) player_remove(p1);
    return COM_OK;
  }
  return COM_BADPARAMETERS;
}

PUBLIC int com_unnotify(int p, param_list param)
{
  char *pname = NULL;
  int i;
  int unc = 0;

  if (param[0].type == TYPE_WORD) {
    pname = param[0].val.word;
  }
  for (i = 0; i < parray[p].num_notify; i++) {
    if (!pname || !strcasecmp(pname, parray[p].notifyList[i])) {
      pprintf(p, "%s%s is removed from your notify list.\n", 
                SendCode(p, INFO), parray[p].notifyList[i]);
      free(parray[p].notifyList[i]);
      parray[p].notifyList[i] = NULL;
      if(i == (parray[p].num_notify) - 1) {
        parray[p].num_notify--;
      }
      unc++;
    }
  }
  if (unc) {
    for (i = 0; i < parray[p].num_notify; i++) {
      if (!parray[p].notifyList[i]) {
        if(parray[p].notifyList[(parray[p].num_notify) - 1]) {
          parray[p].notifyList[i] = parray[p].notifyList[(parray[p].num_notify) - 1];
          i = i - 1;
          parray[p].num_notify = parray[p].num_notify - 1;
        }
      }
    }
  } else {
    pprintf(p, "%sNo one was removed from your notify list.\n", 
                  SendCode(p, ERROR));
  }
  return COM_OK;
}

PUBLIC int com_znotify(int p, param_list param)
{
  int p1, count = 0;

  for (p1 = 0; p1 < p_num; p1++) {
    if (player_notified(p, p1)) {
      if (!count)
        pprintf(p, "%sPresent company on your notify list:\n%s", 
                    SendCode(p, INFO), SendCode(p, INFO));
      pprintf(p, " %s", parray[p1].name);
      count++;
    }
  }
  if (count)
    pprintf(p, ".\n");
  else
    pprintf(p, "%sNo one from your notify list is logged on.\n", 
                 SendCode(p, INFO));

  count = 0;
  for (p1 = 0; p1 < p_num; p1++) {
    if (player_notified(p1, p)) {
      if (!count)
        pprintf(p,"%sThe following players have you on their notify list:\n%s", 
                SendCode(p, INFO), SendCode(p, INFO));
      pprintf(p, " %s", parray[p1].name);
      count++;
    }
  }
  if (count)
    pprintf(p, ".\n");
  else
    pprintf(p, "%sNo one logged in has you on their notify list.\n", 
               SendCode(p,INFO));
  return COM_OK;
}

PUBLIC int player_notified(int p, int p1)
/* is p1 on p's notify list? */
{
  int i;
 
  /* possible bug: p has just arrived! */
  if(!parray[p].name)  return 0;
 
  for (i = 0; i < parray[p].num_notify; i++) {
    if (parray[p1].status == PLAYER_PROMPT) {
      if (!strcasecmp(parray[p].notifyList[i], parray[p1].name))
        return 1;
    }
  }
  return 0;
}

PUBLIC void player_notify_departure(int p)
/* Notify those with notifiedby set on a departure */
{
  int p1;
  for (p1 = 0; p1 < p_num; p1++) {
    if ((parray[p1].notifiedby) && 
        (!player_notified(p1, p)) && (player_notified(p, p1))) {
      if (parray[p1].bell)
        pprintf(p1, "%s^G\n", SendCode(p1, BEEP));
      pprintf_prompt(p1, "%sNotification: %s has departed and isn't on your notify list.\n", SendCode(p1, ERROR), parray[p].name);
    }
  }
}

PUBLIC int player_notify_present(int p)
/* output Your arrival was notified by..... */
/* also notify those with notifiedby set if necessary */
{
  int p1;
  int count = 0;

  for (p1 = 0; p1 < p_num; p1++) {
    if (player_notified(p, p1)) {
      if (!count) {
        pprintf(p, "%sPresent company includes:", SendCode(p, INFO));
      }
      count++;
      pprintf(p, " %s", parray[p1].name);
      if ((parray[p1].notifiedby) && (!player_notified(p1, p))) {
        if (parray[p1].bell)
          pprintf(p1, "%s\007\n", SendCode(p1, BEEP));
          pprintf_prompt(p1, "%sNotification: %s has arrived and isn't on your notify list.\n", SendCode(p1, ERROR), parray[p].name);
      }
    }
  }
  if (count)
    pprintf(p, ".\n");
  return count;
}

PUBLIC int player_notify(int p, char *note1, char *note2)
/* notify those interested that p has arrived/departed */
{
  int p1;
  int count = 0;

  for (p1 = 0; p1 < p_num; p1++) {
    if (player_notified(p1, p)) {
      if (parray[p1].bell)
        pprintf(p1, "%s\007\n", SendCode(p1, BEEP));
      pprintf_prompt(p1, "%sNotification: %s has %s.\n", SendCode(p1, INFO), parray[p].name, note1);
      if (!count) {
        pprintf(p, "%sYour %s was noted by:", SendCode(p, INFO), note2);
      }
      count++;
      pprintf(p, " %s", parray[p1].name);
    }
  }
  if (count)
    pprintf(p, ".\n");
  return count;
}
#endif


/**************************************************************************
 * automatch.c               by Erik Ekholm                Created 960305 *
 **************************************************************************/


/* KOMIn is the standard komi for an even game on an nxn board. The value of
   one stone is twice as much. */

#define KOMI19 5.5  /*standard komi = 5.5 => one stone is worth 11 points
		      From this follows that each komi point corresponds
		      to a 9 (100/11) point rating difference */

#define KOMI13 8.5  /* From the Go FAQ. (Note BTW that the table in the FAQ
		       isn't quite consistent; the gap between kyu diff.
		       3 and 4 is much larger than between 2 and 3.) */

#define KOMI9  5.5  /* 5.5 according to some pro-pro games. */



/* STONEPTSn is how many points in the rating system a stone on an nxn board
   corresponds to. The figures for smaller boards could be changed to fit
   statistical data better. */

#define STONEPTS19 100    /* 100 pts = one grade at NNGS */
#define STONEPTS13 300    /* 1 stone at 13x13 is 3 kyu grades, see Go FAQ. */
#define STONEPTS9  600    /* My best guess */


/* THRESHOLDn decides at which rating difference handicap is increased.
   Avoiding negative komi might be a good idea, since it makes it harder
   for Black to succeed with a mirror go strategy. */

#define THRESHOLD19 0  /* Values from 0 to 5 possible. 5 avoids neg. komi. */
#define THRESHOLD13 0  /* 0 - 8 possible. 8 avoids neg. komi */
#define THRESHOLD9  0  /* 0 - 5 possible. 5 avoids neg. komi */


/* OFFSETn is used for eliminating round-off errors */

#define OFFSET19 ((STONEPTS19 + 2 * KOMI19) / (KOMI19 * 4))
#define OFFSET13 ((STONEPTS13 + 2 * KOMI13) / (KOMI13 * 4))
#define OFFSET9  ((STONEPTS9  + 2 * KOMI9)  / (KOMI9  * 4))


/*---------------------------------------------------------------------------
 * AutoMatch sets stones and komi given positive ratingdiff (in NNGS rating
 * points) and boardsize (for 19, 13 and 9), so that both players have as
 * close to 50% chance of winning as possible.
 *
 * Please note that an even game is represented as a game with 1 stone 5.5
 * komi, thus NOT a zero stone game
 */

void AutoMatch(int ratingdiff, int boardsize, int *stones, float *komi)
{
  int units;  /* help variable, number of total komi units. */

  switch ( boardsize ) {
    
  case 19:
    units = (ratingdiff + OFFSET19) / (STONEPTS19 / (KOMI19 * 2.0));
    *stones = 1 + (units + THRESHOLD19) / (KOMI19 * 2);
    *komi = KOMI19 - units + (KOMI19 * 2) * (*stones - 1);
    break;

  case 13:
    units = (ratingdiff + OFFSET13) / (STONEPTS13 / (KOMI13 * 2.0));
    *stones = 1 + (units + THRESHOLD13) / (KOMI13 * 2);
    *komi = KOMI13 - units + (KOMI13 * 2) * (*stones - 1);
    break;

  case 9:
    units = (ratingdiff + OFFSET9) / (STONEPTS9 / (KOMI9 * 2.0));
    *stones = 1 + (units + THRESHOLD9) / (KOMI9 * 2);
    *komi = KOMI9 - units + (KOMI9 * 2) * (*stones - 1);
    break;
  }
}
  
/* I have not approached the question of how to deal with rating differences
   of more than 9 stones.  Solutions: 1) Allowed, use free or default
   placement.  2) Use max 9 stones, but larger komi. 3) Truncate at 9
   stones. 4) Let the match command reject such automatch requests.  */

