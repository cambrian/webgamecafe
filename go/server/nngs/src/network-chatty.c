/*
 * network.c
*/

/*
    NNGS - The No Name Go Server
    Copyright (C) 1995-1996  Bill Shubert (wms@hevanet.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/



#include <sys/select.h>
#include <socketbits.h>

#include "stdinclude.h"
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <errno.h>
#include <ctype.h>
#ifdef SYSTEM_SUN5
#include <sys/filio.h>
#endif
#ifdef AIX
#include <sys/select.h>
#endif

#include "common.h"
#include "network.h"
#include "bm.h"
#include "utils.h"

/*#define  buglog(x)  Logit x */
#define  buglog(x) 

/**********************************************************************
 * Data types
 **********************************************************************/

typedef enum  {
  netState_empty, netState_connected, netState_listening
} NetState;


typedef struct Net_struct  {
  NetState  state;
  unsigned int fromHost;
  int telnetState;
  
  /* Input buffering */
  int  inEnd; /* The amount of data in the inBuf. */
  int  cmdEnd;  /* The end of the first command in the buffer. */
  char  *inParseDest, *inParseSrc;
  int  inFull, inThrottled;
  unsigned char  inBuf[MAX_STRING_LENGTH];

  /*
   * For output buffering, we use a circular buffer.  
   */
  int  outLen;
  int  outEnd;  /* How many bytes waiting to be written? */
  char *outBuf;                 /* our send buffer, or NULL if none yet */
} Net;


/**********************************************************************
 * Static variables
 **********************************************************************/

/*
 * conns hold all data associated with an i/o connection.  It is indexed by
 *   file descriptor.
 */
static Net  conns[net_maxConn];

/*
 * listenFds are the file descriptors connected to the port we are listening on
 *   to receive incoming connections.
 */
static int  numListenFds = 0;
static int  listenFds[net_maxListens];

/*
 * We keep these fd_sets up to date so that we don't have to reconstruct them
 *   every time we want to do a select().
 */
static fd_set  readSet, writeSet;


/**********************************************************************
 * Forward declarations
 **********************************************************************/

static int  newConnection(int fd);
static int  serviceWrite(int fd);
static int  serviceRead(int fd);
static int  clearCmd(int fd);
static int  checkForCmd(int fd);
static void  initConn(int fd);
static void  flushWrites(void);


/**********************************************************************
 * Functions
 **********************************************************************/

static void
set_nonblocking(int s)
{
  int flags;

  if ((flags = fcntl(s, F_GETFL, 0)) >= 0)
  {
    flags |= O_NONBLOCK;
    fcntl(s, F_SETFL, flags);
  }
}

/*
 * Every time you call this, another fd is added to the listen list.
 */
int net_init(int port)  {
  static int  firstTime = 1;
  int  i;
  int  opt;
  struct sockaddr_in  addr;
  struct linger  lingerOpt;

  assert(net_maxConn <= FD_SETSIZE);
  assert(numListenFds < net_maxListens);
  if (firstTime)  {
    firstTime = 0;
    for (i = 0;  i < net_maxConn;  ++i)  {
      /*
       * Set up all conns to be ignored.
       */
      conns[i].state = netState_empty;
      conns[i].cmdEnd = 0;
      conns[i].outEnd = 0;
      conns[i].outBuf = NULL;
    }
    FD_ZERO(&readSet);
    FD_ZERO(&writeSet);
    
    /*
     * Set up the console.
     * On second thought, don't.
     *
     * initConn(0);
     * conns[0].fromHost = 0;
     */
  }

  /* Open a TCP socket (an Internet stream socket). */
  if ((listenFds[numListenFds] = 
       socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "NNGS: can't open stream socket\n");
    return -1;
  }
  /* Bind our local address so that the client can send to us */
  memset((void *)&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons(port);

  /* added in an attempt to allow rebinding to the port */
  opt = 1;
  setsockopt(listenFds[numListenFds],
	     SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  opt = 1;
  setsockopt(listenFds[numListenFds],
	     SOL_SOCKET, SO_KEEPALIVE, &opt, sizeof(opt));
  lingerOpt.l_onoff = 0;
  lingerOpt.l_linger = 0;
  setsockopt(listenFds[numListenFds],
	     SOL_SOCKET, SO_LINGER, &lingerOpt, sizeof(lingerOpt));

  if (Debug > 99) {
    opt = 1;
    setsockopt(listenFds[numListenFds],
	       SOL_SOCKET, SO_DEBUG, &opt, sizeof(opt));
  }

  if (bind(listenFds[numListenFds],
	   (struct sockaddr *)&addr, sizeof(addr)) < 0)  {
    fprintf(stderr, "NNGS: can't bind local address.  errno=%d\n", errno);
    return -1;
  }
#if 0
  /* [PEM]: I'm not sure this works any more. */
  opt = 1;
  ioctl(listenFds[numListenFds], FIONBIO, &opt);
#else
  set_nonblocking(listenFds[numListenFds]);
#endif

  listen(listenFds[numListenFds], 5);

  FD_SET(listenFds[numListenFds], &readSet);
  conns[listenFds[numListenFds]].state = netState_listening;
  ++numListenFds;
  return(0);
}


/*
 * net_select spins, doing work as it arrives.
 *
 * It is written to prevent any one user from sucking up too much server time
 *   by sending tons of commands at once. It consists essentially of two
 *   loops:
 *
 * for (;;) {
 *   select() to find out which connections have input data.
 *   Loop over all connections {
 *     If the connection has input data waiting, read it in.
 *     If the connection has data in its input buffer (from a previous loop
 *       or from just being read in), then process *ONE* command.
 *   }
 * }
 *
 * From this basic system there are some optomizations. If we manage to
 *   process every command waiting in an inner loop, then we sleep in the
 *   "select()" call since we have nothing else to do. If we leave a command
 *   in any of the input buffers (because there were two or more there
 *   before), then we don't wait in the "select()" and just poll.
 * Also, each connection has a maximum amount of data that will be buffered.
 *   Once the connection has all of its buffer space filled, we no longer
 *   try to read in more data. This will make the data back up on the client,
 *   keeping us from using up too much memory when a client sends tons of
 *   commands at once.
 */
void  net_select(int timeout)  {
  int  i, numConns;
  fd_set  read, write;
  struct timeval  timer;
  /*
   * When moreWork is TRUE, that means that we have data we have read in but
   *   not processed yet, so we should poll with the select() instead of
   *   sleep for a second.
   */
  int  moreWork = 0;
  int  lastClock = time(NULL), thisClock, newConn;

  /*
   * If we returned a command from the last call, then we now have to
   *   clear the command from the buffer.
   */
  buglog(("net_select  {"));
  for (;;)  {
    read = readSet;
    write = writeSet;
    timer.tv_usec = 0;
    if (moreWork) {
      /* Poll. */
      timer.tv_sec = 0;
    } else {
      /* Have nothing better to do than sit and wait for data. */
      timer.tv_sec = 1;
    }
    flushWrites();
    numConns = select(net_maxConn, &read, &write, NULL, &timer);
    if (numConns < 0)  {
      if (errno == EBADF)  {
	Logit("EBADF error from select ---");
	abort();
      } else if (errno == EINTR)  {
	Logit("select interrupted by signal, continuing ---");
	continue;
      } else if (errno == EINVAL)  {
	Logit("select returned EINVAL status ---");
	abort();
      } else  {
	Logit("Unknown error (%d) from select ---", errno);
	continue;
      }
    }

    /* Before every loop we clear moreWork. Then if we find a conn with more
     *   than one command in its input buffer, we process that command, then
     *   set moreWork to indicate that even after this work loop is done we
     *   have more work to do.
     */
    moreWork = 0;
    thisClock = time(NULL);
    if (thisClock != lastClock)  {
      lastClock = thisClock;
      if (process_heartbeat(&i) == COM_LOGOUT)  {
	process_disconnection(i);
	net_close(i);
	FD_CLR(i, &read);
	FD_CLR(i, &write);
      }
    }
    for (i = 0;  i < net_maxConn;  ++i)  {
      if (conns[i].cmdEnd)  {
	buglog(("%3d: Command \"%s\" left in buf", i, conns[i].inBuf));	
	if (process_input(i, conns[i].inBuf) == COM_LOGOUT)  {
	  process_disconnection(i);
	  net_close(i);
	  FD_CLR(i, &read);
	  FD_CLR(i, &write);
	} else  {
	  if (clearCmd(i) == -1)  {
	    buglog(("%3d: Closing", i));
	    process_disconnection(i);
	    net_close(i);
	    FD_CLR(i, &read);
	    FD_CLR(i, &write);
	  } else  {
	    if (conns[i].cmdEnd) {
	      /*
	       * We still have more data in our input buffer, so set the
	       *   moreWork flag.
	       */
	      moreWork = 1;
	    }
	  }
	}
      } else if (FD_ISSET(i, &read))  {
	if (conns[i].state == netState_listening)  {
	  /* A new inbound connection. */
	  newConn = newConnection(i);
	  buglog(("%d: New conn", newConn));
	  if (newConn >= 0)  {
	    process_new_connection(newConn, net_connectedHost(newConn));
	  }
	} else  {
	  /* New incoming data. */
	  buglog(("%d: Ready for read", i));
	  if (conns[i].state == netState_connected)  {
	    assert(conns[i].state == netState_connected);
	    assert(!conns[i].inFull);
	    if (serviceRead(i) == -1)  {
	      buglog(("%d: Closed", i));
	      net_close(i);
	      FD_CLR(i, &read);
	      FD_CLR(i, &write);
	      process_disconnection(i);
	    } else  {
	      if (conns[i].cmdEnd)  {
		if (process_input(i, conns[i].inBuf) == COM_LOGOUT)  {
		  process_disconnection(i);
		  net_close(i);
		  FD_CLR(i, &read);
		  FD_CLR(i, &write);
		} else  {
		  if (clearCmd(i) == -1)  {
		    buglog(("%3d: Closing", i));
		    process_disconnection(i);
		    net_close(i);
		    FD_CLR(i, &read);
		    FD_CLR(i, &write);
		  } else  {
		    if (conns[i].cmdEnd) {
		      /*
		       * We still have more data in our input buffer, so set
		       *   the moreWork flag.
		       */
		      moreWork = 1;
		    }
		  }
		}
	      }
	    }
	  } else  {
	    /* It is not connected. */
	    assert(!FD_ISSET(i, &readSet));
	  }
	}
      }
    }
  }
}


static int  newConnection(int listenFd)  {
  int  newFd;
  struct sockaddr_in  addr;
  int  addrLen = sizeof(addr);

  newFd = accept(listenFd, (struct sockaddr *)&addr, &addrLen);
  if (newFd < 0)
    return(newFd);
  if (newFd >= net_maxConn)  {
    close(newFd);
    return(-1);
  }
  assert(conns[newFd].state == netState_empty);
  set_nonblocking(newFd);

  initConn(newFd);
  conns[newFd].fromHost = addr.sin_addr.s_addr;

  /* Logit("New connection on fd %d ---", newFd); */
  return(newFd);
}


static void  initConn(int fd)  {
  conns[fd].state = netState_connected;
  conns[fd].telnetState = 0;

  conns[fd].inEnd = 0;
  conns[fd].cmdEnd = 0;
  conns[fd].inParseSrc = conns[fd].inBuf;
  conns[fd].inParseDest = conns[fd].inBuf;
  conns[fd].inFull = 0;
  conns[fd].inThrottled = 0;

  conns[fd].outLen = net_defaultOutBufLen;
  conns[fd].outEnd = 0;
  conns[fd].outBuf = malloc(net_defaultOutBufLen);
  FD_SET(fd, &readSet);
}


static void  flushWrites(void)  {
  int  fd;

  for (fd = 0;  fd < net_maxConn;  ++fd)  {
    if ((conns[fd].state == netState_connected) &&
	(conns[fd].outEnd > 0))  {
      if (serviceWrite(fd) < 0)  {
	buglog(("%3d: Write failed.", fd));
	conns[fd].outEnd = 0;
      }
    }
  }
}


static int  serviceWrite(int fd)  {
  int  writeAmt;
  Net  *conn = &conns[fd];

  assert(conn->state == netState_connected);
  assert(conn->outEnd > 0);
  writeAmt = write(fd, conn->outBuf, conn->outEnd);
  if (writeAmt < 0)  {
    if (errno == EAGAIN)
      writeAmt = 0;
    else  {
      return(-1);
    }
  }
  assert(writeAmt <= conn->outEnd);
  if (writeAmt == conn->outEnd)  {
    conn->outEnd = 0;
    FD_CLR(fd, &writeSet);
    if (conn->inThrottled)  {
      conn->inThrottled = 0;
      if (!conn->inFull)
	FD_SET(fd, &readSet);
    }
  } else  {
    fprintf(stderr, "Could write only %d of %d bytes to fd %d.\n",
	    writeAmt, conn->outEnd, fd);
    /*
     * This memmove is costly, but on ra (where NNGS runs) the TCP/IP
     *   sockets have 60K buffers so this should only happen when netlag
     *   is completely choking you, in which case it will happen like once
     *   and then never again since your writeAmt will be 0 until the netlag
     *   ends.  I pity the fool who has netlag this bad and keep playing!
     */
    if (writeAmt > 0)
      memmove(conn->outBuf, conn->outBuf + writeAmt, conn->outEnd - writeAmt);
    conn->outEnd -= writeAmt;
    if (!conn->inThrottled)  {
      conn->inThrottled = 1;
      FD_CLR(fd, &readSet);
    }
  }
  return(0);
}


static int  clearCmd(int fd)  {
  Net  *conn = &conns[fd];
  int  i;

  if (conn->state != netState_connected)
    return(0);
  assert(conn->cmdEnd);
  assert(conn->cmdEnd <= conn->inEnd);
  if (conn->cmdEnd == conn->inEnd)  {
    conn->inParseDest -= conn->cmdEnd;
    conn->inParseSrc -= conn->cmdEnd;
    conn->inEnd = 0;
    conn->cmdEnd = 0;
  } else  {
    for (i = conn->cmdEnd;  i < conn->inEnd;  ++i)  {
      conn->inBuf[i - conn->cmdEnd] = conn->inBuf[i];
    }
    conn->inEnd -= conn->cmdEnd;
    conn->inParseDest -= conn->cmdEnd;
    conn->inParseSrc -= conn->cmdEnd;
    conn->cmdEnd = 0;
  }
  if (checkForCmd(fd) == -1)
    return(-1);
  if (conn->inFull)  {
    conn->inFull = 0;
    if (!conn->inThrottled)  {
      FD_SET(fd, &readSet);
    }
  }
  return(0);
}


static int  checkForCmd(int fd)  {
  Net  *conn = &conns[fd];
  int  i;
  unsigned char  c;
  unsigned char  *dest, *src;
  static unsigned char will_tm[] = {IAC, WILL, TELOPT_TM, '\0'};
  static unsigned char will_sga[] = {IAC, WILL, TELOPT_SGA, '\0'};
  static unsigned char ayt[] = "[Responding to AYT: Yes, I'm here.]\n";

  dest = conn->inParseDest;
  src = conn->inParseSrc;
  for (i = src - conn->inBuf;  i < conn->inEnd;  ++i, ++src)  {
    c = (unsigned char)*src;
    switch (conn->telnetState) {
    case 0:                     /* Haven't skipped over any control chars or
                                   telnet commands */
      if (c == IAC) {
        conn->telnetState = 1;
      } else if ((c == '\n') || (c == '\r') || ( c == '\004')) {
	*dest = '\0';
	++i;
	while ((i < conn->inEnd) &&
	       ((conn->inBuf[i] == '\n') || (conn->inBuf[i] == '\r')))
	  ++i;
	conn->cmdEnd = i;
	conn->telnetState = 0;
	conn->inParseSrc = conn->inBuf + i;
	conn->inParseDest = conn->inBuf + i;
	return(0);
      } else if (!isprint(c) && c <= 127) {/* no idea what this means */
        conn->telnetState = 0;
      } else  {
        *(dest++) = c;
      }
      break;
    case 1:                /* got telnet IAC */
      *src = '\n';
      if (c == IP)  {
        return(-1);            /* ^C = logout */
      } else if (c == DO)  {
        conn->telnetState = 4;
      } else if ((c == WILL) || (c == DONT) || (c == WONT))  {
        conn->telnetState = 3;   /* this is cheesy, but we aren't using em */
      } else if (c == AYT) {
        net_send(conn - conns, ayt, strlen((char *) ayt));
        conn->telnetState = 0;
      } else if (c == EL) {    /* erase line */
        dest = &conn->inBuf[0];
        conn->telnetState = 0;
      } else  {                  /* dunno what it is, so ignore it */
        conn->telnetState = 0;
      }
      break;
    case 3:                     /* some telnet junk we're ignoring */
      conn->telnetState = 0;
      break;
    case 4:                     /* got IAC DO */
      if (c == TELOPT_TM)
        net_send(fd, (char *) will_tm, strlen((char *) will_tm));
      else if (c == TELOPT_SGA)
        net_send(fd, (char *) will_sga, strlen((char *) will_sga));
      conn->telnetState = 0;
      break;
    default:
      assert(0);
    }
  }
  conn->inParseSrc = src;
  conn->inParseDest = dest;
  if (conn->inEnd == MAX_STRING_LENGTH)  {
    conn->inBuf[MAX_STRING_LENGTH - 1] = '\0';
    conn->cmdEnd = MAX_STRING_LENGTH;
  }
  return(0);
}


static int  serviceRead(int fd)  {
  int  readAmt;
  Net  *conn = &conns[fd];

  if(conn->inEnd > MAX_STRING_LENGTH) conn->inEnd = MAX_STRING_LENGTH -1;
  assert(conn->state == netState_connected);
  assert(conn->inEnd < MAX_STRING_LENGTH);
  readAmt = read(fd, conn->inBuf + conn->inEnd,
		 MAX_STRING_LENGTH - conn->inEnd);
  if (readAmt == 0)  {
    return(-1);
  }
  buglog(("    serviceRead(%d) read %d bytes\n", readAmt));
  if (readAmt < 0)  {
    if (errno == EAGAIN)  {
      return(0);
    } else  {
      net_close(fd);
      return(-1);
    }
  }
  conn->inEnd += readAmt;
  if (!conn->cmdEnd)
    return(checkForCmd(fd));
  return(0);
}


int  net_send(int fd, const char *src, int bufLen)  {
  int  i;
  char  *newBuf;
  Net  *net;

  if (fd == -1)
    return(0);
  assert((fd >= 0) && (fd < net_maxConn));
  net = &conns[fd];
  if (net->state != netState_connected)
    return(0);
  byte_count += (long) bufLen;
  for (i = 0;  i < bufLen;  ++i)  {
    if (*src == '\n')
      net->outBuf[net->outEnd++] = '\r';
    net->outBuf[net->outEnd++] = *(src++);

    if (net->outEnd + 2 >= net->outLen)  {
      newBuf = malloc(net->outLen *= 2);
      memcpy(newBuf, net->outBuf, net->outEnd);
      free(net->outBuf);
      net->outBuf = newBuf;
    }
  }
  FD_SET(fd, &writeSet);
  return(0);
}


void  net_close(int fd)  {
  Net  *conn = &conns[fd];

  if (conn->state != netState_connected)
    return;
  if (conn->outEnd > 0)
    write(fd, conn->outBuf, conn->outEnd);
  if(Debug) Logit("Disconnecting fd %d ---", fd);
  free(conn->outBuf);
  conn->outBuf = NULL;
  conn->state = netState_empty;
  conn->cmdEnd = 0;
  conn->outEnd = 0;
  FD_CLR(fd, &readSet);
  FD_CLR(fd, &writeSet);
  close(fd);
}


void  net_closeAll(void)  {
  int  i;

  for (i = 0;  i < net_maxConn;  ++i)  {
    net_close(i);
  }
}


int  net_connectedHost(int fd)  {
  return(conns[fd].fromHost);
}


void  net_echoOn(int fd)  {
  static unsigned char wont_echo[] = {IAC, WONT, TELOPT_ECHO};

  net_send(fd, (char *) wont_echo, 3);
}


void  net_echoOff(int fd)  {
  static unsigned char will_echo[] = {IAC, WILL, TELOPT_ECHO};

  net_send(fd, (char *) will_echo, 3);
}
