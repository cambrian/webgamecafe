#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/select.h>
#include <time.h>
#include <errno.h>

#define PORT 4242

static void
set_nonblocking(int s)
{
  int flags;

  if ((flags = fcntl(s, F_GETFL, 0)) >= 0)
  {
    flags |= O_NONBLOCK;
    fcntl(s, F_SETFL, flags);
  }
}

static void
print_int_opt(int s, int optarg, char *optname)
{
  int opt;
  unsigned optlen = sizeof(opt);

  if (getsockopt(s, SOL_SOCKET, optarg, &opt, &optlen) < 0)
    fprintf(stderr, "getsockopt(%s) failed\n", optname);
  else
    printf("%s = %d\n", optname, opt);
}

static void
print_opts(char *h, int s)
{
#if 0
  struct linger ling;
  unsigned optlen = sizeof(ling);
#endif

  printf("%s\n", h);
#if 0
  print_int_opt(s, SO_SNDBUF, "SO_SNDBUF");
  print_int_opt(s, SO_RCVBUF, "SO_RCVBUF");
  print_int_opt(s, SO_KEEPALIVE, "SO_KEEPALIVE");
  print_int_opt(s, SO_REUSEADDR, "SO_REUSEADDR");
  if (getsockopt(s, SOL_SOCKET, SO_LINGER, &ling, &optlen) < 0)
    fprintf(stderr, "getsockopt(%s) failed\n", "SO_LINGER");
  else
    printf("SO_LINGER: %d %u\n", ling.l_onoff, ling.l_linger);
#endif
  printf("O_NONBLOCK = %x\n", fcntl(s, F_GETFL, 0) & O_NONBLOCK);
}

int
main(int argc, char **argv)
{
  struct sockaddr_in addr;
  int s, ns, opt;
  unsigned addrlen, optlen;

  if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    fprintf(stderr, "socket() failed\n");
    exit(1);
  }

  print_opts("Listen socket 1", s);

  opt = 1; optlen = sizeof(opt);
  if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &opt, optlen) < 0)
  {
    fprintf(stderr, "setsockopt(SO_REUSEADDR) failed\n");
    exit(1);
  }
  opt = 1; optlen = sizeof(opt);
  if (setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, &opt, optlen) < 0)
  {
    fprintf(stderr, "setsockopt(SO_KEEPALIVE) failed\n");
    exit(1);
  }

  memset((void *)&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons(PORT);
  addrlen = sizeof(addr);

  if (bind(s, (struct sockaddr *)&addr, addrlen) < 0)
  {
    fprintf(stderr, "bind() failed\n");
    exit(1);
  }

  if (listen(s, 5))
  {
    fprintf(stderr, "listen() failed\n");
    exit(1);
  }

  set_nonblocking(s);

  print_opts("Listen socket 2", s);

  {
    fd_set rdset, wrset;
    int x;
#if 0
    struct timeval t;

    t.tv_sec = 1; t.tv_usec = 0;
#endif
    FD_ZERO(&rdset);
    FD_SET(s, &rdset);
    FD_ZERO(&wrset);
    FD_SET(s, &wrset);
    while ((x = select(s+1, &rdset, &wrset, NULL, NULL)) == 0)
    {
      printf("Tick!\n");
      /*      sleep(1);*/
    }
    if (x < 0)
    {
      fprintf(stderr, "select() failed\n");
      exit(1);
    }
    printf("x=%d\n", x);
  }

  if ((ns = accept(s, (struct sockaddr *)&addr, &addrlen)) < 0)
  {
    fprintf(stderr, "accept() failed\n");
    exit(1);
  }

  set_nonblocking(ns);
  print_opts("Accepted socket", ns);

  {
    int x = 5;

    while (x--)
    {
      time_t t0, t1;

      t0 = time(NULL);
      write(ns, "foo\n", 5);
      t1 = time(NULL);
      printf("write(..., 5) in %g s\n", difftime(t1,t0));
    }
  }

  close(ns);
  close(s);

  exit(0);
}
