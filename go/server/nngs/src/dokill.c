
#include <sys/types.h>
#include <sys/dir.h>
#include "stdinclude.h"

#include "common.h"
#include "nngsconfig.h"
#include "utils.h"
#include "playerdb.h"
#include "command.h"
#include "nngsmain.h"

extern time_t time();

PUBLIC int port, Ladder9, Ladder19, num_19, num_9, completed_games,
       num_logins, num_logouts, new_players, Debug, testing_8bit;

DIR *dirp;
struct direct *directp;

PRIVATE void usage(char *progname)
{
  fprintf(stderr, "Usage: %s [-l] [-n] UserName\n", progname);
  exit(1);
}

/* Parameters */
char *funame = NULL;

#define PASSLEN 4
PUBLIC int main(int argc, char *argv[])
{
  int i, p, result, let;
  char *letter[26] = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                       "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                       "w", "x", "y", "z" };
  char current_dir[256];

/*  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      default:
	usage(argv[0]);
	break;
      }
    } else {
      if (!funame)
	funame = argv[i];
      else
	usage(argv[0]);
    }
  }
  if (!funame)
    usage(argv[0]);
*/
  player_array_init();
  player_init();
  Debug = 0;

  for(let = 0; let < 26; let++) {
    sprintf(current_dir, "/2/nngs/src/new-players/%s", letter[let]);
    dirp = opendir(current_dir);
    while((directp = readdir(dirp)) != NULL) {
      if(strstr(directp->d_name, ".")) continue;
      /*printf("%s\n", directp->d_name);*/
      /*srand(time(0));*/
      p = player_search(p, directp->d_name);
      if(!p) continue;
      p = (-p) - 1;
/*      if (player_read(p, directp->d_name)) {
        fprintf(stderr, "%s doesn't exists.\n", funame);
        exit(0);
      }*/
      if(parray[p].lastHost == 0) {
        printf("%s\n", parray[p].name);
        player_kill(directp->d_name);
      }
      player_free(p);
    }
    closedir(dirp);
  }

/*  result = player_delete(p);
  printf("Deleted player >%s< (%d)\n", funame, result);
*/
}

