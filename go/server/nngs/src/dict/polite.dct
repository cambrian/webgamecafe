#########################################################################
#									#
#	    	       The Internet Go Dictionary			#
#									#
#				 by the					#
#		       The Internet Go Community			#
#									#
#-----------------------------------------------------------------------#
# File    : polite.dct 						#
# Purpose : Courtesy phrases in various languages			#
# Version : 1.10 						#
# Modified: 9/13/93 10:56:21						#
# E-mail  : Jim Yu            (jyu@eecs.nwu.edu)			#
# E-mail  : Jan van der Steen (jansteen@cwi.nl) 			#
#########################################################################
#
# The Go Dictionary is a joint effort by the Internet Go community.
# You would help your fellow country men and the Go community as a
# whole by adding definitions/translations to the list below.
# Please send them to either:
#
#	Jim Yu			jyu@eecs.nwu.edu
#	Jan van der Steen	jansteen@cwi.nl
#
# Explanation of the format of the dictionary:
#
# Empty lines and lines starting with a "hash" (like these lines) are ignored.
# All other lines are expected to have the format:
#
#	FIELD=VALUE
#
# where:
#
#	Field	Meaning
#	-------------------------
#	CD	Code (see below)
#	JP	Japanese
#	GB	English
#	CH	Chinese (pinyin)
#	RK	Republic of Korea
#	NL	Dutch
#	GE	German
#	FR	France
#	SV	Swedish
#       IT      Italy
#
#	Code	Classification
#	-------------------------
#	t	(T)echnical
#	n	(N)ame
#	c	(C)hampionship
#	d	(d)igits
#	p	(p)olite, courtesy phrases
#
# Contributions:
#
#   Name		E-mail				Language
#   -------------------------------------------------------------------
#   Barry Phease	BARRYP@otago.ac.nz		English/various
#   Jan van der Steen	jansteen@cwi.nl			Dutch/English
#   Jim Yu		jyu@eecs.nwu.edu		Chinese/English
#   Joachim Beggerow	job@shusaku.escape.de		German
#   Kim Juwhan		juwhan@eve.kaist.ac.kr		Korean
#   Kishiko Shimizu	jansteen@cwi.nl			Japanese
#   Nate Smith		nates@ll.mit.edu		English
#   Philippe Bizard	bizard@imag.fr			French
#   Roy Schmidt		schmidt@usthk.ust.hk		English/various
#   Ryu Kwangseon	juwhan@eve.kaist.ac.kr		Korean
#   Yang Wonsuk		osl2@mgt.kaist.ac.kr		Korean
#   Park Misa		juwhan@eve.kaist.ac.kr		Korean
#   Peter Smidt		smidt@fy.chalmers.se		Swedish
#   Sergio Martino      martino@minsky.csata.it         Italian
#

CD=p(olite)
JP=domo arigato gozaimasu
CH=xie4 xie4
GB=thanks
NL=dank U (je) wel
RK=Ko-ma-wa-yo.
FR=merci beaucoup
SV=tack
IT=grazie

CD=p(olite)
JP=do itashimashite
CH=bu yong4 xie4 
GB=you're welcome
NL=het genoegen is mijnerzijds
RK=Cheon-man-e-yo.
FR=je vous en prie (en reponse a 'merci')
SV=Ta f|r dej d}! G|r som du vill. K{nn dej som hemma.
IT=prego

CD=p
JP=sayonara
CH=zai4 jian4
GB=see you/bye
NL=tot ziens
RK=Ahn-nyeong!
FR=au revoir
SV=Hej d}!
IT=arrivederci (ciao is more confidential)

CD=p
JP=hajimemashite
GB=nice to meet you
CH=hen3 gao xing4 jian4 dao4 ni3
NL=aangenaam kennis met U te maken
RK=Man-na-seo pan-ga-wa-yo.
FR=enchante (de faire votre connaissance)
SV=Men, vad trevligt att tr{ffa dej! Trevligt att r}kas.
IT=piacere (di conoscerla)

CD=p
JP=kon'nichiwa (daytime)
GB=hello,hi
NL=hallo,hoi
CH=ni3 hao ma? (chi le ma? :-)
RK=Ahn-nyeong?
FR=bonjour
SV=Hej! Hall}!
IT=salve

CD=p
JP=ohayo gozaimasu
GB=good morning
CH=zao3 shang4 hao3
NL=goedenmorgen
RK=Ahn-nyeong? (bap-meok-ut-ni? (^_*))
FR=bonjour (seulement le matin)
SV=God morgon!
IT=buon giorno

CD=p
JP=konbanwa
GB=good evening
CH=wan3 shang4 hao3
NL=goedenavond
RK=Ahn-nyeong?
FR=bonsoir (en se rencontrant, pas en se quittant)
SV=God kv{ll!
IT=buona sera

CD=p
JP=onegai dekimasu ka?
GB=shall we play a game?
NL=zullen we een partij spelen?
CH=lai2 yi4 pan2, xing2 ma?
RK=Han-pan tool-kka-yo?
FR=voulez-vous jouer ?
SV=Ska vi spela ett parti?
IT=desidera giocare una partita

CD=p
JP=teai wa doshimasu ka?
GB=what is the handicap?
NL=hoeveel voorgift?
CH=rang4 ji3 (ke) zi3?
RK=Myeot-jeom kkal-kka-yo?
FR=quel est le handicap ?
SV=Hur mycket handikapp? Hur m}nga stenar?
IT=Qual e' l'handicap?

CD=p
JP=yoroshiku onegaishimasu
GB=have a nice game
NL=een prettige partij
CH=hao3 hao xia4!
RK=Keon-too-reul pip-ni-da.
FR=je vous souhaite une bonne partie
SV=Spela d}! Hoppas det blir en bra match.
IT=Le auguro una piacevole partita

CD=p
JP=dono kurai ouchini narimasu ka?
GB=what is your strength?
NL=hoe sterk bent U?
SV=Vilken ranking har du? Hur bra {r du?
IT=Qual e' il suo livello?

