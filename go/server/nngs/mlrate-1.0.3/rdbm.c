/* rdbm.c
**
** Per-Erik Martin (pem@nexus.se) 1997-12.
**
**   Copyright (C) 1998  Per-Erik Martin
**
**   This program is free software; you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation; either version 2 of the License, or
**   (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

static char sccsid[] = "@(#) rdbm.c 1.5 98/04/07";

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <assert.h>		/* Disable with -DNDEBUG */
#include "rdbm.h"
#include "player.h"

#ifdef NO_NDBM

rdbm_t
rdbm_create(char *dbname)
{
  return (void *)4;
}

rdbm_t
rdbm_open(char *dbname)
{
  return (void *)4;
}

void
rdbm_close(rdbm_t db)
{
  return;
}

int
rdbm_store(rdbm_t db, rdbm_player_t *rp)
{
  return 1;
}

int
rdbm_fetch(rdbm_t db, char *name, rdbm_player_t *rp)
{
  return 0;
}

int
rdbm_delete(rdbm_t db, char *name)
{
  return 1;
}

void
rdbm_start(rdbm_t db)
{
  return;
}

int
rdbm_next(rdbm_t db, rdbm_player_t *rp)
{
  return 0;
}

#else  /* !NO_NDBM */

/* Buggy Linux include files. */
#ifdef LINUX
#define CREATE_MODE 0744
#else
#define CREATE_MODE (S_IRWXU|S_IRGRP|S_IROTH)
#endif

#ifdef GDBM

#define DBM_CREATE(N) \
  gdbm_open((N), 0, GDBM_WRCREAT, CREATE_MODE, 0)
#define DBM_OPEN(N) \
  gdbm_open((N), 0, GDBM_READER, 0, 0)
#define DBM_CLOSE(DB) \
  gdbm_close(DB)
#define DBM_STORE(DB, K, D) \
  gdbm_store((DB), (K), (D), GDBM_REPLACE)
#define DBM_FETCH(DB, K) \
  gdbm_fetch((DB), (K))
#define DBM_DELETE(DB, K) \
  gdbm_delete((DB), (K))
#define DBM_FIRSTKEY(DB) \
  gdbm_firstkey(DB)
#define DBM_NEXTKEY(DB, K) \
  gdbm_nextkey((DB), (K))

#else

#define DBM_CREATE(N) \
  dbm_open(N, O_CREAT|O_RDWR, CREATE_MODE)
#define DBM_OPEN(N) \
  dbm_open(N, O_RDWR, 0)
#define DBM_CLOSE(DB) \
  dbm_close(DB)
#define DBM_STORE(DB, K, D) \
  dbm_store((DB), (K), (D), DBM_REPLACE)
#define DBM_FETCH(DB, K) \
  dbm_fetch((DB), (K))
#define DBM_DELETE(DB, K) \
  dbm_delete((DB), (K))
#define DBM_FIRSTKEY(DB) \
  dbm_firstkey(DB)
#define DBM_NEXTKEY(DB, K) \
  dbm_nextkey(DB)

#endif /* GDBM */

rdbm_t
rdbm_create(char *dbname)
{
  rdbm_t db;

  assert(dbname != NULL);
  db = DBM_CREATE(dbname);
  return db;
}

rdbm_t
rdbm_open(char *dbname)
{
  rdbm_t db;

  assert(dbname != NULL);
  db = DBM_OPEN(dbname);
  return db;
}

void
rdbm_close(rdbm_t db)
{
  assert(db != NULL);
  DBM_CLOSE(db);
}

int
rdbm_store(rdbm_t db, rdbm_player_t *rp)
{
  datum k, d;
  char n[RDBM_NAMEMAX+1];
  unsigned i;

  assert(db != NULL && rp != NULL);
  for (i = 0 ; i < RDBM_NAMEMAX && rp->name[i] != '\0' ; i++)
    n[i] = toupper(rp->name[i]);
  n[i++] = '\0';
  k.dptr = n;
  k.dsize = i;
  d.dptr = (char *)rp;
  d.dsize = sizeof(rdbm_player_t);
  return (DBM_STORE(db, k, d) < 0 ? 0 : 1);
}

int
rdbm_fetch(rdbm_t db, char *name, rdbm_player_t *rp)
{
  size_t s;
  datum k, d;
  char n[RDBM_NAMEMAX+1];
  unsigned i;

  assert(db != NULL && name != NULL && rp != NULL);
  for (i = 0 ; i < RDBM_NAMEMAX && name[i] != '\0' ; i++)
    n[i] = toupper(name[i]);
  n[i++] = '\0';
  k.dptr = n;
  k.dsize = i;
  d = DBM_FETCH(db, k);
  if (d.dptr == NULL)
    return 0;
  s = (d.dsize < sizeof(rdbm_player_t) ? d.dsize : sizeof(rdbm_player_t));
  while (s--)
    ((char *)rp)[s] = d.dptr[s];
  return 1;
}

int
rdbm_delete(rdbm_t db, char *name)
{
  datum k;
  char n[RDBM_NAMEMAX+1];
  unsigned i;

  assert(db != NULL && name != NULL);
  for (i = 0 ; i < RDBM_NAMEMAX && name[i] != '\0' ; i++)
    n[i] = toupper(name[i]);
  n[i++] = '\0';
  k.dptr = n;
  k.dsize = i;
  return (DBM_DELETE(db, k) < 0 ? 0 : 1);
}

static datum Nextkey;

void
rdbm_start(rdbm_t db)
{
  assert(db != NULL);
  Nextkey = DBM_FIRSTKEY(db);
}

int
rdbm_next(rdbm_t db, rdbm_player_t *rp)
{
  size_t s;
  datum d;

  assert(db != NULL && rp != NULL);
  if (Nextkey.dptr == NULL)
    return 0;
  d = DBM_FETCH(db, Nextkey);
  if (d.dptr == NULL)
    return 0;
  s = (d.dsize < sizeof(*rp) ? d.dsize : sizeof(*rp));
  while (s--)
    ((char *)rp)[s] = d.dptr[s];
  Nextkey = DBM_NEXTKEY(db, Nextkey);
  return 1;
}

#endif /* NO_NDBM */

void
rdbm_printheader(FILE *fp)
{
  int i;

  fprintf(fp, "%-*s Rank  Rate     W+L   =Games  -Low  +High\n",
	  PLAYER_MAX_NAMELEN, "Name");
  for (i = 0 ; i < PLAYER_MAX_NAMELEN ; i++)
    putc('-', fp);
  fputs(" ----- -----  --------------  ----- -----\n", fp);
}

void
rdbm_printline(FILE *fp, rdbm_player_t *rp)
{
  fprintf(fp, "%-*s %4s%c %5.2f  %3u+%-3u =%5u  %+5.2f %+5.2f\n",
	  PLAYER_MAX_NAMELEN,
	  rp->name, rp->rank, (rp->star ? '*' : ' '), rp->rating,
	  rp->wins, rp->losses, rp->wins+rp->losses,
	  rp->low, rp->high);
}

void
rdbm_wprintline(FILE *fp, rdbm_player_t *rp)
{
  fprintf(fp, "%-*s %4s%c %5.2f  %5.1f+%-5.1f =%7.1f  %+5.2f %+5.2f\n",
	  PLAYER_MAX_NAMELEN,
	  rp->name, rp->rank, (rp->star ? '*' : ' '), rp->rating,
	  rp->wwins, rp->wlosses, rp->wwins+rp->wlosses,
	  rp->low, rp->high);
}
