/* weight.h
**
** Per-Erik Martin (pem@nexus.se) 1997-12.
**
**   Copyright (C) 1998  Per-Erik Martin
**
**   This program is free software; you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation; either version 2 of the License, or
**   (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

#ifndef _weight_h
#define _weight_h

#include <time.h>
#include "rank.h"

/* With "differentiated" aging, games age by the function:
**
**          age
**  1 - -----------
**       x^2
**       --- + base
**       dwr
**
** Where is the weaker player's rank, 'dwr' is Dweight_rate and
** 'base' is Dweight_base.
*/
extern unsigned Dweight_base;	/* Typically 30-300 (default 30) */
extern unsigned Dweight_rate;	/* Typically 2-4 (default 3) */

/* In Pettersen's "uniform" system game age by the function:
**
**  1 - age/Pett_max_days
**
*/
extern unsigned Pett_max_days;	/* 180 at NNGS */


typedef enum
{
  weight_off,			/* No weight. */
  weight_diff,			/* Differentiated over ranks. */
  weight_unif			/* Uniform weight for all ranks. */
} weight_style_t;

extern double
weight_game(time_t date,
	    irank_t whiterank, int whasrank, irank_t blackrank, int bhasrank,
	    weight_style_t);

#endif /* _weight_h */
