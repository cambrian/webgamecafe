/* rdbm.h
**
** Per-Erik Martin (pem@nexus.se) 1997-12.
**
**   Copyright (C) 1998  Per-Erik Martin
**
**   This program is free software; you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation; either version 2 of the License, or
**   (at your option) any later version.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program; if not, write to the Free Software
**   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
*/

#ifndef _rdbm_h
#define _rdbm_h

#ifndef RDBM_NAMEMAX
#define RDBM_NAMEMAX 32
#endif

#include <time.h>

typedef struct
{
  char name[RDBM_NAMEMAX+1];
  char rank[4];
  int star;			/* 1 or 0 */
  double rating, error, low, high;
  unsigned wins, losses;
  double wwins, wlosses;	/* Weighted sums. */
  time_t lastplayed;
} rdbm_player_t;

#include <stdio.h>

#ifdef NO_NDBM

typedef void *rdbm_t;

#else

#ifdef GDBM

#include <gdbm.h>
typedef GDBM_FILE rdbm_t;

#else

#include <ndbm.h>
typedef DBM *rdbm_t;

#endif /* GDBM */
#endif /* NO_NDBM */

extern rdbm_t rdbm_create(char *dbname);

extern rdbm_t rdbm_open(char *dbname);

extern void rdbm_close(rdbm_t);

extern int rdbm_store(rdbm_t, rdbm_player_t *);

extern int rdbm_fetch(rdbm_t, char *name, rdbm_player_t *);

extern int rdbm_delete(rdbm_t, char *name);

extern void rdbm_start(rdbm_t);

extern int rdbm_next(rdbm_t, rdbm_player_t *);

extern void rdbm_printheader(FILE *fp);

extern void rdbm_printline(FILE *fp, rdbm_player_t *rp);

extern void rdbm_wprintline(FILE *fp, rdbm_player_t *rp);

#endif /* _rdbm_h */
