#include <stdlib.h>
#include <stdio.h>
#include <time.h>

static char sccsid[] = "@(#) itime.c 1.3 98/04/07";

void
main()
{
  printf("%lu\n", time(NULL));
  exit(0);
}
